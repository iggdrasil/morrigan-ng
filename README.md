# Morrigan #

[![build status](https://gitlab.com/iggdrasil/morrigan-ng/badges/master/build.svg)](https://gitlab.com/iggdrasil/morrigan-ng/commits/master) [![coverage report](https://gitlab.com/iggdrasil/morrigan-ng/badges/master/coverage.svg)](https://gitlab.com/iggdrasil/morrigan-ng/commits/master)

Morrigan is an online text role-playing Game engine. Using tabletop RPG mechanism with an omniscient game master and non player characters, it can (optionally) offers many automatic actions allowing characters to explore the world and interact with it (or between characters) in an autonomous way. Morrigan can manage a small team of five characters up to half a thousand characters (with many game masters).

