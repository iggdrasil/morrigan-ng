#!/bin/bash

. /etc/morrigan/master/config



    cat >&2 <<-'EOF'

*******************************************************************************
++++++                 Morrigan create instance script                   ++++++
*******************************************************************************

EOF

project_name=''
cat >&2 <<-'EOF'

-------------------------------------------------------------------------------
  The name of your new game. Don't use double quote.

EOF
MSG=""
while [ "$project_name" == '' ]
do
    read -p '* Name of your game? ' choice
    project_name=$choice
done

app_name=''
cat >&2 <<-'EOF'

-------------------------------------------------------------------------------
  A short name for the instance is given to distinguish it between other
  instances. No spaces, only lower digit, numbers (but do not start with it)
  and underscore.

EOF
MSG=""
while [ "$app_name" == '' ]
do
    read -p '* What is your short name for your instance? [morrigan_instance] ' choice
    if [ -z "$choice" ]; then
        app_name='morrigan_instance'
    else
        app_name=$choice
    fi
done

domain=''
cat >&2 <<-'EOF'

-------------------------------------------------------------------------------
  Domain used for your morrigan instance. Of course don't forget to update
  your DNS entries if you dedicate a new domain for this instance.
  You can set to "localhost" if your instance is a local installation for
  testing purpose.

    e.g.: morrigan.my-domain.org

EOF
MSG=""
while [ "$domain" == '' ]
do
    read -p '* Domain for your instance? ' choice
    domain=$choice
done


db_host=$default_db
db_password=`apg -a 0 -M ncl -n 6 -x 10 -m 10 |head -n 1`
python=$morrigan_path"/venv/bin/python3"

db_name='morrigan_'$app_name
install_path=$full_install_path"/morrigan-ng"$db_name
date=`date +%F`
secret_key=`apg -a 0 -M ncl -n 6 -x 10 -m 40 |head -n 1`



#    echo "morrigan_path="$full_install_path"/morrigan-ng" > $etc_path"config"
#    echo "default_db="$default_db >> $etc_path"config"
#    echo "webserver="$webserver >> $etc_path"config"
#    echo "uwsgi_available_path='/etc/uwsgi/apps-available/'" >> $etc_path"config"
#    echo "uwsgi_enable_path='/etc/uwsgi/apps-enabled/'" >> $etc_path"config"
#    echo "nginx_available_path='/etc/nginx/sites-available/'" >> $etc_path"config"
#    echo "nginx_enable_path='/etc/nginx/sites-enabled/'" >> $etc_path"config"
