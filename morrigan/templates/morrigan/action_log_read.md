{% load i18n staticfiles morrigan_utilities %}# Morrigan - {{current_date}}
{% for chapter in chapters %}
#{% for _ in chapter.level|times %}#{% endfor %} {{chapter.name}}
{{chapter.content|safe}}
{% endfor %}
