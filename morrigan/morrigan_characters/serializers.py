#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.conf import settings

from rest_framework import serializers
from morrigan_characters import models
from morrigan_inventory.models import Item
from morrigan_inventory.serializers import ItemSerializer, \
    ItemFullSerializer, ItemSemiFullSerializer
from morrigan_common.utils import format_markdown


class DocumentSerializer(serializers.Serializer):
    documents = serializers.SerializerMethodField()

    def get_documents(self, obj):
        return obj.get_documents()


class DocumentHiddenSerializer(serializers.Serializer):
    documents = serializers.SerializerMethodField()

    def get_documents(self, obj):
        return obj.get_documents(hidden=True)


class LiveActionInitiative(serializers.Serializer):
    initiatives = serializers.SerializerMethodField()
    editable_fields = serializers.SerializerMethodField()

    def get_initiatives(self, obj):
        return obj.get_initiatives()

    def get_editable_fields(self, obj):
        return obj.get_live_action_editable_fields()


class OracleListSerializer(serializers.Serializer):
    oracles = serializers.SerializerMethodField()

    def get_oracles(self, obj):
        return obj.get_oracles()


class SexSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Sex
        fields = ('name', 'icon',)


def can_view_all(user, character):
    player = user.profile
    try:
        can_view = player == character.player or user.is_superuser
        if not can_view:
            game = character.game
            q = models.GameMaster.objects.filter(game=game,
                                                 player=player)
            if q.count():
                can_view = True
    except models.GameMaster.DoesNotExist:
        can_view = False
    return can_view


class ShapeSimpleSerializer(serializers.ModelSerializer):
    items = serializers.SerializerMethodField()
    background = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()

    class Meta:
        model = models.Shape
        fields = ('description', 'current_description', 'items', 'background',
                  'image')

    def get_items(self, obj):
        user = self.context['request'].user
        can_view = can_view_all(user, obj.character)
        # equipped items
        items = list(Item.objects.filter(character_areas__shape=obj))
        if can_view:
            # items in bags
            items += list(Item.objects.filter(
                inside__character_areas__shape=obj))
        if not items:
            return []
        if can_view:
            serializer = ItemFullSerializer
        else:
            serializer = ItemSemiFullSerializer
        items = serializer(items, many=True, read_only=True)
        return items.data

    def get_background(self, obj):
        user = self.context['request'].user
        if not can_view_all(user, obj.character):
            return ""
        return obj.character.background

    def get_image(self, obj):
        if not obj.image:
            return ""
        return obj.image.url


class CharacterSimpleSerializer(serializers.ModelSerializer):
    items = serializers.SerializerMethodField()
    sex = SexSerializer(read_only=True)
    race = serializers.StringRelatedField()
    is_trusted = serializers.SerializerMethodField()
    avatar = serializers.ImageField(required=False)
    subcharacters = serializers.SerializerMethodField()

    class Meta:
        model = models.Character
        fields = ('name', 'race', 'sex', 'avatar', 'shape_slug',
                  'health', 'is_trusted', 'items', 'subcharacters')

    def get_items(self, obj):
        # only equipped items are visible
        items = list(Item.objects.filter(
            character_areas__shape__character=obj,
            character_areas__shape__is_current_shape=True,
        ))
        items = ItemSerializer(items, many=True, read_only=True)
        return items.data

    TRUSTED = {
        (None, None): False,
        (None, "temporary"): "false-temporary",
        (None, "temporary"): "false-full",
    }

    def get_subcharacters(self, obj):
        q = obj.owns.all()
        serializer = CharacterSimpleSerializer(q, many=True)
        return serializer.data

    def get_is_trusted(self, obj):
        if not self.context.get("character", None):
            return False
        shape = obj.current_shape
        source_shape = self.context["character"].current_shape
        if not shape or not source_shape:
            return False
        trusted = "false"
        if source_shape.trusted.filter(pk=shape.pk).count():
            trusted = "full"
        elif source_shape.temporary_trusted.filter(pk=shape.pk).count():
            trusted = "temporary"
        trusted += "-"
        if source_shape.trusting.filter(pk=shape.pk).count():
            trusted += "full"
        elif source_shape.temporary_trusting.filter(pk=shape.pk).count():
            trusted += "temporary"
        else:
            trusted += "false"
        return trusted


class CharacterStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Character
        fields = ('ap', 'max_ap', 'fp', 'max_fp', 'mp', 'max_mp',
                  'hp', 'max_hp')


class CharacterRPStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Character
        fields = ('ap', 'max_ap', 'fp_percent', 'mp_percent', 'max_mp',
                  'hp_percent')


class CharacterFullSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Character
        fields = ('slug', 'name', 'ap', 'max_ap', 'fp', 'max_fp',
                  'mp', 'max_mp', 'hp', 'max_hp', 'background',
                  'biography', 'current_shape')


class MenuSerializer(serializers.ModelSerializer):
    menu = serializers.SerializerMethodField()

    class Meta:
        model = models.Character
        fields = ('slug', 'menu')

    def get_menu(self, obj):
        return obj.current_menu(
            self.context['view'].kwargs.get('current_page'))


class CharacterSheetSerializer(serializers.ModelSerializer):
    sections = serializers.SerializerMethodField()
    race = serializers.StringRelatedField()
    current_name = serializers.StringRelatedField()
    klass = serializers.StringRelatedField()
    biography = serializers.SerializerMethodField()
    background = serializers.SerializerMethodField()
    notes = serializers.SerializerMethodField()

    class Meta:
        model = models.Character
        fields = (
            'slug', 'sections', 'race', "current_name", "klass",
            'ap', 'max_ap', 'fp', 'max_fp', 'biography', 'background',
            'mp', 'max_mp', 'hp', 'max_hp', 'notes'
        )

    def get_sections(self, obj):
        return obj.get_character_sheet() or []

    def get_notes(self, obj):
        if not obj.notes:
            notes = "-"
        else:
            notes = format_markdown(obj.notes)
        return notes

    def get_biography(self, obj):
        if not obj.biography:
            return ""
        return format_markdown(obj.biography)

    def get_background(self, obj):
        if not obj.background:
            return ""
        return format_markdown(obj.background)


class CharacterSheetsSerializer(serializers.ModelSerializer):
    sections = serializers.SerializerMethodField()

    class Meta:
        model = models.Character
        fields = (
            'slug', 'sections',
        )

    def get_sections(self, obj):
        return obj.get_neighbors_character_sheet()


class CharacterSelectorSerializer(serializers.ModelSerializer):
    menu = serializers.SerializerMethodField()

    class Meta:
        model = models.Character
        fields = ('slug', 'menu')

    def get_menu(self, obj):
        return obj.character_select_menu()


class ActionLogSerializer(serializers.ModelSerializer):
    content = serializers.SerializerMethodField()

    class Meta:
        model = models.ActionLog
        fields = ('character', 'human_date', 'number', 'content', 'state',
                  'icon', 'id')

    def get_content(self, action_log):
        content = dict(action_log.content)
        if "description" in content:
            content["description"] = format_markdown(content["description"])
        return content


class BaseActionLogSerializer:
    act_log_key = "character"

    def get_actlogquery(self):
        logs = models.ActionLog.objects.filter(
            **{self.act_log_key: self.instance})
        if self.context.get('filters'):
            filters = "".join(self.context.get("filters"))
            parts = filters.split("--")
            icons = []
            states = []
            for part in parts:
                if not part:
                    continue
                if part.startswith("icon-"):
                    icons.append(part[len("icon-"):])
                else:
                    states += [p for p in part]
            if states:
                logs = logs.filter(state__in=states)
            if 'H' not in states:
                logs = logs.exclude(state='H')
            if icons:
                logs = logs.filter(icon__in=icons)
        else:
            logs = logs.exclude(state='H')
        return logs

    def get_page(self, obj):
        return self.context['page']

    def get_number_of_pages(self, obj):
        item_by_page = settings.MORRIGAN_ACTIONLOG_ITEM_BY_PAGE
        return int(self.get_actlogquery().count() / item_by_page) + 1

    def paginated_logs(self, obj):
        logs = self.get_actlogquery()

        q = logs.values('number')
        if not q.count():
            return ActionLogSerializer(logs, many=True).data

        page = self.get_page(obj)

        max_number = q.first()['number']
        item_by_page = settings.MORRIGAN_ACTIONLOG_ITEM_BY_PAGE

        higher = max_number - (page - 1) * item_by_page
        if higher < 0:
            return ActionLogSerializer(models.ActionLog.objects.none(),
                                       many=True).data
        lower = higher - item_by_page

        q = logs.filter(number__gt=lower, number__lte=higher)
        serializer = ActionLogSerializer(q, many=True)
        return serializer.data


class UserActionLogSerializer(BaseActionLogSerializer,
                              serializers.ModelSerializer):
    act_log_key = "character"
    number_of_pages = serializers.SerializerMethodField()
    page = serializers.SerializerMethodField()
    logs = serializers.SerializerMethodField('paginated_logs')

    class Meta:
        model = models.Character
        fields = ('slug', 'logs', 'number_of_pages', 'page', 'unread')


class ActionLogChooserSerializer(serializers.ModelSerializer):
    action_logs = serializers.SerializerMethodField()

    class Meta:
        model = models.Character
        fields = ('action_logs',)

    def get_action_logs(self, character):
        action_logs = [{"name": character.current_name,
                        "slug": "character-" + character.slug}]
        if not character.player.game_master.count():
            return action_logs
        if character.location_id:
            action_logs = [{"name": str(character.location),
                            "slug": "location-" + character.location.slug}
                           ] + action_logs
        for neiborgh in character.get_neighbors(need_conscious=False):
            action_logs.append(
                {"name": neiborgh.current_name,
                 "slug": "character-" + neiborgh.slug}
            )
        return action_logs


class ActionLogIconsSerializer(serializers.ModelSerializer):
    icons = serializers.SerializerMethodField()

    class Meta:
        model = models.Character
        fields = ("icons",)

    def get_icons(self, character):
        if not character.player.game_master.count():
            q = models.ActionLog.objects.filter(character=character)
        else:
            characters = [character.id]
            characters += character.get_neighbors(
                need_conscious=False).values_list('pk', flat=True)
            q = models.ActionLog.objects.filter(character_id__in=characters)
        return q.values_list("icon", flat=True).exclude(
            icon=None).exclude(icon="").distinct("icon").order_by("icon")


class ActionSerializer(serializers.Serializer):
    slug = serializers.SlugField()
    name = serializers.CharField(max_length=500)
    icon = serializers.CharField(max_length=50)
    ap_cost = serializers.IntegerField()


class ActionsSerializer(serializers.Serializer):
    actions = serializers.SerializerMethodField()

    def get_actions(self, obj):
        targets = self.context.get('targets')
        if not targets:
            targets = []
        serializer = ActionSerializer(obj.actions(targets), many=True)
        return serializer.data
