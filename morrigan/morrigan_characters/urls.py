#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.conf import settings
from django.conf.urls import url
from django.views.generic import RedirectView

from morrigan_characters import views

urlpatterns = [
    url(r'^base/(?P<slug>[\w-]+)/',
        RedirectView.as_view(pattern_name='character-page-location'),
        name='character-page-base'),
    url(r'^logs/(?P<slug>[\w-]+)/',
        views.ActionLogPageView.as_view(), name='character-page-log'),
    url(r'^read-logs/(?P<slug>[\w-]+)/',
        views.ActionLogReadView.as_view(), name='read-actionlog'),
    url(r'^export-pdf-logs/(?P<slug>[\w-]+)/',
        views.export_history_book_to_pdf, name='export-pdf-actionlog'),
    url(r'^export-md-logs/(?P<slug>[\w-]+)/',
        views.export_history_book_to_md, name='export-md-actionlog'),
    url(r'^edit-log/(?P<pk>[\w-]+)/',
        views.edit_actionlog, name='edit-log'),
    url(r'^main/(?P<slug>[\w-]+)/',
        views.MainPageView.as_view(), name='main-page'),
    url(r'^character-full/(?P<slug>[\w-]+)/',
        views.CharacterFullInformation.as_view(), name='character-full'),
    url(r'^character-sheet/(?P<slug>[\w-]+)/?',
        views.CharacterSheet.as_view(), name='character-sheet'),
    url(r'^characters-sheet/(?P<slug>[\w-]+)/?',
        views.CharacterSheets.as_view(), name='characters-sheet'),
    url(r'^character-menu/(?P<slug>[\w-]+)/(?P<current_page>[\w-]+)?',
        views.CharacterMenu.as_view(), name='character-menu'),
    url(r'^character-selector/(?P<slug>[\w-]+)/?',
        views.CharacterSelector.as_view(), name='character-selector'),
    url(r'^character-documents/(?P<slug>[\w-]+)/?',
        views.CharacterDocument.as_view(), name='character-documents'),
    url(r'^character-document/(?P<slug>[\w-]+)/(?P<document_slug>[\w-]+)/',
        views.CharacterDocument.as_view(), name='character-document'),
    url(r'^character-oracles/(?P<slug>[\w-]+)/?',
        views.CharacterOracleList.as_view(), name='character-oracles'),
    url(r'^character-ask-oracle/(?P<slug>[\w-]+)/(?P<oracle>[\w-]+)/?',
        views.ask_oracle, name='character-ask-oracle'),
    url(r'^character-documents-hidden/(?P<slug>[\w-]+)/?',
        views.CharacterDocumentHidden.as_view(),
        name='character-documents-hidden'),
    url(r'^characters-initiative/(?P<slug>[\w-]+)/?',
        views.CharactersInitiative.as_view(), name='characters-initiative'),
    url(r'^character-log/(?P<slug>[\w-]+)/(?P<filter>[\w-]+)?',
        views.ActionLog.as_view(), name='character-log'),
    url(r'^character-log-chooser/(?P<slug>[\w-]+)/',
        views.ActionLogChooser.as_view(), name='character-log-chooser'),
    url(r'^action-log-icons/(?P<slug>[\w-]+)/',
        views.ActionLogIcons.as_view(), name='character-log-icons'),
    url(r'^shape-simple/(?P<slug>[\w-]+)/',
        views.ShapeDescription.as_view(), name='shape-simple'),
    url(r'^set-log/(?P<pk>\d+)/(?P<state>[R|U|H|I|D])/',
        views.SetActionLog.as_view(), name='set-log'),
    url(r'^edit-character/(?P<slug>[\w-]+)/',
        views.EditCharacterForm.as_view(), name='edit-character'),
    url(r'^edit-audio/(?P<slug>[\w-]+)/',
        views.edit_audio_form, name='edit-audio'),
    url(r'^quick-edit/(?P<slug>[\w-]+)/',
        views.quick_edit_form, name='quick-edit'),
    url(r'^post-note/(?P<slug>[\w-]+)/',
        views.post_note_form, name='post-note'),
    url(r'^actions/(?P<slug>[\w-]+)/(?:(?P<targets>[\w-]+)/)?',
        views.Actions.as_view(), name='action-list'),
    url(r'^action/(?P<slug>[\w-]+)/(?P<action>[\w-]+)/?',
        views.ActionView.as_view(), name='do-action'),
    url(r'^gm-new-character/(?P<slug>[\w-]+)/',
        views.NewCharacterForm.as_view(), name='gm-new-character'),
    url(r'^gm-document/(?P<slug>[\w-]+)/',
        views.GMDocumentForm.as_view(), name='gm-document'),
    url(r"^edit-profile/$", views.edit_profile,
        name="edit-profile"),
]

if settings.MORRIGAN_RP_MODE:
    urlpatterns.append(
        url(r'^status/(?P<slug>[\w-]+)/',
            views.CharacterRPStatus.as_view(), name='status')
    )
else:
    urlpatterns.append(
        url(r'^status/(?P<slug>[\w-]+)/',
            views.CharacterStatus.as_view(), name='status')
    )
