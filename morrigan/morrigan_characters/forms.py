#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2017  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import datetime

from django import forms
from django.conf import settings
from django.utils.timezone import localtime
from django.core.exceptions import ObjectDoesNotExist
from django.template.defaultfilters import filesizeformat
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from morrigan_common.models import Action, Document, DocumentCategory, Stylesheet
from morrigan_characters import models
from morrigan_common.widgets import BootstrapCheckboxMultiple, BootstrapRadio
from morrigan_common.utils import format_markdown


class FormAction(forms.Form):
    action_slug = None  # only used to get the icon

    def __init__(self, *args, **kwargs):
        self.character = None
        if 'character' in kwargs:
            self.character = kwargs.pop('character')
        self.action = Action.cache(self.action_slug)
        self.icon = self.action['icon'] \
            if self.action and 'icon' in self.action \
            else None
        super(FormAction, self).__init__(*args, **kwargs)
        for key in self.fields.keys():
            self.fields[key].widget.attrs = {"class": "form-control"}

    def save(self, character, targets, result):
        return []


class FormActionSimpleTarget(FormAction):
    targets = forms.ChoiceField(
        label=_("Target"), choices=[], widget=BootstrapRadio)

    def __init__(self, *args, **kwargs):
        if 'targets' in kwargs:
            self.targets = kwargs.pop('targets')
        super(FormActionSimpleTarget, self).__init__(*args, **kwargs)
        self.fields['targets'].choices = self.targets

    def save(self, character, targets, result):
        return []


class FormActionMultipleTarget(FormActionSimpleTarget):
    targets = forms.MultipleChoiceField(
        label=_("Targets"), choices=[], widget=BootstrapCheckboxMultiple())


class TrustAction(FormActionSimpleTarget):
    action_slug = 'trust'
    is_trusted = forms.BooleanField(
        label=_("Is trusted"), required=False)
    temporary = forms.BooleanField(
        label=_("Temporary"), required=False,
        help_text=_("Trust will be removed when an object is given."))

    def save(self, character, targets, result):
        target_character = targets[0]

        if self.cleaned_data.get("is_trusted", False):
            if self.cleaned_data.get("temporary", False):
                character.current_shape.temporary_trusted.add(
                    target_character.current_shape)
                message = str(_("You temporary trust {}")).format(
                    target_character.current_name)
                other_message = str(_("{} temporary trust you")).format(
                    character.current_name)
                chat_message = str(_("{} temporary trust {}")).format(
                    character.current_name, target_character.current_name)
            else:
                character.current_shape.trusted.add(
                    target_character.current_shape)
                message = str(_("You trust {}")).format(
                    target_character.current_name)
                other_message = str(_("{} trust you")).format(
                    character.current_name)
                chat_message = str(_("{} trust {}")).format(
                    character.current_name, target_character.current_name)
        else:
            if character.current_shape.trusted.filter(
                    pk=target_character.current_shape.pk).count():
                character.current_shape.trusted.remove(
                    target_character.current_shape)
            if character.current_shape.temporary_trusted.filter(
                    pk=target_character.current_shape.pk).count():
                character.current_shape.temporary_trusted.remove(
                    target_character.current_shape)
            message = str(_("You take away trust to {}")).format(
                target_character.current_name)
            other_message = str(_("{} take away his/her trust in you")).format(
                character.current_name)
            chat_message = str(_("{} take away trust to {}")).format(
                character.current_name, target_character.current_name)
        character.write_log(message, name=_('Trust'), frm=character,
                            to=targets, state='R', icon=self.icon)
        targets[0].write_log(other_message, name=_('Trust'), frm=character,
                             to=targets, icon=self.icon)
        character.send_to_chat(chat_message, targets[0].slug,
                               frm=character, icon=self.icon)
        return message


class TalkAction(FormActionMultipleTarget):
    message = forms.CharField(label=_("Message"), required=True,
                              widget=forms.Textarea)
    action_slug = 'talk'

    def save(self, character, targets, result):
        message = {"description": self.cleaned_data['message'],
                   "type": "talk-private"}
        targets = [t for t in targets if t.is_conscious]
        message["target"] = ";".join(t.slug for t in targets)
        for target in targets:
            target.write_log(message, name=_('Message'), frm=character,
                             to=targets, icon=self.icon)
        log = character.write_log(message, name=_('Message'), frm=character,
                                  to=targets, state='R', icon=self.icon)
        return _("You just say: {}").format(log.content['description'])


class TalkAllAction(FormAction):
    message = forms.CharField(label=_("Message"), required=True,
                              widget=forms.Textarea)
    action_slug = 'talk-all'

    def __init__(self, *args, **kwargs):
        if 'targets' in kwargs:
            self.targets = kwargs.pop('targets')
        super(TalkAllAction, self).__init__(*args, **kwargs)

    def save(self, character, targets, result):
        message = {"description": self.cleaned_data['message'],
                   "type": "talk-all"}
        targets = [c for c in character.get_neighbors() if c.is_conscious]
        for target in targets:
            target.write_log(message, name=_('Message'), frm=character,
                             to=targets, icon=self.icon)
        character.write_log(message, name=_('Message'), frm=character,
                            to=targets, state='R', icon=self.icon)

        character.send_to_chat(message, "location", frm=character)
        res = str(_("You just say: {}")) + "<br>"
        return res.format(format_markdown(message["description"]))


class TakeNotesAction(FormAction):
    message = forms.CharField(label=_("Message"), required=True,
                              widget=forms.Textarea)
    action_slug = 'notes'

    def save(self, character, targets, result):
        message = {
            "description": self.cleaned_data['message'],
            "type": "notes"
        }
        character.write_log(message, name=_('Notes'), frm=character,
                            state='R', icon=self.icon)
        character.send_to_chat(message, character.slug, frm=character, icon=self.icon)
        res = str( _("Notes taken: {}")) + "<br>"
        return res.format(format_markdown(message["description"]))


class AddChapterAction(FormAction):
    current_chapter = forms.CharField(label="Current chapter", required=False)
    message = forms.CharField(label=_("Chapter name"), required=True)
    level = forms.ChoiceField(label=_("Level"), choices=[], required=True)
    action_slug = 'add-chapter'

    def __init__(self, *args, **kwargs):
        super(AddChapterAction, self).__init__(*args, **kwargs)
        character = kwargs.get("character", None)
        current_chapter = None
        if character:
            q = models.ActionLog.objects.filter(
                character=character, content__type="chapter").order_by("-date")
            if q.count():
                current_chapter = q.all()[0]
                name = current_chapter.content.get("chapter_name", "")
                current_level = current_chapter.content.get("level")
                current_date = current_chapter.date
                q = models.ActionLog.objects.filter(
                    character=character,
                    date__lte=current_date,
                    content__level=current_level - 1,
                    content__type="chapter").order_by("-date")
                while q.count():
                    c = q.all()[0]
                    name = c.content.get("chapter_name", "") + " > " + name
                    if int(c.content.get("level", 1)) == 1:
                        break
                    current_level = int(c.content.get("level"))
                    current_date = c.date
                    q = models.ActionLog.objects.filter(
                        character=character,
                        date__lte=current_date,
                        content__level=current_level - 1,
                        content__type="chapter").order_by("-date")

                self.fields["current_chapter"].initial = name
                self.fields["current_chapter"].widget.attrs = {
                    "readonly": True,
                    "style": "width: 100%"
                }
            else:
                self.fields.pop("current_chapter")
        max_range = 2
        if current_chapter:
            max_range = current_chapter.content.get("level", 1) + 2
        self.fields["level"].choices = [(l, l) for l in range(1, max_range)]

    def save(self, character, targets, result):
        name = self.cleaned_data['message']
        level = int(self.cleaned_data["level"])
        message = {
            "description": "#" + level * "#" + " " + name,
            "chapter_name": name,
            "level": level,
            "type": "chapter",
        }
        character.write_log(message, name=_('Chapter'), frm=character,
                            state='R', icon=self.icon)
        character.send_to_chat(message, character.slug, frm=character, icon=self.icon)
        return _("New chapter: {}").format(name)


class ImageForm:
    def _clean_image(self, attr):
        image = self.cleaned_data[attr]
        if not image:
            return
        if image.size > settings.MAX_UPLOAD_SIZE:
            raise forms.ValidationError(
                str(
                    _('Please keep file size under {}. Current file size {}')
                ).format(
                    filesizeformat(settings.MAX_UPLOAD_SIZE),
                    filesizeformat(image.size))
            )
        return image

    def clean_image(self):
        return self._clean_image("image")

    def clean_small_image(self):
        return self._clean_image("small_image")

    def save_images(self, shape):
        small_image = self.cleaned_data['small_image']
        image = self.cleaned_data['image']
        changed = False
        if small_image:
            changed = True
            shape.small_image = small_image
        if image:
            changed = True
            shape.image = image
        return changed


class ModifyCurrentDescriptionAction(FormAction, ImageForm):
    current_description = forms.CharField(
        label=_("Current description"), required=False, widget=forms.Textarea)
    small_image = forms.ImageField(label=_("Small image"), required=False)
    image = forms.ImageField(label=_("Image"), required=False)
    action_slug = 'modify-current-description'

    def __init__(self, *args, **kwargs):
        kwargs["initial"]["current_description"] = \
            kwargs["character"].current_shape and \
            kwargs["character"].current_shape.current_description or ""
        super(ModifyCurrentDescriptionAction, self).__init__(*args, **kwargs)

    def save(self, character, targets, result):
        message = self.cleaned_data['current_description']
        log = character.write_log(
            message, name=_('Change current description'), frm=character,
            state='R', icon=self.icon)
        shape = character.current_shape
        if shape:
            shape.current_description = message
            self.save_images(shape)
            shape.save()
        return _("Current description changed to: {}").format(
            log.content['description'])


class EditCharacterForm(forms.Form, ImageForm):
    notes = forms.CharField(
        label=_("Notes"), required=False,
        widget=forms.Textarea(attrs={"class": "form-control"}))
    current_description = forms.CharField(
        label=_("Current description"), required=False, widget=forms.Textarea)
    small_image = forms.ImageField(label=_("Small image"), required=False)
    image = forms.ImageField(label=_("Image"), required=False)

    def __init__(self, character, *args, **kwargs):
        self.character = character
        super(EditCharacterForm, self).__init__(*args, **kwargs)
        self.fields["notes"].large = True
        self.fields["notes"].markdown = True
        self.fields["notes"].initial = character.notes
        if self.character.location and self.character.location.live_action:
            self.fields["initiative"] = forms.IntegerField(
                label=_("Initiative"),
                initial=character.initiative, min_value=0)
        shape = self.character.current_shape
        if shape:
            self.fields["current_description"].initial = \
                shape.current_description

        game = self.character.game
        if game.hp_editable:
            self.fields["hp"] = forms.IntegerField(
                label=_("Hit points"), initial=character.hp, min_value=0,
                max_value=character.max_hp)
        if game.mp_editable:
            self.fields["mp"] = forms.IntegerField(
                label=_("Magic points"), initial=character.mp, min_value=0,
                max_value=character.max_mp)
        if game.fp_editable:
            self.fields["fp"] = forms.IntegerField(
                label=_("Food points"), initial=character.fp, min_value=0,
                max_value=character.max_fp)
        if game.ap_editable:
            self.fields["ap"] = forms.IntegerField(
                label=_("Action points"), initial=character.ap, min_value=0,
                max_value=character.max_ap)
        self.headers = []
        current_ability_type = ""
        for ability in character.abilities.filter(
                    ability__editable=True).order_by(
                    "ability__ability_type__order").all():
            key = "ability_{}".format(ability.pk)
            self.fields[key] = \
                forms.IntegerField(label=ability.ability.name,
                                   initial=ability.score_calculated)
            ability_type = ability.ability.ability_type
            if ability_type.name != current_ability_type:
                self.headers.append((key, ability_type.name))
                current_ability_type = ability_type.name
        for key in self.fields.keys():
            self.fields[key].widget.attrs = {"class": "form-control"}

    def save(self):
        self.character.notes = self.cleaned_data["notes"]
        for attr in ("ap", "mp", "hp", "fp", "initiative"):
            if attr not in self.cleaned_data:
                continue
            setattr(self.character, attr, self.cleaned_data[attr])
        self.character.save()
        message = self.cleaned_data['current_description']
        shape = self.character.current_shape
        if shape:
            self.save_images(shape)
            shape.current_description = message
            shape.save()
        for k in self.cleaned_data:
            if k.startswith("ability_"):
                ability_pk = int(k[len("ability_"):])
                try:
                    ability = self.character.abilities.get(
                        ability__editable=True, pk=ability_pk)
                except ObjectDoesNotExist:
                    continue
                ability.score_calculated = self.cleaned_data[k]
                ability.save()


class NewCharacterForm(forms.Form):
    name = forms.CharField(label=_("Name"), max_length=200)
    type = forms.ChoiceField(label=_("Type"), choices=[])
    ap = forms.IntegerField(label=_("Action points"), initial=10)
    fp = forms.IntegerField(label=_("Food points"), initial=10)
    mp = forms.IntegerField(label=_("Magic points"), initial=0)
    hp = forms.IntegerField(label=_("Health points"), initial=10)
    race = forms.ChoiceField(label=_("Race"), choices=[], required=False)
    sex = forms.ChoiceField(label=_("Sex"), choices=[], required=False)
    description = forms.CharField(
        label=_("Description"), required=False, widget=forms.Textarea)
    image = forms.ImageField(label=_("Image"), required=False)

    def __init__(self, location, *args, **kwargs):
        self.location = location
        super(NewCharacterForm, self).__init__(*args, **kwargs)
        self.fields["type"].choices = [
            (c.pk, str(c))
            for c in models.CharacterType.objects.filter(available=True)]
        self.fields["race"].choices = [("", '--')] + [
            (c.pk, str(c)) for c in models.Race.objects.all()]
        self.fields["sex"].choices = [("", '--')] + [
            (c.pk, str(c)) for c in models.Sex.objects.all()]

    def clean_name(self):
        name = self.cleaned_data['name']
        q = models.Character.objects.filter(initial_name=name)
        if q.count():
            raise forms.ValidationError(_("Name already taken"))
        return name

    def save(self, location, player):
        game = location.game
        initial_name = self.cleaned_data['name']
        slug = slugify(initial_name)[:50]
        idx = 0
        base_slug = slug[:45]
        while models.Character.objects.filter(slug=slug).count():
            idx += 1
            slug = "{}-{}".format(base_slug, idx)
        character = models.Character.objects.create(
            game=game, location=location, initial_name=initial_name,
            player=player, current_name=initial_name, slug=slug,
            type_id=self.cleaned_data["type"],
            ap=self.cleaned_data["ap"], max_ap=self.cleaned_data["ap"],
            fp=self.cleaned_data["fp"], max_fp=self.cleaned_data["fp"],
            mp=self.cleaned_data["mp"], max_mp=self.cleaned_data["mp"],
            hp=self.cleaned_data["hp"], max_hp=self.cleaned_data["hp"],
        )
        shape = character.current_shape
        if self.cleaned_data["race"]:
            shape.race_id = self.cleaned_data["race"]
        if self.cleaned_data["sex"]:
            shape.sex_id = self.cleaned_data["sex"]
        if self.cleaned_data["image"]:
            shape.image = self.cleaned_data["image"]
            shape.small_image = self.cleaned_data["image"]
        if self.cleaned_data["description"]:
            shape.description = self.cleaned_data["description"]
        shape.save()
        return character


class NewDocumentForm(forms.Form):
    file = forms.ImageField(label=_("Document"))
    name = forms.CharField(label=_("Name"), max_length=200, required=False)
    category = forms.ChoiceField(label=_("Category"), choices=[], required=False)
    available = forms.BooleanField(label=_("Make it available in the menu"),
                                   required=False)
    location = forms.BooleanField(label=_("Limit to this location"), required=False)
    character = forms.ChoiceField(label=_("Limit to this character"), required=False)
    display_now = forms.BooleanField(label=_("Display now"), required=False,
                                     initial=True)

    def __init__(self, location, *args, **kwargs):
        self.location = location
        super(NewDocumentForm, self).__init__(*args, **kwargs)
        self.fields["character"].choices = [("", "--")] + [
            (c.pk, str(c))
            for c in self.location.characters.order_by("current_name").all()]
        self.fields["category"].choices = [("", "--")] + [
            (c.pk, str(c))
            for c in DocumentCategory.objects.filter(available=True)]

    def save(self, location, player):
        game = location.game or models.Game.objects.all()[0]
        initial_name = self.cleaned_data['name'] or \
                       self.cleaned_data["file"].name.split(".")[0]
        name = initial_name
        idx = 0
        q = Document.objects.filter(name=name, game=game)
        q_filters = {
            "name": name,
            "game": game,
        }
        if self.cleaned_data.get("category", None):
            q_filters["category_id"] = self.cleaned_data["category"]
        else:
            q_filters["category__isnull"] = True
        while Document.objects.filter(**q_filters).count():
            idx += 1
            name = "{} - {}".format(initial_name, idx)
            q_filters["name"] = name

        slug = slugify(name)[:50]
        idx = 0
        base_slug = slug[:45]
        while Document.objects.filter(slug=slug).count():
            idx += 1
            slug = "{}-{}".format(base_slug, idx)

        data = {
            "slug": slug,
            "game": game,
            "name": name,
            "image": self.cleaned_data["file"],
            "category_id": self.cleaned_data.get("category", None),
            "hidden": not self.cleaned_data.get("available", False),
        }
        document = Document.objects.create(**data)
        q = player.characters.filter(game=game, location__isnull=False)
        if self.cleaned_data.get("location", None) and q.count():
            document.locations.add(q.all()[0].location)
        if self.cleaned_data.get("character", None):
            q = models.Character.objects.filter(pk=self.cleaned_data["character"],
                                                game=game)
            if q.count():
                document.characters.add(q.all()[0])
        return document, self.cleaned_data.get("display_now", False)


class EditProfileForm(forms.Form):
    stylesheet = forms.ChoiceField(label=_("Stylesheet"), required=False, choices=[])

    def __init__(self, profile, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.fields["stylesheet"].choices = [('', _("Default"))] + [
            (s.pk, s.name) for s in Stylesheet.objects.order_by("name").all()
        ]
        if profile.stylesheet_id:
            self.fields["stylesheet"].initial = profile.stylesheet_id

    def save(self, profile):
        profile.stylesheet_id = self.cleaned_data.get('stylesheet', None) or None
        profile.save()


class EditLogForm(forms.Form):
    description = forms.CharField(
        label=_("Description"), required=True,
        widget=forms.Textarea(attrs={"class": "form-control"}))
    date = forms.DateField(label=_("Date"), required=True,
                           widget=forms.DateInput)
    time = forms.TimeField(label=_("Time"), required=True)
    delete = forms.BooleanField(label=_("Delete ?"), required=False)

    def __init__(self, log, *args, **kwargs):
        self.log = log
        super(EditLogForm, self).__init__(*args, **kwargs)
        self.fields["description"].large = True
        self.fields["description"].markdown = True
        self.fields["description"].initial = log.content.get("description", "")
        date = localtime(log.date)
        self.fields["date"].initial = date
        self.fields["time"].initial = date.time

    def clean(self):
        self.cleaned_data["datetime"] = datetime.datetime.strptime(
            "{} {}".format(self.cleaned_data["date"],
                           self.cleaned_data["time"]),
            "%Y-%m-%d %H:%M:%S"
        )

    def save(self):
        if self.cleaned_data.get("delete", False):
            self.log.delete()
        else:
            self.log.content["description"] = self.cleaned_data['description']
            self.log.date = self.cleaned_data["datetime"]
            self.log.save()

        # renumber
        for idx, log in enumerate(models.ActionLog.objects.filter(
                character=self.log.character).order_by("date")):
            if log.number != idx + 1:
                log.number = idx + 1
                log.save()
