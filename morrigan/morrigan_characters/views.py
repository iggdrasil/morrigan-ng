#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from asgiref.sync import async_to_sync
from bs4 import BeautifulSoup
from channels.layers import get_channel_layer
import datetime
from django.http import Http404, HttpResponse
from django.utils.timezone import now
from rest_framework import generics
import tempfile
from weasyprint import HTML, CSS
from weasyprint.fonts import FontConfiguration

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.formats import localize
from django.utils.translation import ugettext_lazy as _
from django.views.generic.base import TemplateView, View
from django.views.generic.edit import FormView

from morrigan_common.event_manager import event_trigger
from morrigan_common.models import Action, GameAudio, Oracle, Game, Player
from morrigan_geography.models import LocationAudio

from morrigan_common.views import MorriganPermission, LoginRequiredMixin, \
    JSONResponseMixin, register_action, MorriganAdminPermission, \
    GameMasterRequiredMixin
from morrigan_characters import serializers, models, forms
from morrigan_common.utils import get_full_path, format_markdown, \
    refresh_interfaces


class SetObjectView(JSONResponseMixin, TemplateView):
    """
    Base for a simple view to set attribute
    Return a simple JSON with a "success" parameters
    """
    model = None

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)

    def dispatch(self, request, *args, **kwargs):
        try:
            self.object = self.model.objects.get(pk=self.kwargs['pk'])
            assert self.object.character.has_permission('full', request.user)
        except (AssertionError, self.model.DoesNotExist):
            raise PermissionDenied
        self.success = False
        self.set_object()
        return super(SetObjectView, self).dispatch(request, *args, **kwargs)

    def set_object(self):
        raise NotImplemented

    def get_data(self, context):
        return {'success': self.success}


class CharacterCheckMixin:
    def pre_dispatch(self, request, *args, **kwargs):
        self.character = None
        try:
            self.character = models.Character.objects.get(
                slug=self.kwargs['slug'])
            assert self.character.has_permission('full', request.user)
        except (AssertionError, models.Character.DoesNotExist):
            return redirect('index')


class CharacterPageView(CharacterCheckMixin, LoginRequiredMixin, TemplateView):
    """
    Main page for a character
    """
    template_name = "main.html"
    current_page = ''

    def pre_dispatch(self, request, *args, **kwargs):
        return super(CharacterPageView, self).pre_dispatch(request, *args,
                                                           **kwargs)

    def dispatch(self, request, *args, **kwargs):
        redir = self.pre_dispatch(request, *args, **kwargs)
        if redir:
            return redir
        return super(CharacterPageView, self).dispatch(request, *args,
                                                       **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CharacterPageView, self).get_context_data(**kwargs)
        if self.current_page:
            context['current_page'] = self.current_page
        else:
            context['current_page'] = str(self.character)
        context['character'] = self.character
        game = self.character.game if self.character.game else Game.objects.all()[0]
        if game.background_image:
            context["background_image"] = game.background_image.url
        context['is_game_master'] = bool(
            self.character.player.game_master.filter(
                game=self.character.game).count())
        if self.character.player.stylesheet:
            context["css_theme"] = self.character.player.stylesheet.klass
        context['RP_MODE'] = settings.MORRIGAN_RP_MODE
        context["game"] = game
        context["has_audio"] = game.audios.count()
        return context


class MainPageView(CharacterPageView):
    template_name = "morrigan/main.html"
    current_page = _("Main")


class CharacterRetrieve(generics.RetrieveAPIView):
    """
    Generic retrieve view for character
    """
    lookup_field = 'slug'
    serializer_class = serializers.CharacterFullSerializer
    queryset = models.Character.objects
    permission_classes = (MorriganPermission,)
    permission_type = 'full'


class CharacterFullInformation(CharacterRetrieve):
    pass


class ShapeDescription(CharacterRetrieve):
    serializer_class = serializers.ShapeSimpleSerializer
    queryset = models.Shape.objects.filter(is_current_shape=True)
    permission_type = 'simple'


class CharacterStatus(CharacterRetrieve):
    serializer_class = serializers.CharacterStatusSerializer


class CharacterRPStatus(CharacterRetrieve):
    serializer_class = serializers.CharacterRPStatusSerializer


class CharacterMenu(CharacterRetrieve):
    serializer_class = serializers.MenuSerializer


class CharacterSheet(CharacterRetrieve):
    serializer_class = serializers.CharacterSheetSerializer


class CharacterSheets(CharacterRetrieve):
    serializer_class = serializers.CharacterSheetsSerializer
    permission_classes = (MorriganAdminPermission,)


class CharacterSelector(CharacterRetrieve):
    serializer_class = serializers.CharacterSelectorSerializer


class CharacterDocument(CharacterRetrieve):
    serializer_class = serializers.DocumentSerializer


class CharacterOracleList(CharacterRetrieve):
    serializer_class = serializers.OracleListSerializer


class CharacterDocumentHidden(CharacterRetrieve):
    serializer_class = serializers.DocumentHiddenSerializer


class CharactersInitiative(CharacterRetrieve):
    serializer_class = serializers.LiveActionInitiative


class BaseActionLog:
    def get_serializer_context(self):
        context = super(BaseActionLog, self).get_serializer_context()
        try:
            context['page'] = int(self.request.GET.get('page'))
        except (ValueError, TypeError):
            context['page'] = 1
        context['filters'] = ''
        if 'filters' in self.request.GET:
            context['filters'] = [s for s in self.request.GET['filters']]
        return context


class ActionLog(BaseActionLog, CharacterRetrieve):
    serializer_class = serializers.UserActionLogSerializer

    def dispatch(self, request, *args, **kwargs):
        if kwargs.get("filter", None) and \
                kwargs["filter"].startswith("location-"):
            return redirect(
                "location-log", slug=kwargs["filter"][len("location-"):])
        return super(ActionLog, self).dispatch(request, *args, **kwargs)

    def get_object(self):
        obj = super(ActionLog, self).get_object()
        if not User.objects.filter(pk=self.request.user.pk).exclude(
                profile__game_master__id__isnull=True).count() or \
                "filter" not in self.kwargs or not self.kwargs["filter"]:
            return obj
        if self.kwargs["filter"].startswith("character-"):
            try:
                return models.Character.objects.get(
                    slug=self.kwargs["filter"][len("character-"):])
            except models.Character.DoesNotExist:
                pass
        return obj


class ActionLogChooser(CharacterRetrieve):
    serializer_class = serializers.ActionLogChooserSerializer


class ActionLogIcons(CharacterRetrieve):
    serializer_class = serializers.ActionLogIconsSerializer


class SetActionLog(LoginRequiredMixin, SetObjectView):
    model = models.ActionLog

    def set_object(self):
        self.object.state = self.kwargs['state']
        self.object.save()
        self.success = True


class ActionLogPageView(CharacterPageView):
    template_name = "morrigan/action_log.html"
    current_page = _("Action log")


HEAD_TEMPLATE = """<div class='head-with-image'>
<div class="log-image"><img src="{image}"></div>
<div class="log-header">
    <span class="log-from">{frm}</span>
    <span class="log-to"> {to}</span>
</div>
</div>"""

SIMPLE_HEAD_TEMPLATE = """<div>
<div class="log-header">
    <span class="log-from">{frm}</span>
    <span class="log-to"> {to}</span>
</div>
</div>"""

MD_HEAD_TEMPLATE = """
*{frm} {to}*

"""


class ActionLogReadView(CharacterPageView):
    current_page = _("Story book")

    def get_template_names(self):
        if self.kwargs.get("md", False):
            return "morrigan/action_log_read.md"
        return "morrigan/action_log_read.html"

    def get_context_data(self, **kwargs):
        data = super(ActionLogReadView, self).get_context_data(**kwargs)
        if self.kwargs.get("pdf", False):
            data["pdf"] = True
        data["current_date"] = localize(datetime.date.today())
        md_render = self.kwargs.get("md", False)
        data["chapters"] = []
        current_chapter = {
            "level": 1,
            "name": str(_("Introduction")),
            "content": "",
            "id": 0
        }
        current_from, current_to, current_image = None, None, None
        q = models.ActionLog.objects.filter(character=self.character).exclude(
            state="H").order_by("date")
        for log in q.all():
            if log.content.get("type", None) == "chapter":
                current_from, current_image = None, None
                if current_chapter["id"] != 0 or current_chapter["content"]:
                    data["chapters"].append(current_chapter)
                current_chapter = {
                    "level": log.content.get("level", 1),
                    "name": log.content.get("chapter_name", ""),
                    "content": "",
                    "id": log.pk
                }
            else:
                frm = log.content.get("from", None)
                for start in ("From ", str(_("From "))):
                    if frm and frm.startswith(start):
                        frm = frm[len(start):]
                        break
                to = log.content.get("to", "")
                for start in (" to ", str(_(" to "))):
                    if to.startswith(start):
                        to = str(_(" for ")) + to[len(start):]
                        break
                image = get_full_path(
                    self.request, log.content.get("image", None))
                if frm and (frm != current_from
                            or image != current_image
                            or to != current_to):
                    current_from, current_image, current_to = frm, image, to
                    if md_render:
                        current_chapter["content"] += MD_HEAD_TEMPLATE.format(
                            image=image, to=to, frm=frm)
                    else:
                        if image:
                            current_chapter["content"] += HEAD_TEMPLATE.format(
                                image=image, to=to, frm=frm)
                        else:
                            current_chapter["content"] += \
                                SIMPLE_HEAD_TEMPLATE.format(to=to, frm=frm)
                desc = log.content.get("description", "")
                soup = BeautifulSoup(desc, 'html.parser')
                for img in soup.findAll('img'):
                    img['src'] = get_full_path(self.request, img['src'])
                desc = str(soup)
                tpe = log.content.get("type", "")
                name = log.content.get("name", "Notes")
                if tpe not in ("talk-all", "talk-private", "notes") and \
                        name not in ("Notes", "Message"):
                    desc = "*{}*".format(desc)
                if md_render:
                    desc += "\n\n"
                else:
                    desc = format_markdown(desc)
                current_chapter["content"] += desc
        if not current_chapter["content"]:
            if md_render:
                current_chapter["content"] = "\n**{}**".format(_("Empty..."))
            else:
                current_chapter["content"] = "<em>{}</em>".format(_("Empty..."))
        data["chapters"].append(current_chapter)
        return data


CSS_PDF = """
@font-face {
    font-family: 'Alegreya';
    font-style: normal;
    font-weight: 400;
    font-display: swap;
    src: url("../fonts/Alegreya.woff2") format("woff2");
    unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
@page {
    @bottom-right {
        font-family: sans-serif;
        content: counter(page) "/" counter(pages);
        text-align: center;
        opacity: .5;
    }
    @top-right {
        font-family: sans-serif;
        content: string(title);
        display: block;
        opacity: .5;
        height: 0.7cm;
        width: 100%;
    }
}
header {
   width: 0;
   height: 0;
   visibility: hidden;
   string-set: title content();
}
.log-image img {
    height: 20px;
    max-width: 40px;
}
.head-with-image .log-header{
    margin-left: 0.2cm;
}
.character-image, .location-image, .log-image {
    float: left;
    margin-right: 6px;
}
.history-book .log-header {
    font-family: sans-serif;
    font-size: 0.8rem;
    padding: 0.2em;
    background-color: #dedbd5;
}
.history-book hr {
    border-top: 1px solid black;
    margin-bottom: 2rem;
}
.history-book em {
    color: #808080;
    font-family: sans-serif;
    font-size: 0.8rem;
}
.history-book h1, .history-book h2, .history-book h3, .history-book h4, .history-book h5, .history-book p {
    font-family: "Alegreya";
    margin-bottom: 1rem;
}
.log-inner-image img {
    width: 4cm;
    border-radius: 0.2cm;
}
.log-inner-image {
    display: block;
    padding: 0 0.5em;
}
hr.spacer {
    clear: both;
    border: 0 solid;
    width: 0;
    margin: 0;
    margin-bottom: 0px;
    padding: 0;
}
.history-book p {
    margin: 0.2rem;
}
p img {
    max-width: 10cm;
    max-height: 7cm;
    display: block;
    border: 1px solid #ccc;
    margin: 0.5cm auto;
}
"""


def export_history_book_to_pdf(request, slug):
    q = models.Character.objects.filter(slug=slug, player_id=request.user.id)
    if not q.count():
        raise Http404()
    response = ActionLogReadView.as_view()(
        request, slug=slug, pdf=True).render()
    font_config = FontConfiguration()
    css = CSS(string=CSS_PDF, font_config=font_config)
    result = HTML(
        string=response.content.decode(response.charset)
    ).write_pdf(stylesheets=[css], font_config=font_config)
    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=morrigan-{}.pdf'.format(
        datetime.date.today().isoformat())
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        with open(output.name, 'rb') as output:
            response.write(output.read())
    return response


def export_history_book_to_md(request, slug):
    q = models.Character.objects.filter(slug=slug, player_id=request.user.id)
    if not q.count():
        raise Http404()
    result = ActionLogReadView.as_view()(
        request, slug=slug, md=True).render()
    response = HttpResponse(content_type='text/markdown; charset=UTF-8')
    response['Content-Disposition'] = 'inline; filename=morrigan-{}.md'.format(
        datetime.date.today().isoformat())
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result.content)
        output.flush()
        with open(output.name, 'r') as out:
            response.write(out.read())
    return response


def edit_actionlog(request, pk):
    q = models.ActionLog.objects.filter(
        pk=pk, character__player_id=request.user.id,
        character__game__can_edit_actionlog=True)
    if not q.count():
        raise Http404()
    log = q.all()[0]
    form_class = forms.EditLogForm
    if request.method == 'POST':
        form = form_class(log, request.POST)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            if log.character.location:
                refresh_interfaces(log.character.location.slug, ["action-log"],
                                   log.character.slug)
            return render(request, 'morrigan/edit_form_result.html')
    else:
        form = form_class(log)

    template_name = 'morrigan/edit_form.html'
    context = {
        "action_label": _("Edit log"),
        "action_icon": "fa fa-pencil",
        'form': form, 'edit_url': 'edit-log', 'url_arg': log.id
    }
    return render(request, template_name, context)


def ask_oracle(request, slug, oracle):
    q = models.Character.objects.filter(slug=slug, player_id=request.user.id)
    if not q.count():
        raise Http404()
    c = q.all()[0]

    q = Oracle.objects.filter(game=c.game, slug=oracle)
    if not models.GameMaster.objects.filter(
            player=c.player, game=c.game).count():
        q = q.exclude(gm_only=False)
    if not q.count():
        raise Http404()
    result = q.all()[0].roll()
    if not result:
        result = str(_("No response..."))
    return JsonResponse({"result": result})


def edit_profile(request):
    q = Player.objects.filter(pk=request.user.pk)
    if not q.count():
        raise Http404()
    form_class = forms.EditProfileForm
    profile = q.all()[0]
    if request.method == 'POST':
        form = form_class(profile, request.POST)
        # check whether it's valid:
        if form.is_valid():
            form.save(profile)
            return redirect('index')
    else:
        form = form_class(profile)

    template_name = 'morrigan/edit_profile.html'
    context = {
        'form': form,
    }
    return render(request, template_name, context)


class Actions(CharacterRetrieve):
    serializer_class = serializers.ActionsSerializer

    def get_serializer_context(self):
        context = super(Actions, self).get_serializer_context()
        try:
            context['targets'] = self.kwargs.get('targets').split('-')
        except (ValueError, TypeError, AttributeError):
            context['targets'] = []
        return context


class NotAvailableView(TemplateView):
    template_name = "not-available.html"


class ActionView(View):

    def dispatch(self, request, *args, **kwargs):
        try:
            view = Action.objects.get(slug=self.kwargs['action']).get_view()
            assert view
        except (Action.DoesNotExist, AssertionError):
            return NotAvailableView.as_view()(request, *args, **kwargs)
        return view.as_view()(request, *args, **kwargs)


class BaseAction(CharacterCheckMixin, LoginRequiredMixin, FormView):
    template_name = 'morrigan/action_form.html'
    result_template = 'morrigan/action_result.html'
    verbose_name = ''
    target_selection_not_available = _(
        "The target selected is not available for this action.")
    reload = []

    def pre_dispatch(self, request, *args, **kwargs):
        self.warnings = []
        redir = super(BaseAction, self).pre_dispatch(request, *args, **kwargs)
        if redir:
            return redir
        # check that the action is now available
        try:
            self.action = models.Action.objects.get(slug=self.slug)
        except models.Action.DoesNotExist:
            return redirect('not-available')
        if not self.action.is_available(self.character):
            return redirect('not-available')

    def dispatch(self, request, *args, **kwargs):
        redir = self.pre_dispatch(request, *args, **kwargs)
        if redir:
            return redir
        return super(BaseAction, self).dispatch(request, *args, **kwargs)

    def render_target(self, target):
        return str(target)

    def get_form_kwargs(self):
        kwargs = super(BaseAction, self).get_form_kwargs()
        kwargs['character'] = self.character
        shape = self.character.current_shape
        need_target = False

        if self.action.target_other:
            need_target = True
            kwargs['targets'] = []
            for target in self.action.get_targets(self.character):
                kwargs['targets'].append(
                    (target.slug, self.render_target(target)))

        if self.action.target_location_item:
            need_target = True
            if 'targets' not in kwargs:
                kwargs['targets'] = []
            if self.character.location:
                q = self.character.location.items
                for item in q.all():
                    if self.action.target_can_be_carried and \
                            not shape.can_be_put_inside_a_bag(item):
                        continue
                    kwargs['targets'].append(
                        (item.pk, self.render_target(item)))

        if self.action.target_own_item:
            need_target = True
            if 'targets' not in kwargs:
                kwargs['targets'] = []

            for item in self.character.get_own_items(
                    natural=not self.action.target_natural_item):
                if self.action.target_can_be_carried and \
                        not shape.can_be_put_inside_a_bag(item):
                    continue
                kwargs['targets'].append(
                    (item.pk, self.render_target(item)))

        if self.action.target_other_item:
            need_target = True
            if 'targets' not in kwargs:
                kwargs['targets'] = []
            for target in self.character.get_neighbors():
                q = target.items
                if not self.action.target_natural_item:
                    q = q.filter(natural=False)
                for item in q.all():
                    if self.action.target_can_be_carried and \
                            not shape.can_be_put_inside_a_bag(item):
                        continue
                    kwargs['targets'].append(
                        (item.pk, "{} - {}".format(target, item)))

        if need_target and not kwargs['targets']:
            self.warnings.append(self.target_selection_not_available)
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context = super(BaseAction, self).get_context_data(*args, **kwargs)
        context['warnings'] = self.warnings
        context['action_label'] = str(self.action)
        context['action_slug'] = self.action.slug
        context['action_icon'] = self.action.icon
        context['action_url'] = reverse(
            'do-action', kwargs={'slug': self.character.slug,
                                 'action': self.action.slug})
        return context

    def reload_interface(self, locations=None):
        if not self.reload or (not locations and
                               (not self.character or not self.character.location)):
            return
        if not locations:
            locations = [self.character.location]
        channel_layer = get_channel_layer()
        for location in locations:
            room_group_name = 'chat_location_{}'.format(
                location.slug)
            async_to_sync(channel_layer.group_send)(room_group_name, {
                "type": "reload",
                "interfaces": self.reload
            })

    def form_valid(self, form):
        targets = []
        # verify that the target are available
        if self.action.target_other and 'targets' in form.cleaned_data:
            submited_targets = form.cleaned_data['targets']
            for target in self.action.get_targets(self.character):
                if target.slug in submited_targets:
                    targets.append(target)
        if (self.action.target_location_item or self.action.target_own_item
                or self.action.target_other_item)\
                and 'targets' in form.cleaned_data:
            submited_targets = form.cleaned_data['targets']
            if type(submited_targets) not in (list, tuple):
                submited_targets = [submited_targets]
            submited_targets = [str(t) for t in submited_targets]
            available_items = []
            if self.action.target_location_item and self.character.location:
                available_items += list(self.character.location.items.all())
            if self.action.target_own_item:
                available_items += self.character.get_own_items()
            if self.action.target_other_item:
                for target in self.character.get_neighbors():
                    # TODO: only get visible items!
                    available_items += list(target.items.all())
            for item in available_items:
                if str(item.pk) in submited_targets:
                    targets.append(str(item.pk))
        result = self.action.do(self.character, targets)
        event_trigger(self.character, action=self.action)
        messages = form.save(self.character, targets, result)
        locations = []
        if getattr(form, "move_start", None):
            event_trigger(self.character, action=self.action,
                          location_action="QUIT", location=form.move_start.pk)
            locations.append(form.move_start)
        if getattr(form, "move_end", None):
            event_trigger(self.character, action=self.action,
                          location_action="ENTER", location=form.move_end.pk)
            locations.append(form.move_end)

        self.reload_interface(locations)
        return render(self.request, self.result_template,
                      {'messages': messages, 'action_label': str(self.action)})


@register_action
class TalkAction(BaseAction):
    slug = 'talk'
    verbose_name = _("Talk")
    form_class = forms.TalkAction


@register_action
class TalkAllAction(BaseAction):
    slug = 'talk-all'
    verbose_name = _("Talk to all")
    form_class = forms.TalkAllAction


@register_action
class TakeNotesAction(BaseAction):
    slug = 'notes'
    verbose_name = _("Take notes")
    form_class = forms.TakeNotesAction


@register_action
class AddChapterAction(BaseAction):
    slug = 'add-chapter'
    verbose_name = _("Add chapter")
    form_class = forms.AddChapterAction


@register_action
class ModifyCurrentDescriptionAction(BaseAction):
    slug = 'modify-current-description'
    verbose_name = _("Modify current description")
    form_class = forms.ModifyCurrentDescriptionAction


@register_action
class TrusAction(BaseAction):
    slug = 'trust'
    verbose_name = _("Trust")
    form_class = forms.TrustAction


def edit_audio_form(request, slug):
    q = models.Character.objects.filter(slug=slug, player_id=request.user.id)
    if not q.count():
        raise Http404()
    c = q.all()[0]
    if not request.user.is_superuser or not models.GameMaster.objects.filter(
            player__characters=c, game=c.game).count():
        raise Http404()
    for value in request.POST:
        if value.startswith("audio-game-"):
            slug = value[len("audio-game-"):]
            q = GameAudio.objects.filter(
                audio__slug=slug, game=c.game)
        elif value.startswith("audio-location-"):
            slug = value[len("audio-location-"):]
            q = LocationAudio.objects.filter(
                audio__slug=slug, location=c.location)
        elif value.startswith("audio-character-"):
            slug = value[len("audio-character-"):]
            q = models.CharacterAudio.objects.filter(
                audio__slug=slug, character__location=c.location)
        else:
            continue
        if not q.count():
            continue
        v = q.all()[0]
        v.loop = bool(request.POST.get("audio-loop-" + slug, None))
        if request.POST.get("audio-play-" + slug, None):
            v.autoplay = True
            if v.loop:
                v.expiration = None
            else:
                # set a delay for timeout
                if v.audio.duration:
                    v.expiration = now() + v.audio.duration
                else:
                    # assume a short duration if not specified
                    v.expiration = now() + datetime.timedelta(seconds=6)
        else:
            v.autoplay = False
        v.save()
    if c.location:
        refresh_interfaces(c.location.slug, ["location", "initiative"])
    return HttpResponse("OK")


def post_note_form(request, slug):
    q = models.Character.objects.filter(slug=slug, player_id=request.user.id)
    if not q.count() or not request.POST:
        raise Http404()
    character = q.all()[0]
    description = ""
    image = request.POST.get("image", None)
    if image:
        description = "![]({})".format(get_full_path(request, image))
    character_slug = request.POST.get("character_slug", None)
    if character_slug:
        if character.slug == character_slug:
            c = character
        else:
            q = character.get_neighbors().filter(slug=character_slug)
            if not q.count():
                raise Http404()
            c = q.all()[0]
        description += c.get_md_description(request)
    description += request.POST.get("description", "")
    message = {
        "description": description,
        "type": "notes"
    }
    character.write_log(message, name=_('Notes'), frm=character, state='R',
                        icon="fa fa-pencil")
    return HttpResponse("OK")


def quick_edit_form(request, slug):
    q = models.Character.objects.filter(slug=slug, player_id=request.user.id)
    if not q.count():
        raise Http404()
    character = q.all()[0]
    character_changed = False
    changed = False
    for key in request.POST:
        if not key.startswith("main-") and not key.startswith("ability-"):
            continue
        try:
            value = int(request.POST[key])
        except ValueError:
            # silent error...
            continue
        if key == "main-initiative":
            if value != character.initiative:
                character.initiative = value
                character_changed = True
                changed = True
        elif key.startswith("main-"):
            slug = key[len("main-"):]
            if character.game and \
                    getattr(character.game, slug + "_editable_live") and \
                    getattr(character, slug) != value:
                setattr(character, slug, value)
                character_changed = True
                changed = True
        elif key.startswith("ability-"):
            slug = key[len("ability-"):]
            q = character.abilities.filter(ability__editable_live=True,
                                           ability__slug=slug)
            if not q.count():
                continue
            a = q.all()[0]
            if a.score_calculated != value:
                a.score_calculated = value
                a.save()
                changed = True
    if character_changed:
        character.save()

    if character.location and changed:
        refresh_interfaces(character.location.slug, ["location", "initiative"])
    return HttpResponse("OK")


class EditCharacterForm(CharacterCheckMixin, LoginRequiredMixin, FormView):
    template_name = 'morrigan/edit_form.html'
    result_template = 'morrigan/edit_form_result.html'
    form_class = forms.EditCharacterForm

    def dispatch(self, request, *args, **kwargs):
        redir = self.pre_dispatch(request, *args, **kwargs)
        if redir:
            return redir
        return super(EditCharacterForm, self).dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(self.character, **self.get_form_kwargs())

    def get_default_context(self):
        return {
            'character_slug': self.character.slug,
            "action_label": _("Edit character"),
            "action_icon": "fa fa-pencil",
            'edit_url': 'edit-character', 'url_arg': self.character.slug
        }

    def form_valid(self, form):
        form.save()
        if self.character.location and \
                self.character.location.live_action:
            refresh_interfaces(self.character.location.slug, ["initiative"])
        return render(self.request, self.result_template,
                      self.get_default_context())

    def get_context_data(self, **kwargs):
        context = super(EditCharacterForm, self).get_context_data(**kwargs)
        context['headers'] = context['form'].headers
        context.update(self.get_default_context())
        return context


class GMLocationForm(GameMasterRequiredMixin, FormView):
    template_name = 'morrigan/game_master/new_gm_form.html'
    result_template = 'morrigan/edit_form_result.html'
    channel_update_interfaces = []

    def dispatch(self, request, *args, **kwargs):
        try:
            self.location = models.Location.objects.get(
                slug=self.kwargs['slug'])
        except (models.Location.DoesNotExist):
            return redirect('index')
        return super(GMLocationForm, self).dispatch(
            request, *args, **kwargs)

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(self.location, **self.get_form_kwargs())

    def get_default_context(self):
        return {
            'location': self.location,
        }

    def channel_update(self):
        if not self.channel_update_interfaces:
            return
        refresh_interfaces(self.location.slug, self.channel_update_interfaces)

    def form_save(self, form, player):
        self.channel_update()
        location_group_name = 'chat_location_{}'.format(
            self.location.slug)
        form.save(self.location, player)
        
        
        self.channel_update(channel_layer, location_group_name)

    def form_valid(self, form):
        try:
            player = models.Player.objects.get(user=self.request.user)
        except models.Player.DoesNotExist:
            raise Http404()
        self.form_save(form, player)
        return render(self.request, self.result_template,
                      self.get_default_context())

    def get_context_data(self, **kwargs):
        context = super(GMLocationForm, self).get_context_data(**kwargs)
        context.update(self.get_default_context())
        return context


class NewCharacterForm(GMLocationForm):
    form_class = forms.NewCharacterForm
    channel_update_interfaces = ["location", "initiative"]

    def get_default_context(self):
        context = super(NewCharacterForm, self).get_default_context()
        context.update({
            "action_label": _("New character"),
            "action_icon": "ra ra-double-team",
            'url': reverse("gm-new-character", args=[self.location.slug])
        })
        return context


class GMDocumentForm(GMLocationForm):
    template_name = 'morrigan/game_master/new_gm_document_form.html'
    form_class = forms.NewDocumentForm
    channel_update_interfaces = ["document-selector", "documents-hidden"]

    def form_save(self, form, player):
        doc, display_now = form.save(self.location, player)
        self.channel_update()
        if display_now:
            channel_layer = get_channel_layer()
            location_group_name = 'chat_location_{}'.format(self.location.slug)
            async_to_sync(channel_layer.group_send)(location_group_name, {
                "type": "show",
                "document": doc.slug
            })
            """
            for c in self.location.characters.filter(hp__gt=0).all():
                c.write_log("![]({})".format(
                    get_full_path(self.request, doc.image.url)), state="R")
            """

    def get_default_context(self):
        context = super(GMDocumentForm, self).get_default_context()
        context.update({
            "action_label": _("New document"),
            "action_icon": "fa fa-file-o",
            'url': reverse("gm-document", args=[self.location.slug]),
            "timestamp": int(datetime.datetime.now().timestamp() * 10)
        })
        return context
