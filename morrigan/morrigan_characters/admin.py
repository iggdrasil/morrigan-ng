#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2010-2017  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

"""
Settings for administration pages
"""
from django.contrib import admin
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

from admin import admin_site
from morrigan_common.admin import GenericAdmin, GenericSlugAdmin
from morrigan_characters import models
from morrigan_inventory.models import Item
from morrigan_scripts.scripts import initialize_pockets


def create_pockets(modeladmin, request, queryset):
    pocket_created = 0
    for character in queryset.all():
        pocket_created += initialize_pockets(character)
    messages.add_message(request, messages.INFO,
                         str(_("{} pockets created")).format(pocket_created))


create_pockets.short_description = _("Create pockets")


class AbilityInline(admin.TabularInline):
    model = models.CharacterAbility


class CalculatedAbilityInline(admin.TabularInline):
    model = models.Character.calculated_abilities.through


class SkillInline(admin.TabularInline):
    model = models.Character.skills.through


class AudioInline(admin.TabularInline):
    model = models.CharacterAudio


@admin.decorators.register(models.Character, site=admin_site)
class CharacterAdmin(admin.ModelAdmin):
    actions = [create_pockets]
    inlines = [AbilityInline, AudioInline]
    prepopulated_fields = {"slug": ("initial_name",)}
    autocomplete_fields = ["calculated_abilities", "skills"]


admin_site.register(models.CharacterType, GenericAdmin)

admin_site.register(models.Race, GenericAdmin)
admin_site.register(models.Sex, GenericAdmin)
admin_site.register(models.Klass, GenericAdmin)
admin_site.register(models.GameMaster)


class ShapeAreaInline(admin.TabularInline):
    model = models.ShapeArea


def create_default_shapeareas(modeladmin, request, queryset):
    created = 0
    for shape in queryset.all():
        for a in models.BodyArea.objects.filter(
                available_by_default=True).all():
            __, c = models.ShapeArea.objects.get_or_create(
                area=a, shape=shape,
                defaults={"place_available": a.default_number_available}
            )
            if c:
                created += 1
    messages.add_message(request, messages.INFO,
                         str(_("{} shape area created")).format(created))


create_default_shapeareas.short_description = _("Create default shapearea")


class ShapeAdmin(admin.ModelAdmin):
    list_display = ('character', 'current_name', 'race', 'sex')
    list_filter = ('race', 'sex')
    inlines = [ShapeAreaInline]
    readonly_fields = ('avatar',)
    actions = [create_default_shapeareas]


admin_site.register(models.Shape, ShapeAdmin)


class ItemInline(admin.TabularInline):
    model = Item


class ShapeAreaAdmin(admin.ModelAdmin):
    list_display = ('area', 'shape', 'place_available', 'place_taken')
    search_fields = ('shape__current_name',)
    list_filter = ('area',)
    # inlines = [ItemInline]


admin_site.register(models.ShapeArea, ShapeAreaAdmin)


class ActionLogAdmin(admin.ModelAdmin):
    list_display = ("character", "date", "state")


admin_site.register(models.ActionLog, ActionLogAdmin)
