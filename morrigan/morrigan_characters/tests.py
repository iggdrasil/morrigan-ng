#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import json
from selenium import webdriver
import time

from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import Client, TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse

from rest_framework.test import APITestCase

from morrigan_common.tests import UserInit, TestMorriganAction, TestMorrigan
from morrigan_common.models import Player
from morrigan_characters import models
from morrigan_common.admintest import adminviewstest

User = get_user_model()


class CharacterInit(UserInit):
    def create_dummy_character(self):
        user = User.objects.create_user("dummy", "a@a.net", "pass")
        character_type, created = models.CharacterType.objects.get_or_create(
            name="Player Character")
        player, c = Player.objects.get_or_create(user=user)
        character = models.Character.objects.create(initial_name="Gros bill",
                                                    type=character_type,
                                                    player=player)
        return user, character

    def create_anotherdummy_character(self):
        user = User.objects.create_user("dummy2", "a@a2.net", "pass")
        character_type, created = models.CharacterType.objects.get_or_create(
            name="Player Character")
        player, c = Player.objects.get_or_create(user=user)
        character = models.Character.objects.create(initial_name="Pti bill",
                                                    type=character_type,
                                                    player=player)
        return user, character


class TestCharacterCreation(TestMorrigan):
    def test_auto_body_creation(self):
        character_type, created = models.CharacterType.objects.get_or_create(
            name="Player Character")
        player, c = Player.objects.get_or_create(user=self.user)
        character = models.Character.objects.create(initial_name="Gros bill",
                                                    type=character_type,
                                                    player=player)
        shape = models.Shape.objects.create(
            character=character, race=models.Race.objects.all()[0],
            sex=models.Sex.objects.all()[0])
        # only verify they have been created
        self.assertTrue(shape.areas.count() > 0)


"""
class TestLiveActionLog(CharacterInit, StaticLiveServerTestCase):
    def setUp(self):
        self.user, self.character = self.create_dummy_character()
        self.selenium = webdriver.Firefox()
        self.selenium.maximize_window()
        super(TestLiveActionLog, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(TestLiveActionLog, self).tearDown()

    def test_logfile_view(self):
        self.selenium.get('%s%s' % (self.live_server_url, "/login/"))
        username = self.selenium.find_element_by_id("id_username")
        username.send_keys("dummy")
        password = self.selenium.find_element_by_id("id_password")
        password.send_keys("pass")
        self.selenium.find_element_by_xpath('//input[@value="Login"]').click()

        for i in range(0, 50):
            self.character.write_log({'description': "Hop hop"},
                                     name='WTF', to=['Timoty'])
        for i in range(0, 50):
            self.character.write_log({'description': "Yop"},
                                     name='WTF', to=['Timoty', 'Rebecca'])
        actionpagelog_url = reverse('character-page-log',
                                    kwargs={"slug": 'gros-bill'})
        self.selenium.get('%s%s' % (self.live_server_url, actionpagelog_url))
        time.sleep(2)  # wait for the JS to load...

        # badge = self.selenium.find_element_by_xpath(
        #            '//li[@id="menu-log"]/a/span[@class="badge"]')
        # self.assertEqual(badge.text, '100')
        self.assertIn("Yop", self.selenium.page_source)
        self.assertIn("bs-callout-success", self.selenium.page_source)

        # mark a log as read
        self.selenium.find_element_by_xpath(
            '//ul[@class="log-actions"]/li/a/i[contains(@class, "fa-check")]')\
            .click()
        time.sleep(2)  # wait for the JS to load...
        self.assertIn("bs-callout-default", self.selenium.page_source)

        # filter unread
        self.selenium.find_element_by_xpath(
            '//ul[@class="filter-logs"]/li/a/i[contains(@class, "fa-check")]')\
            .click()
        time.sleep(2)  # wait for the JS to load...
        self.assertNotIn("bs-callout-default", self.selenium.page_source)

        # deselect filter unread
        self.selenium.find_element_by_xpath(
            '//ul[@class="filter-logs"]/li/a/i[contains(@class, "fa-check")]')\
            .click()
        time.sleep(2)  # wait for the JS to load...
        self.assertIn("bs-callout-default", self.selenium.page_source)

        # mark a log as important
        self.selenium.find_element_by_xpath(
            '//ul[@class="log-actions"]/li/a/i[contains(@class, '
            '"fa-exclamation-triangle")]').click()
        time.sleep(2)  # wait for the JS to load...
        # badge = self.selenium.find_element_by_xpath(
        #            '//li[@id="menu-log"]/a/span[@class="badge"]')
        # self.assertEqual(badge.text, '99')
        self.assertIn("bs-callout-warning", self.selenium.page_source)

        # filter important
        self.selenium.find_element_by_xpath(
            '//ul[@class="filter-logs"]/li/a/i[contains(@class,'
            ' "fa-exclamation-triangle")]')\
            .click()
        time.sleep(2)  # wait for the JS to load...
        self.assertNotIn("bs-callout-success", self.selenium.page_source)
        self.assertIn("bs-callout-warning", self.selenium.page_source)

        # deselect filter important
        self.selenium.find_element_by_xpath(
            '//ul[@class="filter-logs"]/li/a/i[contains(@class,'
            ' "fa-exclamation-triangle")]')\
            .click()
        time.sleep(2)  # wait for the JS to load...
        self.assertIn("bs-callout-success", self.selenium.page_source)

        # mark a log as hidden
        self.selenium.find_element_by_xpath(
            '//ul[@class="log-actions"]/li/a/i[contains(@class, "fa-ban")]')\
            .click()
        time.sleep(2)  # wait for the JS to load...
        # by default hidden is not visible ;)
        self.assertNotIn("bs-callout-hide", self.selenium.page_source)

        # filter hidden
        self.selenium.find_element_by_xpath(
            '//ul[@class="filter-logs"]/li/a/i[contains(@class, "fa-ban")]')\
            .click()
        time.sleep(2)  # wait for the JS to load...
        self.assertNotIn("bs-callout-success", self.selenium.page_source)
        self.assertIn("bs-callout-hide", self.selenium.page_source)

        # deselect filter hidden
        self.selenium.find_element_by_xpath(
            '//ul[@class="filter-logs"]/li/a/i[contains(@class, "fa-ban")]')\
            .click()
        time.sleep(2)  # wait for the JS to load...
        self.assertIn("bs-callout-success", self.selenium.page_source)
        self.assertNotIn("bs-callout-hide", self.selenium.page_source)

        # test pagination
        next_page = self.selenium.find_element_by_xpath(
            '//li[contains(@class, "btn-next-page")]/a')
        next_page.click()
        time.sleep(1)  # wait for the JS to load...
        next_page.click()
        time.sleep(1)  # wait for the JS to load...
        next_page.click()
        time.sleep(1)  # wait for the JS to load...
        self.assertIn("Hop hop", self.selenium.page_source)
"""


class TestActionLog(CharacterInit, APITestCase):
    def setUp(self):
        self.user, self.character = self.create_dummy_character()
        self.user2, self.character2 = self.create_anotherdummy_character()

    def test_logfile_view(self):
        client = Client()
        client.login(username="dummy", password="pass")
        actionlog_url = reverse('character-log', kwargs={"slug": 'gros-bill'})
        response = client.get(actionlog_url, format='json')
        self.assertEqual(
            len(json.loads(response.content.decode("utf-8"))['logs']), 0)
        log = self.character.write_log({
            'name': 'New action',
            'description': "Something strange is happening..."})
        self.assertEqual(log.number, 1)
        log = self.character.write_log({
            'name': 'New action',
            'description': "Something VERY strange is happening..."})
        self.assertEqual(log.number, 2)
        for i in range(0, 100):
            self.character.write_log({
                'name': 'WTF', 'description': "Nooooooooooooon..."})
        response = client.get(actionlog_url, format='json')
        logs = json.loads(response.content.decode("utf-8"))['logs']
        item_by_page = settings.MORRIGAN_ACTIONLOG_ITEM_BY_PAGE
        self.assertEqual(len(logs), item_by_page)
        # first item is the last writen
        nb_actionlog = models.ActionLog.objects.count()
        self.assertEqual(logs[0]['number'], nb_actionlog)
        # test pagination
        response = client.get(actionlog_url, {'page': 2}, format='json')
        data = json.loads(response.content.decode("utf-8"))
        logs = data['logs']
        self.assertEqual(data['page'], 2)
        self.assertEqual(data['unread'], 102)
        self.assertEqual(
            data['number_of_pages'],
            int(102 / settings.MORRIGAN_ACTIONLOG_ITEM_BY_PAGE) + 1)
        self.assertEqual(logs[0]['number'], nb_actionlog - item_by_page)
        # test request page higher than page number
        response = client.get(actionlog_url, {'page': 99}, format='json')
        logs = json.loads(response.content.decode("utf-8"))['logs']
        self.assertEqual(len(logs), 0)
        actionpagelog_url = reverse('character-page-log',
                                    kwargs={"slug": 'gros-bill'})
        response = client.get(actionpagelog_url)
        self.assertEqual(response.status_code, 200)
        # test filters
        logs = list(models.ActionLog.objects
                                    .filter(character=self.character).all())
        logs[0].state = 'H'
        logs[0].save()
        logs[1].state = 'R'
        logs[1].save()
        logs[2].state = 'I'
        logs[2].save()
        # unread
        response = client.get(actionlog_url, {'filters': 'U'}, format='json')
        data = json.loads(response.content.decode("utf-8"))
        logs = data['logs']
        self.assertEqual(len(logs),
                         len([True for l in logs if l['state'] == 'U']))
        # important
        response = client.get(actionlog_url, {'filters': 'I'}, format='json')
        data = json.loads(response.content.decode("utf-8"))
        logs = data['logs']
        self.assertEqual(len(logs), 1)
        self.assertEqual(len(logs),
                         len([True for l in logs if l['state'] == 'I']))
        # hidden
        response = client.get(actionlog_url, {'filters': 'H'}, format='json')
        data = json.loads(response.content.decode("utf-8"))
        logs = data['logs']
        self.assertEqual(len(logs), 1)
        self.assertEqual(len(logs),
                         len([True for l in logs if l['state'] == 'H']))

    def test_logfile_change(self):
        client = Client()
        client.login(username="dummy2", password="pass")
        log = self.character.write_log({
            'name': 'New action',
            'description': "Something strange is happening..."})
        # test permissions
        url = reverse('set-log', kwargs={"pk": log.pk, "state": 'R'})
        response = client.get(url, format='json')
        self.assertEqual(response.status_code, 403)
        log2 = self.character2.write_log({
            'name': 'New action',
            'description': "Something strange is happening..."})
        # test permissions
        url = reverse('set-log', kwargs={"pk": log2.pk, "state": 'R'})
        response = client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        log2 = models.ActionLog.objects.filter(
            character=self.character2).all()[0]
        self.assertEqual(log2.state, 'R')


class TestMainPage(CharacterInit, APITestCase):
    def setUp(self):
        self.superuser = self.create_superuser()
        self.user, self.character = self.create_dummy_character()

    def test_redirection_and_permissions(self):
        index_url = reverse("index")
        admin_client = Client()
        admin_client.login(username="admin", password="pass")
        response = admin_client.get(index_url)
        # the admin has no associated character -> no redirection
        self.assertEqual(response.status_code, 200)
        dummy_client = Client()
        dummy_client.login(username="dummy", password="pass")
        response = dummy_client.get(index_url)
        # dummy has a character associated to him
        dummy_char_url = reverse('character-page-location',
                                 kwargs={"slug": 'gros-bill'})
        self.assertRedirects(response, dummy_char_url)
        response = dummy_client.get(dummy_char_url)
        self.assertEqual(response.status_code, 200)

        # with no character, no redirection
        self.character.player = None
        self.character.save()
        response = dummy_client.get(index_url)
        self.assertEqual(response.status_code, 200)
        # the character page is not accessible anymore
        response = dummy_client.get(dummy_char_url)
        self.assertRedirects(response, index_url)


class TestChararacter(CharacterInit, APITestCase):
    def setUp(self):
        self.superuser = self.create_superuser()
        self.user, self.character = self.create_dummy_character()

    def test_character_db(self):
        # test slug
        self.assertTrue(self.character.slug)
        character_2 = models.Character.objects.create(
            initial_name="Gros-bill", type=self.character.type,
            player=self.user.profile)
        self.assertTrue(character_2.slug)
        self.assertTrue(character_2.slug != self.character.slug)

    def test_character_view(self):
        url = reverse("character-full", kwargs={"slug": 'gros-bill'})
        response = self.client.get(url, format='json')
        # must be authentificated
        self.assertEqual(response.status_code, 403)
        # admin can see any character
        admin_client = Client()
        admin_client.login(username="admin", password="pass")
        response = admin_client.get(url, format='json')
        self.assertEqual(response.status_code, 200)

        # deassociate user to character
        self.character.player = None
        self.character.save()
        # dummy player cannot

        dummy_client = Client()
        dummy_client.login(username="dummy", password="pass")
        response = dummy_client.get(url, format='json')
        self.assertEqual(response.status_code, 403)
        # ...except if it is his own character
        self.character.player = self.user.profile
        self.character.save()
        response = dummy_client.get(url, format='json')
        self.assertEqual(response.status_code, 200)

    def test_character_menu(self):
        client = Client()
        client.login(username="dummy", password="pass")
        url = reverse("character-menu", kwargs={"slug": 'gros-bill'})
        response = client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        menu = json.loads(response.content.decode("utf-8"))
        # no HP only the 3 main item in menu
        self.assertEqual(len(menu['menu']), 2)
        self.character.hp = 10
        self.character.save()
        # character is awake now the full menu is available
        response = client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        menu = json.loads(response.content.decode("utf-8"))
        self.assertEqual(len(menu['menu']), 3)
        # by default base is selected
        self.assertEqual(menu['menu'][0]['selected'], True)
        # select another page
        url = reverse("character-menu", kwargs={"slug": 'gros-bill',
                                                'current_page': 'log'})
        response = client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        menu = json.loads(response.content.decode("utf-8"))
        for item in menu['menu']:
            if "log" in item['url']:
                self.assertEqual(item['selected'], True)


"""
class TestLiveAction(TestMorriganAction):
    def test_doaction_view(self):
        self.identification('arkenlond', 'password')

        self.assertNotIn("action-talk", self.selenium.page_source)

        self.select_character('nimnae')

        self.assertIn("action-talk", self.selenium.page_source)

        self.action_show('talk')

        self.assertIn("id_message", self.selenium.page_source)
        message = self.selenium.find_element_by_id("id_message")
        message.send_keys("Hello everybody!")
        self.action_submit()
        self.action_close()

        self.select_character('arkenlond')

        # cannot talk to himself
        self.assertRaises(Exception, self.selenium.find_element_by_id,
                          "action-talk")

        # take notes
        self.action_show('notes')

        self.assertIn("id_message", self.selenium.page_source)
        message = self.selenium.find_element_by_id("id_message")
        message.send_keys("Hello myself!")
        self.action_submit()
        self.action_close()
"""


class TestAdminViews(TestCase):
    def test_admin_views(self):
        adminviewstest(self, 'morrigan_characters')
