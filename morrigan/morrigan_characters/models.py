#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2017  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import datetime
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.contrib.postgres.fields import JSONField
from django.core.cache import cache
from django.db import models
from django.db.models import Max, Count, Q
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.html import strip_tags
from django.utils.translation import ugettext_lazy as _

from morrigan_common.models import GenericType, Effect, Player, Ability, \
    pre_save_slug, Action, BodyArea, AvatarModel, SlugManager, Game, Document,\
    CalculatedAbility, Skill, AbilityType, SkillType, BasicFormula, Audio, \
    Oracle
from morrigan_geography.models import World, Path, Location, Sector
from morrigan_inventory.models import GenericItem, Item, Currency
from morrigan_common.utils import get_full_path, format_markdown


class Sex(GenericType):
    icon = models.CharField(
        _('Icon name'), blank=True, null=True, max_length=50,
        help_text=_('Class name of the icon'))
    class Meta:
        verbose_name = _("Sex")
        verbose_name_plural = _("Sexs")


class Klass(GenericType):
    """
    RPG character Class
    """
    class Meta:
        verbose_name = _("Class")
        verbose_name_plural = _("Classes")


class CharacterType(GenericType):
    class Meta:
        verbose_name = _("Type: Character")
        verbose_name_plural = _("Types: Character")


class Character(models.Model):
    """
    Character for player and NPC.
    Can be auxiliary character attached to one main character (main field to
    True) by owner field.
    Background is the story before the game, biography is the story during
    the game.
    """
    game = models.ForeignKey(
        Game, null=True, blank=True, on_delete=models.CASCADE,
        verbose_name=_("Game"), related_name="characters")
    player = models.ForeignKey(
        Player, null=True, blank=True, verbose_name=_("Player"),
        on_delete=models.CASCADE, related_name='characters')
    main = models.BooleanField(_("Main"), default=True)
    owner = models.ForeignKey(
        'self', null=True, blank=True, verbose_name=_("Owner"),
        on_delete=models.SET_NULL, related_name="owns")
    initial_name = models.TextField(_("Initial name"), max_length=200,
                                    unique=True)
    slug = models.SlugField(_("Slug"), blank=True, null=True, unique=True,
                            max_length=200)
    current_name = models.TextField(_("Current name"), unique=True, null=True,
                                    blank=True)
    background = models.TextField(_("Background"), null=True, blank=True,
                                  help_text=_("Story before the game"))
    biography = models.TextField(_("Biography"), null=True, blank=True,
                                 help_text=_("Story in game"))
    notes = models.TextField(
        _("Notes"), null=True, blank=True,
        help_text=_("Game master's notes about the character - "
                    "not know by the player"))
    registration_date = models.DateTimeField(_("Registration date"),
                                             auto_now_add=True)
    last_connection = models.DateTimeField(_("Last connection"), null=True,
                                           blank=True)
    calculated_abilities = models.ManyToManyField(
        CalculatedAbility, verbose_name=_("Calculated abilities"),
        blank=True)
    skills = models.ManyToManyField(
        Skill, verbose_name=_("Skills"), blank=True)
    ap = models.IntegerField(_("Action points"), default=0)
    max_ap = models.IntegerField(_("Action points - max"), default=0)
    fp = models.IntegerField(_("Food points"), default=0)
    max_fp = models.IntegerField(_("Food points - max"), default=0)
    mp = models.IntegerField(_("Magic points"), default=0)
    max_mp = models.IntegerField(_("Magic points - max"), default=0)
    hp = models.IntegerField(_("Hit points"), default=0)
    max_hp = models.IntegerField(_("Hit points - max"), default=0)
    bp = models.IntegerField(_("Battle points"), default=0)
    max_bp = models.IntegerField(_("Battle points - max"), default=0)
    initiative = models.IntegerField(
        _("Current initiative"), default=0, help_text=_("Used for live action"))
    type = models.ForeignKey(CharacterType, verbose_name=_("Type"),
                             on_delete=models.CASCADE)
    location = models.ForeignKey(
        Location, null=True, blank=True, verbose_name=_("Location"),
        related_name='characters', on_delete=models.SET_NULL)
    arrival_date = models.DateTimeField(
        _("Arrival date"), null=True, blank=True,
        help_text=_("Arrival date in the current location"))
    step_number_index = models.FloatField(_("Step number index"), default=1)
    delta_compass = models.IntegerField(
        _("Delta compass"), default=0,
        help_text=_("To what extent this character is lost"))
    knowned_access = models.ManyToManyField(Path, related_name='knowed_by',
                                            blank=True)
    knowned_worlds = models.ManyToManyField(World, related_name='knowed_by',
                                            blank=True)
    knowned_generic_items = models.ManyToManyField(
        GenericItem, related_name='knowed_by', blank=True)
    knowned_rumors = models.ManyToManyField('Rumor', related_name='knowed_by',
                                            blank=True)
    visible_items = models.ManyToManyField(Item, related_name='visible_to',
                                           blank=True)
    objects = SlugManager()

    class Meta:
        verbose_name = _("Character")
        verbose_name_plural = _("Characters")

    def natural_key(self):
        return (self.slug, )

    def has_permission(self, permission_type, user):
        if user.is_superuser or User.objects.filter(
                     pk=user.pk).exclude(
                profile__game_master__id__isnull=True).count():
            return True
        if permission_type == 'full' and \
           (self.player and self.player.user == user):
            return True
        elif permission_type == 'simple' and \
                (self.player and self.player.user == user or
                 User.objects.filter(
                     pk=user.pk, profile__character__location=self.location)):
            # simple description when you are at the same place
            return True
        return False

    def __str__(self):
        return self.name

    HEALTH_STATES = [
        (0.10, _("Nearly dead"), 'bs-callout bs-callout-default'),
        (0.30, _("Badly wounded"), 'bs-callout bs-callout-danger'),
        (0.70, _("Wounded"), 'bs-callout bs-callout-warning'),
        (1, _("Healthy"), 'bs-callout bs-callout-success'),
    ]

    HEALTH_COLORS = {
        'bs-callout bs-callout-default': "#777",
        'bs-callout bs-callout-danger': "#d9534f",
        'bs-callout bs-callout-warning': "#f0ad4e",
        'bs-callout bs-callout-success': "#5cb85c",
    }

    def health(self):
        if not self.max_hp:
            return self.HEALTH_STATES[-1][1:]
        ratio = int(self.hp) / int(self.max_hp)
        if self.game and self.game.reverse_hp:
            ratio = 1 - ratio
        for r, desc, cls in self.HEALTH_STATES:
            if ratio <= r:
                return (desc, cls)
        return (desc, cls)

    @property
    def fp_percent(self):
        if not self.max_fp:
            return 0
        return self.fp / self.max_fp * 100

    @property
    def hp_percent(self):
        if not self.max_hp:
            return 0
        return self.hp / self.max_hp * 100

    @property
    def mp_percent(self):
        if not self.max_mp:
            return 0
        return self.mp / self.max_mp * 100

    @property
    def is_in_a_fight(self):
        return bool(self.involved_primary.count()
                    or self.involved_secondary.count())

    @property
    def name(self):
        if self.current_shape and self.current_shape.current_name:
            name = self.current_shape.current_name
        else:
            name = self.current_name or self.initial_name
        return name

    def get_md_description(self, request):
        desc = "#### {}".format(self.name)
        if self.sex:
            desc += " - {}".format(self.sex)
        h = self.health()
        if h:
            desc += " - {}".format(self.health()[0])
        desc += "\n\n"
        shape = self.current_shape
        if shape.image:
            desc += '![]({})\n'.format(
                get_full_path(request, shape.image.url))
        if shape.description:
            desc += "\n" + shape.description + "\n"
        if shape.current_description:
            desc += "\n" + shape.current_description + "\n"
        return desc

    def update_current_shape(self):
        key = 'shape-%s' % self.pk
        q = self.shapes.filter(is_current_shape=True)
        if not q.count():
            cache.set(key, None, settings.CACHE_TIMEOUT)
            return None
        value = q.all()[0]
        cache.set(key, value, settings.CACHE_TIMEOUT)
        return value

    @property
    def current_shape(self):
        key = 'shape-%s' % self.pk
        value = cache.get(key, None)
        if value:
            return value
        value = self.update_current_shape()
        return value

    def can_equip(self, item):
        """
        Check if an item can be equipped by the character.
        """
        return self.current_shape.can_equip(item)

    @property
    def shape_slug(self):
        if not self.current_shape:
            return ''
        return self.current_shape.slug

    @property
    def items(self):
        return Item.objects.filter(
            character_areas__shape__character=self,
            character_areas__shape__is_current_shape=True)

    @property
    def sex(self):
        shape = self.current_shape
        if not shape:
            return
        return shape.sex

    @property
    def race(self):
        shape = self.current_shape
        if not shape:
            return
        return shape.race

    @property
    def klass(self):
        shape = self.current_shape
        if not shape:
            return
        return shape.klass

    @property
    def avatar(self):
        shape = self.current_shape
        if not shape:
            return
        return shape.avatar

    @property
    def is_conscious(self):
        return self.hp > 0

    @property
    def unread(self):
        return self.logs.filter(state='U').count()

    def get_ability_score(self, ability_pk):
        q = list(self.abilities.filter(ability=ability_pk)[:1])
        if not q:
            return 0
        return q[0].score_calculated

    def actions(self, targets=('self',)):
        """
        Return available actions for a character as a list of tuples
        with: (slug, name, icon, AP cost)
        """
        actions = []
        q = Action.objects.filter(available=True)

        if 'multiple' in targets or 'multipleitem' in targets:
            q = q.filter(target_multiple=True)

        if not self.is_conscious:
            q = q.exclude(need_conscious=True)
        if 'self' in targets:
            q = q.filter(target_self=True)
        if 'other' in targets:
            q = q.filter(target_other=True)

        if 'ownitem' in targets:
            q = q.filter(target_own_item=True)
        if 'otheritem' in targets:
            q = q.filter(target_other_item=True)
        if 'locationitem' in targets:
            q = q.filter(target_location_item=True)

        if 'equippeditem' in targets:
            q = q.filter(target_equipped_item__in=['Y', 'A'])
        elif 'nonequippeditem' in targets:
            q = q.filter(target_equipped_item__in=['N', 'A'])
        elif 'self' not in targets and 'other' not in targets:
            q = q.filter(target_equipped_item='A')

        if 'equipableitem' in targets:
            q = q.filter(target_equippable_item__in=['Y', 'A'])
        elif 'nonequipableitem' in targets:
            q = q.filter(target_equippable_item__in=['N', 'A'])

        if 'naturalitem' in targets:
            q = q.filter(target_natural_item__in=['Y', 'A'])
        elif 'nonnaturalitem' in targets:
            q = q.filter(target_natural_item__in=['N', 'A'])

        if self.location:
            limit = self.location.actions
            if limit.count():
                q = q.filter(pk__in=limit.values_list("id", flat=True))

        for action in q.all():
            ap_cost = 0
            if action.formula:
                ap_cost = round(
                    action.formula.evaluate(self, targets=['ap'])['ap'])
            if action.is_available(self):
                actions.append({'slug': action.slug, 'name': action.name,
                                'icon': action.icon, 'ap_cost': ap_cost})
        return actions

    def _get_character_menu(self, current_page=''):
        return {
            'label': str(self.name),
            'selected': current_page == 'base',
            'url': reverse('main-page', kwargs={"slug": self.slug}),
            'icon': "fa fa-home",
            'slug': 'base'
        }

    def character_select_menu(self):
        q = self.player.characters.exclude(pk=self.pk)
        if q.count():
            items = [self._get_character_menu()]
            for character in q.order_by('pk'):
                items.append(character._get_character_menu())
        else:
            items = self._get_character_menu()
        return items

    def get_neighbors_character_sheet(self):
        characters = [self] + list(self.get_neighbors())
        ability_list = {}
        values = {}
        for character in characters:
            abilities = character.get_character_sheet(visible=False)
            for ability in abilities:
                if ability["slug"] not in ability_list:
                    ability_list[ability["slug"]] = {
                        "name": ability['name'],
                        "slug": ability["slug"],
                        "type": ability["type"],
                        "order": ability["order"],
                        "icon": ability["icon"],
                        "values": []
                    }
                    values[ability["slug"]] = {}
                for item in ability["items"]:
                    if item["slug"] not in values[ability["slug"]]:
                        values[ability["slug"]][item["slug"]] = {
                            "name": item['name'],
                            "slug": item['slug'],
                            "icon": item['icon'],
                            "order": item["order"],
                            "values": [],
                            "items": {}
                        }
                    if item["skill"]:
                        values[ability["slug"]][item["slug"]]["items"][
                            character.slug] = 1
                    else:
                        values[ability["slug"]][item["slug"]]["items"][
                            character.slug] = item["value"]
        items = [{"type": "characters",
                  "items": [c.current_name for c in characters]}]
        # reorder and put empty value for missing
        for k in sorted(ability_list.keys(),
                        key=lambda k: ability_list[k]["order"]):
            item = ability_list[k]
            for ability_key in sorted(
                    values[k].keys(),
                    key=lambda akey: values[k][akey]["order"]):
                ability = values[k][ability_key]
                for character in characters:
                    if character.slug in ability["items"].keys():
                        ability["values"].append(ability["items"][
                                                     character.slug])
                    else:
                        ability["values"].append("-")
                ability.pop("items")
                item["values"].append(ability)
            items.append(item)
        return items

    def get_character_sheet(self, visible=True):
        abilities = {}
        types = {}
        q = self.abilities
        if visible:
            q = q.filter(ability__visible=True)
        for ability in q.values(
                    "ability__ability_type_id", "ability__slug",
                    "ability__name", "score_calculated",
                    "ability__order").order_by("ability__order"):
            ability_type = ability["ability__ability_type_id"]
            ability_type_key = "ability-" + str(ability_type)
            if ability_type_key not in types:
                try:
                    types[ability_type_key] = AbilityType.objects.get(
                        id=ability_type)
                    abilities[ability_type_key] = []
                except AbilityType.DoesNotExist:
                    continue
            abilities[ability_type_key].append({
                "name": ability["ability__name"],
                "slug": ability["ability__slug"],
                "icon": "",
                "value": ability["score_calculated"],
                "order": ability["ability__order"],
                "skill": False,
            })
        q = self.calculated_abilities
        if visible:
            q = q.filter(visible=True)
        for ability in q.values(
                "ability_type_id", "slug",
                "name", "formula_id", "order").order_by("order"):
            ability_type = ability["ability_type_id"]
            ability_type_key = "ability-" + str(ability_type)
            if ability_type_key not in types:
                try:
                    types[ability_type_key] = AbilityType.objects.get(
                        id=ability_type)
                    abilities[ability_type_key] = []
                except AbilityType.DoesNotExist:
                    continue
            try:
                formula = BasicFormula.objects.get(id=ability["formula_id"])
            except BasicFormula.DoesNotExist:
                continue
            value = formula.evaluate(self)
            if "main" not in value:
                continue
            abilities[ability_type_key].append({
                "name": ability["name"],
                "slug": ability["slug"],
                "icon": "",
                "value": int(value["main"]),
                "order": ability["order"],
                "skill": False,
            })
        q = self.skills
        if visible:
            q = q.filter(visible=True)
        for skill in q.values("skill_type_id", "slug", "name", "description",
                              "order").order_by("order"):
            skill_type = skill["skill_type_id"]
            skill_type_key = "skill-" + str(skill_type)
            if skill_type_key not in types:
                try:
                    types[skill_type_key] = SkillType.objects.get(
                        id=skill_type)
                    abilities[skill_type_key] = []
                except SkillType.DoesNotExist:
                    continue
            abilities[skill_type_key].append({
                "name": skill["name"],
                "slug": skill["slug"],
                "icon": "",
                "value": skill["description"],
                "order": skill["order"],
                "skill": True,
            })
        ability_list = []
        for type_key in sorted(types, key=lambda x: types[x].order):
            ability_type = types[type_key]
            ability_list.append({
                "name": ability_type.name,
                "slug": ability_type.slug,
                "type": ability_type.slug,
                "order": ability_type.order,
                "icon": "",
                "items": abilities[type_key]
            })

        return ability_list

    def current_menu(self, current_page='base'):
        if not current_page:
            current_page = 'base'
        items = []
        game = self.game if self.game else Game.objects.all()[0]
        label = _("Location") if game.display_location and self.is_conscious \
            else _("Game")
        icon = "i-morrigan-mountains" if game.display_location else \
            "ra ra-dice-six"
        items.append({
            'label': label,
            'icon': icon,
            'selected': current_page == 'location',
            'slug': 'location'})
        if self.location and self.is_conscious and self.location.sector_image_x and \
                game.display_map:
            items.append({
                'label': _("Map"),
                'icon': "fa fa-globe",
                'selected': current_page == 'map',
                'slug': 'map'})

        if game.display_action_log:
            items.append({
                'label': _("Story Book"),
                'selected': current_page == 'log',
                'icon': "fa fa-book",
                'slug': 'log'})
            nb = self.unread
            if nb:
                items[-1]['badge'] = nb
        if game.display_character_log:
            items.append({'label': _("My character"),
                          'selected': current_page == 'characteristics',
                          'icon': "fa fa-user-circle-o",
                          'slug': 'characteristics'})
        '''
        items.append({'label': _("Achievements"),
                      'url': '',
                      'selected': current_page == 'characteristics',
                      'icon': "fa fa-bar-chart",
                      'slug': 'characteristics'})
        if self.is_conscious:
            items.append({'label': _("Fight"),
                          'url': '',
                          'selected': current_page == 'fight',
                          'icon': "fa fa-gavel",
                          'slug': 'fight'})
            items.append({'label': _("Map"),
                          'url': '',
                          'selected': current_page == 'map',
                          'icon': "fa fa-globe",
                          'slug': 'map'})
        '''
        return items

    def send_to_chat(self, content, chat_target, frm=None, icon=None):
        if not isinstance(content, dict):
            content = {'description': content}
        if not self.location or not content.get("description", None):
            return
        if not frm:
            frm = self

        channel_layer = get_channel_layer()
        room_group_name = 'chat_location_{}'.format(self.location.slug)

        msg = """<div id="direct-chat-msg-{timestamp}" class='direct-chat-msg in-game'>
        <div class='direct-chat-infos clearfix'>
          <span class='direct-chat-name float-left'>{from}</span>
          <span class='direct-chat-timestamp float-right'>{hour}</span>
        </div>
        {image}
        <div class='direct-chat-text'>{description}</div>
        """
        hour = datetime.datetime.now().strftime("%H:%M:%S")
        image = ""
        if icon:
            image = "<i class='direct-chat-icon direct-chat-img {}' "\
                    "aria-hidden=''></i>".format(icon)
        elif frm and frm.current_shape and frm.current_shape.avatar:
            image = '<img class="direct-chat-img" '\
                    'src="{}">'.format(frm.current_shape.avatar.url)

        msg_dct = {
            "from": "-",
            "description": format_markdown(content["description"]),
            "image": image,
            "hour": hour,
            "timestamp": int(datetime.datetime.now().timestamp() * 10)
        }
        room_content = {
            "type": "chat_message",
            "target": chat_target,
            "source": "",
            "extra-class": "",
            "character": self.slug,
        }
        room_content["source"] = frm.slug
        msg_dct["from"] = frm.current_name
        room_content["message"] = msg.format(**msg_dct)
        async_to_sync(channel_layer.group_send)(room_group_name,
                                                room_content)

    def write_log(self, content, name='', frm=None, to=None,
                  from_is_hidden=False, state='U', icon=''):
        """
        "content" must be a dict or string.
        "to" must be an iterable of character (or character name)
        """
        if not to:
            to = []
        if type(content) != dict:
            content = {'description': content}
        if "description" in content and not settings.MORRIGAN_TRUSTED_USER:
            content['description'] = strip_tags(content["description"])
        if not frm:
            frm = self
        if not from_is_hidden:
            content['from-slug'] = frm.slug
            content['from'] = str(_("From ")) + str(frm)
            if frm.current_shape and frm.current_shape.avatar:
                content['image'] = frm.current_shape.avatar.url
        if name:
            content['name'] = str(name)
        if to:
            content['to'] = str(_(" to "))
            if len(to) == 1:
                content['to'] += str(to[0])
            else:
                content['to'] += ', '.join(str(t) for t in to[0:-1])
                content['to'] += str(_(" and "))
                content['to'] += str(to[-1])
        # group contents if possible
        q = ActionLog.objects.filter(character=self).order_by("-id")
        if q.count():
            last_log = q.all()[0]
            if last_log.content.get("type", None) != "chapter":
                old_content = last_log.content
                for key in ("name", "from", "to"):
                    if old_content.get(key, None) != content.get(key, None):
                        break
                else:
                    if last_log.location == frm.location:
                        old_content["description"] += "\n\n" + content[
                            "description"]
                        last_log.state = state
                        last_log.icon = icon
                        last_log.save()
                        return last_log
        return ActionLog.objects.create(character=self, content=content,
                                        state=state, icon=icon,
                                        location=frm.location)

    def get_initiatives(self):
        if not self.location or not self.location.live_action:
            return []
        current_init = self.location.current_initiative if self.location else -1
        data = []
        if self.initiative > 0:
            data = [{
                "slug": self.slug,
                "name": self.current_name,
                "value": self.initiative,
                "is_current_init": self.initiative == current_init,
                "myself": True
            }]
        for c in self.get_neighbors().values_list(
                "slug", "initiative", "current_name"):
            slug, initiative, current_name = c
            if initiative > 0:
                data.append({
                    "slug": slug,
                    "name": current_name,
                    "value": initiative,
                    "is_current_init": initiative == current_init,
                    "myself": False
                })
        return reversed(sorted(data, key=lambda x: (x["value"], x["name"])))

    def get_live_action_editable_fields(self):
        fields = [(_("Initiative"), "main-initiative", self.initiative)]
        for k in ("hp", "mp", "fp", "ap"):
            if self.game and getattr(self.game, k + "_editable_live"):
                fields.append(
                    (self.__class__._meta.get_field(k).verbose_name,
                     "main-" + k,
                     getattr(self, k)))
        for ability in self.abilities.filter(ability__editable_live=True).all():
            fields.append(
                (ability.ability.name,
                 "ability-" + ability.ability.slug,
                 ability.score_calculated))
        return fields

    def get_oracles(self):
        q = Oracle.objects.filter(game=self.game)
        if not GameMaster.objects.filter(
                player=self.player, game=self.game).count():
            q = q.exclude(gm_only=False)
        return [{"name": o.name, "slug": o.slug} for o in q.all() if
                o.choices.count()]

    def get_documents(self, hidden=False):
        q = Q(locations=None) & Q(characters=None)
        q |= Q(locations=None) & Q(characters=self)
        if self.location:
            q |= Q(locations=self.location) & Q(characters=self)
            q |= Q(locations=self.location) & Q(characters=None)
        q = Document.objects.filter(
            game=self.game, hidden=hidden, preload=True).filter(q)
        documents = {}
        current_category = False
        categories = {}
        for document in q.all().order_by("category", "name"):
            key = document.category
            if document.category:
                key = document.category.slug
                categories[key] = document.category
            if current_category != document.category:
                documents[key] = []
                current_category = document.category
            documents[key].append(document)
        document_list = []
        keys = [key for key in documents.keys() if key]
        for key in sorted(keys, key=lambda k: (categories[k].order,
                                               categories[k].name)):
            category = categories[key]
            cat = {"name": category.name,
                   "order": category.order, "slug": category.slug,
                   "items": []}
            for document in documents[key]:
                cat["items"].append({
                    "name": document.name, "slug": document.slug,
                    "image": document.image.url, "hidden": document.hidden})
            document_list.append(cat)
        # no category
        if None in documents.keys():
            for document in documents[None]:
                document_list.append({
                    "name": document.name, "slug": document.slug,
                    "items": [],
                    "image": document.image.url, "hidden": document.hidden})
        return document_list

    def get_neighbors(self, extra_filter=None, extra_exclude=None,
                      need_conscious=True):
        if not self.location:
            return Character.objects.filter(id=-1)
        q = self.location.characters.exclude(pk=self.pk)
        if need_conscious:
            q = q.exclude(hp__lte=0)
        if extra_filter:
            q = q.filter(**extra_filter)
        if extra_exclude:
            q = q.filter(**extra_exclude)
        return q

    def get_own_items(self, natural=None, equiped=None):
        q = self.items
        q2 = Item.objects.filter(
            inside__character_areas__shape__character=self
        )
        if natural is not None:
            q = q.filter(natural=natural)
            q2 = q2.filter(natural=natural)
        if equiped is not None:
            q = q.annotate(area_nbs=Count('character_areas'))
            q2 = q2.annotate(area_nbs=Count('character_areas'))
            if equiped:
                q = q.filter(area_nbs__gt=0)
                q2 = q2.filter(area_nbs__gt=0)
            else:
                q = q.filter(area_nbs=0)
                q2 = q2.filter(area_nbs=0)
        return list(q.all()) + list(q2.all())


def init_initial_name(sender, instance, *args, **kwargs):
    if not instance.current_name:
        instance.current_name = instance.initial_name
    pre_save_slug(sender, instance, *args, **kwargs)


pre_save.connect(init_initial_name, sender=Character)


class CharacterAudio(models.Model):
    audio = models.ForeignKey(Audio, on_delete=models.CASCADE)
    character = models.ForeignKey(Character, on_delete=models.CASCADE)
    autoplay = models.BooleanField(verbose_name=_("Autoplay"), default=False)
    loop = models.BooleanField(verbose_name=_("Loop"), default=True)
    order = models.IntegerField(verbose_name=_("Order"), default=10)
    expiration = models.DateTimeField(
        verbose_name=_("Expiration"), blank=True, null=True)

    class Meta:
        verbose_name = _("Character - Audio")
        verbose_name_plural = _("Character - Audios")
        ordering = ('order',)


LOG_STATES = (
    ('U', _("Unread")),
    ('R', _("Read")),
    ('I', _("Important")),
    ('H', _("Hide")),
)


class ActionLog(models.Model):
    character = models.ForeignKey(Character, related_name='logs',
                                  on_delete=models.CASCADE)
    date = models.DateTimeField(_("Date"), auto_now_add=True)
    state = models.CharField(_("State"), choices=LOG_STATES, default='U',
                             max_length=1)
    location = models.ForeignKey("morrigan_geography.Location", blank=True,
                                 null=True, on_delete=models.SET_NULL)
    icon = models.CharField(_("Icon"), max_length=200, blank=True, null=True)
    number = models.IntegerField(
        _("Number"), blank=True, help_text=_("Per character dialing"))
    content = JSONField(_("Content"))

    class Meta:
        verbose_name = _("Action log")
        verbose_name_plural = _("Action logs")
        ordering = ('character', '-number')

    def human_date(self):
        return naturaltime(self.date)


@receiver(pre_save, sender=ActionLog)
def actionlog_number(sender, instance, *args, **kwargs):
    if not instance.number:
        v = ActionLog.objects.filter(character=instance.character
                                     ).aggregate(Max('number'))
        instance.number = (v['number__max'] or 0) + 1


class CharacterMoney(models.Model):
    character = models.ForeignKey(Character, on_delete=models.CASCADE)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)


class CharacterEffect(models.Model):
    character = models.ForeignKey("Character", on_delete=models.CASCADE)
    effect = models.ForeignKey(Effect, on_delete=models.CASCADE)
    start_date = models.DateField(auto_now_add=True)
    end_date = models.DateField(null=True, blank=True)


class GameMaster(models.Model):
    game = models.ForeignKey(Game, related_name="game_masters", null=True,
                             blank=True, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, related_name='game_master',
                               on_delete=models.CASCADE)
    name = models.TextField(_("Name"), unique=True)
    last_connection = models.DateTimeField(_("Last connection"), null=True,
                                           blank=True)

    class Meta:
        verbose_name = _("Game master")
        verbose_name_plural = _("Game masters")

    def __str__(self):
        return self.name


class Race(GenericType):
    effect = models.ForeignKey(Effect, verbose_name=_("Effect"), null=True,
                               blank=True, on_delete=models.SET_NULL)
    can_be_mount = models.BooleanField(_("Can be mount"), default=False)
    size_index = models.IntegerField(_("Size index"), null=True, blank=True)
    open_for_registration = models.BooleanField(_("Open for registration"),
                                                default=True)


class StepType(GenericType):
    pass


class Shape(AvatarModel, models.Model):
    """
    A shape associated to a character (one character can have multi-shape)
    """
    character = models.ForeignKey(Character, related_name='shapes',
                                  on_delete=models.CASCADE)
    slug = models.SlugField(_("Slug"), blank=True, null=True, unique=True,
                            max_length=200)
    race = models.ForeignKey(Race, on_delete=models.CASCADE, blank=True,
                             null=True)
    sex = models.ForeignKey(Sex, on_delete=models.CASCADE, blank=True,
                            null=True)
    klass = models.ForeignKey(Klass, blank=True, null=True,
                              on_delete=models.SET_NULL)
    current_name = models.TextField(_("Current name"), unique=True, null=True,
                                    blank=True)
    size_index = models.IntegerField(_("Size index"), null=True, blank=True)
    is_current_shape = models.BooleanField(_("Is current shape"),
                                           default=False)
    description = models.TextField(_("Description"), null=True, blank=True)
    current_description = models.TextField(_("Current description"), null=True,
                                           blank=True)
    image = models.ImageField(_("Image"), blank=True, null=True)
    small_image = models.ImageField(
        _("Small image"), blank=True, null=True,
        help_text=_("Image used to generate an avatar"))
    step_type = models.ForeignKey(
        StepType, verbose_name=_("Step type"), blank=True, null=True,
        on_delete=models.SET_NULL)
    step_deep = models.IntegerField(_("Step deep"), default=0)
    step_size = models.IntegerField(_("Step size"), default=0)
    temporary_trusted = models.ManyToManyField(
        "Shape", blank=True, related_name="temporary_trusting")
    trusted = models.ManyToManyField("Shape", blank=True,
                                     related_name="trusting")

    AVATAR_FIELD = 'small_image'

    class Meta:
        verbose_name = _("Shape")
        verbose_name_plural = _("Shapes")

    def __str__(self):
        return self.current_name or str(self.character)

    def has_permission(self, permission_type, user):
        if user.is_superuser:
            return True
        profile = user.profile
        if permission_type == 'simple' and \
                (self.character.player and self.character.player.user == user
                 or (self.is_current_shape and Player.objects.filter(
                     pk=profile.pk,
                     characters__location=self.character.location)
                     .count())):
            # simple description when you are at the same place
            return True
        return False

    @property
    def name(self):
        if self.current_name:
            return self.current_name
        return self.character.current_name

    def available_areas(self):
        areas = {}
        for area in self.areas.all():
            slug = area.area.slug
            areas[slug] = area.place_available
            for item in area.items.all():
                if hasattr(item, 'place_taken'):
                    areas[slug] -= item.place_taken
                if areas[slug] < 0:
                    # should not occurs normally
                    areas[slug] = 0
        return areas

    def can_equip(self, item):
        """
        Check if an item can be equipped by the character.
        This method check only for space available and equipability of the 
        item all other check must be implemented on action level.
        
        :param item: the concerned item (instance of Item)
        :return: True if this item can be equipped
        """
        if not item.equipable or item.character_areas.count() or item.natural:
            return False
        available_areas = self.available_areas()
        if not hasattr(item, 'armors') or not item.armors.count():
            # configuration issue...
            return False
        armor = item.armors.all()[0]
        for itemarea in armor.areas.all():
            if not hasattr(item, 'place_taken') or not item.place_taken:
                continue
            slug = itemarea.slug
            if slug not in available_areas or \
                    available_areas[slug] < item.place_taken:
                return False
        return True

    def can_be_put_inside_a_bag(self, item):
        """
        Check if an item can be put inside one of our bag.
        This method check only for space available inside bags all other check
        must be implemented on action level.
        
        :param item: the concerned item (instance of Item)
        :return: True if this item can be put inside our bag
        """
        # contain capacity > 0: it is a bag
        q_bag = self.character.items.filter(contain_capacity__gt=0)
        for bag in q_bag.all():
            if bag.available_space >= item.size.index:
                return True
        return False

    def save(self, *args, **kwargs):
        super(Shape, self).save(*args, **kwargs)
        if not self.areas.count():
            for body_area in BodyArea.objects.filter(available=True):
                ShapeArea.objects.create(
                    shape=self, area=body_area,
                    is_default=body_area.is_default,
                    place_available=body_area.default_number_available)
        # update cache
        if self.is_current_shape:
            q = Character.objects.filter(shapes__pk=self.pk)
            if q.count():
                c = q.all()[0]
                c.update_current_shape()


pre_save.connect(pre_save_slug, sender=Shape)


def auto_create_shape(sender, instance, *args, **kwargs):
    if not instance or Shape.objects.filter(character_id=instance.pk).count():
        return
    slug = instance.slug
    idx = 0
    while Shape.objects.filter(slug=slug).count():
        if not idx:
            base_slug = slug[:198]
        idx += 1
        slug = "{}-{}".format(base_slug, idx)
    Shape.objects.create(character_id=instance.pk,
                         slug=slug,
                         is_current_shape=True,
                         current_name=instance.current_name)


post_save.connect(auto_create_shape, sender=Character)


class ShapeArea(models.Model):
    """
    Body area of a specific shape.
    """
    area = models.ForeignKey(BodyArea, related_name='+',
                             on_delete=models.CASCADE)
    shape = models.ForeignKey(Shape, related_name='areas',
                              on_delete=models.CASCADE)
    place_available = models.IntegerField(_("Place available"), default=1)
    # TODO: check that only one is default for a shape
    is_default = models.BooleanField(
        _("Is the default body shape"), default=False,
        help_text=_("Default body shape is used when, for instance, you want "
                    "to pick up a thing"))

    class Meta:
        verbose_name = _("Shape area")
        verbose_name_plural = _("Shape areas")

    def __str__(self):
        return "{} - {}".format(self.area, self.shape)

    @property
    def place_taken(self):
        # TODO: not so simple -> check place_taken for each armor area
        return sum(item.size.index for item in self.items.all())


class Itinerary(models.Model):
    """
    An itinerary used by a character (used by character tracking) following
    a path or on the map
    """
    path = models.ForeignKey(Path, verbose_name=_("Path"), null=True,
                             blank=True, on_delete=models.CASCADE)
    shape = models.ForeignKey(Shape, verbose_name=_("Shape"),
                              on_delete=models.CASCADE)
    start = models.ForeignKey(Location, null=True, blank=True,
                              related_name='itinerary_start',
                              on_delete=models.CASCADE)
    arrival = models.ForeignKey(Location, null=True, blank=True,
                                related_name='itinerary_arrival',
                                on_delete=models.CASCADE)
    initial_steps = models.IntegerField(_("Initial steps"), null=True,
                                        blank=True)
    remaining_steps = models.IntegerField(_("Remaining steps"), null=True,
                                          blank=True)
    date = models.DateTimeField(_("Date"), auto_now_add=True)
    erase_index = models.IntegerField(_("Erase index"), default=1)
    step_type = models.ForeignKey(StepType, verbose_name=_("Step type"),
                                  on_delete=models.CASCADE)
    step_deep = models.IntegerField(_("Step deep"), default=0)
    step_size = models.IntegerField(_("Step size"), default=0)


class CharacterAbility(models.Model):
    character = models.ForeignKey(Character, related_name='abilities',
                                  on_delete=models.CASCADE)
    ability = models.ForeignKey(Ability, on_delete=models.CASCADE)
    training_gauge = models.IntegerField(_("Training gauge"), default=0)
    current_xp = models.IntegerField(_("Current XP"), default=0)
    max_xp = models.IntegerField(_("Max XP"), default=0)
    score_calculated = models.IntegerField(_("Value"), default=0)
    score_max = models.IntegerField(_("Max value"), default=0)

    class Meta:
        unique_together = ('character', 'ability')


class Rumor(models.Model):
    """
    Rumors. Can be launched by players or game masters.
    """
    name = models.TextField(_("Rumor"))
    character = models.ForeignKey(Character, null=True, blank=True,
                                  on_delete=models.CASCADE)
    content = models.TextField(_("Content"))
    language = models.ForeignKey(Ability, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    radius = models.IntegerField(_("Radius"), null=True, blank=True)
    sector = models.ForeignKey(Sector, null=True, blank=True,
                               on_delete=models.CASCADE)
    difficulty = models.FloatField(_("Difficulty"), null=True, blank=True)
    start_date = models.DateField(_("Start date"), auto_now_add=True)
    end_date = models.DateField(_("End date"), null=True, blank=True)


class FollowingType(GenericType):
    pass


class Following(models.Model):
    follower = models.ForeignKey(Character, related_name='following',
                                 on_delete=models.CASCADE)
    followed = models.ForeignKey(Shape, related_name='followed_by',
                                 on_delete=models.CASCADE)
    type = models.ForeignKey(FollowingType, on_delete=models.CASCADE)
    start_date = models.DateTimeField(_("Start date"), auto_now_add=True)


class KnownedStep(models.Model):
    step_type = models.ForeignKey(StepType, null=True, blank=True,
                                  on_delete=models.CASCADE)
    step_deep = models.IntegerField(_("Step deep"), default=0)
    step_size = models.IntegerField(_("Step size"), default=0)
    followed_shape = models.ForeignKey(
        Shape, null=True, blank=True, verbose_name=_("Followed shape"),
        on_delete=models.CASCADE)
    following_character = models.ForeignKey(
        Character, null=True, blank=True, verbose_name=_("Following character"),
        on_delete=models.CASCADE)
    end_date = models.DateTimeField(_("End date"), null=True, blank=True)
    given_name = models.TextField(_("Given name"), blank=True, null=True)
