#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.views.static import serve

from morrigan_common.views import HomePageView
from admin import admin_site
import morrigan_chat.urls
import morrigan_common.urls
import morrigan_characters.urls
import morrigan_geography.urls

admin.autodiscover()

urlpatterns = [
    url(r'^$', HomePageView.as_view(), name='index'),
    url(r'', include(morrigan_common.urls)),
    url(r'chat/', include(morrigan_chat.urls)),
    url(r'', include(morrigan_characters.urls)),
    url(r'', include(morrigan_geography.urls)),
    url(r'^login/$', auth_views.LoginView.as_view(template_name='login.html'),
        name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(next_page="/"),
        name='logout'),
    url(r"^change-password/$", auth_views.PasswordChangeView.as_view(),
        name="password_change"),
    url(r"^password-change-done/$",
        auth_views.PasswordChangeDoneView.as_view(),
        name="password_change_done"),
    url(r'^admin/', admin_site.urls),
]

if settings.DEBUG:
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve,
            {'document_root': settings.MEDIA_ROOT}),
    ]
