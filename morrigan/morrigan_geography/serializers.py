#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015-2017  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.utils.timezone import now

from rest_framework import serializers
from morrigan_common.models import GameAudio
from morrigan_characters.models import CharacterAudio
from morrigan_geography import models
from morrigan_characters.serializers import CharacterSimpleSerializer, \
    BaseActionLogSerializer
from morrigan_inventory.serializers import ItemSerializer


class MapSerializer(serializers.ModelSerializer):
    sector_image = serializers.SerializerMethodField()
    sector_image_dimensions = serializers.SerializerMethodField()
    points = serializers.SerializerMethodField()

    class Meta:
        model = models.Location
        fields = ('name', 'sector_image', 'sector_image_x', 'sector_image_y',
                  'sector_image_dimensions', 'points', 'live_action')

    def get_points(self, obj):
        points = [
            {"name": obj.short_name,
             "x": obj.sector_image_x,
             "y": obj.sector_image_y,
             "color": "#ff6600",
             "slug": obj.slug}
        ]
        sector = self.get_sector(obj)
        if not sector:
            return points
        location_list = []
        for path in obj.path_start.all():
            arrival = path.arrival
            if arrival.pk in location_list:
                continue
            location_list.append(arrival.pk)
            sec = self.get_sector(arrival)
            if sec != sector:
                continue
            points.append({
                "name": arrival.short_name,
                "x": arrival.sector_image_x,
                "y": arrival.sector_image_y,
                "slug": arrival.slug,
                "color": "#000000"}
            )
        return points

    def get_sector(self, obj):
        if not hasattr(obj, "_sector"):
            sector = obj.sector
            while sector and not sector.border_image:
                sector = sector.parent
            obj._sector = sector
        return obj._sector

    def get_sector_image(self, obj):
        sector = self.get_sector(obj)
        if sector:
            return sector.border_image.url
        return ""

    def get_sector_image_dimensions(self, obj):
        sector = self.get_sector(obj)
        if sector and hasattr(sector.border_image, "width"):
            return (sector.border_image.width, sector.border_image.height)
        return [0, 0]


class LiveMapSerializer(serializers.ModelSerializer):
    live_image = serializers.ImageField(required=False)
    over_live_image = serializers.ImageField(required=False)

    class Meta:
        model = models.Location
        fields = ('live_image', 'over_live_image', 'live_action')


class NearLocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Location
        fields = ('name', 'type', 'short_description')


class LocationSerializer(serializers.ModelSerializer):
    items = serializers.SerializerMethodField()
    characters = serializers.SerializerMethodField()
    image = serializers.ImageField(required=False)
    parent_images = serializers.SerializerMethodField()
    audios = serializers.SerializerMethodField()
    is_game_master = serializers.SerializerMethodField()

    class Meta:
        model = models.Location
        fields = ('name', 'type', 'description', 'sector', 'size_index',
                  'population_index', 'image', 'characters', 'items',
                  'slug', 'parent_images', 'audios', 'is_game_master')

    def get_is_game_master(self, obj):
        if (self.context["character"].player.game_master.filter(
                game=obj.game).count()):
            return True
        return False

    def _serialize_audio(self, q, audio_type, n):
        return [
            {"order": ag.order,
             "autoplay": ag.autoplay and (not ag.expiration
                                          or ag.expiration >= n),
             "slug": ag.audio.slug, "url": ag.audio.link,
             "loop": ag.loop,
             "name": ag.audio.name,
             "expiration": ag.expiration,
             "audiotype": audio_type,
             "type": "audio/ogg" if ag.audio.link and ag.audio.link.lower(
             ).endswith(".ogg") else 'audio/mpeg'}
            for ag in q.all()]

    def get_audios(self, obj):
        n = now()
        q1 = GameAudio.objects.filter(game=obj.game)
        audios = self._serialize_audio(q1, "game", n)
        q2 = models.LocationAudio.objects.filter(location=obj)
        audios += self._serialize_audio(q2, "location", n)
        q3 = CharacterAudio.objects.filter(character__location=obj)
        audios += self._serialize_audio(q3, "character", n)
        return sorted(audios, key=lambda x: x["order"])

    def get_items(self, obj):
        # TODO: manage visibility
        items = ItemSerializer(obj.items, many=True, read_only=True)
        return items.data

    def get_parent_images(self, obj):
        images = []
        if obj.sector_image:
            images.append(obj.sector_image.url)
        if obj.sector:
            sector = obj.sector
            while sector:
                if not sector.parent_image or sector.parent_image.url in images:
                    break
                images.append(sector.parent_image.url)
                sector = sector.parent
        return images

    def get_characters(self, obj):
        # own character first
        q = obj.characters.extra(
            select={
                'ordering': """(
                    case when slug='%s' then 1
                    else 2
                    end)""" % self.context['view'].kwargs['characters__slug']
            }
        ).filter(owner__isnull=True).order_by('ordering', 'current_name')
        characters = CharacterSimpleSerializer(q, many=True, read_only=True,
                                               context=self.context)
        return characters.data


class LocationActionLogSerializer(BaseActionLogSerializer,
                                  serializers.ModelSerializer):
    act_log_key = "location"
    number_of_pages = serializers.SerializerMethodField()
    page = serializers.SerializerMethodField()
    logs = serializers.SerializerMethodField('paginated_logs')

    class Meta:
        model = models.Location
        fields = ('slug', 'logs', 'number_of_pages', 'page')
