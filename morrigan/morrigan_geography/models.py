#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2017  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import datetime
import os
from PIL import Image, ImageDraw
from tempfile import TemporaryDirectory
import shutil

from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.utils.translation import ugettext_lazy as _

from morrigan_common.models import GenericType, Effect, Ability, \
    Formula, Action, pre_save_slug, Game, Audio
from morrigan_common.utils import put_cross_on_map, add_margin, \
    margin_image_path

"""
The Morrigan geography is divided into one (or many) world(s).
Theses worlds can have sectors that contains locations.
To theses worlds can be associated maps (with x, y coordinates).
"""


class Season(GenericType):
    default = models.BooleanField(default=True)

    class Meta:
        verbose_name = _("Season")
        verbose_name_plural = _("Season")


class WorldType(GenericType):
    class Meta:
        verbose_name = _("Type: World")
        verbose_name_plural = _("Types: World")


class World(models.Model):
    name = models.TextField()
    description = models.TextField(null=True, blank=True)
    father = models.ForeignKey('self', null=True, blank=True,
                               on_delete=models.SET_NULL)
    level = models.IntegerField(null=True, blank=True)
    effect = models.ForeignKey(Effect, null=True, blank=True,
                               on_delete=models.SET_NULL)
    image = models.ImageField(blank=True, null=True)

    class Meta:
        verbose_name = _("World")
        verbose_name_plural = _("Worlds")

    def __str__(self):
        return self.name


class LocationType(GenericType):
    actions = models.ManyToManyField(Action, blank=True)

    class Meta:
        verbose_name = _("Type: Location")
        verbose_name_plural = _("Types: Location")


class LocationAudio(models.Model):
    audio = models.ForeignKey(Audio, on_delete=models.CASCADE)
    location = models.ForeignKey("Location", on_delete=models.CASCADE)
    autoplay = models.BooleanField(verbose_name=_("Autoplay"), default=False)
    loop = models.BooleanField(verbose_name=_("Loop"), default=True)
    order = models.IntegerField(verbose_name=_("Order"), default=10)
    expiration = models.DateTimeField(
        verbose_name=_("Expiration"), blank=True, null=True)

    class Meta:
        verbose_name = _("Location - Audio")
        verbose_name_plural = _("Location - Audios")
        ordering = ('order',)


class Location(models.Model):
    PARENT_SECTOR = "sector"
    game = models.ForeignKey(
        Game, null=True, blank=True,
        verbose_name=_("Game"), related_name="locations",
        on_delete=models.CASCADE)
    type = models.ForeignKey("LocationType", on_delete=models.CASCADE)
    name = models.TextField()
    slug = models.SlugField(_("Slug"), blank=True, null=True, unique=True)
    short_description = models.TextField(
        null=True, blank=True,
        help_text=_("What can be seen from a path. Should be filled to be "
                    "displayed on the map."))
    description = models.TextField(null=True, blank=True)
    available = models.BooleanField(_("Available"), default=True)
    sector = models.ForeignKey("Sector", null=True, blank=True,
                               on_delete=models.SET_NULL)
    sector_image_x = models.IntegerField(_("X position on sector image"),
                                         blank=True, null=True)
    sector_image_y = models.IntegerField(_("Y position on sector image"),
                                         blank=True, null=True)
    sector_image = models.ImageField(
        _("Sector image"), blank=True, null=True,
        help_text=_("Automatically generated with position on the image, "
                    "delete to force regeneration."))
    size_index = models.IntegerField(_("Size index"), null=True, blank=True)
    population_index = models.IntegerField(_("Population index"), default=0)
    factor_step_erasure = models.FloatField(_("Factor step erasure"),
                                            default=1)
    factor_step_number = models.FloatField(_("Factor step number"), default=0)
    image = models.ImageField(_("Image"), blank=True, null=True)
    live_action = models.BooleanField(_("Live action"), default=False)
    live_image = models.ImageField(
        _("Live image"), blank=True, null=True,
        help_text=_("Used for live action"))
    over_live_image = models.ImageField(
        _("Over live image"), blank=True, null=True,
        help_text=_("Used for live action - automatically generated with "
                    "positions on the image"))
    current_initiative = models.IntegerField(
        _("Current initiative in live action"), default=0)
    actions = models.ManyToManyField(
        Action, blank=True, help_text=_("If set only theses actions are "
                                        "allowed"))
    audios = models.ManyToManyField(
        Audio, verbose_name=_("Audio URL"), through=LocationAudio, blank=True)

    class Meta:
        verbose_name = _("Location")
        verbose_name_plural = _("Locations")

    def __str__(self):
        return self.name

    @property
    def short_name(self):
        if len(self.name) < 32:
            return self.name
        return self.name[:26] + "..."

    def has_permission(self, permission_type, user):
        if permission_type == 'full' and \
           ((self.characters.filter(player__user=user).count())
                or user.is_superuser):
            return True
        return False

    def get_paths(self, character=None):
        paths = list(self.path_start.filter(forth_is_open=True).all())
        paths += list(self.path_arrival.filter(back_is_open=True).all())
        return paths

    def generate_over_live_image(self):
        if not self.live_image:
            self.over_live_image = None
            self.over_live_image.save()
            return
        image_copy = Image.open(self.live_image.path).copy()
        width, height = image_copy.size
        over = Image.new('RGBA', (width, height), (0, 0, 0, 0))
        for point in list(self.live_points.all()):
            character = point.character
            if character.location != self:
                point.delete()
                continue
            if point.position_x is None or point.position_y is None:
                continue
            if not character.current_shape or \
                    not character.current_shape.avatar:
                continue
            avatar = Image.open(character.current_shape.avatar.path)
            x = point.position_x - int(settings.MORRIGAN_AVATAR_SIZE[0] / 2)
            if x < 0:
                x = 0
            y = point.position_y - int(settings.MORRIGAN_AVATAR_SIZE[1] / 2)
            if y < 0:
                y = 0
            # colored border
            color = character.HEALTH_COLORS[character.health()[-1]]
            size = settings.MORRIGAN_AVATAR_SIZE[0] + 8
            size = size * 3
            ellipse = Image.new('RGBA', (size, size), 0)
            draw = ImageDraw.Draw(ellipse)
            draw.ellipse((0, 0, size, size), fill=color)
            ellipse = ellipse.resize((int(size/3), int(size/3)),
                                     Image.ANTIALIAS)
            mask = Image.new('L', (size, size), 0)
            draw = ImageDraw.Draw(mask)
            draw.ellipse((0, 0, size, size), fill=255)
            mask = mask.resize((int(size/3), int(size/3)), Image.ANTIALIAS)
            over.paste(ellipse, (x - 4, y - 4), mask=mask)

            size = settings.MORRIGAN_AVATAR_SIZE[0]
            mask = Image.new('L', (size*3, size*3), 0)
            draw = ImageDraw.Draw(mask)
            draw.ellipse((0, 0, size*3, size*3), fill=255)
            mask = mask.resize((size, size), Image.ANTIALIAS)
            over.paste(avatar, (x, y), mask=mask)

        with TemporaryDirectory() as temp_dir:
            dest = os.sep.join([temp_dir, "live.png"])
            over.save(dest)
            with open(dest, 'rb') as new_image:
                temp_file = SimpleUploadedFile('temp', new_image.read())
                self.over_live_image.save(
                    "live-{}.png".format(
                        datetime.datetime.now().isoformat(
                            ).replace(".", "-").replace(':', '-')),
                    temp_file)


def generate_sector_image(sender, instance, *args, **kwargs):
    parent_attr = getattr(sender, "PARENT_SECTOR")
    if not instance:
        return
    sector = getattr(instance, parent_attr)
    sector_image = getattr(instance, parent_attr + "_image")
    sector_image_x = getattr(instance, parent_attr + "_image_x")
    sector_image_y = getattr(instance, parent_attr + "_image_y")
    if hasattr(instance, "_sector_image_generated") or not sector \
            or not sector.image or sector_image_x is None or \
             sector_image_y is None or sector_image:
        return
    new_image, tmp_dir = put_cross_on_map(sector.image,
                                          sector_image_x,
                                          sector_image_y)
    with open(new_image, 'rb') as new_image:
        temp_file = SimpleUploadedFile('temp', new_image.read())
        instance._sector_image_generated = True
        sector_image.save(sector.image.name, temp_file)
    shutil.rmtree(tmp_dir)


def generate_border_image(sender, instance, *args, **kwargs):
    if not instance or not instance.image or hasattr(
            instance, "_border_image_generated"):
        return
    new_image, tmp_dir = add_margin(instance.image, 30, 30, 30, 30,
                                    "#aeaeae")
    with open(new_image, 'rb') as new_image:
        temp_file = SimpleUploadedFile('temp', new_image.read())
        instance._border_image_generated = True
        instance.border_image.save(
            margin_image_path(instance.image.name), temp_file)
    shutil.rmtree(tmp_dir)


pre_save.connect(pre_save_slug, sender=Location)
post_save.connect(generate_sector_image, sender=Location)


class LiveMapPoint(models.Model):
    location = models.ForeignKey(
        Location, related_name='live_points', on_delete=models.CASCADE)
    character = models.ForeignKey(
        "morrigan_characters.Character", related_name="live_points",
        on_delete=models.CASCADE)
    position_x = models.IntegerField(_("X"), blank=True, null=True)
    position_y = models.IntegerField(_("Y"), blank=True, null=True)

    class Meta:
        verbose_name = _("Live map point")
        verbose_name_plural = _("Live map points")

    def save(self, *args, **kwargs):
        super(LiveMapPoint, self).save(*args, **kwargs)
        self.location.generate_over_live_image()


class MapLocation(models.Model):
    world = models.ForeignKey(World, on_delete=models.CASCADE)
    x = models.IntegerField(_("X"))
    y = models.IntegerField(_("Y"))
    location = models.ForeignKey(
        Location, null=True, blank=True, related_name='map_locations',
        on_delete=models.SET_NULL)
    season = models.ForeignKey("Season", null=True, blank=True,
                               on_delete=models.SET_NULL)
    layers = models.ManyToManyField('Layer')

    class Meta:
        verbose_name = _("World - Map location")
        verbose_name_plural = _("World - Map locations")

    def __str__(self):
        return "{} - {} - {}".format(self.world, self.x, self.y)


class PathType(GenericType):
    class Meta:
        verbose_name = _("Type: Path")
        verbose_name_plural = _("Types: Path")


class PathFeature(models.Model):
    class Meta:
        verbose_name = _("Type: Path feature")
        verbose_name_plural = _("Types: Path feature")


class Path(models.Model):
    start = models.ForeignKey(Location, related_name='path_start',
                              on_delete=models.CASCADE)
    arrival = models.ForeignKey(Location, related_name='path_arrival',
                                on_delete=models.CASCADE)
    formula = models.ForeignKey(
        Formula, verbose_name=_('Formula'), blank=True, null=True,
        related_name='paths', on_delete=models.SET_NULL)
    difficulty = models.IntegerField(_("Difficulty"), default=0)
    forth_is_open = models.BooleanField(_("Forth is open"), default=True)
    back_is_open = models.BooleanField(_("Back is open"), default=True)
    is_open = models.BooleanField(_("Is open"), default=True)
    blocking_failure = models.BooleanField(_("Failure is blocking"),
                                           default=False)
    forth_description = models.TextField(_('Forth description'), blank=True,
                                         null=True)
    back_description = models.TextField(_("Back description"), blank=True,
                                        null=True)
    key = models.ForeignKey('morrigan_inventory.GenericItem', null=True,
                            blank=True, verbose_name=_('Key'),
                            on_delete=models.SET_NULL)
    hidden_index = models.IntegerField(_("Hidden index"), default=0)

    step_erasement_factor = models.FloatField(_("Step erasement factor"),
                                              default=1)
    step_number_index = models.FloatField(_("Step number index"), default=1)
    size_index = models.IntegerField(_("Size index"), default=10)
    path_feature = models.ForeignKey("PathFeature", null=True, blank=True,
                                     on_delete=models.SET_NULL)

    class Meta:
        verbose_name = _("Path")
        verbose_name_plural = _("Paths")

    def __str__(self):
        return "{} -> {}".format(self.start, self.arrival)


class DescriptionLocation(models.Model):
    """
    A location can have many description to be discovered by characters
    """
    available = models.BooleanField(default=True)
    location = models.ForeignKey(Location, verbose_name=_("Location"),
                                 on_delete=models.CASCADE)
    difficulty = models.IntegerField(_("Difficulty"), default=1)
    description = models.TextField(_("Description"))

    class Meta:
        verbose_name = _("Location description")
        verbose_name_plural = _("Location descriptions")


class SpecificAction(models.Model):
    available = models.BooleanField(default=True)
    autorised = models.BooleanField(default=True)
    difficulty = models.IntegerField(default=0)
    ap_cost = models.IntegerField(_("Action point cost"), default=0)
    fp_cost = models.IntegerField(_("Feed point cost"), default=0)
    mp_cost = models.IntegerField(_("Magic point cost"), default=0)
    hp_cost = models.IntegerField(_("Hit point cost"), default=0)
    bp_cost = models.IntegerField(_("Battle point cost"), default=0)

    class Meta:
        abstract = True


class SectorType(GenericType):
    class Meta:
        verbose_name = _("Type: Sector")
        verbose_name_plural = _("Types: Sector")


class VehicleType(GenericType):
    fuel = models.ForeignKey(
        'morrigan_inventory.GenericItem', verbose_name=_("Fuel"), null=True,
        blank=True, on_delete=models.SET_NULL)
    power = models.IntegerField(_("Power"), null=True, blank=True)
    compumption = models.IntegerField(_("Comsumption"), null=True, blank=True)
    max_wear = models.IntegerField(_("Max wear"), null=True, blank=True)
    wear = models.IntegerField(_("Wear"), null=True, blank=True)

    class Meta:
        verbose_name = _("Type: Vehicle")
        verbose_name_plural = _("Types: Vehicle")


class Vehicle(models.Model):
    type = models.ForeignKey(VehicleType, verbose_name=_("Type"),
                             on_delete=models.CASCADE)
    tank_capacity = models.IntegerField(_("Tank capacity"), null=True,
                                        blank=True)
    current_tank = models.IntegerField(_("Current tank content"), null=True,
                                       blank=True)
    current_wear = models.IntegerField(_("Current wear"), null=True,
                                       blank=True)
    docked = models.BooleanField(_("Docked"), default=False)
    engine_on = models.BooleanField(_("Engine on"), default=False)
    mass = models.IntegerField(_("Mass"), null=True, blank=True)
    engine_flange = models.IntegerField(_("Engine flange"), null=True,
                                        blank=True)
    wind_flange = models.IntegerField(_("Wind flange"), null=True, blank=True)
    direction = models.TextField(_("Direction"), blank=True)
    reset_date = models.DateField(_("Reset date"), auto_now_add=True)
    food_stockpile = models.IntegerField(_("Food stockpile"), null=True,
                                         blank=True)
    current_food_stockpile = models.IntegerField(_("Current food stockpile"),
                                                 null=True, blank=True)


class EnvironmentType(models.Model):
    name = models.TextField(_("Name"))
    ability = models.ForeignKey(Ability, blank=True, null=True,
                                on_delete=models.SET_NULL)

    class Meta:
        verbose_name = _("Type: Environment")
        verbose_name_plural = _("Types: Environment")

    def __str__(self):
        return self.name


class Sector(models.Model):
    PARENT_SECTOR = "parent"
    name = models.TextField(_("Name"), unique=True)
    description = models.TextField(null=True, blank=True)
    type = models.ForeignKey(SectorType, on_delete=models.CASCADE)
    parent = models.ForeignKey("self", blank=True, null=True,
                               on_delete=models.CASCADE)
    parent_image_x = models.IntegerField(_("X position on parent image"),
                                         blank=True, null=True)
    parent_image_y = models.IntegerField(_("Y position on parent image"),
                                         blank=True, null=True)
    parent_image = models.ImageField(
        _("Parent image"), blank=True, null=True,
        help_text=_("Automatically generated with position on the image, "
                    "delete to force regeneration."))
    image = models.ImageField(_("Image"), blank=True, null=True)
    border_image = models.ImageField(
        _("Bordered image"), blank=True, null=True,
        help_text=_("Auto generated"))
    population_index = models.IntegerField(default=0)
    vehicle = models.ForeignKey(Vehicle, null=True, blank=True,
                                on_delete=models.SET_NULL)
    fp_cost = models.IntegerField(_("Feed point cost"), default=0)
    hp_cost = models.IntegerField(_("Hit point cost"), default=0)
    rumor_difficulty = models.IntegerField(default=0)
    environment_type = models.ForeignKey(EnvironmentType, blank=True,
                                         null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = _("Sector")
        verbose_name_plural = _("Sectors")

    def __str__(self):
        return self.name


def post_save_sector(sender, instance, *args, **kwargs):
    generate_sector_image(sender, instance, *args, **kwargs)
    generate_border_image(sender, instance, *args, **kwargs)


post_save.connect(post_save_sector, sender=Sector)


class LayerType(GenericType):
    class Meta:
        verbose_name = _("Type: Layer")
        verbose_name_plural = _("Types: Layer")


class Layer(models.Model):
    name = models.TextField(_("Name"), unique=True)
    description = models.TextField(null=True, blank=True)
    population_index = models.IntegerField(default=0)
    type = models.ForeignKey(LayerType, on_delete=models.CASCADE)
    color = models.CharField(_('Hexa color code'), max_length=7,
                             default='000000')
    step_erasement_factor = models.FloatField(_("Step erasement factor"),
                                              default=1)
    step_number_index = models.FloatField(_("Step number index"), default=1)
    size_index = models.IntegerField(_("Size index"), default=10)
    blocking_failure = models.BooleanField(_("Failure is blocking"),
                                           default=False)
    ability_1 = models.ForeignKey(
        Ability, null=True, blank=True,
        related_name='associated_layer_primary', on_delete=models.SET_NULL)
    ability_2 = models.ForeignKey(
        Ability, null=True, blank=True,
        related_name='associated_layer_secondary', on_delete=models.SET_NULL)

    class Meta:
        verbose_name = _("Layer")
        verbose_name_plural = _("Layers")

    def __str__(self):
        return self.name