#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015-2017  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import time

from django.contrib.auth import get_user_model
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from django.test import Client, TestCase
from django.utils.translation import ugettext_lazy as _

from rest_framework.test import APITestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from morrigan_geography import forms

from morrigan_common.admintest import adminviewstest
from morrigan_common.tests import DEFAULT_FIXTURES, \
    TestMorrigan, TestMorriganAction
from morrigan_common.models import Action
from morrigan_characters.models import ActionLog, Character
from morrigan_geography import models, views


class TestAdminViews(TestCase):
    def test_admin_views(self):
        adminviewstest(self, 'morrigan_common')

User = get_user_model()


class TestLocation(APITestCase):
    fixtures = DEFAULT_FIXTURES

    def setUp(self):
        self.user = User.objects.get(username='arkenlond')
        self.user.set_password('password')
        self.user.save()
        self.admin = User.objects.get(username='nimnae')
        self.admin.set_password('password')
        self.admin.save()

    def test_location_db(self):
        # test slug
        loaded_loc = models.Location.objects.all()[0]
        location = models.Location.objects.create(
            name=loaded_loc.name, type=loaded_loc.type)
        self.assertTrue(location.slug)
        self.assertTrue(location.slug != loaded_loc.slug)

    def test_location_view(self):
        url_marche = reverse("location", kwargs={"characters__slug": 'nimnae'})
        url_shape_nimnae = reverse("shape-simple", kwargs={"slug": 'nimnae'})
        # must be authentificated
        response = self.client.get(url_marche, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.client.get(url_shape_nimnae, format='json')
        self.assertEqual(response.status_code, 403)

        client = Client()
        client.login(username="arkenlond", password="password")
        response = client.get(url_marche, format='json')
        self.assertEqual(response.status_code, 200)
        # TODO
        # url_shape_ark = reverse("shape-simple", kwargs={"slug": 'arkenlond'})
        # response = self.client.get(url_shape_ark, format='json')
        # self.assertEqual(response.status_code, 200)
        # response = self.client.get(url_shape_nimnae, format='json')
        # self.assertEqual(response.status_code, 200)

        # can only view location you are in and character you are with
        url_taverne = reverse("location",
                              kwargs={"characters__slug": 'siltaom'})
        response = client.get(url_taverne, format='json')
        self.assertEqual(response.status_code, 403)
        url_shape_siltaom = reverse("shape-simple",
                                    kwargs={"slug": 'siltaom'})
        response = client.get(url_shape_siltaom, format='json')
        self.assertEqual(response.status_code, 403)

        # except if you are an admin
        client = Client()
        client.login(username="nimnae", password="password")
        response = client.get(url_taverne, format='json')
        self.assertEqual(response.status_code, 200)
        response = client.get(url_marche, format='json')
        self.assertEqual(response.status_code, 200)
        response = client.get(url_shape_siltaom, format='json')
        self.assertEqual(response.status_code, 200)


class TestMove(TestMorrigan):
    def test_move(self):
        character = Character.objects.get(slug='arkenlond')

        # test form
        form = forms.MoveAction(character=character)
        self.assertEqual(len(form.paths), 1)
        path = form.paths[0]
        path.difficulty = 0  # automatic success
        path.save()
        form = forms.MoveAction(character=character,
                                data={'direction': path.pk})
        self.assertTrue(form.is_valid())

        url_move = reverse('do-action', kwargs={'slug': 'arkenlond',
                                                'action': 'move'})
        client = Client()
        client.login(username="arkenlond", password="password")
        response = client.get(url_move)
        self.assertEqual(response.status_code, 200)
        response = client.post(url_move, {'direction': path.pk})
        self.assertEqual(response.status_code, 200)

        path.difficulty = 1000  # impossible success
        path.blocking_failure = True
        path.formula = models.Formula.objects.create(
            formula='- [DIFFICULTY]')
        path.save()
        response = client.post(url_move, {'direction': path.pk})
        self.assertEqual(response.status_code, 200)


"""
class TestLiveLocation(TestMorriganAction):

    def setUp(self):
        self.user = User.objects.get(username='arkenlond')
        self.user.set_password('password')
        self.user.save()
        self.selenium = webdriver.Firefox()
        self.selenium.maximize_window()
        super(TestLiveLocation, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(TestLiveLocation, self).tearDown()

    def test_location_view(self):
        self.identification("arkenlond", "password")

        location_url = reverse('character-page-location',
                               kwargs={"slug": 'arkenlond'})
        self.selenium.get('%s%s' % (self.live_server_url, location_url))
        WebDriverWait(self.selenium, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME,
                                            "character-view-detail"))
        )

        self.assertIn("Arkenlond", self.selenium.page_source)
        self.assertIn("Nimnae", self.selenium.page_source)
        self.assertIn("bs-callout-success", self.selenium.page_source)

        # items possessed by character and location are displayed
        self.assertIn("fa-suitcase", self.selenium.page_source)
        self.assertIn("i-morrigan-chest", self.selenium.page_source)
        # display description
        self.assertNotIn("body painting", self.selenium.page_source)
        self.selenium.find_element_by_xpath(
            '//ul[@class="characters"]/li/span[@class="character-view-detail"]'
            '/a/i[contains(@class, "fa-eye")]')\
            .click()
        WebDriverWait(self.selenium, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME,
                                            "character-description"))
        )
        time.sleep(2)  # wait for the JS to load...
        self.assertIn("body painting", self.selenium.page_source)

        # hide description
        self.selenium.find_element_by_xpath(
            '//ul[@class="characters"]/li/span[@class="character-view-detail"]'
            '/a/i[contains(@class, "fa-eye")]')\
            .click()
        time.sleep(2)  # wait for the JS to load...
        self.assertNotIn("body painting", self.selenium.page_source)


class TestLiveMove(StaticLiveServerTestCase):
    fixtures = DEFAULT_FIXTURES

    def setUp(self):
        self.user = User.objects.get(username='arkenlond')
        self.user.set_password('password')
        self.user.save()
        self.selenium = webdriver.Firefox()
        self.selenium.maximize_window()
        super(TestLiveMove, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(TestLiveMove, self).tearDown()

    def test_move(self):
        self.selenium.get('%s%s' % (self.live_server_url, "/login/"))
        username = self.selenium.find_element_by_id("id_username")
        username.send_keys("arkenlond")
        password = self.selenium.find_element_by_id("id_password")
        password.send_keys("password")
        self.selenium.find_element_by_xpath('//input[@value="Login"]').click()
        WebDriverWait(self.selenium, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, "location-image"))
        )

        location_url = reverse('character-page-location',
                               kwargs={"slug": 'arkenlond'})
        self.selenium.get('%s%s' % (self.live_server_url, location_url))
        WebDriverWait(self.selenium, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME,
                                            "character-view-detail"))
        )

        # check own character
        character_path = '//li[@id="character-detail-%s"]'\
            '/span[contains(@class, "character-view-detail")]'\
            '/a[contains(@class, "character-check")]'
        ark_check = self.selenium.find_element_by_xpath(
            character_path % 'arkenlond'
        )
        ark_check.click()

        self.selenium.find_element_by_xpath(
            '//ul[@class="characters"]/li/span[@class="character-view-detail"]'
            '/a/i[contains(@class, "fa-eye")]')\
            .click()
        WebDriverWait(self.selenium, 10).until(
            EC.presence_of_element_located((By.ID,
                                            "action-move"))
        )
        self.assertIn("action-move", self.selenium.page_source)
        move_icon = self.selenium.find_element_by_xpath(
            '//div[@id="action-list"]'
            '/div/ul/li[@id="action-move"]/a'
        )
        move_icon.click()
        WebDriverWait(self.selenium, 10).until(
            EC.presence_of_element_located((By.ID,
                                            "id_direction"))
        )

        self.assertIn("id_direction", self.selenium.page_source)
        # test with no direction selected
        submit = self.selenium.find_element_by_xpath(
            '//div[@class="modal-footer"]/button[contains(@class, "submit")]'
        )
        submit.click()
        WebDriverWait(self.selenium, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME,
                                            "errorlist"))
        )
        self.assertIn("errorlist", self.selenium.page_source)

        direction = self.selenium.find_element_by_xpath(
            '//label[@for="id_direction_0"]')
        direction.click()
        submit.click()
        WebDriverWait(self.selenium, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME,
                                            "action-result"))
        )
        # close window
        self.selenium.find_element_by_xpath(
            '//div[@class="modal-footer"]/button[@data-dismiss="modal"]'
        ).click()
        # now in the taverne
        self.assertIn("Taverne", self.selenium.page_source)

        # check action files
        # start
        q = ActionLog.objects.filter(character__slug='nimnae',
                                     content__name=str(_('Events')),
                                     content__from="From Arkenlond l'orc")
        self.assertEqual(q.count(), 1)
        # arrival
        q = ActionLog.objects.filter(character__slug='siltaom',
                                     content__name=str(_('Events')),
                                     content__from="From Arkenlond l'orc")
        self.assertEqual(q.count(), 1)
        # own
        q = ActionLog.objects.filter(character__slug='arkenlond',
                                     content__name=str(_('Move')),
                                     content__from="From Arkenlond l'orc")
        self.assertEqual(q.count(), 1)
"""
