#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from dal import autocomplete
from rest_framework import generics

from django.contrib.auth.models import User
from django.db.models import Q
from django.http import Http404, HttpResponse
from django.shortcuts import redirect, render, get_object_or_404
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from django.views.generic.edit import FormView

from morrigan_common.views import register_action, MorriganPermission, \
    GameMasterRequiredMixin
from morrigan_characters.views import CharacterRetrieve, CharacterPageView, \
    BaseAction, BaseActionLog

from morrigan_common.models import Player
from morrigan_characters.models import Character

from morrigan_geography import models
from morrigan_geography import serializers
from morrigan_geography import forms


class ActionLog(BaseActionLog, generics.RetrieveAPIView):
    lookup_field = 'slug'
    serializer_class = serializers.LocationActionLogSerializer
    queryset = models.Location.objects
    permission_classes = (MorriganPermission,)
    permission_type = 'full'

    def get_object(self):
        if not User.objects.filter(pk=self.request.user.pk).exclude(
                profile__game_master__id__isnull=True).count():
            raise Http404()
        obj = super(ActionLog, self).get_object()
        return obj


class CharacterLocation(CharacterRetrieve):
    """
    A location with the character point of view
    """
    serializer_class = serializers.LocationSerializer
    queryset = models.Location.objects
    lookup_field = 'characters__slug'

    def get_serializer_context(self):
        context = super(CharacterLocation, self).get_serializer_context()
        character = None
        try:
            c = Character.objects.filter(
                slug=self.kwargs[self.lookup_field]).all()[0]
            if c.player == context["request"].user.profile:
                character = c
        except (ValueError, KeyError, Character.DoesNotExist, IndexError):
            pass
        context["character"] = character
        return context


class NearLocation(generics.RetrieveAPIView):
    """
    Locations visible from the user path
    """
    serializer_class = serializers.NearLocationSerializer
    queryset = models.Location.objects
    permission_classes = (MorriganPermission,)
    permission_type = 'full'

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        character_slug = self.kwargs["character_slug"]
        filter_kwargs = {
            "slug": self.kwargs["slug"],
            "path_arrival__start__characters__slug": character_slug,
            "path_arrival__start__characters__player":
                self.request.user.profile
        }
        try:
            obj = get_object_or_404(queryset, **filter_kwargs)
        except Http404:
            filter_kwargs = {
                "slug": self.kwargs["slug"],
                "characters__slug": character_slug,
                "characters__player": self.request.user.profile
            }
            obj = get_object_or_404(queryset, **filter_kwargs)
        self.check_object_permissions(self.request, obj)
        return obj


class LiveMapPageView(CharacterLocation):
    serializer_class = serializers.LiveMapSerializer


class FullMapPageView(CharacterLocation):
    serializer_class = serializers.MapSerializer


class LocationPageView(CharacterPageView):
    template_name = "morrigan/location.html"
    current_page = _("Location")


@register_action
class MoveAction(BaseAction):
    slug = 'move'
    verbose_name = _("Move")
    form_class = forms.MoveAction
    reload = ["location"]


class TeleportCharacterForm(GameMasterRequiredMixin, FormView):
    template_name = 'morrigan/game_master/teleport_form.html'
    result_template = 'morrigan/edit_character_form_result.html'
    form_class = forms.TeleportForm

    def dispatch(self, request, *args, **kwargs):
        try:
            self.location = models.Location.objects.get(
                slug=self.kwargs['slug'])
        except (models.Location.DoesNotExist):
            return redirect('index')
        return super(TeleportCharacterForm, self).dispatch(
            request, *args, **kwargs)

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(self.location, **self.get_form_kwargs())

    def get_default_context(self):
        return {
            'location': self.location,
            "action_label": _("Teleport"),
            "action_icon": "ra ra-player-teleport",
            'url': reverse("gm-teleport", args=[self.location.slug])
        }

    def form_valid(self, form):
        channel_layer = get_channel_layer()
        room_group_name = 'chat_location_{}'.format(
            self.location.slug)
        form.save()
        async_to_sync(channel_layer.group_send)(room_group_name, {
            "type": "reload",
            "interfaces": ["location", "initiative"]
        })
        return render(self.request, self.result_template,
                      self.get_default_context())

    def get_context_data(self, **kwargs):
        context = super(TeleportCharacterForm, self).get_context_data(**kwargs)
        context.update(self.get_default_context())
        return context


class LocationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_superuser:
            return models.Location.objects.none()

        qs = models.Location.objects.all()
        if self.q:
            qs = qs.filter(Q(name__istartswith=self.q) |
                           Q(sector__name__istartswith=self.q))

        return qs.filter(available=True).order_by("name")


class NewLocationForm(GameMasterRequiredMixin, FormView):
    template_name = 'morrigan/game_master/new_gm_form.html'
    result_template = 'morrigan/edit_form_result.html'
    form_class = forms.NewLocationForm

    def dispatch(self, request, *args, **kwargs):
        try:
            self.location = models.Location.objects.get(
                slug=self.kwargs['slug'])
        except (models.Location.DoesNotExist):
            return redirect('index')
        return super(NewLocationForm, self).dispatch(
            request, *args, **kwargs)

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(self.location, **self.get_form_kwargs())

    def get_default_context(self):
        return {
            'location': self.location,
            "action_label": _("New Location"),
            "action_icon": "ra ra-capitol",
            'url': reverse("gm-new-location", args=[self.location.slug])
        }

    def form_valid(self, form):
        channel_layer = get_channel_layer()
        room_group_name = 'chat_location_{}'.format(
            self.location.slug)
        try:
            player = Player.objects.get(user=self.request.user)
        except Player.DoesNotExist:
            raise Http404()
        form.save(self.location, player)
        async_to_sync(channel_layer.group_send)(room_group_name, {
            "type": "reload",
            "interfaces": ["location", "initiative"]
        })
        return render(self.request, self.result_template,
                      self.get_default_context())

    def get_context_data(self, **kwargs):
        context = super(NewLocationForm, self).get_context_data(**kwargs)
        context.update(self.get_default_context())
        return context


class NewPathForm(NewLocationForm):
    form_class = forms.NewPathForm

    def get_default_context(self):
        return {
            'location': self.location,
            "action_label": _("New path"),
            "action_icon": "ra ra-trail",
            'url': reverse("gm-new-path", args=[self.location.slug])
        }


def live_map_set_position(request, character, x, y):
    q = Character.objects.filter(
        slug=character, player_id=request.user.id)
    if not q.count():
        raise Http404()
    character = q.all()[0]
    if not character.location:
        raise Http404()
    mp, created = models.LiveMapPoint.objects.get_or_create(
        location=character.location,
        character=character,
        defaults={"position_x": x, "position_y": y})
    if not created:
        mp.position_x, mp.position_y = x, y
        mp.save()

    channel_layer = get_channel_layer()
    room_group_name = 'chat_location_{}'.format(
        character.location.slug)
    async_to_sync(channel_layer.group_send)(room_group_name, {
        "type": "reload",
        "interfaces": ['overlivemap']
    })
    return HttpResponse("OK")