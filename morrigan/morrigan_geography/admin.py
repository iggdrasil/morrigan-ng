#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from admin import admin_site
from morrigan_common.admin import GenericAdmin
from morrigan_geography import models

admin_site.register(models.Season, GenericAdmin)
admin_site.register(models.LocationType, GenericAdmin)
admin_site.register(models.PathType, GenericAdmin)
admin_site.register(models.SectorType, GenericAdmin)
admin_site.register(models.LayerType, GenericAdmin)
admin_site.register(models.EnvironmentType)

admin_site.register(models.World)


class DescriptionLocationAdmin(admin.ModelAdmin):
    list_display = ('location', 'available', 'difficulty', 'description')
    list_editable = ('available', 'difficulty', 'description')
admin_site.register(models.DescriptionLocation, DescriptionLocationAdmin)


class LayerAdmin(admin.ModelAdmin):
    list_display = ('name', 'type')
    list_filter = ('type',)
admin_site.register(models.Layer, LayerAdmin)


class StartInline(admin.TabularInline):
    model = models.Path
    fk_name = 'start'
    extra = 1
    verbose_name = _("Path from this location")
    verbose_name_plural = _("Paths from this location")


class ArrivalInline(admin.TabularInline):
    model = models.Path
    fk_name = 'arrival'
    extra = 1
    verbose_name = _("Path toward this location")
    verbose_name_plural = _("Paths toward this location")


class AudioInline(admin.TabularInline):
    model = models.LocationAudio


class LocationAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'sector', 'description')
    list_filter = ('type', 'sector')
    list_editable = ('description',)
    inlines = [StartInline, ArrivalInline, AudioInline]
    search_fields = ("name", "sector__name")
    prepopulated_fields = {"slug": ("name",)}


admin_site.register(models.Location, LocationAdmin)


class MapLocationAdmin(admin.ModelAdmin):
    list_display = ('world', 'x', 'y', 'location')
    list_filter = ('world',)
admin_site.register(models.MapLocation, MapLocationAdmin)


class PathAdmin(admin.ModelAdmin):
    list_display = ('start', 'arrival', 'forth_is_open', 'back_is_open',
                    'is_open', 'blocking_failure', 'difficulty', 'hidden_index')
    list_editable = ('forth_is_open', 'back_is_open', 'is_open',
                     'blocking_failure', 'difficulty', 'hidden_index')
admin_site.register(models.Path, PathAdmin)


class SectorAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'description')


admin_site.register(models.Sector, SectorAdmin)
