# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2020-04-21 16:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('morrigan_geography', '0006_location_game'),
    ]

    operations = [
        migrations.AlterField(
            model_name='environmenttype',
            name='ability',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='morrigan_common.Ability'),
        ),
        migrations.AlterField(
            model_name='locationtype',
            name='actions',
            field=models.ManyToManyField(blank=True, to='morrigan_common.Action'),
        ),
        migrations.AlterField(
            model_name='sector',
            name='environment_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='morrigan_geography.EnvironmentType'),
        ),
    ]
