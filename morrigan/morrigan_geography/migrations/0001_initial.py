# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-03-30 15:58
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('morrigan_common', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DescriptionLocation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('difficulty', models.IntegerField(default=1, verbose_name='Difficulty')),
                ('description', models.TextField(verbose_name='Description')),
            ],
        ),
        migrations.CreateModel(
            name='EnvironmentType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(verbose_name='Name')),
            ],
        ),
        migrations.CreateModel(
            name='Layer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(unique=True, verbose_name='Name')),
                ('description', models.TextField(blank=True, null=True)),
                ('population_index', models.IntegerField(default=0)),
                ('color', models.CharField(default='000000', max_length=7, verbose_name='Hexa color code')),
                ('step_erasement_factor', models.FloatField(default=1, verbose_name='Step erasement factor')),
                ('step_number_index', models.FloatField(default=1, verbose_name='Step number index')),
                ('size_index', models.IntegerField(default=10, verbose_name='Size index')),
                ('blocking_failure', models.BooleanField(default=False, verbose_name='Failure is blocking')),
            ],
        ),
        migrations.CreateModel(
            name='LayerType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('description', models.TextField(blank=True, null=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('slug', models.SlugField(blank=True, null=True, unique=True, verbose_name='Slug')),
                ('description', models.TextField(blank=True, null=True)),
                ('size_index', models.IntegerField(blank=True, null=True, verbose_name='Size index')),
                ('population_index', models.IntegerField(default=0, verbose_name='Population index')),
                ('factor_step_erasure', models.FloatField(default=1, verbose_name='Factor step erasure')),
                ('factor_step_number', models.FloatField(default=0, verbose_name='Factor step number')),
                ('image', models.ImageField(blank=True, null=True, upload_to='', verbose_name='Image')),
            ],
            options={
                'verbose_name_plural': 'Locations',
                'verbose_name': 'Location',
            },
        ),
        migrations.CreateModel(
            name='LocationType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('description', models.TextField(blank=True, null=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MapLocation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('x', models.IntegerField(verbose_name='X')),
                ('y', models.IntegerField(verbose_name='Y')),
            ],
        ),
        migrations.CreateModel(
            name='Path',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('forth_description', models.TextField(blank=True, null=True, verbose_name='Forth description')),
                ('back_description', models.TextField(blank=True, null=True, verbose_name='Back description')),
                ('difficulty', models.IntegerField(default=0, verbose_name='Difficulty')),
                ('forth_is_open', models.BooleanField(default=True, verbose_name='Forth is open')),
                ('back_is_open', models.BooleanField(default=True, verbose_name='Back is open')),
                ('is_open', models.BooleanField(default=True, verbose_name='Is open')),
                ('hidden_index', models.IntegerField(default=0, verbose_name='Hidden index')),
                ('step_erasement_factor', models.FloatField(default=1, verbose_name='Step erasement factor')),
                ('step_number_index', models.FloatField(default=1, verbose_name='Step number index')),
                ('size_index', models.IntegerField(default=10, verbose_name='Size index')),
                ('blocking_failure', models.BooleanField(default=False, verbose_name='Failure is blocking')),
            ],
        ),
        migrations.CreateModel(
            name='PathFeature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='PathType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('description', models.TextField(blank=True, null=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Season',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('description', models.TextField(blank=True, null=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
                ('default', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(unique=True, verbose_name='Name')),
                ('description', models.TextField(blank=True, null=True)),
                ('population_index', models.IntegerField(default=0)),
                ('fp_cost', models.IntegerField(default=0, verbose_name='Feed point cost')),
                ('hp_cost', models.IntegerField(default=0, verbose_name='Hit point cost')),
                ('rumor_difficulty', models.IntegerField(default=0)),
            ],
            options={
                'verbose_name_plural': 'Sectors',
                'verbose_name': 'Sector',
            },
        ),
        migrations.CreateModel(
            name='SectorType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('description', models.TextField(blank=True, null=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Vehicle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tank_capacity', models.IntegerField(blank=True, null=True, verbose_name='Tank capacity')),
                ('current_tank', models.IntegerField(blank=True, null=True, verbose_name='Current tank content')),
                ('current_wear', models.IntegerField(blank=True, null=True, verbose_name='Current wear')),
                ('docked', models.BooleanField(default=False, verbose_name='Docked')),
                ('engine_on', models.BooleanField(default=False, verbose_name='Engine on')),
                ('mass', models.IntegerField(blank=True, null=True, verbose_name='Mass')),
                ('engine_flange', models.IntegerField(blank=True, null=True, verbose_name='Engine flange')),
                ('wind_flange', models.IntegerField(blank=True, null=True, verbose_name='Wind flange')),
                ('direction', models.TextField(blank=True, verbose_name='Direction')),
                ('reset_date', models.DateField(auto_now_add=True, verbose_name='Reset date')),
                ('food_stockpile', models.IntegerField(blank=True, null=True, verbose_name='Food stockpile')),
                ('current_food_stockpile', models.IntegerField(blank=True, null=True, verbose_name='Current food stockpile')),
            ],
        ),
        migrations.CreateModel(
            name='VehicleType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('description', models.TextField(blank=True, null=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
                ('power', models.IntegerField(blank=True, null=True, verbose_name='Power')),
                ('compumption', models.IntegerField(blank=True, null=True, verbose_name='Comsumption')),
                ('max_wear', models.IntegerField(blank=True, null=True, verbose_name='Max wear')),
                ('wear', models.IntegerField(blank=True, null=True, verbose_name='Wear')),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='World',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('description', models.TextField(blank=True, null=True)),
                ('level', models.IntegerField(blank=True, null=True)),
                ('image', models.ImageField(blank=True, null=True, upload_to='')),
                ('effect', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='morrigan_common.Effect')),
                ('father', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='morrigan_geography.World')),
            ],
        ),
        migrations.CreateModel(
            name='WorldType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('description', models.TextField(blank=True, null=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
        ),
    ]
