# Generated by Django 2.2 on 2021-01-05 10:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('morrigan_geography', '0014_location_available'),
    ]

    operations = [
        migrations.AddField(
            model_name='sector',
            name='border_image',
            field=models.ImageField(blank=True, help_text='Auto generated', null=True, upload_to='', verbose_name='Bordered image'),
        ),
    ]
