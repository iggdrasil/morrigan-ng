# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2019-09-29 06:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('morrigan_geography', '0004_auto_20170331_2239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='actions',
            field=models.ManyToManyField(blank=True, help_text='If set only theses actions are allowed', to='morrigan_common.Action'),
        ),
    ]
