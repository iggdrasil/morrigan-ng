#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from dal import autocomplete

from django import forms
from django.utils.text import format_lazy, slugify
from django.utils.translation import ugettext_lazy as _

from morrigan_common.models import apply_result
from morrigan_common.widgets import BootstrapRadio
from morrigan_characters.forms import FormAction

from morrigan_geography import models


class MoveAction(FormAction):
    direction = forms.ChoiceField(
        label=_("Directions"), choices=[], widget=BootstrapRadio)
    fail_msg_character_no_path = _(
        "You try to leave %(source)% %(path)s.")
    fail_msg_character = _(
        "You try to leave %(source)% %(path)s but fail.")
    success_msg_character_no_path = _(
        "You leave %(source)s. You arrive to %(destination)s.")
    success_msg_character = _(
        "You leave %(source)s %(path)s. You arrive to %(destination)s.")
    success_path = _("Nothing disturbs your movement.")
    fail_path = _("Hard is the road.")
    leave_msg_neighbor_no_path = _("%(character)s leaves %(source)s.")
    leave_msg_neighbor = _("%(character)s leaves %(source)s %(path)s.")
    arrive_msg_neighbor_no_path = _(
        "%(character)s arrive to %(destination)s.")
    arrive_msg_neighbor = _(
        "%(character)s arrive to %(destination)s %(path)s.")
    action_slug = 'move'

    def __init__(self, *args, **kwargs):
        super(MoveAction, self).__init__(*args, **kwargs)
        self.paths = self.character.location.get_paths(self.character)
        choices = []
        for path in self.paths:
            ap_cost = 0
            if path.formula:
                ap_cost = - round(
                    path.formula.evaluate(
                        self.character,
                        extra_values={'DIFFICULTY': path.difficulty},
                        targets=['ap'])['ap'])
            lbl = None
            if self.character.location == path.start:
                if path.forth_description:
                    lbl = path.forth_description
                else:
                    lbl = str(path.arrival)
            else:
                if path.back_description:
                    lbl = path.back_description
                else:
                    lbl = str(path.start)
            if ap_cost:
                lbl = _('{} - {} APs').format(lbl, ap_cost)
            choices.append((path.pk, lbl))
        self.fields['direction'].choices = choices

    def save(self, character, targets, result):

        # get the selected direction
        direction = str(self.cleaned_data['direction'])
        destination, current_path, path_desc = None, None, None
        for path in self.paths:
            if direction == str(path.pk):
                current_path = path
                if self.character.location == path.start:
                    destination = path.arrival
                    if path.forth_description:
                        path_desc = path.forth_description
                else:
                    destination = path.start
                    if path.back_description:
                        path_desc = path.back_description
                break
        if not destination:
            return _("Action not available anymore.")

        # evaluate specific formula
        success = True
        if result:
            success = result['character']['parameters']['SUCCESS'] == 1
        if success and current_path.formula:
            result = current_path.formula.evaluate(
                self.character,
                extra_values={'DIFFICULTY': current_path.difficulty})
            apply_result(self.character, result)
            success = result['main'] > 0

        # messages in action files
        dct = {"character": str(self.character),
               'destination': str(destination),
               'source': str(self.character.location),
               'path': path_desc}

        msg_character, msg_leave, msg_arrive = "", "", ""
        if success or not current_path.blocking_failure:
            if path_desc:
                msg_character = self.success_msg_character % dct
                msg_leave = self.leave_msg_neighbor % dct
                msg_arrive = self.arrive_msg_neighbor % dct
            else:
                msg_character = self.success_msg_character_no_path % dct
                msg_leave = self.leave_msg_neighbor_no_path % dct
                msg_arrive = self.arrive_msg_neighbor_no_path % dct
            if success:
                msg_character = str(format_lazy(msg_character, ' ',
                                                self.success_path))
            else:
                msg_character = str(format_lazy(msg_character, ' ',
                                                self.fail_path))
        else:
            if path_desc:
                msg_character = self.fail_msg_character % dct
            else:
                msg_character = self.fail_msg_character_no_path % dct

        character.write_log(msg_character, name=_('Move'), frm=character,
                            state='R', icon=self.icon)

        if not success and current_path.blocking_failure:
            return msg_character

        # moving
        self.move_start = self.character.location
        for neighbor in self.character.get_neighbors():
            neighbor.write_log(msg_leave, name=_('Events'), frm=character,
                               icon=self.icon)
        character.send_to_chat(msg_leave, "location", frm=character,
                               icon=self.icon)
        self.move_end = destination
        self.character.location = destination
        self.character.save()
        for neighbor in self.character.get_neighbors():
            neighbor.write_log(msg_arrive, name=_('Events'), frm=character,
                               icon=self.icon)
        character.send_to_chat(msg_arrive, "location", frm=character,
                               icon=self.icon)
        character.send_to_chat(msg_arrive, character.slug, frm=character,
                               icon=self.icon)
        return msg_character


class TeleportForm(forms.Form):
    characters = forms.ModelMultipleChoiceField(
        label=_("Characters"), queryset=None,
        widget=forms.CheckboxSelectMultiple)
    location = forms.ModelChoiceField(
        queryset=models.Location.objects.all(),
        widget=autocomplete.ModelSelect2(url='autocomplete-location')
    )

    def __init__(self, location, *args, **kwargs):
        self.location = location
        super(TeleportForm, self).__init__(*args, **kwargs)
        q = self.location.characters.order_by("current_name")
        self.fields["characters"].queryset = q
        self.fields["characters"].initial = [character for character in q.all()]

    def save(self):
        location = self.cleaned_data['location']
        for character in self.cleaned_data["characters"]:
            character.location = location
            character.save()


class NewLocationForm(forms.Form):
    name = forms.CharField(label=_("Name"), max_length=200)
    sector = forms.ChoiceField(label=_("Sector"), choices=[], required=False)
    type = forms.ChoiceField(label=_("Type"), choices=[])
    description = forms.CharField(
        label=_("Description"), required=False, widget=forms.Textarea)
    image = forms.ImageField(label=_("Image"), required=False)

    def __init__(self, location, *args, **kwargs):
        self.location = location
        super(NewLocationForm, self).__init__(*args, **kwargs)
        self.fields["type"].choices = [
            (c.pk, str(c))
            for c in models.LocationType.objects.filter(
                available=True).order_by("name")]
        self.fields["sector"].choices = [("", '--')] + [
            (c.pk, str(c)) for c in models.Sector.objects.order_by(
                "name").all()]

    def clean_name(self):
        name = self.cleaned_data['name']
        q = models.Location.objects.filter(name=name)
        if q.count():
            raise forms.ValidationError(_("Name already taken"))
        return name

    def save(self, current_location, current_player):
        name = self.cleaned_data['name']
        slug = slugify(name)
        idx = 0
        while models.Location.objects.filter(slug=slug).count():
            if not idx:
                base_slug = slug[:198]
            idx += 1
            slug = "{}-{}".format(base_slug, idx)
        location = models.Location.objects.create(
            name=name, sector_id=self.cleaned_data.get("sector", None),
            type_id=self.cleaned_data["type"], slug=slug,
            description=self.cleaned_data.get("description", None),
            image=self.cleaned_data.get("image", None),
        )
        return location


class NewPathForm(forms.Form):
    destination = forms.ModelChoiceField(
        queryset=models.Location.objects,
        widget=autocomplete.ModelSelect2(url='autocomplete-location')
    )
    forth_description = forms.CharField(
        label=_("Forth description"), required=False, widget=forms.Textarea)
    back_description = forms.CharField(
        label=_("Back description"), required=False, widget=forms.Textarea)
    round_trip = forms.BooleanField(
        label=_("Round trip"), initial=True, required=False)

    def __init__(self, location, *args, **kwargs):
        self.start = location
        super(NewPathForm, self).__init__(*args, **kwargs)
        self.fields["destination"].queryset = models.Location.objects.exclude(
            pk=self.start.pk)

    def save(self, current_location, current_player):
        arrival = self.cleaned_data["destination"]
        values = {
            "back_is_open": self.cleaned_data.get("round_trip", True),
            "forth_description": self.cleaned_data.get("forth_description", ""),
            "back_description": self.cleaned_data.get("back_description", "") or
                                self.cleaned_data.get("forth_description", "")
        }
        path, c = models.Path.objects.get_or_create(
            start=self.start, arrival=arrival,
            defaults=values)
        if not c:
            for k,v in values.items():
                setattr(path, k, v)
            path.save()
        return path
