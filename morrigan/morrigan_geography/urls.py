#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^autocomplete-location/$', views.LocationAutocomplete.as_view(),
        name='autocomplete-location'),
    url(r'^character-location/(?P<characters__slug>[\w-]+)/',
        views.CharacterLocation.as_view(), name='location'),
    url(r'^near-location/(?P<character_slug>[\w-]+)/'
        r'(?P<slug>[\w-]+)',
        views.NearLocation.as_view(), name='location'),
    url(r'^character-full-map/(?P<characters__slug>[\w-]+)/',
        views.FullMapPageView.as_view(), name='full-map'),
    url(r'^character-live-map/(?P<characters__slug>[\w-]+)/',
        views.LiveMapPageView.as_view(), name='live-map'),
    url(r'^character-live-map-position/(?P<character>[\w-]+)/'
        r'(?P<x>[\d]+)/(?P<y>[\d]+)/',
        views.live_map_set_position, name='set-live-map'),
    url(r'^gm-teleport/(?P<slug>[\w-]+)/',
        views.TeleportCharacterForm.as_view(), name='gm-teleport'),
    url(r'^gm-new-location/(?P<slug>[\w-]+)/',
        views.NewLocationForm.as_view(), name='gm-new-location'),
    url(r'^gm-new-path/(?P<slug>[\w-]+)/',
        views.NewPathForm.as_view(), name='gm-new-path'),
    url(r'^location/(?P<slug>[\w-]+)/',
        views.LocationPageView.as_view(), name='character-page-location'),
    url(r'^location-log/(?P<slug>[\w-]+)/(?P<filter>[\w-]+)?',
        views.ActionLog.as_view(), name='location-log'),
]
