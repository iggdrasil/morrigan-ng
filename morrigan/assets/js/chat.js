
var i18n_has_joined = " has joined";
var i18n_has_leaved = " has leaved";
var i18n_disconnected_reload = "Disconnected from the chat - try to reload:";
var i18n_refresh = "refresh";

var current_character = "character_slug";
var character_name = "character_name";
var current_chat = "offgame";
var current_chat_window = "#chat-player-log";
var old_message = "";
var delayed_messages = new Array();
var whole_character_list = new Array();
var character_name_list = new Array();
var character_list = new Array();
var available_chats = new Array();
var extra_actions = new Array();
var oracle = {};

const join_template = '<div class="direct-chat-infos clearfix"><span class="direct-chat-join float-left text-success">';
const leave_template = '<div class="direct-chat-infos clearfix"><span class="direct-chat-leave float-left text-danger">';
const join_leave_close = "</span></div>";
const help_template = '<div class="direct-chat-infos clearfix"><span class="direct-chat-join float-left">';
const loader_template = '<div class="direct-chat-msg chat-loader"><div class="direct-chat-infos clearfix"></div><span></span></div>';


var base_full_help = help_template;
base_full_help += "<span class='help-msg'>Commands available:</span>";
base_full_help += "<span class='help-msg'><ul>";
base_full_help += "<li>/me doing something - display a /me message</li>";
base_full_help += "<li>/roll 3d6 + 1d4 + 5 - roll dices!</li>";
base_full_help += "<li>!3d6 + 1d4 + 5 - also roll dices</li>";
base_full_help += "<li>/show document-id - display a document to everyone</li>";

var oracle_help;
var full_help;

var chat_names = ["chat-player-log", "chat-log"];


var slow_display = function(message_id){
	$(message_id).hide().fadeIn(800);
}

var display_loader = function(target_id){
	if(!$("#" + target_id + " .chat-loader").length){
		$("#" + target_id).append(loader_template);
		chat_scroll(target_id);
	}
}

var hide_loader = function(target_id){
	$("#" + target_id + " .chat-loader").fadeOut().remove();
}


var display_document = function(key){
	displayDocument(key);
	let document = $(".document-item[data-slug=" + key + "]").attr("data-target");
	let message = "<img class='chat-image' onclick='displayDocument(\"" + key + "\");' src='" + document + "'>";
	$("#chat-log").append(message);
	let wtf = $("#chat-log");
	let height = wtf[0].scrollHeight;
	wtf.scrollTop(height);
}

var focus_on_next_chat = function(){
	let chat_index;
	switch (current_chat){
		case "offgame":
			chat_index = 0;
			break;
		case "location":
			chat_index = 1;
			break;
		default:
			chat_index = available_chats.indexOf(current_chat) + 2;
	}
	chat_index += 1;
	if (chat_index + 1 > chat_names.length){
		chat_index = 0;
	}
	$("#btn-" + chat_names[chat_index]).click();
}

var document_to_display = new Object();

var delayed_document_display = function(){
	let delayed = false;
	for (var key in document_to_display){
		let displayed = false;
		if (documentList.indexOf(key) != -1){
			display_document(key);
			displayed = true;
		}
		if (documentListHidden.indexOf(key) != -1){
			display_document(key);
			displayed = true;
		}
		if (displayed || document_to_display[key] == 5){
			delete document_to_display[key];
		} else {
			document_to_display[key] += 1;
			delayed = true;
		}
	}
	if (delayed){
		setTimeout(delayed_document_display, 5000);
	}
};

var reinit_chat_vars = function(){
	whole_character_list = new Array();
	character_name_list = new Array();
	full_help = base_full_help;
    extra_actions = new Array();
}

var chat_scroll = function(target_id){
	var wtf = $("#" + target_id);
	var height = wtf[0].scrollHeight;
	wtf.scrollTop(height);
}

var create_user_chat = function(slug, lbl){
	available_chats.push(slug);
	if (chat_names.indexOf("chat-" + slug) == -1){
		chat_names.push("chat-" + slug);
	}
	var html = '<li data-character="' + slug;
	html += '" class="btn btn-secondary chat-character-btn" id="btn-chat-' + slug + '">';
	html += lbl + "&nbsp;<span class='miss'>&nbsp;</span></li>";
	$("#chan-list").append(html);
	var html = '<div class="alt-chat direct-chat-messages" id="chat-' + slug + '"></div>';
	$("#chat-window").append(html);
	$("#chan-list .chat-character-btn").click(register_chat_click);
};

var display_off_chat = function(){
	current_chat = "offgame";
	current_chat_window = "#chat-player-log";
	$("#chat-window > div").removeClass("active");
	$("#chan-list li").removeClass("active");
	$("#chat-characters").removeClass("active");
	$("#btn-chat-log").removeClass("active");
	$(".chat-character-list").addClass("active");
	$("#chat-log").removeClass("active");
	$("#warn-in-game").removeClass("active");
	$("#warn-off-game").addClass("active");

	$("#chat-players").addClass("active");
	$("#btn-chat-player-log").addClass("active");
	$("#chat-player-log").addClass("active");
	$("#btn-chat-player-log .miss").html("&nbsp;");
	if(!$('.modal.show').length) $("#chat-message-input").focus();
};


var display_location_chat = function(){
	current_chat = "location";
	current_chat_window = "#chat-log";
	$("#chat-window > div").removeClass("active");
	$("#chat-players").removeClass("active");
	$("#btn-chat-player-log").removeClass("active");
	$("#chat-player-log").removeClass("active");
	$("#chan-list li").removeClass("active");
	$("#chat-characters").addClass("active");
	$("#btn-chat-log").addClass("active");
	$(".chat-character-list").addClass("active");
	$("#chat-log").addClass("active");
	$("#btn-chat-log .miss").html("&nbsp;");
	$("#warn-off-game").removeClass("active");
	$("#warn-in-game").addClass("active");
	if(!$('.modal.show').length) $("#chat-message-input").focus();
};

var display_user_chat = function(slug){
	current_chat = slug;
	current_chat_window = "#chat-" + slug;
	$("#chat-window > div").removeClass("active");
	$("#chat-" + slug).addClass("active");
	$("#chat-characters").removeClass("active");
	$("#chan-list li").removeClass("active");
	$("#chat-log").removeClass("active");
	$("#btn-chat-" + slug).addClass("active");
	$("#btn-chat-" + slug + " .miss").html("&nbsp;");
	$("#chat-players").removeClass("active");
	$("#btn-chat-player-log").removeClass("active");
	$("#chat-player-log").removeClass("active");
	$("#warn-in-game").addClass("active");
	$("#warn-off-game").removeClass("active");
	$("#chat-message-input").focus();
};

var register_chat_click = function(){
	var chat_slug = $(this).attr("data-character");
	if (available_chats.indexOf(chat_slug) == -1){
		create_user_chat(chat_slug, $(this).html());
	}
	display_user_chat(chat_slug);
};

var display_chat_user_list = function(){
	$('#chat-characters').html("");
	var sorted_characters = character_name_list.slice().sort();
	for (idx=0; idx < sorted_characters.length; idx++){
		var lbl = sorted_characters[idx];
		var slug = whole_character_list[character_name_list.indexOf(lbl)];
		var className = "chat-character-name";
		if (slug == current_character){
			className = "chat-myself";
		} else if (character_list.indexOf(slug) == -1){
			className += " disabled";
		}
		$('#chat-characters').append(
			"<li data-character='" + slug + "' class='" +
			className + "'>" + sorted_characters[idx] + "</li>");
	}
	$("#chat-characters li.chat-character-name").click(register_chat_click);
	$("#chan-list .chat-character-btn").click(register_chat_click);
};

var display_chat_player_list = function(){
	$('#chat-players').html("");
	var sorted_characters = character_name_list.slice().sort();
	for (idx=0; idx < sorted_characters.length; idx++){
		var lbl = sorted_characters[idx];
		var slug = whole_character_list[character_name_list.indexOf(lbl)];
		var className = "chat-character-name";
		if (slug == current_character){
			className = "chat-myself";
		} else if (character_list.indexOf(slug) == -1){
			className = "";
		}
		if (className){
			$('#chat-players').append(
				"<li data-character='" + slug + "' class='" +
				className + "'>" + sorted_characters[idx] + "</li>");
		}
	}
	$("#chat-players li.chat-character-name").click(register_chat_click);
};


var tab_timestamp = 0;
const ws_prefix = (location.protocol === 'https:' ? "wss" : "ws");
var launch_chat = function(room_type, character_slug, room_name, is_game_master) {
	$("#btn-chat-player-log").click(display_off_chat);
	$("#btn-chat-log").click(display_location_chat);

	chatSocket = new WebSocket(
		ws_prefix + '://'
		+ window.location.host
		+ '/ws/chat/' + room_type + '/'
		+ room_name
		+ '/' + character_slug + '/'
	);

	chatSocket.onmessage = function(e) {
		const data = JSON.parse(e.data);
		if (data.reload){
            if (!data.target || data.target == current_character || data.target == "offgame"){
				for (idx in data.reload){
					vueEventManager.reload(data.reload[idx]);
				}
            }
		}
		if (data.type == "chat_message"){
			message = data.message;
			if (!message) return false;
			if (data.character != current_character){
				var message_list = $($.parseHTML(message)).addClass("right");
				for (idx=0; idx < message_list.length; idx++){
					var msg = message_list[idx];
					if (msg.outerHTML){
						message = msg.outerHTML;
					}
				}
			}
			let target_id;
			if (data.target && data.target != "location" && data.target != "offgame"){
				let target = data.target;
				if (data.target == current_character && data.source){
					target = data.source;
				}
				if (!$('#chat-' + target).length){
					var idx = whole_character_list.indexOf(target);
					create_user_chat(target, character_name_list[idx]);
				}
				target_id = 'chat-' + target;
			} else if (data.target && data.target == "location"){
				target_id = 'chat-log';
			} else if (data.target && data.target == "offgame"){
				target_id = 'chat-player-log';
			} else {
				return;
			}
			if(!$("#" + target_id).is(":visible")){
				$("#btn-" + target_id + " .miss").html(
					'<i class="fa fa-circle" aria-hidden="true"></i>');
			}
			if(!$("#chan-list").is(":visible")){
				$("#location-chat .card-title .miss").html(
					'<i class="fa fa-circle" aria-hidden="true"></i>');
			}
			message_id = '#' + $(message).attr("id")
			if (data.wait){
				delayed_messages.push(message);
				display_loader(target_id);
				setTimeout(function(){
					if (delayed_messages.length){
						$("#" + target_id).append(delayed_messages.pop(0));
						if (delayed_messages.length === 0){
							hide_loader(target_id);
						}
						slow_display(message_id);
						chat_scroll(target_id);
					}
				}, data.wait * 1000);
			} else {
				$("#" + target_id).append(message);
				slow_display(message_id);
			}
			chat_scroll(target_id);
		}
		if (data.type == "join"){
			if (character_list.indexOf(data.character) == -1){
				character_list.push(data.character);
				if (whole_character_list.indexOf(data.character) == -1){
					character_name_list.push(data.character_name);
					whole_character_list.push(data.character);
				}
				display_chat_user_list();
				display_chat_player_list();
				var current_msg = join_template + data.character_name + i18n_has_joined + join_leave_close;
				$('#chat-log').append(current_msg);
				$('#chat-player-log').append(current_msg);
				chat_scroll("chat-log");
				chat_scroll("chat-player-log");
				if (data.character != current_character){
					chatSocket.send(JSON.stringify({
						'type': "join",
						'character': current_character,
						'character_name': character_name,
					}));
				}
			}
		}
		if (data.type == "leave"){
			var char_idx = character_list.indexOf(data.character);
			if (char_idx != -1){
				character_list.pop(char_idx);
				character_name_list.pop(char_idx);
				var current_msg = leave_template + data.character_name + i18n_has_leaved +
					join_leave_close;
				$('#chat-log').append(current_msg);
				$('#chat-player-log').append(current_msg);
				chat_scroll("chat-log");
				chat_scroll("chat-player-log");
			}
			display_chat_user_list();
			display_chat_player_list();
		}
		if (data.type == "show"){
			var displayed = false;
			if (documentList.indexOf(data.document) != -1){
				display_document(data.document);
				displayed = true;
			}
			if (documentListHidden.indexOf(data.document) != -1){
				display_document(data.document);
				displayed = true;
			}
			if (!displayed){  // waiting for documents to be fully load
				document_to_display[data.document] = 0;
				setTimeout(delayed_document_display, 2000);
			}
		}
		if (data.type == "roll"){
			throw_dices(data.dices);
		}
		if (data.type == "get_form"){
			$("#action-" + data.action + " a").click()[0].click();
		}
		if (data.type == "reload"){
            if (data.target && (data.target != current_character && data.target != 'offgame')) return;
			setTimeout(function(){
				for (idx=0 ; idx < data.interfaces.length ; idx ++){
					vueEventManager.reload(data.interfaces[idx]);
				}
			}, data.wait * 1000);
		}
	};

	chatSocket.onopen = function(e){
		reload_chat = 0;
	}

	chatSocket.onclose = function(e) {
		for (let idx_chat in chat_names){
			$('#' + chat_names[idx_chat]).append(
				leave_template +
				i18n_disconnected_reload +
				" <a href='#' onClick='window.location.reload();'>" +
				i18n_refresh + "</a>" +
				join_leave_close
			);
			chat_scroll(chat_names[idx_chat]);
		}

		console.error('Chat socket closed unexpectedly');
		reload_chat += 1;
		if (reload_chat < 5){
			// location should have change try to reload
			vueEventManager.reload('status-div');
			vueEventManager.reload('location');
			vueEventManager.reload('initiative');
			vueEventManager.reload('document');
			vueEventManager.reload('document-hidden');
			vueEventManager.reloadList('character-desc');
			init_location_chat();
		}
	};

	if(!$('.modal.show').length) document.querySelector('#chat-message-input').focus();

	$('#chat-message-input').on( 'keydown', function( e ) {
		if (e.keyCode === 13) {  // enter, return
			e.preventDefault();
			document.querySelector('#chat-message-submit').click();
			return false;
		}
		if (e.keyCode === 27) {  // esc
			$("#full-screen").hide();
		}
		if (e.keyCode === 9) {  // tab
			var val = $('#chat-message-input').val();

			if (!val || val[0] != "/"){
				if (Date.now() - tab_timestamp < 500){
					focus_on_next_chat();
				}
				tab_timestamp = Date.now();
				return false;
			}
			if (val == "/") {
				$(current_chat_window).append(full_help);
				chat_scroll(current_chat_window.substring(1));
				return false;
			}
			if (val == "/m" || val == "/me") {
				$('#chat-message-input').val("/me ");
				return false;
			}
			if (val == "/r" || val == "/ro"
				|| val == "/rol"
				|| val == "/roll"
				) {
				$('#chat-message-input').val("/roll ");
				return false;
			}
			if (Object.keys(oracle).length){
				if (val == "/o" || val == "/or"
					|| val == "/ora"
					|| val == "/orac"
					|| val == "/oracl"
					|| val == "/oracle"
					) {
					$('#chat-message-input').val("/oracle ");
					return false;
				}
				if (val.slice(0, 8) == "/oracle ") {
					var called_oracle = val.slice(8);
					oracle_help = '<span class="help-msg">';
					var proposed_key = "";
					for (key in oracle){
						if (called_oracle.length == 0 ||
							called_oracle == key.slice(0, called_oracle.length)){
							if (proposed_key ==  ""){
								proposed_key = key;
							} else {
								proposed_key = "-1";  // multiple
							}
							oracle_help += "<i>" + key + "</i> - " + oracle[key] + "<br>";
						}
					}
					oracle_help += '</span>';
					if (proposed_key != "" && proposed_key != "-1"){
						$('#chat-message-input').val("/oracle " + proposed_key);
					} else {
						$(current_chat_window).append(oracle_help);
						chat_scroll(current_chat_window.substring(1));
					}
					return false;
				}
			}
			if (val == "/s" || val == "/sh"
				|| val == "/sho"
				|| val == "/show"
				) {
				$('#chat-message-input').val("/show ");
				return false;
			}
			if (val.slice(0, 6) == "/show ") {
				var img_slug = val.slice(6);
				var complete_candidate = new Array();
				if (img_slug.length == 0){
					complete_candidate = documentList.slice();
					if (is_game_master){
						complete_candidate = complete_candidate.concat(documentListHidden);
					}
				} else {
					for (var slug_idx=0; slug_idx < documentList.length;
						 slug_idx++){
						 if (documentList[slug_idx].slice(
								0, img_slug.length) == img_slug){
							 complete_candidate.push(documentList[slug_idx]);
						 }
					}
					if (is_game_master){
					for (var slug_idx=0; slug_idx < documentListHidden.length;
							slug_idx++){
						 if (documentListHidden[slug_idx].slice(
								0, img_slug.length) == img_slug){
							 complete_candidate.push(documentListHidden[slug_idx]);
						 }
					}
					}
				}
				if (complete_candidate.length == 1){
					$('#chat-message-input').val(
						"/show " + complete_candidate[0]);
					return false
				}
				if (complete_candidate.length > 1){
					var msg = help_template;
					for (idx=0; idx < complete_candidate.length; idx ++){
						msg += complete_candidate[idx] + " ";
					}
					msg += join_leave_close;
					$(current_chat_window).append(msg);
					chat_scroll(current_chat_window.substring(1));
					return false
				}
				var msg = help_template;
				msg += "No available image."
				msg += join_leave_close;
				$(current_chat_window).append(msg);
				chat_scroll(current_chat_window.substring(1));
				return false;
			}
			/* TODO: manage all actions with this */
			for (idx=0; idx < extra_actions.length ; idx++){
				var action = extra_actions[idx];
				var action_ok = true;
				for (idx_command=1; idx_command < val.length; idx_command++){
					if (action.length < idx_command ||
						action[idx_command - 1] != val[idx_command])
						action_ok = false;
				}
				if (action_ok){ // manage multiple answers...
					$('#chat-message-input').val("/" + action + " ");
					return false;
				}
			}
			return false;
		}
		if (e.keyCode === 38 && old_message) {  // up
			new_message = $('#chat-message-input').val();
			$('#chat-message-input').val(old_message);
		}
		if (e.keyCode === 40 && new_message) {  // up
			old_message = $('#chat-message-input').val();
			$('#chat-message-input').val(new_message);
		}
		return e.keyCode;
	});

	document.querySelector('#chat-message-submit').onclick = function(e) {
		const messageInputDom = document.querySelector('#chat-message-input');
		const message = messageInputDom.value;
		if (!message) return false;
		old_message = message;
		new_message = "";
		var target = current_chat;
		var source = "";
		if (current_chat != "location" && current_chat != "offgame"){
			target = "user-" + current_chat;
			source = current_character;
		}
		chatSocket.send(JSON.stringify({
			'message': message,
			'type': 'message',
			'target': target,
			'source': source
		}));
		messageInputDom.value = '';
		return false;
	};
	$(".direct-chat-primary .btn-plus").click(function(){
		display_location_chat();
		$(".card-title .miss").html("&nbsp;");
		$(".direct-chat-primary").removeClass("collapsed");
	});
	$(".direct-chat-primary .btn-minus").click(function(){
		$(".direct-chat-primary").addClass("collapsed");
	});
};
