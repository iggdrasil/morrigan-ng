// messages

var i18n_along = "Along with:";
var i18n_desc_added = "Description added to your history book";
var i18n_edit = "Edit";
var i18n_filter_unread = "To be read";
var i18n_filter_important = "Important";
var i18n_filter_hidden = "Hidden";
var i18n_first = "First";
var i18n_form_saved = "Form saved";
var i18n_form_save_failed = "Error while saving";
var i18n_history_book = "Write in your history book";
var i18n_image_added = "Image added to your history book";
var i18n_klass = "Class";
var i18n_last = "Last";
var i18n_map_set_position = "Set position";
var i18n_mutual_trust = "Mutual trust";
var i18n_mutual_temp_trust = "Mutual temporary trust";
var i18n_name = "Name";
var i18n_notes = "Notes";
var i18n_people = "People";
var i18n_read = "Reading";
var i18n_send = "Send";
var i18n_temp_trust = "Temporary trusted by you";
var i18n_temp_trust_you = "Temporary trusting you";
var i18n_trust = "Trusted by you";
var i18n_trust_you = "Trusting you";

// urls

var baseActionsUrl;
var baseDoActionUrl;
var baseSetLogUrl;
var formChangeAudioUrl;
var formQuickEditUrl;
var getActionReadUrl;
var getActionLogUrl;
var getActionLogIconsUrl;
var getActionLogChooserUrl;
var getCharacterSheetUrl;
var getCharactersSheetUrl;
var getDocumentUrl;
var getEditActionlog;
var getEditCharacter;
var getHiddenDocumentUrl;
var getInitiativeUrl;
var getLiveMapUrl;
var getLocationUrl;
var getMapUrl;
var getMenuUrl;
var getSelectCharacterMenuUrl;
var getShapeUrl;
var getStatusUrl;
var postNote;
var setLiveMapPositionUrl;

// registers

var documentList = [];
var documentListHidden = [];
var vueEventManager = new VueEventManager();

// global vars

var current_character;
var rp_mode;
var is_game_master;
var can_edit_actionlog;
var current_action_log = "";
var display_location;
var selected_characters = [];
var selected_items = [];
var selected_location_items = [];
var selected_own_items = [];
var selected_equipped_items = [];
var selected_non_equipped_items = [];
var selected_equipable_items = [];
var selected_non_equipable_items = [];
var selected_natural_items = [];
var selected_non_natural_items = [];

var current_audio_level = 0.5;

var action_point_short_label;
var ap_icon;
if (!ap_icon) ap_icon = 'fa fa-hand-paper-o';
var fp_icon;
if (!fp_icon) fp_icon = 'ra ra-beer';
var hp_icon;
if (!hp_icon) hp_icon = 'ra ra-hearts';
var mp_icon;
if (!mp_icon) mp_icon = 'fa fa-magic';

var date_obj = new Date();
var time_key = date_obj.getTime();

// pager

var page_by_block = 5;
var first_label;
var next_label;
var last_label;
var prev_label;
