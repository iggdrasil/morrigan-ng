var currentLocationSlug = "";
var oldLocationSlug = "";
var chatSocket;

var reload_chat = 0;

init_location_chat = function(){
	if (oldLocationSlug != currentLocationSlug){
		if (oldLocationSlug && chatSocket){
            chatSocket.send(JSON.stringify({
            	'character': current_character,
	            'character_name': character_name,
    	        'type': "leave"
            }));
		}
		var url = "/chat/location/" + currentLocationSlug + "/" + current_character + "/";
		$.ajax({
			url: url,
			success: function(content){
				$("#location-chat").html(content)
			}
		});
		oldLocationSlug = currentLocationSlug;
	}
}

md_converter = new showdown.Converter();

var update_preview_height = function(item, item_id){
	if(item.oldheight === null){
		item.oldheight= item.style.height;
	}
	else if(item.style.height != item.oldheight){
		item.oldheight = item.style.height;
		$("#" + item_id + "_preview").height(item.style.height);
	}
} ;

var register_live_md = function(item_id){
	$("#" + item_id).bind('mouseup mousemove', function(){
		update_preview_height(this, item_id)
	});
	$("#" + item_id + "_preview").height($("#" + item_id).height());
	$("#" + item_id).on('change keyup paste', function(){
		$("#" + item_id + "_preview").html(md_converter.makeHtml($(this).val()));
		$("#" + item_id + "_preview").height($("#" + item_id).height());
	})
};

var _draggging_started = 0;
var _last_mousemove_position = { x: null, y: null };
var _div_offset;
var _container_width;
var _container_height;
var _image_width;
var _image_height;
var _image_loaded = 0;

function ImageLoaded() {
	_image_width = $("#drag-image").width();
	_image_height = $("#drag-image").height();
	_container_width = $("#map-container").outerWidth();
	_container_height = $("#map-container").outerHeight();
	_div_offset = $('#map-container').offset();
	_image_loaded = 1;
}

var register_map = function(){
	$('#drag-image').on('load', function() {
		ImageLoaded();
	});
	_div_offset = $('#map-container').offset();
	_container_width = $("#map-container").outerWidth();
	_container_height = $("#map-container").outerHeight();
	$('#map-container').on('mousedown', function(event) {
		/* image should be loaded before it can be dragged */
		if(_image_loaded == 1) {
			_draggging_started = 1;

			/* save mouse position */
			_last_mouse_position = {
				x: event.pageX - _div_offset.left,
				y: event.pageY - _div_offset.top };
		}
	});

	$('#map-container').on('mouseup', function() {
		_draggging_started = 0;
	});

	$('#map-container').on('mousemove', function(event) {
		if(_image_loaded != 1){
			ImageLoaded();
		}
		if(_draggging_started == 1) {
			var current_mouse_position = {
				x: event.pageX - _div_offset.left,
				y: event.pageY - _div_offset.top
			};
			var change_x = current_mouse_position.x - _last_mouse_position.x;
			var change_y = current_mouse_position.y - _last_mouse_position.y;


			/* Save mouse position */
			_last_mouse_position = current_mouse_position;

			var img_top = parseInt($("#drag-image").css('top'), 10);
			var img_left = parseInt($("#drag-image").css('left'), 10);

			var img_top_new = img_top + change_y;
			var img_left_new = img_left + change_x;

			/* Validate top and left do not fall outside the image,
			   otherwise white space will be seen */
			if(img_top_new > 0)
				img_top_new = 0;
			if(img_left_new > 0)
				img_left_new = 0;
			if(img_top_new < (_container_height - _image_height))
				img_top_new = _container_height - _image_height;
			if(img_left_new < (_container_width - _image_width))
				img_left_new = _container_width - _image_width;

			$("#drag-image").css(
				{top: img_top_new + 'px',
				 left: img_left_new + 'px' });
		}
	});

};





