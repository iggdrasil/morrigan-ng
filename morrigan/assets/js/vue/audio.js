var load_audio = function(){
    if (!$("audio").length){
        $("#btn-music-edit").hide();
    } else {
        $("#btn-music-edit").show();
    }
    if (!$("audio[data-play=true]").length){
        $("#toggle-music").hide();
        if (!$("#toggle-music.d-none").length) {
            $("#toggle-music").addClass("d-none");
        }
    } else {
        $("#toggle-music").show();
        $("#toggle-music").removeClass("d-none");
    }

    if ($("#toggle-music .active").length){
        $(".audio-volume-container").show();
        $("audio[data-play!=true]").animate({volume: 0}, 2000, function(){
            $("audio[data-play=false]").trigger("pause");
        });
        $("audio[data-play=true]").prop("volume", current_audio_level);
        $("audio[data-play=true]").trigger("play");
    } else {
        $(".audio-volume-container").hide();
        $("audio").animate({volume: 0}, 2000, function(){
            $("audio").trigger("pause");
        });
    }
};

Vue.component("audio-item", {
	props: ["url", "type", "slug", "loop", "autoplay"],
	template: `
	  <audio :id="'audio-' + this.slug"
		:src="this.url"
		:type="this.type"
		:loop="this.loop"
		:data-play="this.autoplay"
		preload>
	  </audio>
	`
});

Vue.component("audio-player-form", {
	props: ["audiolist"],
	data: function(){
		return {
			"form_url": formChangeAudioUrl,
			"i18n_send": i18n_send,
			"get_cookie": get_cookie
		}
	},
	methods: {
		handleSubmit(e){
			e.preventDefault();
			let form = $("#audio-player-form");
			$.ajax({
			  url: this.form_url,
			  type: "POST",
			  data: new FormData(form[0]),
			  contentType: false,
			  cache: false,
			  processData:false,
			  success: function(data) {
				$("#audio-player-form-msg").html(
				  "<span class='text-success'>" +
				  i18n_form_saved
				  + "</span>");
				setTimeout(function(){
				  $("#audio-player-form-msg").html("");
				}, 4000);
			  },
			  error: function(xhr, status, err) {
				$("#audio-player-form-msg").html(
				  "<span class='text-danger'>" +
				  i18n_form_save_failed
				  + "</span>");
				setTimeout(function(){
				  $("#audio-player-form-msg").html("");
				}, 4000);
			  }
			});
		}
	},
	template: `
      <div class="audio-player-form form" id="music-edit">
        <h4>Audio</h4>
        <p id="audio-player-form-msg"></p>
        <form v-on:submit="this.handleSubmit" id="audio-player-form">
          <input type="hidden" name="csrfmiddlewaretoken"
          	:value="this.get_cookie('csrftoken')" />
          <table>
            <tbody>
				<tr v-for="audio in this.audiolist"
					:key="audio.slug">
				  <th><p>{{audio.name}}</p></th>
				  <td class="pl-2">
					<input type="hidden"
					  :name="'audio-' + audio.audiotype + '-' + audio.slug"
					  value="1" />&nbsp;
					<input :id="'audio-play-' + audio.slug"
					  class="check-with-label"
					  type="checkbox"
					  :name="'audio-play-' + audio.slug"
					  :checked="audio.autoplay" />&nbsp;
					<label :for="'audio-play-' + audio.slug">
					  <i class="fa fa-play" aria-hidden="true"></i></label>&nbsp;
					<input :id="'audio-loop-' + audio.slug"
					  class="check-with-label"
					  type="checkbox"
					  :name="'audio-loop-' + audio.slug"
					  :checked="audio.loop"/>&nbsp;
					<label :for="'audio-loop-' + audio.slug">
					  <i class="fa fa-repeat" aria-hidden="true"></i></label>&nbsp;
				  </td>
				</tr>
				<tr>
					<td>
					<button class="btn btn-primary" type="submit">
					  	{{this.i18n_send}}
					</button>
					</td>
				</tr>
            </tbody>
          </table>
        </form>
      </div>
	`
});
