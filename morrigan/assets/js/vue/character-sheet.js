
if ($("#character-sheet").length) {
	var character_sheet_vue = new Vue({
		el: '#character-sheet',
		mixins: [ajaxGetMixin],
		data: function(){
			return {
			    "gen_id": "character-sheet", 
				"url": getCharacterSheetUrl,
				"fetch_key": null,
				"i18n_edit": i18n_edit,
				"i18n_klass": i18n_klass,
				"i18n_name": i18n_name,
				"i18n_notes": i18n_notes,
				"i18n_people": i18n_people,
				"ap_icon": ap_icon,
				"fp_icon": fp_icon,
				"hp_icon": hp_icon,
				"mp_icon": mp_icon,
				"item": {
					"background": "",
					"biography": "",
					"name": "",
					"notes": "",
					"klass": "",
					"race": "",
					"hp": "",
					"ap": "",
					"fp": "",
					"mp": "",
					"max_hp": "",
					"max_ap": "",
					"max_fp": "",
					"max_mp": "",
					"sections": []
				}
			}
		},
		methods: {
			editClick(event){
				event.preventDefault();
				let url = getEditCharacter + current_character + "/";
				ajax_get(url, function(content){
					refresh_edit_modal(content);
					$('.modal-edit').modal('show');
				}, this);
			}
		},
		template: `
		<div id='character-sheet'>
		  <div id="character-sheet-detail">
			<div class="row character-detail-general">
			  <div class="col-6 col-lg-3">
				<span class="character-detail-name">
				  {{i18n_name}}
				</span>
				<span class="character-detail-value">
				  {{this.item.current_name}}
				</span>
			  </div>
			  <div class="col-6 col-lg-3">
				<span class="character-detail-name">
				  {{i18n_people}}
				</span>
				<span class="character-detail-value">
				  {{ this.item.race ? this.item.race : "-" }}
				</span>
			  </div>

			  <div class="col-6 col-lg-3">
				<span class="character-detail-name">
				  {{i18n_klass}}
				</span>
				<span class="character-detail-value">
				  {{ this.item.klass ? this.item.klass : "-" }}
				</span>
			  </div>
			  <div class="col-6 col-lg-3">
				<button class="btn btn-secondary"
						id="edit-character-btn" v-on:click="this.editClick">
				  <i class="fa fa-pencil"></i>&nbsp; {{i18n_edit}}
				</button>
			  </div>
			</div>

			<div class="row character-detail-general">
			  <div class="col-4 col-lg-2 character-hp">
				<i :class="hp_icon"></i>
				<span class="character-detail-main-attribute">
				  {{this.item.hp}}
				</span>
				<span class="character-detail-main-attribute-max">
				  {{this.item.max_hp}}
				</span>
			  </div>
			  <div class="col-4 col-lg-2 character-fp">
				<i :class="fp_icon"></i>
				<span class="character-detail-main-attribute">
				  {{this.item.fp}}
				</span>
				<span class="character-detail-main-attribute-max">
				  {{this.item.max_fp}}
				</span>
			  </div>
			  <div class="col-4 col-lg-2" v-if="this.item.mp">
				<i :class="mp_icon"></i>
				<span class="character-detail-main-attribute">
				  {{this.item.mp}}
				</span>
				<span class="character-detail-main-attribute-max">
				  {{this.item.max_mp}}
				</span>
			  </div>
			</div>
			<div class="row" v-if="this.item.background">
			  <div class="col-12">
				<div id="sheet-background" v-html="this.item.background">
				</div>
			  </div>
			</div>
			<div class="row" v-if="this.item.biography">
			  <div class="col-12">
				<div id="sheet-biography" v-html="this.item.biography">
				</div>
			  </div>
			</div>
			<div class="row">
			  <div
				v-for="section in this.item.sections"
				:class="'character-sheet-section col-lg-6 ' + section.type">
				<h4><i :class="section.icon"></i> {{section.name}}</h4>
				<table class="character-sheet-items table table-hover table-striped">
				  <tbody>
				  <tr
					v-for="node in section.items"
					:key="node.slug">
					<td class='sheet-item-icon'><i :class='node.icon ? node.icon : "fa fa-genderless"'></i>
						&nbsp;
					</td>
					<td class='sheet-item-name'>{{node.name}}</td>
					<td class='sheet-item-value'>{{node.value}}</td>
				  </tr>
				  </tbody>
				</table>
			  </div>
			</div>
			<div class="row">
			  <div class="col-12">
				<h4>{{i18n_notes}}</h4>
				<div id="sheet-notes" v-html="this.item.notes">
				</div>
			  </div>
			</div>
		  </div>
		 </div>
		`
	});
}

// game master overview

Vue.component("characters-sheet-section-row", {
	props: ["slug", "values", "name"],
	template: `
      <tr>
        <th class="ability">{{this.name}}</th>
        <td v-for="item in this.values">{{item}}</td>
      </tr>
	`
});


Vue.component('characters-sheet-section', {
	props: ["name", "slug", "type", "values", "icon"],
    template: `
      <tbody v-if="this.values">
        <tr>
          <th class="table-characteristic"
          	  :colSpan="this.values[0].values.length + 1">
          	{{this.name}}
          </th>
        </tr>
        <characters-sheet-section-row
        		  v-for="item in this.values"
                  :slug="item.slug"
                  :key="item.slug"
                  :name="item.name"
                  :values="item.values" />
      </tbody>
	`
});

if ($("#characters-sheet").length) {
	var characters_sheet_vue = new Vue({
		el: '#characters-sheet',
		mixins: [ajaxGetMixin],
		data: function(){
			return {
			    "gen_id": "characters-sheet", 
				"url": getCharactersSheetUrl,
				"i18n_edit": i18n_edit,
				"fetch_key": null,
				"item": {"sections": []}
			}
		},
		methods: {
			editClick(event){
				event.preventDefault();
				let url = getEditCharacter + current_character + "/";
				ajax_get(url, function(content){
					refresh_edit_modal(content);
					$('.modal-edit').modal('show');
				}, this);
			}
		},
		template: `
		<div id='characters-sheet'>
		  <div id="character-sheet-detail">
			<div class="row character-detail-general">
			  <div class="col-6 col-lg-3">
				<button class="btn btn-secondary"
						id="edit-character-btn"
						v-on:click="this.editClick">
				  <i class="fa fa-pencil"></i>&nbsp; {{i18n_edit}}
				</button>
			  </div>
			</div>
			<table
				v-if="item.sections.length"
				class='table table-hover table-striped table-responsive table-sm'>
			<thead>
			  <tr>
				<th></th>
				<th
					v-for="character_section in item.sections[0].items"
					class='ability-header'>
					{{character_section}}
				</th>
			  </tr>
			</thead>
			<characters-sheet-section
				v-for="section in item.sections.slice(1)"
				:key="section.key"
				:name="section.name" :slug="section.slug"
				:type="section.type" :values="section.values"
				:icon="section.icon" />
			</table>
		  </div>
	    </div>
		`
	});
}
