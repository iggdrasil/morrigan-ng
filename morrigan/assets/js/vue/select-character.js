
if($("#select-character-menu").length){
	var select_character_menu_vue = new Vue({
		el: '#select-character-menu',
		mixins: [ajaxGetMixin],
		data: function(){
			return {
			    "gen_id": "select-character-menu", 
				"url": getSelectCharacterMenuUrl,
				"fetch_key": "menu",
				"items": [
					{
					"icon": "",
					"label": "",
					"badge": "",
					"selected": false
					}
				]
			}
		},
		template: `
		<div id="select-character-menu" class="nav nav-pills">
		  <ul class="nav nav-pills" role="menu">
			  <li
			  	v-if="Array.isArray(this.items)"
			  	class='nav-item dropdown'>
				<button class="btn btn-secondary dropdown-toggle"
					type="button" id="dropdownMenuButton" data-toggle="dropdown">
				  <i :class="'nav-icon ' + this.items[0].icon"></i>
				  &nbsp;{{this.items[0].label}} {{this.items[0].badge}}
				</button>
				<div class="dropdown-menu">
      				<a class="dropdown-item"
      					v-for="item in this.items"
      					:href="item.url">
      					{{item.label}}
      				</a>
				</div>
			  </li>
			  <li
			  	v-else
			  	class='nav-item'>
				<a :href="this.items.url"
				   :class="this.items.selected ? 'active nav-link' : 'nav-link'">
					<i :class="'nav-icon ' + this.items.icon"></i>
					&nbsp;{{this.items.label}} {{this.items.badge}}</a>
			  </li>

		  </ul>
		</div>
		`
	});
}
