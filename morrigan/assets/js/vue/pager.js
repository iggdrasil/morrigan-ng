/* jshint esversion: 6 */

var TITLES = {
	first: i18n_first,
	prev: '\u00AB',
	prevSet: '...',
	nextSet: '...',
	next: '\u00BB',
	last: i18n_last
};

let range = function(r, current) {
    var res = [];
    for (let i=r[0]; i < r[1]; i++) {
        res.push(i);
    }
    if (!res.length) res.push(current);
    return res;
};


Vue.component("page", {
	props: ["class_name", "is_active", "is_disabled", "on_click", "children", "slug",
			"value"],
	template: `
		<li
			:class="(this.class_name ? this.class_name + ' ' : '') + (this.is_active ? 'active' : '') + (this.is_disabled ? ' disabled' : '')">
			<a class="page-link"
				v-on:click='this.on_click'>{{this.children}}</a>
		</li>
	`
});

Vue.component("pager", {
	props: [
		"current", "total", "visiblePages", "titles",
		"onPageChanged", "onPageSizeChanged"
	],
	data: function(){
		return {
			"BASE_SHIFT": 1,
			"TITLE_SHIFT": 1,
			"TITLES": TITLES,
			"range": range
		}
	},
	methods: {
		/* helpers */
		// Calculates "blocks" of buttons with page numbers
		calcBlocks: function () {
			let total = this.total;
		  	let blockSize  = this.visiblePages;
			let current = this.current + this.TITLE_SHIFT
			let blocks = Math.ceil(total / blockSize)
			let currBlock = Math.ceil(current / blockSize) - this.TITLE_SHIFT;
			return {
				total: blocks,
				current: currBlock,
				size: blockSize
			};
		},
		isPrevDisabled: function () {
			return this.current <= this.BASE_SHIFT;
		},
		isNextDisabled: function () {
			return this.current > (this.total - this.TITLE_SHIFT);
		},
		isPrevMoreHidden: function () {
			let blocks = this.calcBlocks();
			return (blocks.total === this.TITLE_SHIFT)
				   || (blocks.current === this.BASE_SHIFT);
		},
		isNextMoreHidden: function () {
			let blocks = this.calcBlocks();
			return (blocks.total === this.TITLE_SHIFT)
				   || (blocks.current === (blocks.total - this.TITLE_SHIFT));
		},
		visibleRange: function () {
			let blocks = this.calcBlocks();
			let start = blocks.current * blocks.size;
			let delta = this.total - start;
			let end = start + ((delta > blocks.size) ? blocks.size : delta);
			return [start + this.TITLE_SHIFT, end + this.TITLE_SHIFT];
		},
		getTitles: function (key) {
			let pTitles = this.titles || {};
			return pTitles[key] || this.TITLES[key];
		},

		/* handlers */
		handleFirstPage: function () {
			if (this.isPrevDisabled()) return;
			this.handlePageChanged(this.BASE_SHIFT);
		},
		handlePreviousPage: function () {
			if (this.isPrevDisabled()) return;
			this.handlePageChanged(this.current - this.TITLE_SHIFT);
		},
		handleNextPage: function () {
			if (this.isNextDisabled()) return;
			this.handlePageChanged(this.current + this.TITLE_SHIFT);
		},
		handleLastPage: function () {
			if (this.isNextDisabled()) return;
			this.handlePageChanged(this.total - this.TITLE_SHIFT);
		},
		//Chooses page before min of currently visible pages.
		handleMorePrevPages: function () {
			let blocks = this.calcBlocks();
			this.handlePageChanged(blocks.current * blocks.size - this.TITLE_SHIFT);
		},
		// Chooses page after max of currently visible
		handleMoreNextPages: function () {
			let blocks = this.calcBlocks();
			this.handlePageChanged((blocks.current + this.TITLE_SHIFT) * blocks.size);
		},
		handlePageChanged: function (el) {
			let handler = this.onPageChanged;
			if (handler) handler(el);
		}
	},
	template: `
		<nav>
			<ul class="pagination">
				<page class_name="page-item"
					  key="btn-first-page"
					  slug="btn-first-page"
					  :is_disabled="this.isPrevDisabled()"
					  :on_click='this.handleFirstPage'
					  :children="this.getTitles('first')" />

				<page class_name="page-item"
					  key="btn-prev-page"
					  slug="btn-prev-page"
					  :is_disabled="this.isPrevDisabled()"
					  :on_click="this.handlePreviousPage"
					  :children="this.getTitles('prev')" />
                <!--
				<page class_name="page-item"
					  key="btn-prev-more"
					  slug="btn-prev-more"
					  :is_hidden="this.isPrevMoreHidden()"
					  :on_click="this.handleMorePrevPages"
					  :children="this.getTitles('prevSet')" />
                -->
				<page v-for="el in this.range(this.visibleRange(), current)"
					class_name="page-item"
					:key="el"
					:slug="el"
					:is_active="current == el"
					:on_click="function(){handlePageChanged(el)}"
					:children="el" />
                <!--
				<page class_name="page-item"
					  key="btn-next-more"
					  slug="btn-next-more"
					  :is_hidden="this.isNextMoreHidden()"
					  :on_click="this.handleMoreNextPages"
					  :children="this.getTitles('nextSet')" />
                -->
				<page class_name="page-item"
					  key="btn-next-page"
					  slug="btn-next-page"
					  :is_disabled="this.isNextDisabled()"
					  :on_click="this.handleNextPage"
					  :children="this.getTitles('next')" />
				<page class_name="page-item"
					  key="btn-last-page"
					  slug="btn-last-page"
					  :is_disabled="this.isNextDisabled()"
					  :on_click="this.handleLastPage"
					  :children="this.getTitles('last')" />
			</ul>
		</nav>
	`
});