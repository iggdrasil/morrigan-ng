


Vue.component("live-map", {
	mixins: [ajaxGetMixin],
	data: function(){
		return {
			"url": getLiveMapUrl,
            "gen_id": "live-map",
            "fetch_key": null,
            "item": null
		}
	},
	template: `
	<div id="live-map">
	</div>
	`
});

Vue.component("simple-location", {
	mixins: [ajaxGetMixin],
	props: ["url", "extra_callback"],
	data: function(){
		return {
            "gen_id": "simple-location",
            "fetch_key": null,
            "item": {}
		}
	},
	methods: {
  		imageClick: function(){
      		if (this.item.image){
		    	displayDocument("location-image-" + this.item.slug);
	      	}
  		}
	},
	updated: function(){
		this.extra_callback();
	},
	template: `
    <div>
    	<div class='location-image'>
        	<span class='document-item'
            	:data-slug="this.item.image ? ('location-image-' + this.state.data.slug): 'location-noimage'"
	            :data-target="this.item.image">
    	        <img v-if="this.item.image" :src="this.item.image" v-on:click="this.imageClick" />
    	  	</span>
      	</div>
      	<div>
        	<h3>{{this.item.name}}</h3>
	        <p class='description'>{{this.item.description}}</p>
     	</div>
   </div>
	`
});

Vue.component("map-point", {
	props: ["slug", "x", "y", "color", "name"],
	data: function(){
		return {
			"image_border": 30,
            "text_style_shadow": {
				cursor: "pointer",
				filter: 'url(#shadow)',
				fill: 'white'
			},
			"text_style": {
				cursor: "pointer",
				fill: 'black'
			}
		};
	},
	computed: {
		"location_style": function(){
			return {
				"cursor": "pointer",
				"fill": this.color,
			};
		}
	},
	methods: {
		"clicked": function(){
			let current_slug = this.slug;
			var maps_vue = new Vue({
				el: "#temp-container",
				data: function(){
					return {
						slug: current_slug,
						current_character: current_character
					};
				},
				methods: {
					display: function(){
						$("#map-description").html($("#temp-container").html());
					}
				},
				template: `
					<div id="temp-container">
						<simple-location
							:extra_callback="this.display"
							:url="'/near-location/' + this.current_character + '/' + this.slug + '/'"/>
					</div>
				`
			});
		}
	},
	template: `
	<g>
		<circle class="locations" r="6"
			v-on:click="this.clicked"
			cursor="thiser"
			fill="black"
			:cx="this.x + this.image_border"
			:cy="this.y + this.image_border" />
		<circle :style="this.location_style" class="locations" r="5"
			:cx="this.x + this.image_border"
			:cy="this.y + this.image_border" />
		<text :style="this.text_style_shadow"
			v-on:click="this.clicked"
			:x="this.x - 15 + this.image_border"
			:y="this.y + 20 + this.image_border">
			{{this.name}}
		</text>
		<text :style="this.text_style"
			v-on:click="this.clicked"
			:x="this.x - 15 + this.image_border"
			:y="this.y + 20 + this.image_border">
			{{this.name}}
		</text>
	</g>
	`
});

Vue.component("full-map", {
	mixins: [ajaxGetMixin],
	data: function(){
		return {
			"url": getMapUrl,
            "gen_id": "full-map",
            "fetch_key": null,
            "item": {
 				"live_action": false,
	            "sector_image": "",
    	        "points": []
            }
		}
	},
	template: `
    <div id="full-map">
		<div class='row'>
			<div id='map-description' className='col-12'></div>
		</div>
		<div id="map-container" class='row'>
			<div className='col-12'>
				<svg id="svg-map"
					:style="this.item.sector_image_dimensions ? ('width:'+ this.item.sector_image_dimensions[0] + ';height:'+ this.item.sector_image_dimensions[1]) :''">
					<defs>
						<filter id="shadow" x="-20%" y="-20%" width="140%" height="140%">
							<feGaussianBlur stdDeviation="2 2" result="shadow"/>
							<feOffset dx="2" dy="2"/>
						</filter>
					</defs>
					<map-point
						v-for="point in this.item.points"
						:key="point.slug"
						:slug="point.slug"
						:name="point.name"
						:color="point.color"
						:x="point.x"
						:y="point.y" />
				</svg>
				<img id="drag-image" :src="this.item.sector_image"/>
			</div>
		</div>
	</div>
	`
});

if ($('#maps').length){
	var maps_vue = new Vue({
		el: "#maps",
		template: `
		<div id="maps">
			<full-map />
			<live-map />
		</div>
		`
	});
}
