Vue.component("menu-item", {
	props: ["slug", "badge", "icon", "label"],
	methods: {
		handleClick() {
  			change_panel(this.slug);
		}
	},
	template: `
      <li :id="'menu-' + this.slug"
      	  :class="this.badge ? 'nav-item has-badge' : 'nav-item'">
        <a href='#' v-on:click="this.handleClick"
           :class="this.selected ? 'active nav-link' : 'nav-link'">
        	<i :class="this.icon ? 'nav-icon ' + this.icon : 'nav-icon'"></i>
        	<p>
        		{{this.label}}
        		<span class='badge badge-light'>{{this.badge}}</span>
        	</p>
		</a>
      </li>
	`
});

var menu_vue = new Vue({
	el: '#menu',
	mixins: [ajaxGetMixin],
	data: function(){
		return {
            "gen_id": "menu", 
			"url": getMenuUrl,
            "fetch_key": "menu",
            "items": []
		}
	},
	template: `
	 <nav id="menu" class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column"
      	data-widget="treeview" role="menu">
		  <menu-item
		  	v-for="item in this.items"
			:slug="item.slug"
			:key="item.slug"
			:badge="item.badge"
			:icon="item.icon"
			:label="item.label"
		  />
      </ul>
     </nav>
	`
});
