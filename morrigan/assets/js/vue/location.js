/* jshint esversion: 6 */

Vue.component("character-description", {
	mixins: [ajaxGetListMixin],
	props: ["url"],
    data: function() {
        return {
            "gen_id": "character-description",
            "fetch_key": null,
            "item": {"items":[]},
			"str_is_inside": str_is_inside,
			"str_equipped": str_equipped,
			"str_natural": str_natural,
			"str_areas": str_areas,
			"str_size": str_size,
			"str_capacity": str_capacity
        };
    },
    methods: {
    	getGenKey: function(){
    		return this.url;
    	},
  		getTimeKey: function(){
  			return time_key;
  		}
    },
	template: `
    <div class="description">
    	<div v-if="this.item.image"
    		class='character-full-image'>
        	<img :src="this.item.image"/>
        </div>
        <div class='character-description'>
        	{{this.item.description}}
        </div>
        <div class='character-current-description'>
        {{this.item.current_description}}
        </div>
        <hr class='spacer' />

	  	<table v-if="this.item.items" className='character-items table-responsive'>
			<tbody>
                <tr>
                	<th colSpan='4'></th>
                	<th>{{this.str_is_inside}}</th>
                	<th>{{this.str_equipped}}</th>
                	<th>{{this.str_natural}}</th>
                	<th>{{this.str_areas}}</th>
                	<th>{{this.str_size}}</th>
                	<th>{{this.str_capacity}}</th>
                </tr>
	  			<item-full-node
	  				v-for="item in this.item.items"
	  				:name="item.current_name"
                    :key="item.id + getTimeKey()"
					:id="item.id"
					:equipped="item.equipped"
					:equipable="item.equipable"
					:areas="item.areas"
					:natural="item.natural"
					:size="item.size"
					:capacity="item.capacity"
					:is_inside="item.is_inside"
					:image="item.image"
					:description="item.description"
					:icon="item.icon" />
            </tbody>
	  	</table>
      </div>
	`
});


Vue.component("character-node", {
	props: ["name", "slug", "sex", "health", "is_trusted", "items", "owned",
			"subcharacters", "image", "race"],
	data: function(){
		return {
			detail: false, selected: false, along: false,
			i18n_history_book: i18n_history_book,
			i18n_mutual_trust: i18n_mutual_trust,
			i18n_temp_trust: i18n_temp_trust,
			i18n_trust: i18n_trust,
			i18n_trust_you: i18n_trust_you,
			i18n_temp_trust_you: i18n_temp_trust_you,
			i18n_along: i18n_along,
			getShapeUrl: getShapeUrl
		}
	},
    created: function(){
    	vueEventManager.registerListItem('character-list', this.slug, this);
    	if (current_character ===  this.slug && !this.selected) this.handleSelect(null);
	},
	beforeDestroy: function(){
    	vueEventManager.unregisterListItem('character-list', this.slug);
	},
	methods: {
  		handleShowHideDesc: function(event){
      		event.preventDefault();
      		this.detail = !this.detail;
  		},
  		handleShowHideAlong: function(event){
      		event.preventDefault();
      		this.along = !this.along;
  		},
  		getTimeKey: function(){
  			return time_key;
  		},
  		descNote: function(event){
      		event.preventDefault();
	  		ajax_post(
		  		postNote,
		  		{character_slug: this.slug},
			  	function(){
            		refresh_modal(i18n_desc_added);
            		$('.modal-action').modal('show');
        			$('.modal-action .submit').hide();
		  		}
		  	);
  		},
  		handleSelect: function(event){
	  		if (event) event.preventDefault();
      		// action based on characters not items
      		unselect_items();
      		if (this.selected){
        		idx = selected_characters.indexOf(this.slug);
        		if (idx >= 0) selected_characters.splice(idx, 1);
      		} else {
        		if (selected_characters.indexOf(this.slug) == -1) {
        			selected_characters.push(this.slug);
        		}
      		}
      		this.selected = !this.selected;

		  	let action_type = '';
			if (selected_characters.indexOf(current_character) >= 0){
		        action_type += 'self';
        		if (selected_characters.length > 1){
            		action_type += '-other';
        		}
      		} else {
        		if (selected_characters.length >= 1){
            		action_type += 'other';
        		}
      		}
      		if (selected_characters.length > 1){
        		action_type += '-multiple';
      		}
      		let url = baseActionsUrl;
      		if (action_type == '') {
          		vueEventManager.setState('action-list', {'actions':[]});
          		return;
      		}
      		url += action_type + "/";
      		ajax_get(url, function(res){
            	vueEventManager.setState('action-list', {'actions':res['actions']});
      		}, this);
  		}
	},
	template: `
	<li :class="(this.detail || this.owned) ? 'col-12': 'col-12 col-lg-6'">
        <div :class="this.health[1] + ' m-1'" :id="'character-detail-' + this.slug" >
        	<div class='character-image'>
          		<img v-if="this.image" :src="this.image" />
          	</div>
          	<span class="character-view-detail">
          		<a :class="this.detail ? '': 'disabled'"
          			v-on:click="this.handleShowHideDesc" href='#'>
          			<i class='fa fa-eye'></i>
          		</a>
          		<a :class="'character-check' + (this.selected ? '': ' disabled')"
          			v-on:click="this.handleSelect" href='#'>
          			<i class='fa fa-check'></i>
          		</a>
          		<a class="disabled" v-on:click="this.descNote" href='#'
	          		:title="this.i18n_history_book">
	          		<i class='fa fa-book'></i>
				</a>
          	</span>
          	<div>
          	<h4 class="character-name">
            	{{this.name}}
              	<span v-if="this.is_trusted == 'full-full'"
              		class="character-trust"
              		:title="this.i18n_mutual_trust">
              		<i class='fa fa-handshake-o'></i>
              	</span>
              	<span v-else-if="this.is_trusted == 'temporary-temporary'"
              		class="character-temporary-trust"
              		:title="this.i18n_temp_trust">
              		<i class='fa fa-handshake-o'></i>
              	</span>
              	<span v-else-if="this.is_trusted.startsWith('temporary')"
              		class="character-temporary-trust"
              		:title="this.i18n_temp_trust">
              		<i class='fa fa-hand-lizard-o fa-flip-horizontal'></i>
              	</span>
              	<span v-else-if="this.is_trusted.startsWith('full')"
              		class="character-trust"
              		:title="this.i18n_trust">
              		<i class='fa fa-hand-lizard-o fa-flip-horizontal'></i>
              	</span>
              	<span v-if="this.is_trusted != 'temporary-temporary' && this.is_trusted.endsWith('temporary')"
              		class="character-temporary-trust"
              		:title="this.i18n_temp_trust_you">
              		<i class='fa fa-hand-lizard-o'></i>
              	</span>
              	<span v-if="this.is_trusted != 'full-full' && this.is_trusted.endsWith('full')"
              		class="character-trust"
              		:title="this.i18n_trust_you">
              		<i class='fa fa-hand-lizard-o'></i>
              	</span>
            </h4>
            <div class="attribute">
            	<span class="character-race">
                	{{this.race}}
                	<i v-if="sex && sex.icon" :class="this.sex.icon"></i>
                	<span v-else-if="this.sex">{{this.sex.name}}</span>
              	</span> &ndash;
              	<span class="attribute character-health">
                	{{this.health[0]}}
              	</span>
            </div>
          	<ul class='items'>
            	<item-node v-for="item in this.items"
            		:name="item.current_name"
                    :key="item.id + getTimeKey()"
					:id="item.id"
					:equipped="item.equipped"
					:equipable="item.equipable"
					:natural="item.natural"
					:character="this.slug"
					:icon="item.icon" />
          	</ul>
          	<hr class='spacer'/>
        	</div>
          	<character-description
          		v-if="this.detail"
          		:url="this.getShapeUrl + this.slug + '/'" />
			<div v-if="this.subcharacters.length"
				class='along'>
        		<a :class="this.along ? '' : 'disabled'"
        			v-on:click="this.handleShowHideAlong" href='#'>
        			<i class='fa fa-eye'></i>
        		</a> &nbsp;{{this.i18n_along}}
        		<span v-for='character in this.subcharacters'>
        			{{character.name}}
        			<span v-if="character.race"> ({{character.race}})</span>
        		</span>
    		</div>
          	<ul :class="'sub-characters' + (this.along ? '' : ' disabled')">
	          	<character-node
	          		v-for="character in this.subcharacters"
	          		:name="character.name"
					:slug="character.shape_slug"
					:key="character.shape_slug"
					:sex="character.sex"
					:health="character.health"
					:is_trusted="character.is_trusted || ''"
					:items="character.items"
					:owned="true"
					:subcharacters="character.subcharacters"
					:image="character.avatar"
					:race="character.race" />
          	</ul>
        </div>
	</li>
	`
});


Vue.component("item-full-node", {
	props: ["name", "id", "equipped", "equipable", "areas", "natural", "size",
			"capacity", "is_inside", "image", "description", "icon"],
	data: function(){
		return {
			"selected": false
		}
	},
	methods: {
  		getTimeKey: function(){
  			return time_key;
  		},
		handleSelect: function(event){
			event.preventDefault();
		}
	},
	template: `
        <tr>
        	<td class='item-image'>
          		<img v-if="this.image" :src="this.image"/>
		  	</td>
          	<td class='item-icon'>
            	<ul class='items'>
                	<item-node
                		:name="this.name"
                        :key="this.id + '-' + getTimeKey()"
                        :id="this.id"
                        :equipped="this.equipped"
                        :equipable="this.equipable"
                        :natural="this.natural"
                        :size="this.size"
                        :capacity="this.capacity"
                        :own="true"
                        :icon="this.icon" />
            	</ul>
          	</td>
          	<td class='item-name'>{{this.name}}</td>
          	<td class='item-description'>{{this.description}}</td>
          	<td class='item-inside'>{{this.is_inside}}</td>
          	<td class='item-equipped'>
          		<i v-if="this.equipped" class='fa fa-check-circle'></i>
          		<i v-else class='fa fa-times-circle'></i>
          	</td>
          	<td class='item-natural'>
          		<i v-if="this.natural" class='fa fa-check-circle'></i>
          		<i v-else class='fa fa-times-circle'></i>
          	<td class='item-areas'>{{this.areas}}</td>
          	<td class='item-size'>{{this.size}}</td>
          	<td class='item-capacity'>{{this.capacity}}</td>
        </tr>
	`
});

Vue.component("item-node", {
	props: ["name", "id", "equipable", "natural", "location", "icon", "own"],
	data: function(){
		return {
			"selected": false
		}
	},
    created: function(){
    	vueEventManager.registerListItem('item-list', this.id, this);
	},
	beforeDestroy: function(){
    	vueEventManager.unregisterListItem('item-list', this.id);
	},
	methods: {
		handleSelect: function(event){
			event.preventDefault();
			// action based on items not characters
			unselect_characters();
			if (this.selected){	// will be unselect
				var idx = selected_items.indexOf(this.id);
				if (idx >= 0) selected_items.splice(idx, 1);
				if (this.location){
					idx = selected_location_items.indexOf(this.id);
					if (idx >= 0) selected_location_items.splice(idx, 1);
				}
				else if (this.character == current_character ||
						 this.own){
					idx = selected_own_items.indexOf(this.id);
					if (idx >= 0) selected_own_items.splice(idx, 1);
				}
				if (this.equipped){
					idx = selected_equipped_items.indexOf(this.id);
					if (idx >= 0) selected_equipped_items.splice(idx, 1);
				} else {
					idx = selected_non_equipped_items.indexOf(this.id);
					if (idx >= 0) selected_non_equipped_items.splice(idx, 1);
				}
				if (this.natural){
					idx = selected_natural_items.indexOf(this.id);
					if (idx >= 0) selected_natural_items.splice(idx, 1);
				} else {
					idx = selected_non_natural_items.indexOf(this.id);
					if (idx >= 0) selected_non_natural_items.splice(idx, 1);
				}
				if (this.equipable){
					idx = selected_equipable_items.indexOf(this.id);
					if (idx >= 0) selected_equipable_items.splice(idx, 1);
				} else {
					idx = selected_non_equipable_items.indexOf(this.id);
					if (idx >= 0) selected_non_equipable_items.splice(idx, 1);
				}
			} else if (selected_items.indexOf(this.id) == -1) {
				selected_items.push(this.id);
				if (this.location){
					selected_location_items.push(this.id);
				}
				else if (this.character == current_character || this.own){
					selected_own_items.push(this.id);
				}
				if (this.equipped){
					selected_equipped_items.push(this.id);
				}
				else {
					selected_non_equipped_items.push(this.id);
				}
				if (this.natural){
					selected_natural_items.push(this.id);
				}
				else {
					selected_non_natural_items.push(this.id);
				}
				if (this.equipable){
					selected_equipable_items.push(this.id);
				}
				else {
					selected_non_equipable_items.push(this.id);
				}
			}
			this.selected = ! this.selected;

			var action_type = '';
			if (selected_own_items.length > 0){
				action_type += 'ownitem';
			}
			if (selected_location_items.length > 0){
				if(action_type != '') action_type += '-';
				action_type += 'locationitem';
			}
			if (selected_items.length > (selected_own_items.length +
								     	 selected_location_items.length)){
				if(action_type != '') action_type += '-';
				action_type += 'otheritem';
			}
			if (selected_equipped_items.length > 0
				&& selected_non_equipable_items.length <= 0 ){
				action_type += '-equippeditem';
			} else if (selected_non_equipped_items.length > 0
				&& selected_equipped_items.length <= 0 ){
				action_type += '-nonequippeditem';
			}
			if (selected_equipable_items.length > 0
				&& selected_non_equipable_items.length <= 0 ){
				action_type += '-equipableitem';
			} else if (selected_non_equipable_items.length > 0
				&& selected_equipable_items.length <= 0 ){
				action_type += '-nonequipableitem';
			}
			if (selected_natural_items.length > 0
				&& selected_non_natural_items.length <= 0 ){
				action_type += '-naturalitem';
			} else if (selected_non_natural_items.length > 0
				&& selected_natural_items.length <= 0 ){
				action_type += '-nonnaturalitem';
			}
			if (selected_items.length > 1){
				action_type += '-multipleitem';
			}
			let url = baseActionsUrl;
			if (action_type == '') {
				vueEventManager.setState('action-list', {'actions':[]});
				return;
			}
			url += action_type + "/";
			ajax_get(url, function(res){
				vueEventManager.setState('action-list', {'actions':res['actions']});
			}, this);
		}
	},
	template: `
        <li data-toggle="tooltip" data-placement="bottom"
        	:title="this.name">
        	<span>
          		<a :class="'item-check' + (this.selected ? '' : ' disabled')"
          			v-on:click="this.handleSelect" href='#'>
          			<i :class="this.icon"></i>
          		</a>
          	</span>
        </li>
	`
});

Vue.component("location", {
	mixins: [ajaxGetMixin],
    data: function() {
        return {
            "gen_id": "location", 
            "url": getLocationUrl,
            "fetch_key": null,
            "display_location": display_location,
            "item": {"items": [], "characters": []},
            "i18n_history_book": i18n_history_book
        };
    },
    created: function() {
        if (!display_location) display_self_actions();
    },
    updated: function() {
        if (!this.item.slug) return;
        currentLocationSlug = this.item.slug;
        init_location_chat();
        // #todo vuejsifier
        load_audio();
        // #todo vuejsifier le volet complet
        if (this.item.is_game_master && this.item.audios.length) {
            let audios = this.item.audios
            var audio_player_form_vue = new Vue({
                el: '#music-edit',
                data: function(){
                    return {audios: audios};
                },
                template: "<audio-player-form :audiolist='this.audios' />"
            });
        }
    },
    methods: {
        descNote: function(){
            var description = "### " + this.item.name;
            if (this.item.description) description += "\n" + this.item.description;
            ajax_post(
                postNote,
                {description: description},
                function(){
                    refresh_modal(i18n_desc_added);
                    $('.modal-action').modal('show');
                    $('.modal-action .submit').hide();
            });
        },
        displayImage: function(slug){
            displayDocument("location-image-" + slug);
        },
        imageClick: function(){
            if (this.item.image){
                this.displayImage(this.item.slug);
            }
        },
        imageNote: function(image){
            ajax_post(
                postNote,
                {image: image},
                function(){
                    refresh_modal(i18n_image_added);
                    $('.modal-action').modal('show');
                    $('.modal-action .submit').hide();
                }
            );
            
        },
        imageNoteClick: function(){
            if (this.item.image){
                this.imageNote(this.item.image);
            }
        },
        parentImageNote: function(image){
            this.imageNote(image);
        }
    },
    template: `
    <div id='location'>
		<audio-item
			v-for="audio in this.item.audios"
			:key="audio.slug"
			:slug="audio.slug"
			:url="audio.url"
			:type="audio.type"
			:loop="audio.loop"
			:autoplay="audio.autoplay"
			/>
        <div v-if="this.display_location">
            <div class='location-image' v-if="this.display_location">
                <span class='document-item'
                    :data-slug="this.item.image ? 'location-image-' + this.item.slug : 'location-noimage'"
                    :data-target='this.item.image'>
                    <img v-if="this.item.image"
                        src="this.item.image" v-on:click="this.imageClick" />
                    <a v-if="this.item.image" href='#' class='image-note'
                        v-on:click="this.imageNote"
                        :title="this.i18n_history_book">
                        <i class="fa fa-book"></i>
                    </a>
                </span>
                <ul class='parent-images'>
                    <li class='parent-location-image document-item'
                        v-for="(image, index) in this.item.parent_images"
                        :key="'location-image-' + index"
                        :data-slug="'location-image-' + index"
                        :data-target="image">
                        <img :src="image" v-on:click="displayImage(index)" />
                        <a href='#' class='image-note' v-on:click="parentImageNote(image)"
                            :title="i18n_history_book">
                            <i class="fa fa-book"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <h3>{{this.item.name}}</h3>
			  <ul class='items'>
			  	<item-node
			  		v-for="item in this.item.items"
			  		:name="item.current_name"
                    :key="'location-item-' + item.id"
                    :id="item.id"
                    :equipable="item.equipable"
                    :natural="item.natural"
                    :location="item.slug"
                    :icon="item.icon" />
              </ul>
            <p class='description' v-if="this.item.description">
                {{this.item.description}}
            </p>
            <a href='#' class='description-note'
                v-on:click="this.descNote"
                :title="this.i18n_history_book">
                <i class="fa fa-book"></i>
            </a>
            <hr class='spacer'/>
            <ul class='characters row'>
            	<character-node
            		v-for="character in this.item.characters"
	        		:name="character.name"
				   	:slug="character.shape_slug"
				   	:key="character.shape_slug"
				   	:sex="character.sex"
					:health="character.health"
					:is_trusted="character.is_trusted"
					:items="character.items"
					:owned="false"
					:subcharacters="character.subcharacters"
					:image="character.avatar"
					:race="character.race" />
            </ul>
        </div>
    </div>
    `
});

if ($("#location").length){
    var location_vue = new Vue({
        el: '#location',
        template: "<location />"
    });
}