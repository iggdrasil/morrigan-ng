Vue.component('nested-document-node', {
    props: ["document"],
    created: function(){
		if (documentList.indexOf(this.document.slug) == -1){
			documentList.push(this.document.slug);
		}
	},
	methods: {
		onClick(){
			display_document(this.document.slug);
		}
	},
    template: `
    <a class="dropdown-item document-item"
      :data-target="document.image"
      :data-slug="document.slug"
      v-on:click='this.onClick'
      href="#">{{document.name}}</a>
	`
});

Vue.component('document-node', {
	props: ["document"],
    created: function(){
		if (documentList.indexOf(this.document.slug) == -1){
			documentList.push(this.document.slug);
		}
	},
	methods: {
		onClick(){
			display_document(this.document.slug);
		}
	},
    template: `
      <div
      	v-if="document.items.length"
      	class="btn-group" role="group">
        <button
        	:id='"document-" + document.slug'
          	type="button"
          	class="btn btn-secondary dropdown-toggle"
          	data-toggle="dropdown" aria-haspopup="true"
          	aria-expanded="false">{{document.name}}</button>
        <div class="dropdown-menu"
          :aria-labelledby="'document-' + document.slug">
          <nested-document-node
          	v-for="item in document.items"
          	:document="item"
          	:key="item.id"/>
        </div>
      </div>

      <button
      		v-else
      		type="button" class="btn btn-secondary document-item"
    		:data-target='document.image'
    		:data-slug='document.slug'
          	v-on:click='this.onClick'
          	href="#">{{document.name}}</button>
	`
});


var documents_vue = new Vue({
	el: '#document-selector',
	mixins: [ajaxGetMixin],
	data: function(){
		return {
            "gen_id": "document-selector", 
			"url": getDocumentUrl,
            "fetch_key": "documents",
            "items": []
		}
	},
	template: `
	<div id="document-selector">
	<div class="btn-group">
		<document-node
			v-for="item in items"
			:document="item"
			:key="item.id">
		</document-node>
	</div>
	</div>
	`
});


Vue.component('hidden-document-node', {
	props: ["document"],
    created: function(){
		if (documentListHidden.indexOf(this.document.slug) == -1){
			documentListHidden.push(this.document.slug);
		}
	},
    template: `
    <input
    	class="document-item"
    	type="hidden"
    	:name='"document-" + document.slug'
    	:data-target='document.image'
    	:data-slug='document.slug'
    	/>`
});

var documents_hidden_vue = new Vue({
	el: '#documents-hidden',
	mixins: [ajaxGetMixin],
	data: function(){
		return {
            "gen_id": "documents-hidden", 
			"url": getHiddenDocumentUrl,
            "fetch_key": "documents",
            "items": []
		}
	},
	template: `
	<div id="documents-hidden">
		<hidden-document-node
			v-for="item in items"
			:document="item"
			:key="item.id">
		</hidden-document-node>
    </div>
	`
});
