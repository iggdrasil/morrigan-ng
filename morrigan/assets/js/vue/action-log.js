/* jshint esversion: 6 */


Vue.component("action-log-icon", {
	props: ["action_log", "icon"],
  	data: function() {
    	return {
            "active": false
        }
    },
    methods: {
        handleClick(event){
            event.preventDefault();
            this.active = ! this.active;
            if (this.active){
  		        this.action_log.handleFilter(
                    "", "--icon-" + this.icon + "--");
            } else {
  		        this.action_log.handleFilter(
  			       "--icon-" + this.icon + "--", "");
            }
        }
  	},
    template: `
        <li v-on:click="this.handleClick">
            <a :class="this.active ? '': 'disabled'" href="#">
                  <i :class="this.icon" />
              </a>
        </li>
    `
});


Vue.component("action-log-icons", {
	mixins: [ajaxGetMixin],
	props: ["url", "parent"],
  	data: function() {
    	return {
            "gen_id": "action-log", 
            "fetch_key": "icons",
            "get_args": null,
            "items": []
        }
    },
    template: `
    <ul class='action-logs-icons filter-logs'>
        <action-log-icon
            v-for="icon in this.items"
            :key="icon"
            :icon="icon"
            :action_log="parent" />
    </ul>
    `
});

Vue.component("action-log-node", {
    props: ["id", "refreshContent", "content", "state", "icon", "human_date"],
  	data: function() {
    	return {
            "i18n_edit": i18n_edit,
            "i18n_filter_unread": i18n_filter_unread,
            "i18n_filter_hidden": i18n_filter_hidden,
            "i18n_filter_important": i18n_filter_important,
			"log_classes": {
			    "U": "bs-callout-success",
			    "I": "bs-callout-warning",
			    "H": "bs-callout-hide",
			    "default": "bs-callout-default"
			 },
			 "current_action_log": current_action_log,
			 "can_edit_actionlog": can_edit_actionlog,
			 "is_game_master": is_game_master
		};
  	},
  	methods: {
        handleSetLog: function(state, event){
            event.preventDefault();
            let url = baseSetLogUrl + this.id + "/";
            url += state + "/";
            ajax_get(
                url,
                function(){
                    this.refreshContent();
                }, this);
        },
        readClick: function(event){
            this.handleSetLog(this.state == 'U' ? 'R': 'U', event);
        },
        importantClick: function(event){
            this.handleSetLog(this.state == 'I' ? 'R': 'I', event);
        },
        hideClick: function(event){
            this.handleSetLog(this.state == 'H' ? 'R': 'H', event);
        },
        editClick: function(event){
            event.preventDefault();
            let url = getEditActionlog + this.id + "/";
            ajax_get(
                url,
                function(content){
                    refresh_edit_modal(content);
                    $('.modal-edit').modal('show');
                }, this);
        }
  	},
    template: `
      <li
        :class="'bs-callout ' + (this.state in this.log_classes ? this.log_classes[this.state] : this.log_classes['default'])">
        <ul class='log-actions'
            v-if="!this.is_game_master || ('character-' + this.current_character) == this.current_action_log">
            <li v-if="this.can_edit_actionlog">
                <a :title="this.i18n_edit"
                    class="disabled"
  		            v-on:click="this.editClick" href='#'>
  		        <i class='fa fa-pencil'></i></a></li>
            <li><a :title="this.i18n_filter_unread"
                    :class="this.state == 'U' ? '' : 'disabled'"
                    v-on:click="this.readClick" href='#'>
                <i class='fa fa-check'></i></a></li>
            <li><a :title="this.i18n_filter_important"
                    :class="this.state == 'I' ? '' : 'disabled'"
                    v-on:click="this.importantClick" href='#'>
                <i class='fa fa-exclamation-triangle'></i></a></li>
            <li><a :title="this.i18n_filter_hidden"
                    :class="this.state == 'H' ? '' : 'disabled'"
                    v-on:click="this.hideClick" href='#'>
                <i class='fa fa-ban'></i></a></li>
        </ul>
        <div class='log-image'>
            <img v-if="this.content.image" :src="this.content.image" />
            <i v-if="this.icon" :class="this.icon"></i>
        </div>
        <div class="log-header">
            <span v-if="this.content.from" class="log-from">{{this.content.from}}</span>
            <span v-if="this.content.to" class="log-to">{{this.content.to}}</span>
            <span v-if="this.content.name" class="log-name">{{this.content.name}}</span>
            <span v-if="this.human_date" class="log-date">{{this.human_date}}</span>
        </div>
        <div class="log-content"
            v-html="this.content.description"></div>
      </li>
  	`
});

Vue.component("action-log", {
	mixins: [ajaxGetMixin],
  	data: function() {
    	return {
            "gen_id": "action-log", 
            "fetch_key": "",
            "get_args": null,
            "get_handler": function(){
            	// force refresh of page
            	if (this.get_args && this.get_args.page){
            		this.current_page = this.get_args.page;
            	} else {
            		this.current_page = 1;
            	}
            },
			"get_url": function() {return getActionLogUrl + current_action_log},
            "i18n_read": i18n_read,
            "i18n_filter_unread": i18n_filter_unread,
            "i18n_filter_hidden": i18n_filter_hidden,
            "i18n_filter_important": i18n_filter_important,
            "getActionLogIconsUrl": getActionLogIconsUrl,
            "current_page": null,
            "page_by_block": page_by_block,
            "pager_titles": {
				'first': first_label,
				'prev': prev_label,
				'prevSet': '<<<',
				'nextSet': '>>>',
				'next': next_label,
				'last': last_label
			},
			"filters": [],
			"item": {}
		};
  	},
  	methods: {
  		handleRead(e){
      		e.preventDefault();
	      	let url = getActionReadUrl + current_character + "/";
      		ajax_get(url, function(content){
	        	refresh_edit_modal(content);
          		$('.modal-edit .modal-footer').hide();
          		$('.modal-edit').modal('show');
		    }, this);
  		},
  		handlePageChanged(page){
    		if (this.get_args){
    			this.get_args["page"] = page;
    		} else {
    			this.get_args = {"page": page};
    		}
            this.refreshContent();
  		},
  		isFilterUnread(){
  			return this.filters.indexOf('U') != -1;
  		},
  		isFilterImportant(){
  			return this.filters.indexOf('I') != -1;
  		},
  		isFilterHidden(){
  			return this.filters.indexOf('H') != -1;
  		},
  		handleFilter: function(current_state, new_state){
			if (this.filters.indexOf(current_state) != -1){
				this.filters.pop(this.filters.indexOf(current_state));
			}
			if (new_state && this.filters.indexOf(new_state) == -1){
				this.filters.push(new_state);
			}
			var filter_arg = '';
			for (let k in this.filters) filter_arg += this.filters[k];

    		if (this.get_args){
    			this.get_args["filters"] = filter_arg;
    		} else {
    			this.get_args = {"filters": filter_arg};
    		}
            this.refreshContent();
		},
  		filterUnread(e){
    		e.preventDefault();
    		if (this.isFilterUnread()){
    			this.handleFilter("U", "");
    		} else {
    			this.handleFilter("", "U");
    		}
  		},
  		filterImportant(e){
    		e.preventDefault();
    		if (this.isFilterImportant()){
    			this.handleFilter("I", "");
    		} else {
    			this.handleFilter("", "I");
    		}
  		},
  		filterHidden(e){
    		e.preventDefault();
    		if (this.isFilterHidden()){
    			this.handleFilter("H", "");
    		} else {
    			this.handleFilter("", "H");
    		}
  		},
  	},
  	template: `
    <div id="action-log">
    	<div>
			<button class="btn btn-secondary m-2" id="read-book-btn"
				v-on:click="this.handleRead">
		  		<i class="fa fa-book"></i>&nbsp; {{this.i18n_read}}
			</button>
      	</div>
	  	<div>
            <pager
                :key="this.current_page"
                :current="this.current_page"
                :total="this.item.number_of_pages"
                :visiblePages="this.page_by_block"
                :onPageChanged="this.handlePageChanged"
                :titles="this.pager_titles" />
	    </div>
	    <hr class='spacer'/>
        <div>
		    <ul class="filter-logs">
				<li>
					<a :title="this.i18n_filter_unread"
					   :class="this.isFilterUnread() ? '': 'disabled'"
					   v-on:click="this.filterUnread" href='#'>
						<i class='fa fa-check'></i>
					</a>
				</li>
				<li>
					<a :title="this.i18n_filter_important"
					   :class="this.isFilterImportant() ? '': 'disabled'"
					   v-on:click="this.filterImportant" href='#'>
						<i class='fa fa-exclamation-triangle'></i>
					</a>
				</li>
				<li>
					<a :title="this.i18n_filter_hidden"
					   :class="this.isFilterHidden() ? '': 'disabled'"
					   v-on:click="this.filterHidden" href='#'>
						<i class='fa fa-ban'></i>
					</a>
				</li>
		    </ul>
		    <!--
		    <action-log-icons
                :parent="this"
                :url="this.getActionLogIconsUrl" />
            -->
	  </div>
      <ul class="logs">
          <action-log-node
            v-for="log in this.item.logs"
            :id="log.id"
            :key="log.id"
            :state="log.state"
            :content="log.content"
            :icon="log.icon"
            :human_date="log.human_date"
            :refreshContent="refreshContent"
            >
          </action-log-node>
      </ul>
     </div>
  	`
});

Vue.component("action-log-chooser-node", {
	props: ["name",  "active", "slug"],
	methods: {
  		handleClick: function(){
    		current_action_log = this.slug;
    		vueEventManager.setState('action-log-chooser', {'active': this.slug});
    		vueEventManager.reload('action-log');
	  	}
	},
	template: `
	<a  v-on:click="this.handleClick" :class="this.active ? 'active': ''" href="#"
		class="dropdown-item">
      	{{this.name}}
	</a>
	`
});


Vue.component("action-log-chooser", {
	mixins: [ajaxGetMixin],
  	data: function() {
    	return {
            "gen_id": "action-log-chooser",
            "fetch_key": "action_logs",
            "get_args": null,
            "items": [],
            "active": "character-" + current_character,
			"url": getActionLogChooserUrl
		}
	},
	template: `
	<div id="action-log-selector">
		<div class='action-logs'>
			<div role="group" class="btn-group">
				<button id="log-chooser-btn" type="button" data-toggle="dropdown"
					aria-haspopup="true" aria-expanded="false"
					class="m-2 btn btn-secondary dropdown-toggle">
					<span v-for="action in this.items"
						v-if="action.slug == active">
						{{action.name}}
					</span>
				</button>
				<div aria-labelledby="log-chooser-btn" class="dropdown-menu">

				<action-log-chooser-node
					v-for="action in this.items"
					:name="action.name"
					:active="action.slug == active"
					:slug="action.slug"
					:key="action.slug" />
				</div>
			</div>
		</div>
    </div>
	`
});


if ($('#action-log-container').length){
	var action_log_vue = new Vue({
		el: "#action-log-container",
		data: function() {
			return {
				"is_game_master": is_game_master
			}
		},
		template: `
		<div id="action-log-container">
			<action-log-chooser v-if='this.is_game_master' />
			<action-log />
		</div>
		`
	});
}
