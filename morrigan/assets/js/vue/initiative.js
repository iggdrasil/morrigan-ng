
Vue.component("initiative", {
	mixins: [ajaxGetMixin],
    data: function() {
        return {
            "gen_id": "initiative",
            "url": getInitiativeUrl,
            "fetch_key": null,
            "get_cookie": get_cookie,
            "i18n_edit": i18n_edit,
            "item": {
            	"initiatives": [],
            	"editable_fields": []
            }
        };
    },
    methods: {
	  	"handleSubmit": function(e) {
			e.preventDefault();
			let form = $("#quick-edit-form");
			$.ajax({
		  		url: formQuickEditUrl,
		  		type: "POST",
		  		data: new FormData(form[0]),
		  		contentType: false,
		  		cache: false,
		  		processData: false,
		  		success: function(data) {
					$("#quick-form-msg").html(
			  			"<div class='text-success pb-2'>" +
			  			i18n_form_saved
			  			+ "</div>");
					setTimeout(function(){
						$("#quick-form-msg").html("");
					}, 4000);
		  		},
		  		error: function(xhr, status, err) {
					$("#quick-form-msg").html(
			  			"<div class='text-danger pb-2'>" +
				  		i18n_form_save_failed
				  		+ "</div>");
					setTimeout(function(){
					  $("#quick-form-msg").html("");
					}, 4000);
		  		}
			});
	 	}
    },
	template: `
    <nav aria-label="breadcrumb"
    	:class="'initiative' + (this.item.initiatives.length ? '': ' d-none') + (this.item.editable_fields.length ? '': ' no-form')">
       	<h4>Initiative</h4>
        <ol class="breadcrumb">
  			<li v-for="init in this.item.initiatives"
  				:key="init.slug" class='breadcrumb-item'>
	    		<div :class="'btn' + (init.myself ? ' myself': '') + (init.is_current_init? ' btn-primary': '')">
	    			{{init.name}}&nbsp;
				    <span :class="'badge ' + (init.is_current_init? 'badge-light': 'badge-secondary')">
				    	{{init.value}}
				    </span>
    			</div>
  			</li>
        </ol>

        <form
        	v-if="this.item.editable_fields.length"
       	 	v-on:submit="this.handleSubmit"
        	class='row' id='quick-edit-form'>
          	<input type="hidden" name="csrfmiddlewaretoken"
          		:value="this.get_cookie('csrftoken')" />
          	<div
          		v-for="item in this.item.editable_fields"
          		class='col-3 p-2' :key="item[1]">
				<label>{{item[0]}}</label>
				<input class='form-control' :name="item[1]"
 						:value="item[2]" />
			</div>
 			<div class='col-3 p-3'>
 				<button type='submit' class='btn btn-primary'>
 					{{this.i18n_edit}}
 				</button>
 			</div>
        	<div class="col-12" id='quick-form-msg'></div>
 		</form>
      </nav>
	`
});

if ($('#initiative').length){
    var initiative_vue = new Vue({
        el: '#initiative',
        template: "<initiative />"
    });
}
