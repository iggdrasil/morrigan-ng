Vue.component("progress-bar", {
	props: ["classname", "icon", "progress_classname", "percent"],
	template: `
		<span :class='this.classname'>
            <i :class='this.icon'></i>
            <div class='progress'>
                <div
                	:class="this.progress_classname ? 'progress-bar ' + this.progress_classname : 'progress-bar'"
                	role="progressbar"
                	:aria-valuenow='this.percent'
                	aria-valuemin="0"
                	aria-valuemax="100"
                	:style='"width: " + this.props.percent + "%"'>
                		{{this.percent}} %
                </div>
            </div>
		</span>
	`
});
if($("#status-div").length){
	var status_bar_vue = new Vue({
		el: '#status-div',
		mixins: [ajaxGetMixin],
		data: function(){
			return {
			    "gen_id": "status-div", 
				"url": getStatusUrl,
				"fetch_key": "",
				"rp_mode": rp_mode,
				"ap_icon": ap_icon,
				"fp_icon": fp_icon,
				"hp_icon": hp_icon,
				"mp_icon": mp_icon,
				"item": {
					"ap": null,
					"max_ap": null,
					"fp_percent": null,
					"fp": null,
					"max_fp": null,
					"hp_percent": null,
					"hp": null,
					"max_hp": null,
					"mp_percent": null,
					"mp": null,
					"max_mp": null
				}
			}
		},
		template: `
		<div id="status-div" class="navbar-nav ml-md-auto">
		  <div id="character-status" class='d-flex'>
			<span class='character-ap d-flex'>
				<i :class="ap_icon"></i>
				<span className="d-flex pl-1">
					{{this.item.ap}}
				</span>
				<span className='max-score'>
					{{this.item.max_ap}}
				</span>
			</span>
			<progress-bar
				v-if="rp_mode"
				classname='character-hp d-flex'
				progress_classname='progress-bar-success'
				:icon="hp_icon"
                :percent="this.item.hp_percent" />
            <span
            	v-else
            	class='character-hp d-flex'>
            	<i :class="hp_icon"></i>
        	  	<span>{{this.item.hp}}</span>
        	  	<span class='max-score'>{{this.item.max_hp}}</span>
        	</span>
			<progress-bar
				v-if="rp_mode"
				classname='character-fp d-flex'
				progress_classname='progress-bar-success'
				:icon="fp_icon"
                :percent="this.item.fp_percent" />
            <span
            	v-else
            	class='character-fp d-flex'>
            	<i :class="fp_icon"></i>
        	  	<span>{{this.item.fp}}</span>
        	  	<span class='max-score'>{{this.item.max_fp}}</span>
        	</span>
			<progress-bar
				v-if="rp_mode"
				classname='character-mp d-flex'
				progress_classname='progress-bar-success'
				:icon="mp_icon"
                :percent="this.item.mp_percent" />
            <span
            	v-else
            	class='character-mp d-flex'>
            	<i :class="mp_icon"></i>
        	  	<span>{{this.item.mp}}</span>
        	  	<span class='max-score'>{{this.item.max_mp}}</span>
        	</span>
		  </div>
		</div>
		`
	});
}
