// generic functions

var ajax_get = function(url, handler, context, data) {
    $.ajax({
        url: url,
        data: data,
        success: handler,
        context: context});
};


var get_cookie = function(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
          var cookie = jQuery.trim(cookies[i]);
          if (cookie.substring(0, name.length + 1) === (name + '=')) {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
          }
      }
  }
  return cookieValue;
}


var ajax_post = function(url, data, handler) {
	var csrftoken = get_cookie('csrftoken');
	data["_token"] = csrftoken;
	$.ajax({
	  type: "POST",
      url: url,
      data: data,
	  datatype: "json",
	  headers: {
        'X-CSRFTOKEN': csrftoken
      },
      success: handler
    });
}

var display_document = function (document_slug){
	var document = $(".document-item[data-slug=" + document_slug + "]");
	$("#full-screen img").attr("src", document.attr("data-target"));
	$("#full-screen").show();
};

var refresh_edit_modal = function(content){
    $('.modal-edit .modal-footer').show();
    $('#edit-form .modal-body').html(content);
    $('.modal-edit').modal('handleUpdate');
}

var modal_prevent_submit = function(submit, e){
	var key = e.which || e.keyCode;
	if (e.ctrlKey && key == 13) {
		$(submit).click();
	}
	if (key == 13) {
		return;
	}
};

var refresh_modal = function(content){
	$('.modal-action .submit').show();
    $('#action-form .modal-body').html(content);
    $('.modal-action').modal('handleUpdate');
    var inputs_str = '.modal-action input[name=targets][value=';
    var selected = selected_characters.concat(selected_items);
    for (var idx in selected){
        $(inputs_str + selected[idx] + ']').click();
    }
}

var clean_action_list = function clean_action_list() {
  unselect_items();
  unselect_characters();
  var url = baseActionsUrl;
  vueEventManager.setState('action-list', { 'actions': [] });
};

var unselect_characters = function unselect_characters() {
  var selected_characters = [];
  vueEventManager.setListState('character-list', { 'selected': false });
};

var unselect_items = function unselect_items() {
  selected_items = [];
  selected_location_items = [];
  selected_own_items = [];
  selected_equipped_items = [];
  selected_non_equipped_items = [];
  selected_equipable_items = [];
  selected_non_equipable_items = [];
  selected_natural_items = [];
  selected_non_natural_items = [];
  // changes could have been made on items - change the time key
  time_key = date_obj.getTime();
  vueEventManager.setListState('item-list', { 'selected': false });
};

var display_self_actions = function(){
	ajax_get(baseActionsUrl + "self/", function(res){
		vueEventManager.setState('action-list', {'actions':res['actions']});
	}, this);
}

var change_panel = function(panel) {
  //clean_action_list();
  $("#menu > ul > li > a").removeClass("active");
  $("#menu-" + panel + " > a").addClass("active");
  if (panel == "location") {
    $("#action-log").hide();
    $("#maps").hide();
    $("#action-log-selector").hide();
    $("#character-sheet").hide();
    $("#characters-sheet").hide();
    $("#location").show();
    if (!display_location) {
      display_self_actions();
    }
  }
  if (panel == "log") {
    $("#action-log").show();
    $("#action-log-selector").show();
    $("#maps").hide();
    $("#location").hide();
    $("#character-sheet").hide();
    $("#characters-sheet").hide();
  }
  if (panel == "characteristics") {
    $("#character-sheet").show();
    $("#characters-sheet").show();
    $("#maps").hide();
    $("#action-log").hide();
    $("#action-log-selector").hide();
    $("#location").hide();
  }
  if (panel == "map") {
    $("#maps").show();
    $("#action-log").hide();
    $("#action-log-selector").hide();
    $("#location").hide();
    $("#character-sheet").hide();
    $("#characters-sheet").hide();
  }
};

var displayDocument = function (document_slug){
	var document = $(".document-item[data-slug=" + document_slug + "]");
	$("#full-screen img").attr("src", document.attr("data-target"));
	$("#full-screen").show();
};

// EventManager

var VueEventManager = function() {
    var items = {};

    this.registerItem = function(name, item){
        items[name] = item;
    },

    this.registerListItem = function(name, key, item){
        if (!items[name]){
            items[name] = {};
        }
        items[name][key] = item;
    },
    this.unregisterItem = function(name){
        delete items[name];
    },
    this.unregisterListItem = function(name, key){
        delete items[name][key];
    },
    this.reload = function(item){
        if (items[item]){
            items[item].refreshContent();
        }
    }
    this.reloadList = function(item){
        if (items[item]){
            for (var key in items[item]){
                items[item][key].refreshContent();
            }
        }
    }
    this.setState = function (item, new_state) {
    	if (items[item]) {
    		for (let k in new_state){
      			items[item][k] = new_state[k];
    		}
    	}
  	};
  	this.setListState = function (name, new_state) {
    	if (items[name]) {
      		for (let key in items[name]) {
    			for (let k in new_state){
        			items[name][key][k] = new_state[k];
				}
      		}
    	}
  	};
};

// Mixins

var ajaxGetMixin = {
  	methods: {
    	refreshContent() {
    		let url = this.url;
    		if (!url) url = this.get_url();
			ajax_get(
				url,
				function(content){
					if (this.fetch_key) {
						this.items = content[this.fetch_key];
					} else {
						this.item = content;
					}
					if(this.get_handler) this.get_handler();
				},
				this,
				this.get_args
			);
    	}
	},
    created: function(){
    	vueEventManager.registerItem(this.gen_id, this);
    	vueEventManager.reload(this.gen_id);
	},
	beforeDestroy: function(){
    	vueEventManager.unregisterItem(this.gen_id);
	}
};

var ajaxGetListMixin = {
  	methods: {
    	refreshContent() {
    		let url = this.url;
    		if (!url) url = this.get_url();
			ajax_get(
				url,
				function(content){
					if (this.fetch_key) {
						this.items = content[this.fetch_key];
					} else {
						this.item = content;
					}
					if(this.get_handler) this.get_handler();
				},
				this,
				this.get_args
			);
    	}
	},
    created: function(){
    	vueEventManager.registerListItem(this.gen_id, this.getGenKey(), this);
    	vueEventManager.reloadList(this.gen_id, this.getGenKey());
	},
	beforeDestroy: function(){
    	vueEventManager.unregisterListItem(this.gen_id, this.getGenKey());
	},
};
