Vue.component("action-node", {
	props: ["name", "slug", "ap_cost", "icon"],
	data: function(){
		return {action_point_short_label: action_point_short_label}
	},
	methods: {
    	handleDoAction: function(event){
        	event.preventDefault();
	        let url = baseDoActionUrl + this.slug + "/";
    	    ajax_get(url, function(content){
        	    refresh_modal(content);
	            $('.modal-action').modal('show');
    			$(".modal-action .form-control").first().focus();
        	}, this);
	    }
	},
	template: `
        <li :id="'action-' + this.slug"
        	class="character-action" data-toggle="tooltip" data-placement="bottom"
        	:title="this.name + (this.ap_cost ? (' - ' + (-this.ap_cost) + ' ' + this.action_point_short_label) : '')">
        	<a v-on:click='this.handleDoAction' href='#'>
          		<i :class="this.icon"></i>
          	</a>
        </li>
	`
});

Vue.component("action-list", {
    data: function() {
        return {
            "gen_id": "action-list",
            "actions": []
        };
    },
    created: function(){
    	vueEventManager.registerItem(this.gen_id, this);
	},
	beforeDestroy: function(){
    	vueEventManager.unregisterItem(this.gen_id);
	},
    updated: function(){
    	$('.actions [data-toggle="tooltip"]').tooltip();
    },
	template: `
    <div id="action-list">
    	<ul class='actions'>
       		<action-node v-for="action in this.actions"
       			:name="action.name"
       			:slug="action.slug"
       			:ap_cost="action.ap_cost"
       			:key="action.slug"
       			:icon="action.icon" />
    	</ul>
    </div>
	`
});

if ($('#action-list').length){
    var actionlist_vue = new Vue({
        el: '#action-list',
        template: "<action-list />"
    });
}
