"use strict";

var throw_dices;
var dice_thrown;
var dices_enabled = true;
var dices_faded = 0;

function dice_initialize(container) {

    var canvas = $t.id('dice-canvas');
    var canvas_width = $("#state-container").width();
    canvas.style.width = canvas_width + 'px';
    var canvas_height = window.innerHeight - 101;
	canvas.style.height = canvas_height + 'px';
    $t.dice.use_true_random = false;

	$('#dice-canvas').css("display", "block");
    var box = new $t.dice.dice_box(canvas, { w: canvas_width,
    										 h: canvas_height });
	$('#dice-canvas').css("display", "none");

    box.animate_selector = false;

    $t.bind(window, 'resize', function() {
    	canvas_width = $("#state-container").width();
	    canvas.style.width = canvas_width + 'px';
    	canvas_height = window.innerHeight - 101;
		canvas.style.height = canvas_height + 'px';
        canvas.style.width = canvas_width + 'px';
		canvas.style.height = canvas_height + 'px';
        box.reinit(canvas, { w: canvas_width, h: canvas_height });
    });

    function notation_getter() { return $t.dice.parse_notation(dice_thrown); }
    function before_roll(vectors, notation, callback) { callback(); }
    function after_roll(notation, result) { return; }

    throw_dices = function(result){
    	if (dices_enabled && result){
    		dice_thrown = result;
    		$('#dice-canvas').show();
    		box.start_throw(notation_getter, before_roll, after_roll);
    		dices_faded += 1;
    		setTimeout(function(){
    			dices_faded -= 1;
    			if(dices_faded <= 0) $('#dice-canvas').fadeOut();
			}, 6000);
    	}
    }
}

