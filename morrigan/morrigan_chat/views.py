from django.contrib.staticfiles.templatetags.staticfiles import static
from django.http import Http404
from django.shortcuts import render
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from morrigan_common.utils import format_markdown

from morrigan_common.models import Action
from morrigan_characters.models import Character, GameMaster, ActionLog
from morrigan_geography.models import Location
from morrigan_chat.consumers import EXTRA_ACTIONS, GM_EXTRA_ACTIONS, \
    format_chat_message, get_form, ChatAction


def room(request, room_type, chat_slug, character_slug):
    try:
        character = Character.objects.get(
            slug=character_slug,
            player__user=request.user,
            location__slug=chat_slug
        )
    except Character.DoesNotExist:
        raise Http404()
    location = None
    character_list = []
    if room_type == "location":
        try:
            location = Location.objects.get(slug=chat_slug)
        except Location.DoesNotExist:
            raise Http404()
        character_list = list(character.get_neighbors(
            extra_filter={"owner__pk": None}).all()) + [character]
    extra_actions = EXTRA_ACTIONS[:]
    if request.user.is_superuser or \
            GameMaster.objects.filter(player=character.player,
                                      game=character.game).count():
        extra_actions += GM_EXTRA_ACTIONS[:]

    for action in Action.objects.filter(available_in_chat=True).order_by("slug"):
        slash = "/" + action.slug
        slash += (" - " + action.help_text) if action.help_text else ""
        extra_actions.append(ChatAction(action.slug, slash, get_form))

    missed_messages = ""
    for log in ActionLog.objects.filter(
            character=character, content__type='talk-all',
            state='U').order_by('id').all():
        date = timezone.localtime(log.date)
        log.state = 'R'
        log.save()
        missed_messages += format_chat_message(
            log.content["from"].replace(str(_("From ")), ""),
            date.strftime("%H:%M:%S"),
            log.content.get("image",
                            static("/morrigan/images/morrigan-logo-chat.png")),
            format_markdown(log.content["description"]), in_game=True
        ) + "</div>"

    missed_private_messages = {}
    for log in ActionLog.objects.filter(
            character=character, content__type='talk-private',
            state='U').order_by('id').all():
        if "target" not in log.content:
            continue
        date = timezone.localtime(log.date)
        log.state = 'R'
        log.save()
        frm = log.content.get("from-slug", "")
        if not frm:
            continue
        if frm not in missed_private_messages:
            c = Character.objects.filter(slug=frm)
            if not c.count():
                continue
            c = c.all()[0]
            missed_private_messages[frm] = [frm, c.current_name, ""]
        msg = missed_private_messages[frm][-1]
        msg += format_chat_message(
            missed_private_messages[frm][1],
            date.strftime("%H:%M:%S"),
            log.content.get("image",
                            static("/morrigan/images/morrigan-logo-chat.png")),
            format_markdown(log.content["description"])
        ) + "</div>"
        missed_private_messages[frm][-1] = msg

    missed_private_messages = [
        (target, name, messages.replace('"', '\\"').replace("\n", "\\\n"))
        for target, name, messages in missed_private_messages.values()]
    return render(request, 'morrigan/room.html', {
        'room_type': room_type,
        'room_name': chat_slug,
        'is_game_master': bool(character.player.game_master.count()),
        'character_slug': character_slug,
        'character_name': character.current_name,
        'location_name': location and location.name,
        'extra_actions': extra_actions,
        'missed_messages': missed_messages,
        'missed_private_messages': missed_private_messages,
        "character_list": character_list,
        "oracles": character.get_oracles(),
    })
