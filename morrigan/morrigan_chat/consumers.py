from collections import Iterable
import datetime
import dice
import json
from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from django.contrib.staticfiles.templatetags.staticfiles import static
from django.db.models import Max
from django.utils.translation import ugettext_lazy as _

from morrigan_common.models import Oracle, Document, Action
from morrigan_characters.models import Character, GameMaster, ActionLog
from morrigan_geography.models import Location

from morrigan_common.utils import get_full_path, format_markdown

operators = ["+", "-", "*", "/"]


MSG_TPL_HEAD = """<div{}>
    <div class='direct-chat-infos clearfix'>
      <span class='direct-chat-name float-left'>{}</span>
      <span class='direct-chat-timestamp float-right'>{}</span>
</div>"""


def format_chat_message(current_name, timestamp, image, extra_message=None,
                        alt_class=None, icon=None, in_game=False):
    div_attr = " class='direct-chat-msg{}'".format(" in-game" if in_game else "")
    div_attr += " id = 'direct-chat-msg-{}'".format(
        int(datetime.datetime.now().timestamp() * 100))
    message = MSG_TPL_HEAD.format(div_attr, current_name,
                                  timestamp)
    if not icon:
        message += '<img class="direct-chat-img" src="{}">'.format(
            image)
    else:  # if no image is an icon class
        message += '<i class="direct-chat-img icon {}"></i>'.format(icon)
    if extra_message:
        message += '<div class="direct-chat-text{}">{}</div>'.format(
            (" " + alt_class) if alt_class else "", extra_message)

    return message


class ChatAction:
    def __init__(self, slug, help_message, call, is_available=None):
        self.slug = slug
        self.help_message = help_message
        self.call = call
        self.is_available = is_available
        if is_available is None:
            self.is_available = lambda x: True


def get_form(action, __, ___):
    return "", "", ("get_form", action.slug), None, None


def set_live(action, character, is_gm, parameters):
    if not is_gm or not character.location:
        msg = "<span class='help-msg'>{}</span>".format(
            _("Action not available"))
        return msg, "no-decoration", None, None, character.slug
    if len(parameters) != 1 or parameters[0] not in ("on", "off"):
        msg = "<span class='help-msg'>{}</span>".format(action.help_message)
        return msg, "no-decoration", None, None, character.slug
    is_set = parameters[0] == "on"
    character.location.live_action = is_set
    character.location.save()
    if is_set:
        msg = _("Live action enabled")
    else:
        msg = _("Live action disabled")
    msg = "<span class='help-msg'>{}</span>".format(msg)
    return msg, "no-decoration", ("reload", ["initiative"]), None, None


def next_init(action, character, is_gm, parameters):
    if not is_gm or not character.location:
        msg = "<span class='help-msg'>{}</span>".format(
            _("Action not available"))
        return msg, "no-decoration", None, None

    base_q = Character.objects.filter(location=character.location,
                                      hp__gt=0)
    q = base_q.filter(initiative__lt=character.location.current_initiative)
    if not q.count():
        initiative = base_q.aggregate(Max('initiative'))["initiative__max"]
    else:
        initiative = q.values_list("initiative", flat=True).order_by(
            "-initiative")[0]
    character.location.current_initiative = initiative
    character.location.save()
    msg = str(_("Initiative: {}")).format(initiative)
    msg = "<span class='help-msg'>{}</span>".format(msg)
    return msg, "no-decoration", ("reload", ["initiative"]), None, None


def refresh_ui(action, character, is_gm, parameters):
    if not is_gm or not character.location:
        msg = "<span class='help-msg'>{}</span>".format(
            _("Action not available"))
        return msg, "no-decoration", None, None, character.slug
    return "", "", (
        "reload", ["initiative", "location", 'status-div', 'document',
                   'document-hidden', 'overlivemap']), None, None


def ask_oracle(action, character, is_gm, parameters):
    oracles = character.get_oracles()
    if not oracles:
        msg = "<span class='help-msg'>{}</span>".format(
            _("Action not available"))
        return msg, "no-decoration", None, None, character.slug
    bad = "<span class='help-msg'>{}</span>".format(_("Bad oracle name"))
    if len(parameters) != 1 or \
            parameters[0] not in [o["slug"] for o in oracles]:
        return bad, "no-decoration", None, None, character.slug
    oracle_slug = parameters[0]
    for o in oracles:
        if o["slug"] == oracle_slug:
            try:
                o = Oracle.objects.get(game=character.game, slug=oracle_slug)
            except Oracle.DoesNotExist:
                return bad, "no-decoration", None, None
            msg = "<span class='dice-roll'>{}</span> ".format(o.name)
            msg += '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
            msg += " <span class='dice-sum'>{}</span> ".format(o.roll())
            return msg, "msg-roll no-decoration", None, 'ra ra-aware', character.slug
    return bad, "no-decoration", None, None, character.slug


EXTRA_ACTIONS = [
    ChatAction(
        "oracle",
        _("/oracle name - ask an oracle"),
        ask_oracle
    )
]
GM_EXTRA_ACTIONS = (
    ChatAction(
        "live",
        _("/live on|off - set live action on/off on the current location"),
        set_live
    ),
    ChatAction(
        "next-init",
        _("/next-init - pass to next initiative score"),
        next_init
    ),
    ChatAction(
        "refresh",
        _("/refresh - force refresh ui"),
        refresh_ui
    ),
)


def split_roll(roll):
    """
    Naive split roll in order to extract the details

    :param roll: roll string
    :return: results, total
    """
    results, total = [], 0
    no_operator = True
    for operator in operators:
        if operator not in roll:
            continue
        no_operator = False
        for item in roll.split(operator):
            int_results, int_total = split_roll(item)
            if "d" in item:
                results += int_results
            val = int(dice.roll(str(int_total)))
            if operator == "+":
                total += val
            elif operator == "-":
                total -= val
            elif operator == "*":
                total *= val
            elif operator == "/":
                total *= val
    if no_operator:
        result = dice.roll(roll)
        if isinstance(result, Iterable) and "d" in roll:
            max_value = result.random_element.max_value
            min_value = result.random_element.min_value
            for val in result:
                if val == min_value:
                    val = "<span class='dice-result-min'>{}</span>".format(
                        val)
                if val == max_value:
                    val = "<span class='dice-result-max'>{}</span>".format(
                        val)
                results.append(val)
        total = int(result)
    return results, total


HELP_MSG = [
    _("/me doing something - display a /me message"),
    _("/roll 3d6 + 1d4 + 5 - roll dices!"),
    _("!3d6 + 1d4 + 5 - also roll dices"),
    _("/show document-id - display a document to everyone"),
    # _("/url http://example.com - display a webpage to everyone - TO BE DONE"),
]

for _action in EXTRA_ACTIONS:
    HELP_MSG.append(_action.help_message)

GM_HELP_MSG = HELP_MSG[:]

for _action in GM_EXTRA_ACTIONS:
    GM_HELP_MSG.append(_action.help_message)


def format_message(txt, character, is_gm=False):
    if txt.startswith("http:") or txt.startswith("https://"):
        parts = [part.strip() for part in txt.split(" ") if part.strip()]
        url = parts[0]
        txt = " ".join(parts[1:]) if len(parts) > 1 else url
        txt = "<a href='{}' target='_blank'>"\
              "<i class='fa fa-external-link' aria-hidden='true'></i>"\
              " {}</a>".format(url, txt)
        return txt, "", None, None, None
    if not txt.startswith("/") and not txt.startswith("!"):
        txt = txt.strip()
        if len(txt) > 2000:
            txt = txt[:2000] + " (...)"
        return txt, "", None, None, None
    if txt.startswith("/me "):
        return "{} {}".format(
            character.current_name, txt[len("/me "):]), \
               'msg-me no-decoration', None, None, None
    if txt.startswith("/show "):
        image_id = txt[len("/show "):].strip()
        return None, "", ("show", image_id), None, None
    roll_prefix = ""
    if txt.startswith("!"):
        roll_prefix = "!"
    elif txt.startswith("/roll "):
        roll_prefix = "/roll "
    if roll_prefix:
        dice_msg = txt[len(roll_prefix):]
        try:
            details, total = split_roll(dice_msg)
        except dice.exceptions.DiceBaseException:
            return str(_("Error in the syntax of your dice: {}")).format(
                dice_msg), "text-danger no-decoration", None, None, character.slug
        msg = "<span class='dice-roll'>{}</span> ".format(dice_msg)
        msg += '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
        msg += " <span class='dice-sum'>{}</span> ".format(total)
        if details and len(details) < 100:
            msg += "<span class='dice-detail'>({})</span>".format(
                " ".join(str(r) for r in details)
            )

        roll = None
        if details and len(details) < 10:
            res = ""
            for detail in details:
                detail = str(detail)
                if "<span" in detail:
                    detail = detail.split(">")[1].split("<")[0]
                if res:
                    res += " "
                res += detail
            roll = ("roll", "{} @ {}".format(dice_msg, res))
        return msg, "msg-roll no-decoration", roll, None, None
    actions = EXTRA_ACTIONS
    if is_gm:
        actions += GM_EXTRA_ACTIONS
    for action in actions:
        if not txt.startswith("/" + action.slug + " ") or \
                not action.is_available(character):
            continue
        parameters = txt.split(" ")[1:]
        result = action.call(action, character, is_gm, parameters)
        if result:
            return result
    for action in Action.objects.filter(available_in_chat=True).order_by("slug"):
        if not txt.startswith("/" + action.slug + " ") or \
                not action.is_available(character):
            continue
        result = get_form(action, character, is_gm)
        if result:
            return result
    msg = str(_("{} - command not found. Commands available are:")).format(txt)
    help_msg = HELP_MSG[:]
    if is_gm:
        help_msg = GM_HELP_MSG[:]
    for action in Action.objects.filter(available_in_chat=True).order_by("slug"):
        if not action.is_available(character):
            continue
        slash = "/" + action.slug
        slash += (" - " + action.help_text) if action.help_text else ""
        help_msg.append(slash)
    help_msg = ["<li>{}</li>".format(m) for m in help_msg]
    msg = "<span class='help-msg'>{}</span><ul class='help-msg'>{}</ul>".format(
        msg, "".join(help_msg))
    return msg, "no-decoration", None, None, character.slug


class ChatConsumer(AsyncWebsocketConsumer):

    @database_sync_to_async
    def location_get_character(self):
        self.is_gm = False
        q = Character.objects.filter(
            player_id=self.user.pk, location__slug=self.room_name,
            slug=self.character_slug
        )
        if not q.count():
            return
        self.location = Location.objects.get(slug=self.room_name)
        character = q.all()[0]
        self.current_shape = character.current_shape
        if not self.current_shape:
            return
        if self.user.is_superuser or GameMaster.objects.filter(
                player=character.player, game=character.game).count():
            self.is_gm = True
        return character

    async def connect(self):
        self.room_type = self.scope['url_route']['kwargs']['room_type']
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.character_slug = self.scope['url_route']['kwargs']['character_slug']
        self.user = self.scope["user"]
        self.room_group_name = 'chat_{}_{}'.format(self.room_type,
                                                   self.room_name)
        self.character = None
        if self.room_type == "location":
            self.character = await self.location_get_character()
        if not self.character:
            return
        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'join',
                'character': self.character_slug,
                'character_name': self.character.current_name,
            }
        )

    async def disconnect(self, close_code):
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'character': self.character_slug,
                'character_name': self.character.current_name,
                'type': "leave",
            }
        )

        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def check_location(self):
        if not self.character:
            return
        character_slug = self.character.slug
        character_name = self.character.current_name
        self.character = await self.location_get_character()
        if self.character:
            return self.character
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': "leave",
                'character': character_slug,
                'character_name': character_name
            }
        )
        return

    async def receive(self, text_data):
        if not await self.check_location():
            return
        text_data_json = json.loads(text_data)
        target, source = "location", ""
        if text_data_json["type"] in ("join", "leave"):
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': text_data_json["type"],
                    'character': text_data_json["character"],
                    'character_name': text_data_json["character_name"]
                }
            )
        elif text_data_json["type"] == "message":
            if self.current_shape.avatar:
                img_src = self.current_shape.avatar.url
            else:
                img_src = static("/morrigan/images/morrigan-logo-chat.png")
            raw_message = text_data_json['message']
            timestamp = "{}-{}".format(
                self.character.slug,
                int(datetime.datetime.now().timestamp()))
            msg, kls, action, base_icon, new_target = format_message(
                text_data_json['message'], self.character, self.is_gm)
            if kls.startswith("msg-me "):
                raw_message = "*{}*".format(msg)
            if msg:
                target = text_data_json.get('target', "location")
                if new_target:
                    target = new_target
                if action and action[0] == "roll":
                    target = "offgame"
                if target.startswith("user-"):
                    target = target[len("user-"):]
                source = text_data_json.get('source', '')
                msg = format_markdown(msg)
            message = format_chat_message(
                self.current_shape.current_name,
                datetime.datetime.now().strftime("%H:%M:%S"), img_src,
                msg, kls, base_icon, "offgame" not in target)

            wait = 0
            if action:
                action, attr = action
                if action == "show":
                    await self.channel_layer.group_send(
                        self.room_group_name,
                        {
                            'type': 'show',
                            'character': self.character_slug,
                            'target': target,
                            'source': source,
                            'document': attr
                        }
                    )
                elif action == "roll":
                    wait = 4
                    await self.channel_layer.group_send(
                        self.room_group_name,
                        {
                            'type': 'roll',
                            'character': self.character_slug,
                            'dices': attr
                        }
                    )
                elif action == "reload":
                    await self.channel_layer.group_send(
                        self.room_group_name,
                        {
                            'type': 'reload',
                            'character': self.character_slug,
                            'interfaces': attr,
                            'target': text_data_json.get("target", "")
                        }
                    )
                elif action == "get_form":
                    await self.channel_layer.group_send(
                        self.room_group_name,
                        {
                            'type': 'get_form',
                            'character': self.character_slug,
                            'action': attr
                        }
                    )
            if msg:
                # Send message to room group
                await self.channel_layer.group_send(
                    self.room_group_name,
                    {
                        'type': 'chat_message',
                        'message': message,
                        'character': self.character_slug,
                        'target': target,
                        'raw_message': raw_message,
                        'timestamp': timestamp,
                        'source': source,
                        'wait': wait,
                        'extra-class': kls
                    }
                )

    async def chat_message(self, event):
        if not await self.check_location():
            return
        if (event.get("target", None)
            and event["target"] not in ("location", "offgame")
            and self.character_slug not in [
                    event["target"], event.get("source", None)]):
            return
        reload = []
        if self.character and event.get('raw_message', None) \
                and (event["raw_message"][0] not in ("/", "!") or
                     event["raw_message"][:4] == "/me "):
            if self.character.slug == event["character"]:
                msg = {
                    "description": event["raw_message"],
                    "timestamp": event["timestamp"]
                }
                if event['target'] == "location":
                    neighbors = list(self.character.get_neighbors().all())
                    msg["type"] = "talk-all"
                    self.character.write_log(
                        msg, name=_("Message"), frm=self.character,
                        to=neighbors,
                        state='R', icon='fa fa-comments-o')
                    for c in neighbors:
                        c.write_log(
                            msg, name=_("Message"), frm=self.character,
                            to=neighbors, icon='fa fa-comments-o')
                    # todo: limit to targets
                    reload = ["action-log"]
                elif event["target"]:
                    reload = ["action-log"]
                    if event["target"] == self.character.slug:
                        msg["type"] = "notes"
                        self.character.write_log(msg, name=_('Notes'),
                                                 frm=self.character, state='R',
                                                 icon="fa fa-pencil")
                    else:
                        msg["type"] = "talk-private"
                        q = self.character.get_neighbors().filter(
                            slug=event["target"])
                        if q.count():
                            c = q.all()[0]
                            msg["target"] = c.slug
                            self.character.write_log(
                                msg, name=_("Message"), frm=self.character,
                                to=[c], state='R', icon='fa fa-comments-o')
                            c.write_log(
                                msg, name=_("Message"), frm=self.character,
                                to=[c], icon='fa fa-comments-o')
            else:
                # mark as read because on the chat
                q = ActionLog.objects.filter(
                        character=self.character,
                        content__timestamp=event["timestamp"])
                if q.count():
                    action = q.all()[0]
                    action.state = 'R'
                    action.save()
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': event["message"],
            'character': event["character"],
            'type': event["type"],
            'target': event["target"],
            'source': event["source"],
            'wait': event.get("wait", 0),
            'reload': reload,
            'extra-class': event["extra-class"]
        }))

    async def join(self, event):
        if not await self.check_location():
            return
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'character': event["character"],
            'character_name': event["character_name"],
            'type': event["type"],
        }))

    async def leave(self, event):
        if not await self.check_location():
            return
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'character': event["character"],
            'character_name': event["character_name"],
            'type': event["type"],
        }))

    async def show(self, event):
        if not await self.check_location():
            return
        q = Document.objects.filter(slug=event["document"])
        if not q.count():
            return
        doc = q.all()[0]
        character = event.get("character", None)
        if character:
            try:
                character = Character.objects.get(slug=character)
            except Character.objects.DoesNotExist:
                character = None
        if (doc.characters.count() and
                self.character.pk not in doc.characters.values_list("pk", flat=True)
        ) or (doc.locations.count() and (
                not self.character.location or
                self.character.location.pk not in doc.locations.values_list("pk",
                                                                            flat=True)
        )):
            return
        self.character.write_log(
            "![]({})".format(
                    get_full_path(self.scope, doc.image.url)),
            frm=character, icon="fa fa-picture-o", state="R")
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'character': str(character) if character else "",
            'target': event.get("target", None),
            'source': event.get("source", None),
            'document': event["document"],
            'type': "show"
        }))

    async def roll(self, event):
        if not await self.check_location():
            return
        await self.send(text_data=json.dumps({
            'character': event["character"],
            'dices': event["dices"],
            'type': "roll"
        }))

    async def reload(self, event):
        wait = 0
        if "wait" in event:
            wait = event["wait"]
        await self.send(text_data=json.dumps({
            'interfaces': event["interfaces"],
            'type': "reload",
            'target': event.get("target", ""),
            'wait': wait
        }))

    async def get_form(self, event):
        if not await self.check_location():
            return
        if event["character"] != self.character.slug:
            return
        await self.send(text_data=json.dumps({
            'type': "get_form",
            'action': event["action"],
        }))
