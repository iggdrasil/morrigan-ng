from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'(?P<room_type>location|group)/(?P<chat_slug>[\w-]+)/'
        r'(?P<character_slug>[\w-]+)/',
        views.room, name='room'),
]
