from django.apps import AppConfig


class MorriganChatConfig(AppConfig):
    name = 'morrigan_chat'
