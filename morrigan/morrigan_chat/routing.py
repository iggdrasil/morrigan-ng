from django.conf.urls import url

from . import consumers


websocket_urlpatterns = [
    url(
        r'ws/chat/(?P<room_type>location|group)/(?P<room_name>[\w-]+)/'
        r'(?P<character_slug>[\w-]+)/$',
        consumers.ChatConsumer),
]