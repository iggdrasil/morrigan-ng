#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.test import TestCase

from morrigan_common.admintest import adminviewstest
from morrigan_common.tests import TestMorriganAction


class TestSlapAction(TestMorriganAction):
    def test_doaction_view(self):
        self.identification('arkenlond', 'password')
        self.select_character('nimnae')
        self.action_show('slap')
        self.action_submit()
        self.action_close()


class TestAdminViews(TestCase):
    def test_admin_views(self):
        adminviewstest(self, 'morrigan_common')
