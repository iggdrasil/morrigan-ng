#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.db.models.signals import pre_save
from django.forms.models import model_to_dict

from morrigan_common.models import GenericSlugType, pre_save_slug
from morrigan_geography.models import Location
from morrigan_characters.models import Character


class ViolenceDegree(GenericSlugType):
    hp_lost_percent = models.IntegerField(
        help_text=_("HP percent lost by the opponent (max threshold)"))

    class Meta:
        verbose_name = _("Violence degree")
        verbose_name_plural = _("Violence degrees")
        ordering = ('hp_lost_percent',)

    @classmethod
    def get_degree(cls, lost_hp, full_hp):
        percent = 100
        if full_hp:
            percent = lost_hp / full_hp * 100
        current_degree = None
        for degree in cls.cache_get_all():
            if degree['hp_lost_percent'] > percent:
                return current_degree or degree
            current_degree = degree
        if not current_degree:
            vd = cls.objects.create(name="Violence degree not set",
                                    slug="not-set", hp_lost_percent=100)
            return model_to_dict(vd)
        return current_degree

pre_save.connect(pre_save_slug, sender=ViolenceDegree)


class Engaged(models.Model):
    character_1 = models.ForeignKey(Character, on_delete=models.CASCADE,
                                    related_name='involved_primary')
    character_2 = models.ForeignKey(Character, on_delete=models.CASCADE,
                                    related_name='involved_secondary')
    init_1 = models.BooleanField(_("Character 1 has the initiative"),
                                 default=True)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    distant = models.BooleanField(_("Distant engagement"), default=False)
