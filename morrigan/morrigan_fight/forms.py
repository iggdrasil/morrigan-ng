#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.utils.translation import ugettext_lazy as _

from morrigan_common.models import Action
from morrigan_fight import models

from morrigan_characters.forms import FormActionSimpleTarget


class FightAction(FormActionSimpleTarget):
    fail_msg_neighbor = _(
        "You see %(character)s try to hit %(target)s. But"
        "%(character)s seems to have missed its target.")
    fail_msg_opponent = _("%(character)s try to hit you. %(character)s "
                          "misses you.")
    fail_msg_character = _("You try to hit %(target)s. You "
                           "miss %(target)s.")
    success_msg_neighbor = _(
        "You see %(character)s hits %(target)s. %(character)s hurts "
        "%(target)s %(violence_degree)s.")
    success_msg_opponent = _("%(character)s hits you. It hurts you "
                             "%(violence_degree)s.")
    success_msg_character = _("You hits %(target)s. You "
                              "hurt %(target)s %(violence_degree)s.")

    def save(self, character, targets, result):
        success = result['character']['parameters']['SUCCESS'] == 1
        target = targets[0]
        dct = {'character': str(character), 'target': str(target), }
        if not success:
            msg_neighbor = self.fail_msg_neighbor % dct
            msg_opponent = self.fail_msg_opponent % dct
            msg_character = self.fail_msg_character % dct
        else:
            lost_hp = result['opponent']['hp']['result'] or 0
            dct['violence_degree'] = models.ViolenceDegree.get_degree(
                abs(lost_hp), target.max_hp)['name']
            msg_neighbor = self.success_msg_neighbor % dct
            msg_opponent = self.success_msg_opponent % dct
            msg_character = self.success_msg_character % dct

        for neighbor in character.get_neighbors():
            if neighbor == target:
                continue
            neighbor.write_log(msg_neighbor, name=_('Fight'), frm=character,
                               to=targets, icon=self.icon)
        target.write_log(msg_opponent, name=_('Fight'), frm=character,
                         to=targets, icon=self.icon)
        character.write_log(msg_character, name=_('Fight'), frm=character,
                            to=targets, state='R', icon=self.icon)
        return msg_character


class SlapAction(FightAction):
    fail_msg_neighbor = _(
        "You see %(character)s try to slap %(target)s. But"
        "%(character)s seems to have missed its target.")
    fail_msg_opponent = _("%(character)s try to slap you. %(character)s "
                          "misses you.")
    fail_msg_character = _("You try to slap %(target)s. You "
                           "miss %(target)s.")
    success_msg = _("You see %(character)s slaps %(target)s. %(character)s "
                    "hurts %(target)s %(violence_degree)s.")
    success_msg_opponent = _("%(character)s slaps you. It hurts you "
                             "%(violence_degree)s.")
    success_msg_character = _("You slaps %(target)s. You "
                              "hurt %(target)s %(violence_degree)s.")
    action_slug = 'slap'
