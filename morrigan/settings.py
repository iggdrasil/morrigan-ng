#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Don't edit this file:
# overload all theses settings in your local_settings.py file

import os

ROOT_PATH = os.path.realpath(os.path.dirname(__file__)) + "/"
STATIC_ROOT = ROOT_PATH + "static"

DEBUG = False

PROJECT = "Morrigan"

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'NAME': 'morrigan',
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': 'postgres',
        'PORT': '5432',
        'USER': 'morrigan',
        'PASSWORD': 'morrigan',
    },
}

USE_TZ = True

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-fr'

LANGUAGES = (
    ('fr', 'Français'),
)

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = ROOT_PATH + 'media/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/media/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = '-82bsk#u*9eq*q8d^8&=er-nu4+cgmy!+@$0*^f!s2qzk-TOBECHANGEINLOCAL'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            ROOT_PATH + 'templates',
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'morrigan_common.context_processors.get_base_context',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ]
        }
    }
]

MIDDLEWARE = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

LOCALE_PATHS = [os.path.join(ROOT_PATH, "locale")]

ROOT_URLCONF = 'urls'

INSTALLED_APPS = (
    'dal',
    'dal_select2',
    'django.contrib.messages',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.staticfiles',
    'rest_framework',
    'channels',
    'morrigan_app',
    'morrigan_chat',
    'morrigan_common',
    'morrigan_geography',
    'morrigan_characters',
    'morrigan_inventory',
    'morrigan_fight',
    'morrigan_scripts',
    'django_nose',
    'adminsortable',
)

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'

NOSE_ARGS = [
    "--with-coverage",
    "--cover-package=morrigan_characters, morrigan_common, "
    "morrigan_geography, morrigan_inventory, morrigan_fight",
    "--verbosity=0",
    "--nocapture",
    "--nologcapture"
]

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    ('morrigan', ROOT_PATH + 'assets'),
)

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    )
}

CROSS_IMAGE = os.path.join(ROOT_PATH, "assets", "images", "cross.png")

CACHE_SMALLTIMEOUT = 120
CACHE_TIMEOUT = 3600

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}

ASGI_APPLICATION = "morrigan_app.routing.application"
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('127.0.0.1', 6379)],
        },
    },
}

MORRIGAN_ACTIONLOG_ITEM_BY_PAGE = 20
MORRIGAN_RP_MODE = True
MORRIGAN_AVATAR_SIZE = (40, 40)
MORRIGAN_TRUSTED_USER = False

MAX_UPLOAD_SIZE = 1024 * 1024 * 25
DATA_UPLOAD_MAX_MEMORY_SIZE = 1024 * 1024 * 50
FILE_UPLOAD_MAX_MEMORY_SIZE = 1024 * 1024 * 25

try:
    from local_settings import *
except ImportError as e:
    print('Unable to load local_settings.py:', e)
