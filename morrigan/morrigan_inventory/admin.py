#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2010-2018  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from admin import admin_site
from morrigan_common.admin import GenericAdmin

from morrigan_inventory import models


class GenericArmorAreaInline(admin.TabularInline):
    model = models.GenericArmorArea
    extra = 1


class GenericArmorDamageInline(admin.TabularInline):
    model = models.GenericArmorDamage
    extra = 1


class GenericArmorAdmin(admin.ModelAdmin):
    list_display = ['name', 'area_names', 'defense_bonus']
    inlines = (GenericArmorAreaInline, GenericArmorDamageInline)


admin_site.register(models.GenericArmor, GenericArmorAdmin)

# admin_site.register(models.Currency, GenericAdmin)
admin_site.register(models.AmmoType, GenericAdmin)
admin_site.register(models.GenericWeapon)
admin_site.register(models.Weapon)


class ArmorAreaInline(admin.TabularInline):
    model = models.ArmorArea
    extra = 1


class ArmorAdmin(admin.ModelAdmin):
    list_display = ['name', 'area_names', 'defense_bonus']
    inlines = (ArmorAreaInline, )


admin_site.register(models.Armor, ArmorAdmin)


class GenericItemAdmin(admin.ModelAdmin):
    list_display = ['name', 'type', 'size', 'generic_armor', 'generic_weapon',
                    'generic_charger', 'equipable', 'available']
    list_filter = ['type', 'size', 'available']


admin_site.register(models.GenericItem, GenericItemAdmin)


class ItemCreationForm(forms.Form):
    generic = forms.ChoiceField(
        label=_("Generic item"), choices=[])

    def __init__(self, *args, **kwargs):
        self.instance = None
        if 'instance' in kwargs:
            self.instance = kwargs.pop('instance')
        super(ItemCreationForm, self).__init__(*args, **kwargs)

        self.fields['generic'].choices = [('', '-' * 10)]

        for label, armor_isnull, weapon_isnull, charger_isnull in (
                (None, True, True, True),
                (_("Armor"), False, True, True),
                (_("Weapon"), True, False, True),
                (_("Charger"), True, True, False),
                (_("Armor + weapon"), False, False, True),
                (_("Armor + charger"), False, True, False),
                (_("Weapon + charger"), True, False, False),
                (_("Armor + weapon + charger"), False, False, False),):
            q = models.GenericItem.objects.filter(
                generic_armor__isnull=armor_isnull,
                generic_weapon__isnull=weapon_isnull,
                generic_charger__isnull=charger_isnull)
            if q.count():
                choices = [(i.pk, str(i)) for i in q.all()]
                if label:
                    self.fields['generic'].choices += [(label, choices)]
                else:
                    self.fields['generic'].choices += choices

    def save(self, *args, **kwargs):
        gen = models.GenericItem.objects.get(pk=self.cleaned_data['generic'])
        self.instance = gen.create_item()
        return self.instance

    def save_m2m(self, *args, **kwargs):
        return


class ArmorInline(admin.TabularInline):
    model = models.Armor
    can_delete = False
    extra = 0

    def has_add_permission(self, request):
        return False


class ItemAdmin(admin.ModelAdmin):
    list_display = ['name', 'type', 'size', 'equipable', 'available',
                    'visible', 'location', 'inside', 'character']
    list_filter = ['type', 'size', 'available']
    search_fields = ['name', 'character_areas__shape__current_name',
                     'character_areas__shape__character__current_name',
                     'location__name']
    inlines = (ArmorInline, )
    autocomplete_fields = ("character_areas", "location", "inside")

    def get_form(self, request, obj=None, **kwargs):
        if not obj:
            kwargs['form'] = ItemCreationForm
        return super(ItemAdmin, self).get_form(request, obj, **kwargs)

    def get_inline_instances(self, request, obj=None):
        if not obj or not \
                obj.generic_item.generic_armor:
            return []
        return super(ItemAdmin, self).get_inline_instances(
            request, obj=obj)

    def get_inline_formsets(self, request, formsets, inline_instances,
                            obj=None):
        if not obj or not obj.generic_item.generic_armor:
            return []
        return super(ItemAdmin, self).get_inline_formsets(
            request, formsets, inline_instances, obj=obj)


admin_site.register(models.Item, ItemAdmin)
