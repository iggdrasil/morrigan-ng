#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015-2017 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from morrigan_common.views import register_action
from morrigan_characters.views import BaseAction

from morrigan_inventory import forms


@register_action
class PickUpAction(BaseAction):
    slug = 'pick-up'
    verbose_name = _("Pick-up")
    form_class = forms.PickUpAction
    target_selection_not_available = _(
        "You cannot pick up this item, you have no space available in your "
        "bags.")


@register_action
class DropAction(BaseAction):
    slug = 'drop'
    verbose_name = _("Drop")
    form_class = forms.DropAction


@register_action
class GiveItemAction(BaseAction):
    slug = "give-item"
    verbose_name = _("Give item")
    form_class = forms.GiveItemAction


@register_action
class StealAction(BaseAction):
    slug = 'steal'
    verbose_name = _("Steal")
    form_class = forms.StealAction


@register_action
class EquipAction(BaseAction):
    slug = 'equip'
    verbose_name = _("Equip")
    form_class = forms.EquipAction
    equipped = False

    def get_form_kwargs(self):
        """
        Generic is not enough: must check equipable item and available space
        """
        kwargs = super(EquipAction, self).get_form_kwargs()
        kwargs['character'] = self.character
        kwargs['targets'] = []
        # equippable and not natural
        q = Q(natural=False, equipped=self.equipped) & (
            Q(generic_item__generic_weapon__isnull=False) |  # weapon
            Q(generic_item__generic_armor__isnull=False)  # armor
        )
        if 'targets' not in kwargs:
            kwargs['targets'] = []
        current_shape = self.character.current_shape
        for area in current_shape.areas.all():
            for item in area.items.filter(q).all():
                if not self.character.can_equip(item):
                    continue
                kwargs['targets'].append((item.pk, item.equipped_label))
        if self.equipped:
            return kwargs
        if self.character.location:
            for item in self.character.location.items.filter(q).all():
                if not self.character.can_equip(item):
                    continue
                kwargs['targets'].append((item.pk, item.equipped_label))
        return kwargs


@register_action
class UnequipAction(EquipAction):
    slug = 'unequip'
    verbose_name = _("Unequip")
    form_class = forms.UnequipAction
    equipped = True


@register_action
class ShowItemAction(BaseAction):
    slug = 'show-item'
    verbose_name = _("Show item")
    form_class = forms.ShowItemAction


@register_action
class PutIntoBagAction(BaseAction):
    slug = 'put-into-bag'
    verbose_name = _("Put into bag")
    form_class = forms.PutIntoBagAction

    def render_target(self, target):
        # display size index
        return "{} ({})".format(target, target.size.index)
