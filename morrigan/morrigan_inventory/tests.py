#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015-2017  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import time

from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from morrigan_common.admintest import adminviewstest
from morrigan_common.tests import TestMorriganAction, DEFAULT_FIXTURES

from morrigan_characters.models import ShapeArea, Character
from morrigan_inventory import models

User = get_user_model()


class TestAdminViews(TestCase):
    def test_admin_views(self):
        adminviewstest(self, 'morrigan_inventory')


class TestSpecificAdminViews(TestCase):
    fixtures = DEFAULT_FIXTURES

    def setUp(self):
        self.user = User.objects.create_superuser('test', 'test@test.com',
                                                  'test')

    def test_add_item_view(self):
        url = reverse("admin:morrigan_inventory_item_add")
        self.client.login(username=self.user.username, password='test')
        response = self.client.get(url, follow=True)
        self.failUnlessEqual(
            response.status_code, 200,
            "%s != %s -> 'morrigan_inventory.Item', url: %s" % (
                response.status_code, 200, url))

        gen_weapon = models.GenericItem.objects.filter(
            generic_weapon__isnull=False).all()[0]
        item_number = models.Item.objects.count()
        weapon_number = models.Weapon.objects.count()
        self.client.post(url, {'generic': gen_weapon.pk})
        self.assertEqual(item_number + 1, models.Item.objects.count())
        self.assertEqual(weapon_number + 1, models.Weapon.objects.count())


"""
class TestLiveInventory(TestMorriganAction):
    def test_action_on_items(self):
        # equip the suitcase
        suit = models.Item.objects.filter(icon='fa fa-suitcase').all()[0]
        suit.character_area = ShapeArea.objects.filter(
            shape__slug='arkenlond',
            area__name='Bras'
        ).all()[0]
        suit.location = None
        suit.save()

        self.identification('arkenlond', 'password')
        # self.location_page('arkenlond')

        # unequip the "suitcase"
        self.select_item('arkenlond', 'fa-suitcase', 'unequip')
        self.action_show('unequip')
        self.action_submit(wait_for_result=True)
        self.action_close()

        suit = models.Item.objects.filter(icon='fa fa-suitcase').all()[0]
        ark = Character.objects.get(slug='arkenlond')
        self.assertEqual(suit.location, ark.location)


class TestLiveInventory(TestMorriganAction):
    def test_action_on_items(self):

        self.identification('arkenlond', 'password')
        time.sleep(1)  # wait for the JS to load...

        self.assertIn("Arkenlond", self.selenium.page_source)
        self.assertIn("Nimnae", self.selenium.page_source)
        self.assertIn("bs-callout-success", self.selenium.page_source)

        # steal a "suitcase"
        # self.select_item('nimnae', 'fa-suitcase', action_awaited='steal')
        # self.action_show('steal')
        # self.action_submit()
        # self.action_close()
        # time.sleep(1)  # wait for the JS to load...
        # steal is disabled for now - dirty fix
        suit = models.Item.objects.filter(icon='fa fa-suitcase').all()[0]
        suit.character_area = ShapeArea.objects.filter(
            shape__character__slug='arkenlond').all()[0]
        suit.save()

        # drop the "suitcase"
        self.select_item('arkenlond', 'fa-suitcase')
        self.action_show('drop')
        self.action_submit()
        self.action_close()
        time.sleep(1)  # wait for the JS to load...

        # pick-up the "suitcase" on the floor
        self.select_location_item('fa-suitcase')
        self.action_show('pick-up')
        self.action_submit()
        self.action_close()
        """
