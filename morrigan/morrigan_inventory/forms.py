#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2015-2018  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django import forms
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from morrigan_inventory import models
from morrigan_characters.models import Character
from morrigan_characters.forms import FormActionMultipleTarget, \
    FormActionSimpleTarget

from morrigan_common.widgets import BootstrapCheckboxMultiple, BootstrapRadio


class PickUpAction(FormActionSimpleTarget):
    action_slug = 'pick-up'

    def save(self, character, targets, result):
        items = []

        success = False
        if result:
            success = result['character']['parameters']['SUCCESS'] == 1
        elif targets:
            success = True

        if success:
            for target in targets:
                try:
                    item = models.Item.objects.get(pk=target)
                    if not item.location \
                            or item.location != character.location:
                        continue
                    # TODO: add a field to choose the default bag
                    if item.give_item(character):
                        items.append(str(item).lower())
                except models.Item.DoesNotExist:
                    pass
        dct = {'item': ", ".join(items), 'location': character.location,
               'character': self.character}
        if not success or not items:
            message = _(
                "You try to take something from {location} but you fail.")\
                .format(**dct)
            msg_neighbor = _(
                "You see {character} try to pick-up something from "
                "{location} but this is not a success.").format(**dct)
        else:
            message = _("You pick-up {item} from {location}.").format(**dct)
            msg_neighbor = _("You see {character} pick-up {item} "
                             "from {location}.").format(**dct)
        log = character.write_log(message, name=_('Items'), frm=character,
                                  state='R', icon=self.icon)
        for neighbor in self.character.get_neighbors():
            neighbor.write_log(msg_neighbor, name=_('Events'), frm=character,
                               icon=self.icon)
        return log.content['description']


class ShowItemAction(FormActionSimpleTarget):
    action_slug = 'show-item'
    characters = forms.MultipleChoiceField(
        label=_("Characters"), choices=[], widget=BootstrapCheckboxMultiple())

    def __init__(self, *args, **kwargs):
        super(ShowItemAction, self).__init__(*args, **kwargs)
        if not self.character:
            return
        self.fields['characters'].choices = [
            (c.pk, c.current_name) for c in self.get_neighbors()
        ]

    def get_neighbors(self):
        characters = []
        if not self.character:
            return characters
        for character in self.character.get_neighbors():
            if not character.current_shape:
                continue
            characters.append(character)
        return characters

    def save(self, character, targets, result):
        own_items = character.get_own_items(natural=False)
        target = targets[0]
        try:
            item = models.Item.objects.get(pk=target)
            if item not in own_items:
                item = None
        except models.Item.DoesNotExist:
            item = None

        if not item:
            return
        current_neighbors = [str(n.pk) for n in self.get_neighbors()]
        neighbors = []
        for neighbor in self.cleaned_data["characters"]:
            if str(neighbor) not in current_neighbors:
                continue
            try:
                neighbors.append(Character.objects.get(pk=neighbor))
            except Character.DoesNotExist:
                continue
        neighbors_str = " ; ".join([n.name for n in neighbors])
        message = str(_("You show an item to ")) + neighbors_str + ". <br>" + \
                  item.full_description
        msg_neighbor = str(_("{} show an item to ")).format(character.name) + \
                       neighbors_str + ". <br>" + item.full_description
        log = character.write_log(message, name=_('Items'), frm=character,
                                  state='R', icon=self.icon)
        for neighbor in neighbors:
            neighbor.write_log(msg_neighbor, name=_('Items'), frm=character,
                               icon=self.icon)
        return log.content['description']


class GiveItemAction(FormActionSimpleTarget):
    action_slug = 'give-item'
    character = forms.ChoiceField(
        label=_("Character"), choices=[], widget=BootstrapRadio,
        help_text=_("Only trusting character can receive your item")
    )

    def __init__(self, *args, **kwargs):
        super(GiveItemAction, self).__init__(*args, **kwargs)
        if not self.character or not self.character.current_shape:
            return
        self.fields['character'].choices = [
            (c.pk, c.current_name) for c in self.get_trusting_characters()
        ]

    def get_trusting_characters(self):
        trusted_characters = []
        if not self.character:
            return trusted_characters
        for character in list(
                self.character.get_neighbors(
                    extra_filter={
                        "shapes__is_current_shape": True,
                        "shapes__trusted__pk": self.character.current_shape.pk})
            ) + list(
                self.character.get_neighbors(
                    extra_filter={
                        "shapes__is_current_shape": True,
                        "shapes__temporary_trusted__pk":
                            self.character.current_shape.pk})):
            if not character.current_shape:
                continue
            trusted_characters.append(character)
        return trusted_characters

    def save(self, character, targets, result):
        own_items = character.get_own_items(natural=False, equiped=False)
        target = targets[0]
        try:
            item = models.Item.objects.get(pk=target)
            if item not in own_items:
                item = None
        except models.Item.DoesNotExist:
            item = None

        if not item:
            return
        current_neighbors = dict([(str(n.pk), n) for n in
                                   self.get_trusting_characters()])
        if str(self.cleaned_data["character"]) not in current_neighbors.keys():
            return
        target_character = current_neighbors[
            str(self.cleaned_data["character"])]

        if not item.give_item(target_character):
            message = str(_("You try to give {} to {} but it seems that "
                            "he/her cannot carry it.")).format(
                item, target_character.current_name)
            message_target = str(_("{} try to give you {} but it seems that "
                                   "you cannot carry it.")).format(
                character.current_name, item)
            message_other = str(_("You see {} trying to give something to {} "
                                  "but it seems that he/her cannot carry it.")
                                ).format(character.current_name,
                                         target_character.current_name)
        else:
            message = str(_("You give {} to {}.")).format(
                item, target_character.current_name)
            message_target = str(_("{} give you {}.")).format(
                character.current_name, item)
            message_other = str(_("You see {} giving something to {}.")
                                ).format(character.current_name,
                                         target_character.current_name)
            if character.current_shape.pk in \
                    target_character.current_shape.temporary_trusted\
                            .values_list("id", flat=True):
                target_character.current_shape.temporary_trusted.remove(
                    character.current_shape.pk)

        log = character.write_log(message, name=_('Items'), frm=character,
                                  state='R', icon=self.icon)
        target_character.write_log(message_target, name=_("Items"),
                                   frm=character, icon=self.icon)
        for neighbor in character.get_neighbors():
            if neighbor == target_character:
                continue
            neighbor.write_log(message_other, name=_('Items'), frm=character,
                               icon=self.icon)
        return log.content['description']


class DropAction(FormActionMultipleTarget):
    action_slug = 'drop'

    def save(self, character, targets, result):
        items = []
        own_items = character.get_own_items(natural=False)
        for target in targets:
            try:
                item = models.Item.objects.get(pk=target)
                if item not in own_items:
                    continue
                item.character_areas.clear()
                item.location = character.location
                item.inside = None
                item.save()
                items.append(str(item).lower())
            except models.Item.DoesNotExist:
                pass
        dct = {'item': ", ".join(items), 'location': character.location,
               'character': self.character}
        if not items:
            message = _(
                "You try to drop something in {location} but you fail.")\
                .format(**dct)
            msg_neighbor = _(
                "You see {character} try to drop something in "
                "{location} but this is not a success.").format(**dct)
        else:
            message = _("You drop {item} in {location}.").format(**dct)
            msg_neighbor = _("You see {character} drop {item} "
                             "in {location}.").format(**dct)
        log = character.write_log(message, name=_('Items'), frm=character,
                                  state='R', icon=self.icon)
        for neighbor in self.character.get_neighbors():
            neighbor.write_log(msg_neighbor, name=_('Events'), frm=character,
                               icon=self.icon)
        return log.content['description']


class PutIntoBagAction(FormActionSimpleTarget):
    action_slug = 'put-into-bag'
    bag = forms.ChoiceField(label=_("Message"), required=True,
                            choices=[])

    def __init__(self, *args, **kwargs):
        super(PutIntoBagAction, self).__init__(*args, **kwargs)
        if not self.character:
            return
        self.fields['bag'].choices = []
        for area in self.character.current_shape.areas.all():
            self.fields['bag'].choices += [
                (item.pk, item.capacity_label)
                for item in area.items.filter(contain_capacity__gt=0)
            ]

    def save(self, character, targets, result):
        bag = models.Item.objects.get(pk=self.cleaned_data['bag'])
        item = models.Item.objects.get(pk=targets[0])
        if bag.available_space < item.size.index:
            message = _("Not enough space available for this object in your "
                        "bag.")
            return message
        item.inside = bag
        item.character_areas.clear()
        item.location = None
        item.save()
        dct = {'item': str(item), 'character': self.character, 'bag': str(bag)}
        message = _("You put {item} in the bag \"{bag}\".").format(**dct)
        msg_neighbor = _(
            "You see {character} put {item} in a bag \"{bag}\".").format(**dct)
        log = character.write_log(message, name=_('Items'), frm=character,
                                  state='R', icon=self.icon)
        for neighbor in self.character.get_neighbors():
            neighbor.write_log(msg_neighbor, name=_('Events'), frm=character,
                               icon=self.icon)
        return log.content['description']


class StealAction(FormActionMultipleTarget):
    action_slug = 'steal'

    def save(self, character, targets, result):
        item = None
        target = targets[0]
        if result:
            success = result['character']['parameters']['SUCCESS'] == 1
        else:
            success = True
        stealed_person = None
        try:
            item = models.Item.objects.get(pk=target)
            if item.character_area:
                stealed_person = item.character_area.shape.character
                if stealed_person.location != character.location:
                    success = False
                if success:

                    # TODO: add a field to choose - check before action
                    q_bag = character.items.filter(contain_capacity__gt=0)
                    q_shape = character.current_shape.areas.filter(
                        is_default=True)
                    # inside a bag
                    if q_bag.count():
                        bag = q_bag.all()[0]
                        item.inside = bag
                    elif q_shape.count():
                        # TODO: check availability
                        area = q_shape.all()[0]
                        item.character_area = area
                    else:
                        success = False
                    if success:
                        item.save()
                        item = str(item).lower()
        except models.Item.DoesNotExist:
            success = False
        dct = {'item': item,
               'character': self.character,
               'target': stealed_person}
        msg_steal = ''
        if not success:
            if stealed_person:
                message = _(
                    "You try to steal something to {target} but you fail.")\
                    .format(**dct)
                msg_steal = _(
                    "You see {character} try to steal something to "
                    "you but this is not a success.").format(**dct)
                msg_neighbor = _(
                    "You see {character} try to steal something to "
                    "{target} but this is not a success.").format(**dct)
            else:
                message = _(
                    "You try to steal something but you fail.")\
                    .format(**dct)
                msg_neighbor = _(
                    "You see {character} try to steal something "
                    "but this is not a success.").format(**dct)
        else:
            message = _("You steal {item} to {target}.").format(**dct)
            msg_steal = _("You see {character} steal {item} "
                          "to you.").format(**dct)
            msg_neighbor = _("You see {character} steal {item} "
                             "to {target}.").format(**dct)
        log = character.write_log(message, name=_('Steal'), frm=character,
                                  state='R', icon=self.icon)
        for neighbor in self.character.get_neighbors():
            if not msg_steal or neighbor != stealed_person:
                neighbor.write_log(msg_neighbor, name=_('Events'),
                                   frm=character, icon=self.icon)
            else:
                neighbor.write_log(msg_steal, name=_('Events'),
                                   frm=character, icon=self.icon)
        return log.content['description']


class EquipAction(FormActionSimpleTarget):
    action_slug = 'equip'
    success_msg_character = _("You equip {item}.")
    success_msg_neighbor = _("You see {character} equip {item}.")
    fail_msg_character = _("You try to equip {item} but you fail.")
    fail_msg_neighbor = _(
        "You see %(character)s try to equip {item} but it is a failure."
    )

    def save(self, character, targets, result):
        item, area = targets[0], None
        try:
            item = models.Item.objects.get(pk=item)
        except (models.Item.DoesNotExist, ValueError):
            item = None
        success = False
        if result:
            success = result['character']['parameters']['SUCCESS'] == 1
        elif item:
            success = True

        if success:
            for armor in item.armors.all():
                success &= armor.equip(character)

        dct = {'item': item,
               'character': self.character}
        if success:
            message = self.success_msg_character.format(**dct)
            msg_neighbor = self.success_msg_neighbor.format(**dct)
        else:
            message = self.fail_msg_character.format(**dct)
            msg_neighbor = self.fail_msg_neighbor.format(**dct)
        log = character.write_log(message, name=_('Unequip'), frm=character,
                                  state='R', icon=self.icon)
        for neighbor in self.character.get_neighbors():
            neighbor.write_log(msg_neighbor, name=_('Events'),
                               frm=character, icon=self.icon)
        return log.content['description']


class UnequipAction(FormActionSimpleTarget):
    action_slug = 'unequip'
    success_msg_character = _("You unequip {item} and drop it on the floor.")
    success_msg_neighbor = _("You see {character} unequip {item} "
                             "and drop it on the floor.")
    fail_msg_character = _("You try to unequip {item} but you fail.")
    fail_msg_neighbor =_("You see {character} try to unequip {item} "
                         "but it is a failure.")

    def save(self, character, targets, result):
        item = targets[0]
        try:
            item = models.Item.objects.get(pk=item)
        except (models.Item.DoesNotExist, ValueError):
            item = None
        dct = {'item': item,
               'character': self.character}
        success = False
        if result:
            success = result['character']['parameters']['SUCCESS'] == 1
        elif item:
            success = True

        if success:
            for armor in item.armors.all():
                success &= armor.unequip(character)

        if success:
            message = self.success_msg_character.format(**dct)
            msg_neighbor = self.success_msg_neighbor.format(**dct)
        else:
            message = self.fail_msg_character.format(**dct)
            msg_neighbor = self.fail_msg_neighbor.format(**dct)
        log = character.write_log(message, name=_('Unequip'), frm=character,
                                  state='R', icon=self.icon)
        for neighbor in self.character.get_neighbors():
            neighbor.write_log(msg_neighbor, name=_('Events'),
                               frm=character, icon=self.icon)
        return log.content['description']
