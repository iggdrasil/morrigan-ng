# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-04-07 13:22
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('morrigan_inventory', '0003_auto_20170519_1237'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='genericitem',
            name='equipable',
        ),
        migrations.RemoveField(
            model_name='item',
            name='equipable',
        ),
    ]
