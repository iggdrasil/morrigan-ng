#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from rest_framework import serializers
from morrigan_inventory import models


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Item
        fields = ('current_name', 'equipped', 'equipable', 'icon', 'id',
                  'natural')


class ItemSemiFullSerializer(serializers.ModelSerializer):
    is_inside = serializers.SerializerMethodField()
    areas = serializers.SerializerMethodField()
    size = serializers.SerializerMethodField()

    class Meta:
        model = models.Item
        fields = ('current_name', 'equipped', 'equipable', 'icon', 'id',
                  'natural', 'is_inside', 'areas', 'description', 'size')

    def get_is_inside(self, obj):
        if obj.inside:
            return obj.inside.current_name
        return ""

    def get_areas(self, obj):
        return ", ".join([str(a.area) for a in obj.character_areas.all()])

    def get_size(self, obj):
        return str(obj.size)


class ItemFullSerializer(ItemSemiFullSerializer):
    capacity = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()

    class Meta:
        model = models.Item
        fields = ('current_name', 'equipped', 'equipable', 'icon', 'id',
                  'natural', 'is_inside', 'areas', 'description', 'size',
                  'capacity', 'image')

    def get_capacity(self, obj):
        if not obj.contain_capacity:
            return ""
        space_taken = obj.space_taken
        if int(space_taken) == space_taken:
            space_taken = int(space_taken)
        return "{}/{}".format(space_taken, obj.contain_capacity)

    def get_image(self, obj):
        if not obj.image:
            return ""
        return obj.image.url
