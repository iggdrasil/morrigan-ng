#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2016  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from morrigan_common.models import Ability, DamageType, Effect, \
    GenericType, BodyArea, Technology, Material, Size, GenericSlugType, \
    SlugManager
from morrigan_geography.models import Location


class ItemType(GenericSlugType):
    objects = SlugManager()


"""
Item, weapon, armor, etc. have a generic and a "real" definition.
Generic make it possible to create a kind of template to make
creation of many items easy.
Once created each item can have his attributes overloaded.
"""


class AbstractGeneric(models.Model):
    name = models.TextField(_("Name"), blank=True, null=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        abstract = True


class AbstractItem(GenericType):
    type = models.ForeignKey(ItemType, on_delete=models.CASCADE)
    price = models.IntegerField(_("Price"), null=True, blank=True)
    effect = models.ForeignKey(
        Effect, null=True, blank=True, on_delete=models.SET_NULL,
        related_name="%(class)s_items")
    equipped_effect = models.ForeignKey(
        Effect, null=True, blank=True,
        related_name="%(class)s_equipped_items", on_delete=models.SET_NULL)
    technology = models.ForeignKey(Technology, null=True, blank=True,
                                   on_delete=models.SET_NULL)
    material = models.ForeignKey(Material, null=True, blank=True,
                                 on_delete=models.SET_NULL)
    visible = models.BooleanField(_("Visible"), default=True)
    icon = models.CharField(
        _('Icon name'), blank=True, null=True, max_length=50,
        help_text=_('Class name of the icon'))
    contain_capacity = models.IntegerField(
        _("Contain capacity"), default=0,
        help_text=_("Relevant only for bags: 0 means that this item is not a "
                    "bag."))
    bag_reduction_factor = models.FloatField(
        _("Bag reduction factor"), default=1,
        help_text=_("Holding something inside a bag is easier. This factor "
                    "allow to express that. 1 for not reduction, 0 for a "
                    "magical bag that remove the size of everything. "
                    "A Standard value could be 0.8.")
    )
    size = models.ForeignKey(Size, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class GenericItem(AbstractItem):
    slug = models.SlugField(_("Slug"), unique=True)
    generic_armor = models.ForeignKey(
        'morrigan_inventory.GenericArmor', blank=True, null=True,
        on_delete=models.CASCADE)
    generic_weapon = models.ForeignKey(
        'morrigan_inventory.GenericWeapon', blank=True, null=True,
        on_delete=models.CASCADE)
    generic_charger = models.ForeignKey(
        'morrigan_inventory.GenericCharger', blank=True, null=True,
        on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Generic item")
        verbose_name_plural = _("Generic items")

    @property
    def equipable(self):
        return self.generic_armor is not None or self.generic_weapon is not None

    def create_item(self):
        dct = {'generic_item': self}
        for field in AbstractItem._meta.get_fields():
            if field.name == 'id':
                continue
            dct[field.name] = getattr(self, field.name)
        item = Item.objects.create(**dct)
        for gen_field, gen_model, model in (
                ('generic_armor', GenericArmor, Armor),
                ('generic_weapon', GenericWeapon, Weapon),
                ('generic_charger', GenericCharger, Charger),):
            gen = getattr(self, gen_field)
            if not gen:
                continue
            dct = {'item': item}
            m2ms = {}
            areas = []
            for field in gen_model._meta.get_fields():
                if field.name in ('id', 'name') or \
                        field.name.startswith('generic'):
                    continue
                if field.name == 'areas':
                    areas = list(getattr(gen, field.name).all())
                elif field.many_to_many:
                    m2ms[field.name] = getattr(gen, field.name).all()
                else:
                    dct[field.name] = getattr(gen, field.name)
            new = model.objects.create(**dct)
            for field in m2ms:
                for val in m2ms[field]:
                    getattr(new, field).add(val)
            for area in areas:
                ArmorArea.objects.create(
                    armor=new, area=area.area, place_taken=area.place_taken)
        return item


class Item(AbstractItem):
    """
    An item must be:
    * worn by a character -> have character_areas or is natural
    * inside a location -> location
    * inside a "bag" (another item)
    Of course one at a time.

    An item that can contains other items (a "bag") has a contain capacity.
    0 capacity means that it is not a bag.

    A bag have a factor that allow a character to carry more that the
    bag contains.

    Each item have a size (that can be equal to 0) that is used to
    figure the place taken by the object (inside the bag or to be carried
    by the user before "overload").
    For simplicity weight and volume are mixed in the same variable: size.

    Warning: the place taken by an item in a bag is not the same as the
    place taken on the body (managed by ArmorArea).
    """
    generic_item = models.ForeignKey(GenericItem, on_delete=models.CASCADE)
    halted = models.NullBooleanField(_("Halted"), null=True, blank=True)
    natural = models.BooleanField(_("Natural"), default=False)
    character_areas = models.ManyToManyField(
        'morrigan_characters.ShapeArea', blank=True,
        related_name='items')
    location = models.ForeignKey(Location, null=True, blank=True,
                                 related_name='items',
                                 on_delete=models.SET_NULL)
    inside = models.ForeignKey("Item", null=True, blank=True,
                               related_name='contains',
                               on_delete=models.SET_NULL)
    equipped = models.BooleanField(_("Equipped"), default=False)
    image = models.ImageField(_("Image"), blank=True, null=True)

    class Meta:
        verbose_name = _("Item")
        verbose_name_plural = _("Items")

    @property
    def equipable(self):
        return self.generic_item.equipable

    @property
    def full_description(self):
        if not settings.MORRIGAN_TRUSTED_USER:
            return self.current_name + "- " + self.current_description
        desc = "<div class='log-inner'>"
        if self.image:
            desc += "<span class='log-inner-image'><img src='{}'/>" \
                    "</span>".format(self.image.url)
        desc += """<span class="log-inner-desc"><i class={}></i> {} - {}</span>
""".format(self.current_icon, self.current_name,self.description)
        desc += "</div><hr class='spacer'>"
        return desc

    @property
    def current_name(self):
        if self.name:
            return self.name
        return self.generic_item.name

    @property
    def current_description(self):
        if self.description:
            return self.description
        return self.generic_item.description

    @property
    def current_icon(self):
        return self.generic_item.icon

    @property
    def equipped_label(self):
        if not self.equipped:
            return str(self)
        areas = " ; ".join(str(ca.area).lower()
                           for ca in self.character_areas.all())
        return "{} ({})".format(self, areas)

    def get_owner(self, parents=None):
        if not parents:
            parents = []
        if self in parents: # recursion
            return None
        if self.inside:
            parents.append(self)
            return self.inside.get_owner(parents)
        if self.location:
            return self.location
        if self.character_areas.count():
            return self.character_areas.all()[0].shape

    def character(self):
        if self.character_areas.count():
            return self.character_areas.all()[0].shape.character.current_name

    def __str__(self):
        name = self.current_name
        owner = self.get_owner()
        if owner:
            name = "{} - {}".format(owner, name)
        return name

    @property
    def space_taken(self):
        if not self.contain_capacity:
            return 0
        return round(sum(
            [item['size__index'] for item in self.contains.values(
                'size__index').all()]
        ) * self.bag_reduction_factor, 1)

    @property
    def capacity_label(self):
        if not self.contain_capacity:
            return "{} (0)".format(self)
        label = "{} ({}/{}".format(
            self, self.space_taken, self.contain_capacity)
        if self.bag_reduction_factor != 1:
            label += " - {} % {})".format(
                int((1 - self.bag_reduction_factor) * 100),
                str(_("reduction"))
            )
        else:
            label += ")"
        return label

    @property
    def available_space(self):
        if not self.contain_capacity:
            return 0
        if self.space_taken >= self.contain_capacity:
            return 0
        return self.contain_capacity - self.space_taken

    def give_item(self, character):
        """
        Give this item to a character
        """
        if self.default_equip(character):
            return True
        q_bag = character.items.filter(contain_capacity__gt=self.size.index)
        # inside a bag
        for bag in q_bag.all():
            if bag.available_space >= self.size.index:
                self.inside = bag
                self.location = None
                self.character_areas.clear()
                self.save()
                return True

    def default_equip(self, character):
        """
        Put this item in character "hands" (default area)
        """
        q = character.current_shape.areas.filter(is_default=True)
        if not q.count():
            return
        default_area = q.all()[0]
        place_available = default_area.place_available - \
                          default_area.place_taken
        if place_available <= self.size.index:
            return False
        self.character_areas.clear()
        default_area.items.add(self)
        self.location = None
        self.save()
        return True


class Safe(models.Model):
    """
    A safe is a bag that can be closed. Shops are kind of specialized safes.
    """
    item = models.OneToOneField(Item, related_name='safes',
                                on_delete=models.CASCADE)
    associated_key = models.ForeignKey(GenericItem, null=True, blank=True,
                                       on_delete=models.SET_NULL)
    size = models.IntegerField(_("Size"))
    opened = models.BooleanField(_("Opened"), default=False)


class Currency(GenericType):
    index = models.IntegerField(default=1)


class ItemSafe(models.Model):
    """
    Items inside the safe
    """
    safe = models.ForeignKey(Safe, related_name='items',
                             on_delete=models.CASCADE)
    item = models.ForeignKey(Item, related_name='inside_a_safe',
                             on_delete=models.CASCADE)
    price = models.IntegerField(_("Price (for shops)"), null=True, blank=True)
    currency = models.ForeignKey(
        Currency, null=True, blank=True, on_delete=models.SET_NULL,
        verbose_name=_("Currency associated to price (for shops)"))


class Robot(models.Model):
    name = models.TextField(_("Name"))


class RobotAbility(models.Model):
    robot = models.ForeignKey("Robot", on_delete=models.CASCADE)
    ability = models.ForeignKey(Ability, on_delete=models.CASCADE)
    score = models.IntegerField(_("Score"), default=0)


class Shop(models.Model):
    """
    A shop can be owned by a robot (automatic shops) or characters.
    """
    safe = models.OneToOneField('Safe', on_delete=models.CASCADE)
    name = models.TextField(_("Name"))
    description = models.TextField(_("Description"), null=True, blank=True)
    owner = models.ForeignKey('morrigan_characters.Character', null=True,
                              blank=True, on_delete=models.SET_NULL)
    robot = models.ForeignKey(Robot, null=True, blank=True,
                              on_delete=models.SET_NULL)
    opened = models.BooleanField(_("Opened"), default=False)


class BuyGenericItem(models.Model):
    """
    A shop can automatically buy generic items
    """
    generic_item = models.ForeignKey(GenericItem, on_delete=models.CASCADE)
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    price = models.IntegerField(_("Price (for shops)"), null=True, blank=True)
    currency = models.ForeignKey(
        Currency, null=True, blank=True, on_delete=models.SET_NULL,
        verbose_name=_("Currency associated to price (for shops)"))
    max_number = models.IntegerField(_("Max number of item the buyer accept"),
                                     null=True, blank=True)


class AbstractWeapon(models.Model):
    damage_type = models.ForeignKey(DamageType, on_delete=models.CASCADE)
    effect = models.ForeignKey(Effect, null=True, blank=True,
                               on_delete=models.SET_NULL)
    weapon_range = models.IntegerField(_("Range"), null=True, blank=True)
    min_damage = models.IntegerField(_("Minimum damage"), default=0)
    max_damage = models.IntegerField(_("Maximum damage"), default=0)
    two_hands = models.BooleanField(_("Two hands"), default=False)
    ammo_types = models.ManyToManyField('AmmoType', blank=True)
    ability_1 = models.ForeignKey(
        Ability, related_name="%(class)s_weapon_primary",
        on_delete=models.CASCADE,
        verbose_name=_("Ability (1)"))
    ability_2 = models.ForeignKey(
        Ability, related_name='%(class)s_weapon_secondary',
        on_delete=models.SET_NULL,
        verbose_name=_("Ability (2)"), null=True, blank=True)
    skill_1 = models.ForeignKey(Ability, verbose_name=_("Skill (1)"),
                                null=True, blank=True,
                                on_delete=models.SET_NULL,
                                related_name='%(class)s_skill_primary')
    skill_2 = models.ForeignKey(Ability, verbose_name=_("Skill (2)"),
                                on_delete=models.SET_NULL,
                                related_name='%(class)s_skill_secondary',
                                null=True, blank=True)
    attack_bonus = models.IntegerField(_("Attack bonus"), default=0)
    defense_bonus = models.IntegerField(_("Defense bonus"), default=0)

    class Meta:
        abstract = True


class GenericWeapon(AbstractGeneric, AbstractWeapon):
    class Meta:
        verbose_name = _("Generic weapon")
        verbose_name_plural = _("Generic weapons")


class Weapon(AbstractWeapon):
    item = models.ForeignKey("Item", related_name='weapons',
                             on_delete=models.CASCADE)
    charger = models.ForeignKey("Charger", null=True, blank=True,
                                on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Weapon")
        verbose_name_plural = _("Weapons")


class AbstractArmor(models.Model):
    """
    An "armor" is something that can be equipped by the character.
    """
    defense_bonus = models.IntegerField(_("Defense bonus"), default=0)

    class Meta:
        abstract = True

    def area_names(self):
        return " ; ".join(["{} ({})".format(area.area, area.place_taken)
                           for area in self.areas.all()])


class GenericArmor(AbstractGeneric, AbstractArmor):
    class Meta:
        verbose_name = _("Generic armor")
        verbose_name_plural = _("Generic armors")


class Armor(AbstractArmor):
    item = models.ForeignKey("Item", related_name='armors',
                             on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Armor")
        verbose_name_plural = _("Armors")

    @property
    def equipable(self):
        return True

    @property
    def name(self):
        return str(self.item)

    def equip(self, character):
        """
        Equip an armor.
        """
        if not character.can_equip(self.item):
            return False
        character_areas = dict(
            [(shape_area.area.slug, shape_area)
             for shape_area in character.current_shape.areas.all()]
        )
        character_areas_done = []
        for armor_area in self.areas.all():
            if armor_area.area.slug not in character_areas:
                # database inconsistency...
                for character_area in character_areas_done:
                    character_area.items.remove(self.item)
                return False
            character_areas[armor_area.area.slug].items.add(self.item)
            character_areas_done.append(character_areas[armor_area.area.slug])
        self.item.location = None
        self.item.inside = None
        self.item.save()
        return True

    def unequip(self, character):
        """
        Unequip an armor.
        """
        if not character.location:
            # no place to drop the item
            return False

        for character_area in self.item.character_areas.all():
            character_area.items.remove(self.item)
        self.item.location = character.location
        self.item.save()
        return True

    def __str__(self):
        return str(self.item)


class AbstractArmorArea(models.Model):
    """
    Area covered by the armor.
    """
    area = models.ForeignKey(BodyArea, related_name='+',
                             on_delete=models.CASCADE)
    place_taken = models.IntegerField(_("Place taken"), default=100)

    class Meta:
        abstract = True


class GenericArmorArea(AbstractArmorArea):
    generic_armor = models.ForeignKey("GenericArmor", related_name='areas',
                                      on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Generic armor")
        verbose_name_plural = _("Generic armors")


class ArmorArea(AbstractArmorArea):
    armor = models.ForeignKey("Armor", related_name='areas',
                              on_delete=models.CASCADE)


class AbstractArmorDamage(models.Model):
    damage_type = models.ForeignKey(DamageType, on_delete=models.CASCADE)
    min_absorption = models.IntegerField(_("Min absorption"), default=0)
    max_absorption = models.IntegerField(_("Max absorption"), default=0)

    class Meta:
        abstract = True


class GenericArmorDamage(AbstractArmorDamage):
    generic_armor = models.ForeignKey("GenericArmor", on_delete=models.CASCADE)


class ArmorDamage(AbstractArmorDamage):
    item = models.ForeignKey("Item", related_name='armor_damages',
                             on_delete=models.CASCADE)
    armor = models.ForeignKey("Armor", on_delete=models.CASCADE)


class AmmoType(GenericType):
    class Meta:
        verbose_name = _("Type: Ammunition")
        verbose_name_plural = _("Types: Ammunition")


class AbstractCharger(models.Model):
    ammo_types = models.ManyToManyField(AmmoType)
    max_number = models.IntegerField(_("Max number"), default=1)

    class Meta:
        abstract = True


class GenericCharger(AbstractGeneric, AbstractCharger):
    pass


class Charger(AbstractCharger):
    item = models.ForeignKey("Item", related_name='chargers',
                             on_delete=models.CASCADE)
    ammo_used = models.BooleanField(_("Ammo used"), default=0)
