// to be defined by the main page
//var getMenuUrl;
//var getEditCharacter;
//var getActionReadUrl;
//var getActionLogUrl;
//var getActionLogIconsUrl;
//var getActionLogChooserUrl;
//var getEditActionlog;
//var getHiddenDocumentUrl;
//var getSelectCharacterMenuUrl;
//var getCharacterSheetUrl;
//var getCharactersSheetUrl;
//var getDocumentUrl;
//var getLocationUrl;
//var getShapeUrl;
//var getActionsUrl;
var getMapUrl;
var getLiveMapUrl;
var getStatusUrl;
//var getInitiativeUrl;
//var postNote;
//var formChangeAudioUrl;
//var formQuickEditUrl;

//var baseDoActionUrl;
//var baseActionsUrl;
//var baseSetLogUrl;
//var setLiveMapPositionUrl;
/*
var i18n_mutual_trust = "Mutual trust";
var i18n_mutual_temp_trust = "Mutual temporary trust";
var i18n_trust = "Trusted by you";
var i18n_temp_trust = "Temporary trusted by you";
var i18n_trust_you = "Trusting you";
var i18n_temp_trust_you = "Temporary trusting you";
var i18n_documents = "Documents";
/*
var i18n_filter_unread = "To be read";
var i18n_filter_important = "Important";
var i18n_filter_hidden = "Hidden";
*/
//var i18n_people = "People";
//var i18n_name = "Name";
//var i18n_klass = "Classe";
//var i18n_notes = "Notes";
//var i18n_edit = "Edit";
//var i18n_read = "Reading";
//var i18n_send = "Send";
//var i18n_form_saved = "Form saved";
//var i18n_form_save_failed = "Error while saving";
//var i18n_along = "Along with:";
//var i18n_image_added = "Image added to your history book";
//var i18n_desc_added = "Description added to your history book";
//var i18n_history_book = "Write in your history book";
//var i18n_map_set_position = "Set position";


//var current_character;
//var rp_mode;
//var is_game_master = false;
//var current_action_log = "";
//var apIcon;
//if (!apIcon) apIcon = 'fa fa-hand-paper-o';
//var fpIcon;
//if (!fpIcon) fpIcon = 'ra ra-beer';
//var hpIcon;
//if (!hpIcon) hpIcon = 'ra ra-hearts';
//var mpIcon;
//if (!mpIcon) mpIcon = 'fa fa-magic';

var longPollInterval = 60000;
var mediumPollInterval = 30000;
var shortPollInterval = 10000;

//var documentList = new Array();
//var documentListHidden = new Array();
//var selectedCharacters = new Array();
//var selectedItems = new Array();
/*
var selectedLocationItems = new Array();
var selectedOwnItems = new Array();
var selectedEquippedItems = new Array();
var selectedNonEquippedItems = new Array();
var selectedEquipableItems = new Array();
var selectedNonEquipableItems = new Array();
var selectedNaturalItems = new Array();
var selectedNonNaturalItems = new Array();
*/
//var date_obj = new Date();
//var time_key = date_obj.getTime();
//var can_edit_actionlog;
//var display_location;


/*
var getCookie = function(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
          var cookie = jQuery.trim(cookies[i]);
          if (cookie.substring(0, name.length + 1) === (name + '=')) {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
          }
      }
  }
  return cookieValue;
}

var ajaxGet = function(url, handler, context) {
    $.ajax({
        url: url,
        success: handler,
        context: context});
}

var ajaxPost = function(url, data, handler) {
	var csrftoken = getCookie('csrftoken');
	data["_token"] = csrftoken;
	$.ajax({
	  type: "POST",
      url: url,
      data: data,
	  datatype: "json",
	  headers: {
        'X-CSRFTOKEN': csrftoken
      },
      success: handler
    });
}

var EventManager = function() {
    var items = {};
    this.registerItem = function(name, item){
        items[name] = item;
    },
    this.registerListItem = function(name, key, item){
        if (!items[name]){
            items[name] = {};
        }
        items[name][key] = item;
    },
    this.unregisterItem = function(name){
        delete items[name];
    },
    this.unregisterListItem = function(name, key){
        delete items[name][key];
    },
    this.setState = function(item, newState){
        if (items[item]){
            items[item].setState(newState);
        }
    }
    this.setListState = function(name, newState){
        if (items[name]){
            for (var key in items[name]){
                items[name][key].setState(newState);
            }
        }
    }
    this.reload = function(item){
        if (items[item]){
            items[item].loadFromServer();
        }
    }
    this.reloadList = function(item){
        if (items[item]){
            for (var key in items[item]){
                items[item][key].loadFromServer();
            }
        }
    }
};

var eventManager = new EventManager();

*/

var loadFromServer = {
  loadFromServer: function(callback) {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      data:this.state.get_args,
      success: function(data) {
        this.setState({data: data},
                      callback);
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  componentDidMount: function() {
    this.loadFromServer();
  },
  componentDidUpdate: function(){
    if (this.props.extra_callback) this.props.extra_callback();
  }
}

var loadWithPollingMethods = {
  loadFromServer: function(callback) {
  	if (this.state.initialized && this.props.target
  		&& !$(this.props.target).is(":visible")){
  		return
  	}
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      data:this.state.get_args,
      success: function(data) {
  		this.state.initialized = true;
        this.setState({data: data},
                      callback);
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  componentDidMount: function() {
    this.loadFromServer();
    setInterval(this.loadFromServer, this.props.pollInterval);
  }
}

var loadDynamicWithPollingMethods = {
  loadFromServer: function(callback) {
  	if (this.state.initialized && this.props.target
  		&& !$(this.props.target).is(":visible")){
  		return
  	}
    $.ajax({
      url: this.props.url(),
      dataType: 'json',
      cache: false,
      data:this.state.get_args,
      success: function(data) {
  		this.state.initialized = true;
        this.setState({data: data},
                      callback);
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url(), status, err.toString());
      }.bind(this)
    });
  },
  componentDidMount: function() {
    this.loadFromServer();
    setInterval(this.loadFromServer, this.props.pollInterval);
  }
}

/*
var unselect_characters = function(){
    var selectedCharacters = new Array();
    eventManager.setListState('character-list', {'selected': false});
}

var unselect_items = function(){
    selectedItems = new Array();
    selectedLocationItems = new Array();
    selectedOwnItems = new Array();
    selectedEquippedItems = new Array();
    selectedNonEquippedItems = new Array();
    selectedEquipableItems = new Array();
    selectedNonEquipableItems = new Array();
    selectedNaturalItems = new Array();
    selectedNonNaturalItems = new Array();
    // changes could have been made on items - change the time key
    time_key = date_obj.getTime();
    eventManager.setListState('item-list', {'selected': false});
}

var clean_action_list = function(){
    unselect_items();
    unselect_characters();
    var url = baseActionsUrl;
    eventManager.setState('action-list', {'actions':[]});
}
*/
/*
var refresh_edit_modal = function(content){
    $('.modal-edit .modal-footer').show();
    $('#edit-form .modal-body').html(content);
    $('.modal-edit').modal('handleUpdate');
}
*/
// badge

/*
var Badge = React.createClass({
  getInitialState: function() {
    return {value:this.props.initialValue};
  },
  render: function(){
    return(
        <span className='badge badge-light'>{this.state.value}</span>
    )
  },
  componentDidMount: function() {
    eventManager.registerItem(this.props.id, this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem(this.props.id);
  }
});
*/

/*
// progress bar
var ProgressBar = React.createClass({
  render: function() {
    var progress_classname = "progress-bar";
    if (this.props.progress_classname){
        progress_classname += " " + this.props.progress_classname;
    }
    return (
            <span className={this.props.classname}>
            <i className={this.props.icon}></i>
            <div className='progress'>
                <div className={progress_classname} role="progressbar" aria-valuenow={this.props.percent} aria-valuemin="0" aria-valuemax="100" style={{width: this.props.percent+'%'}}>{this.props.percent} %
                </div>
            </div>
            </span>
    );
  }
});

// character status
var StatusNode = React.createClass({
  mixins: [loadWithPollingMethods],
  getInitialState: function() {
    return {data: {}, get_args:{}};
  },
  componentDidMount: function() {
    eventManager.registerItem('status-div', this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem('status-div');
  },
  render: function() {
    var ap = (<span className='character-ap d-flex'><i className={apIcon}></i>
    			<span className="d-flex pl-1">{this.state.data.ap}</span><span className='max-score'>{this.state.data.max_ap}</span></span>);
    var fp = '';
    var mp = '';
    var hp = '';
    if (rp_mode){
        fp = (
            <ProgressBar classname='character-fp d-flex' icon={fpIcon}
                         progress_classname='progress-bar-success'
                         percent={this.state.data.fp_percent} />
        );
        hp = (
            <ProgressBar classname='character-hp d-flex' icon={hpIcon}
                         progress_classname='progress-bar-danger'
                         percent={this.state.data.hp_percent} />
        );
        if (this.state.data.max_mp){
            mp = (
            <ProgressBar classname='character-mp d-flex' icon={mpIcon}
                         progress_classname='progress-bar-info'
                         percent={this.state.data.mp_percent} />
            );
        }
    } else {
        fp = (<span className='character-fp d-flex'><i className={fpIcon}></i>
        	  <span>{this.state.data.fp}</span><span className='max-score'>{this.state.data.max_fp}</span></span>);
        hp = (<span className='character-hp d-flex'><i className={hpIcon}></i>
           	  <span>{this.state.data.hp}</span><span className='max-score'>{this.state.data.max_hp}</span></span>);
        if (this.state.data.max_mp){
            mp = (<span className='character-mp d-flex'><i className={mpIcon}></i>
            <span>{this.state.data.mp}</span><span className='max-score'>{this.state.data.max_mp}</span></span>);
        }
    }
    var classname = 'd-flex';
    return (
      <div id="character-status" className={classname}>
        {ap}
        {hp}
        {fp}
        {mp}
      </div>
    );
  }
});

ReactDOM.render(
  <StatusNode url={getStatusUrl} pollInterval={longPollInterval} />,
  document.getElementById('status-div')
);
*/

// audio
/*
var AudioPlayer = React.createClass({
  render: function() {
    var audio_id = "audio-" + this.props.slug;
    return (
      <div className="audio-player">
          <audio id={audio_id}
            src={this.props.url}
            type={this.props.type}
            loop={this.props.loop}
            data-play={this.props.autoplay}
            preload>
          </audio>
      </div>
    );
  }

});

var AudioPlayerForm = React.createClass({
  handleSubmit: function(e) {
    e.preventDefault();
    var form = $("#audio-player-form");
    $.ajax({
      url: formChangeAudioUrl,
      type: "POST",
      data: new FormData(form[0]),
      contentType: false,
      cache: false,
      processData:false,
      success: function(data) {
        $("#audio-player-form-msg").html(
          "<span class='text-success'>" +
          i18n_form_saved
          + "</span>");
        setTimeout(function(){
          $("#audio-player-form-msg").html("");
        }, 4000);
      },
      error: function(xhr, status, err) {
        $("#audio-player-form-msg").html(
          "<span class='text-danger'>" +
          i18n_form_save_failed
          + "</span>");
        setTimeout(function(){
          $("#audio-player-form-msg").html("");
        }, 4000);
      }
    });

  },

  render: function() {
    var audio_fields = this.props.audiolist.map(function(audio){
      var play_name = "audio-play-" + audio.slug;
      var loop_name = "audio-loop-" + audio.slug;
      var base_name = "audio-" + audio.audiotype + "-" + audio.slug;
      return (
        <tr key={audio.slug}>
          <th><p>{audio.name}</p></th>
          <td className="pl-2">
            <input type="hidden"
              name={base_name} value="1" />&nbsp;
            <input id={play_name}
              className="check-with-label"
              type="checkbox"
              name={play_name} defaultChecked={audio.autoplay}/>&nbsp;
            <label htmlFor={play_name}>
              <i className="fa fa-play" aria-hidden="true"></i></label>&nbsp; 
            <input id={loop_name}
              className="check-with-label"
              type="checkbox"
              name={loop_name} defaultChecked={audio.loop}/>&nbsp;
            <label htmlFor={loop_name}>
              <i className="fa fa-repeat" aria-hidden="true"></i></label>&nbsp; 
          </td>
        </tr>
      );
    });
    var csrftoken = getCookie('csrftoken');
    return (
      <div className="audio-player-form form">
        <h4>Audio</h4>
        <p id="audio-player-form-msg"></p>
        <form onSubmit={this.handleSubmit} id="audio-player-form">
          <input type="hidden" name="csrfmiddlewaretoken" value={csrftoken} />
          <table>
            <tbody>
              {audio_fields}
              <tr>
                <td><button className="btn btn-primary" type="submit">
                  {i18n_send}
                </button>
                </td>
              </tr>
            </tbody>
          </table>
        </form>
      </div>
    );
  }

});

*/

/*

// selector character menu

var renderDropdown = function (menu) {
	return (
      <a className="dropdown-item" key={menu.url} href={menu.url}>{menu.label}</a>
	)
}

var renderSelectCharacter = function (menu) {
  var menu_icon = "nav-icon";
  if ($.isArray(menu)){
    var subMenu = menu.map(renderDropdown);
    var menu_icon = menu_icon + " " + menu[0].icon;
    var menu_label = menu[0].label;
    var menu_url = menu[0].url;
    var menu_badge = menu[0].badge;
    return (
  <li key={menu_url} className='nav-item dropdown'>
	<button className="btn btn-secondary dropdown-toggle"
	 	type="button" id="dropdownMenuButton" data-toggle="dropdown">
	  <i className={menu_icon}></i>
	  &nbsp;{menu_label} {menu_badge}
	</button>
	<div className="dropdown-menu">
		{subMenu}
    </div>
  </li>
    );
  }
  if (menu.icon){
  	  menu_icon = menu_icon + " " + menu.icon;
  }
  var badge = '';
  if (menu.badge){
    var badge_id = 'badge-' + menu.slug;
    badge = <Badge initialValue={menu.badge} id={badge_id} />
  }
  return (
      <li key={menu.slug} className='nav-item'>
        <a href={menu.url}
           className={menu.selected ? 'active nav-link' : 'nav-link'}>
        	<i className={menu_icon}></i>
        	&nbsp;{menu.label} {badge}</a>
      </li>
  );
}


var SelectCharacterMenu = React.createClass({
  mixins: [loadFromServer],
  getInitialState: function() {
    return {data: {'menu':[]}, get_args:{}};
  },
  render: function() {
    var menuNodes = "";
    if (this.state.data.menu.length){
      menuNodes = this.state.data.menu.map(renderSelectCharacter);
    }
    return (
      <ul className="nav nav-pills" role="menu">
        {menuNodes}
      </ul>
    );
  }
});

ReactDOM.render(
  <SelectCharacterMenu url={getSelectCharacterMenuUrl} />,
  document.getElementById('select-character-menu')
);

*/
// state menu

/*
var changePanel = function(panel){
	clean_action_list();
	$("#menu > ul > li > a").removeClass("active");
	$("#menu-" + panel + " > a").addClass("active");
	if (panel == "location") {
		$("#action-log").hide();
		$("#maps").hide();
		$("#action-log-selector").hide();
		$("#character-sheet").hide();
		$("#characters-sheet").hide();
		$("#location").show();
		if (!display_location){
	  		display_self_actions();
		}
	}
	if (panel == "log") {
		$("#action-log").show();
		$("#action-log-selector").show();
		$("#maps").hide();
		$("#location").hide();
		$("#character-sheet").hide();
		$("#characters-sheet").hide();
	}
	if (panel == "characteristics") {
		$("#character-sheet").show();
		$("#characters-sheet").show();
		$("#maps").hide();
		$("#action-log").hide();
		$("#action-log-selector").hide();
		$("#location").hide();
	}
	if (panel == "map") {
		$("#maps").show();
		$("#action-log").hide();
		$("#action-log-selector").hide();
		$("#location").hide();
		$("#character-sheet").hide();
		$("#characters-sheet").hide();
	}
};

*/
/* NOT USED?
var display_action_log = function(){
	$("#action-log").show();
	$("#action-log-selector").show();
};
*/

/*

var renderMenu = function (menu) {
  var li_id = 'menu-' + menu.slug;
  var menu_icon = "nav-icon";
  if ($.isArray(menu)){
    var subMenu = menu.slice(1).map(renderMenu);
    var menu_icon = menu_icon + " " + menu[0].icon;
    var menu_label = menu[0].label;
    var menu_url = menu[0].url;
    var menu_badge = menu[0].badge;
    return (
  <li key={menu_url} id={li_id} className='nav-item has-treeview'>
	<a href={menu_url} className='nav-link'>
	  <i className={menu_icon}></i>
	  &nbsp;{menu_label} {menu_badge}
	  &nbsp;<i className="fa fa-caret-down" aria-hidden="true"></i>
	</a>
	<ul className="nav nav-treeview">
		{subMenu}
	</ul>
  </li>
    );
  }
  if (menu.icon){
  	  menu_icon = menu_icon + " " + menu.icon;
  }
  var badge = '';
  var css = "nav-item"
  if (menu.badge){
  	css += ' has-badge';
    var badge_id = 'badge-' + menu.slug;
    badge = <Badge initialValue={menu.badge} id={badge_id} />
  }
  var handleClick = function(){
  	changePanel(menu.slug);
  }
  return (
      <li key={menu.slug} id={li_id} className={css}>
        <a href='#' onClick={handleClick}
           className={menu.selected ? 'active nav-link' : 'nav-link'}>
        	<i className={menu_icon}></i>
        	<p>{menu.label} {badge}</p>
		</a>
      </li>
  );
}


var StateMenu = React.createClass({
  mixins: [loadWithPollingMethods],
  getInitialState: function() {
    return {data: {'menu':[]}, get_args:{}};
  },
  render: function() {
    var menuNodes = this.state.data.menu.map(renderMenu);
    return (
      <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu">
        {menuNodes}
      </ul>
    );
  }
});


ReactDOM.render(
  <StateMenu url={getMenuUrl} pollInterval={longPollInterval} />,
  document.getElementById('menu')
);

// document selector

var displayDocument = function (document_slug){
	var document = $(".document-item[data-slug=" + document_slug + "]");
	$("#full-screen img").attr("src", document.attr("data-target"));
	$("#full-screen").show();
};

var renderNestedDocument = function (document) {
  var onClick = function(){
	displayDocument(document.slug);
  }
  if (documentList.indexOf(document.slug) == -1){
    documentList.push(document.slug);
  }
  return (
    <a className="dropdown-item document-item"
      key={document.slug}
      data-target={document.image}
      onClick={onClick}
      data-slug={document.slug}
      href="#">{document.name}</a>
  ); 
}

var renderDocument = function (document) {
  var onClick = function(){
	displayDocument(document.slug);
  }
  if (document.items.length){
    var document_list = document.items.map(renderNestedDocument);
    return (
      <div className="btn-group" role="group"
          key={document.slug}>
        <button id="document-{document.slug}"
          type="button" className="btn btn-secondary dropdown-toggle"
          data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">{document.name}</button>
        <div className="dropdown-menu"
          aria-labelledby="document-{document.slug}">
          {document_list}
        </div>
      </div>
    );
  }
  else {
    if (documentList.indexOf(document.slug) == -1){
      documentList.push(document.slug);
    }
    return (
      <button type="button" className="btn btn-secondary document-item"
          key={document.slug}
          data-target={document.image}
          onClick={onClick}
          data-slug={document.slug}
          href="#">{document.name}</button>
    );
  }
};


var DocumentSelector = React.createClass({
  mixins: [loadWithPollingMethods],
  getInitialState: function() {
    return {data: {'documents':[]}, get_args:{}};
  },
  componentDidMount: function() {
    eventManager.registerItem('document', this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem('document');
  },
  render: function() {
  	documentList = new Array();
  	if (!this.state.data.documents.length){
  		return(<span></span>)
  	}
    var documentNodes = this.state.data.documents.map(renderDocument);
    return (
	<div className="btn-group">
		{documentNodes}
	</div>
    );
  }
});

ReactDOM.render(
  <DocumentSelector url={getDocumentUrl} pollInterval={shortPollInterval} />,
  document.getElementById('document-selector')
);

var renderDocumentHidden = function (document) {
	if (documentListHidden.indexOf(document.slug) == -1){
		documentListHidden.push(document.slug);
	}
	return (
	    <input className="document-item" key={document.slug}
	    	type='hidden'
	    	name="document-{document.slug}"
	    	data-target={document.image}
	    	data-slug={document.slug} />
	);
};

var DocumentHidden = React.createClass({
  mixins: [loadWithPollingMethods],
  componentDidMount: function() {
    eventManager.registerItem('document-hidden', this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem('document-hidden');
  },
  getInitialState: function() {
    return {data: {'documents':[]}, get_args:{}};
  },
  componentDidMount: function() {
    eventManager.registerItem('document-hidden', this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem('document-hidden');
  },
  render: function() {
  	documentListHidden = new Array();
  	if (!this.state.data.documents.length){
  		return(<span></span>)
  	}
    var documentNodes = this.state.data.documents.map(renderDocumentHidden);
    return (
    	<span>{documentNodes}</span>
    );
  }
});

ReactDOM.render(
  <DocumentHidden url={getHiddenDocumentUrl} pollInterval={longPollInterval} />,
  document.getElementById('documents-hidden')
);
*/

// action log

/*
var page_by_block = 5;
var first_label;
var next_label;
var last_label;
var prev_label;
*/
/*
var ActionLogChooserNode = React.createClass({
  getInitialState () {
    //this.handleClick = this.handleClick.bind(this);
    return {};
  },
  handleClick: function(){
    current_action_log = this.props.slug;
    eventManager.setState('action-log-selector', {'active': this.props.slug});
    eventManager.reload('action-log');
  },
  render: function() {
    var classname = "";
    if (this.props.active){
      classname="active";
    }
    return (
      <li onClick={this.handleClick} className={classname}>{this.props.name}</li>
    );
  }
});

var ActionLogChooser = React.createClass({
  mixins: [loadFromServer],
  getInitialState: function() {
    return {
        data: []
    };
  },
  componentDidMount: function() {
    eventManager.registerItem('action-log-selector', this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem('action-log-selector');
  },
  render: function() {
    var actionLogNodes;
    var current_active = this.state.active;
    if (!current_active){
      current_active = "character-" + current_character;
    }
    if (this.state.data.action_logs){
      actionLogNodes = this.state.data.action_logs.map(function(action){
          var active = false;
          if (current_active == action.slug){
            active = true;
          }
          return (<ActionLogChooserNode name={action.name}
                                        active={active}
                                        slug={action.slug}
                                        key={action.slug} />)
      }, this);
    }
    return (
      <div className='action-logs'>
        <ul>
          {actionLogNodes}
        </ul>
      </div>
    );
  }
});

if ($('#action-log-selector').length){
  ReactDOM.render(
    <ActionLogChooser url={getActionLogChooserUrl} />,
    document.getElementById('action-log-selector')
  );
}

/*
var ActionLogIcon = React.createClass({
  getInitialState () {
    return {};
  },
  handleClick: function(event){
  	this.state.active = ! this.state.active;
  	if (this.state.active){
  		this.props.actionLog.handleFilter(
  			"", "--icon-" + this.props.icon + "--", event);
  	} else {
  		this.props.actionLog.handleFilter(
  			"--icon-" + this.props.icon + "--", "", event);
  	}
	eventManager.reload('action-log-icons');
  },
  render: function() {
    var klass = "disabled";
    if (this.state.active) klass = "";
    return (
      <li onClick={this.handleClick}>
      	<a className={klass} href="#">
      	<i className={this.props.icon} /></a></li>
    );
  }
});

/*
var ActionLogIcons = React.createClass({
  mixins: [loadFromServer],
  getInitialState: function() {
    return {
        data: []
    };
  },
  componentDidMount: function() {
    eventManager.registerItem('action-log-icons', this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem('action-log-icons');
  },
  render: function() {
    var actionLogIcons;
    if (this.state.data.icons){
      actionLogIcons = this.state.data.icons.map(function(icon){
          return (<ActionLogIcon icon={icon} key={icon}
		 				         actionLog={this.props.actionLog}/>)
      }, this);
    }
    return (
      <ul className='action-logs-icons filter-logs'>
          {actionLogIcons}
	  </ul>
    );
  }
});


var ActionLogNode = React.createClass({
    propTypes: {
        initialState:React.PropTypes.string,
        baseSetLogUrl:React.PropTypes.string,
        id:React.PropTypes.number,
        content:React.PropTypes.object,
        //loadFromServer:React.PropTypes.function,
        humanDate:React.PropTypes.string,
        icon:React.PropTypes.string
    },
    getInitialState: function() {
        return {state: this.props.initialState};
    },
  	handleEdit: function(event){
      event.preventDefault();
      var url = getEditActionlog + this.props.id + "/";
      ajaxGet(url, function(content){
          refresh_edit_modal(content);
          $('.modal-edit').modal('show');
      }, this);
  	},
    handleSetLog: function(state, event){
        event.preventDefault();
        var url = baseSetLogUrl + this.props.id + "/";
        url += state + "/";
        ajaxGet(url, function(){
            this.setState({'state':state},
                this.props.loadFromServer
            );
        }, this);
    },
    render: function(){
      var klass = "bs-callout ";
      var readklass = "disabled";
      var readclick = this.handleSetLog.bind(this, 'U');
      var importantklass = "disabled";
      var importantclick = this.handleSetLog.bind(this, 'I');
      var hideklass = "disabled";
      var hideclick = this.handleSetLog.bind(this, 'H');
      if (this.state.state == 'U'){
        klass += 'bs-callout-success';
        readklass= "";
        readclick = this.handleSetLog.bind(this, 'R');
      } else if (this.state.state == 'I') {
        klass += 'bs-callout-warning';
        importantklass = "";
        importantclick = this.handleSetLog.bind(this, 'R');
      } else if (this.state.state == 'H') {
        klass += 'bs-callout-hide';
        hideklass = "";
        hideclick = this.handleSetLog.bind(this, 'R');
      } else {
        klass += "bs-callout-default";
      }

      var editClick = this.handleEdit;

      var edit_button = "";
      if (can_edit_actionlog){
    	edit_button = (
            <li><a title={i18n_edit} className="disabled"
  		          onClick={editClick} href='#'><i className='fa fa-pencil'></i></a></li>
    	);
      }

      var edit = (
        <ul className='log-actions'>
			{edit_button}
            <li><a title={i18n_filter_unread} className={readklass} onClick={readclick} href='#'><i className='fa fa-check'></i></a></li>
            <li><a title={i18n_filter_important} className={importantklass} onClick={importantclick} href='#'><i className='fa fa-exclamation-triangle'></i></a></li>
            <li><a title={i18n_filter_hidden} className={hideklass} onClick={hideclick} href='#'><i className='fa fa-ban'></i></a></li>
        </ul>
      );

      if (is_game_master && current_action_log &&
            ("character-" + current_character) != current_action_log){
          edit = "";
      }

      return (
          <li className={klass}>
            {edit}
            <div className='log-image'>
                {this.props.content.image ? <img src={this.props.content.image} /> : ''}
                {this.props.icon ? <i className={this.props.icon}></i> : ''}
            </div>
            <div className="log-header">
                {this.props.content.from ? <span className="log-from">{this.props.content.from}</span> : ''}
                {this.props.content.to ? <span className="log-to">{this.props.content.to}</span> : ''}
				{this.props.content.name ? <span className="log-name">{this.props.content.name}</span> : ''}
				{this.props.humanDate ? <span className="log-date">{this.props.humanDate}</span> : ''}
            </div>
            <div className="log-content" dangerouslySetInnerHTML={{ __html: this.props.content.description}}></div>
          </li>
      );

    }
});

var ActionLog = React.createClass({
  mixins: [loadDynamicWithPollingMethods],
  getInitialState: function() {
    return {data:{"number_of_pages":0, "page":0, "unread":0, "logs":[]},
            get_args:{}};
  },
  reloadFromServer: function(){
    this.loadFromServer(function(){
        eventManager.setState('badge-log', {'value':this.state.data.unread});
    });
  },
  handlePageChanged: function(page){
    // page+1: API count as human whereas Pager component don't
    this.setState({get_args:{page:page+1}},
                  this.loadFromServer);
  },
  handleRead: function(event){
      event.preventDefault();
      var url = getActionReadUrl + current_character + "/";
      ajaxGet(url, function(content){
          refresh_edit_modal(content);
          $('.modal-edit .modal-footer').hide();
          $('.modal-edit').modal('show');
      }, this);
  },
  componentDidMount: function() {
    eventManager.registerItem('action-log', this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem('action-log');
  },
  filters:[],
  handleFilter: function(current_state, new_state, event){
    event.preventDefault();
    if (this.filters.indexOf(current_state) != -1){
        this.filters.pop(this.filters.indexOf(current_state));
    }
    if (new_state && this.filters.indexOf(new_state) == -1){
        this.filters.push(new_state);
    }
    var filter_arg = '';
    for (var k in this.filters) filter_arg += this.filters[k];
    var args = {'filters': filter_arg}
    this.setState({get_args:args}, this.loadFromServer);
  },
  render: function() {
    var logNodes = this.state.data.logs.map(function(log){
        return (<ActionLogNode content={log.content}
                               id={log.id}
                               key={log.id}
                               icon={log.icon}
                               humanDate={log.human_date}
                               loadFromServer={this.reloadFromServer}
                               initialState={log.state} />)
    }, this);
    // page - 1: API count as human whereas Pager component don't
    var current_page = this.state.data.page - 1;
    var number_of_pages = this.state.data.number_of_pages;

    // filters
    var filter_read = this.handleFilter.bind(this, '', 'U');
    var filter_read_klass = "disabled";
    if (this.filters.indexOf('U') != -1){
        filter_read = this.handleFilter.bind(this, 'U', '');
        filter_read_klass = "";
    }
    var filter_important = this.handleFilter.bind(this, '', 'I');
    var filter_important_klass = "disabled";
    if (this.filters.indexOf('I') != -1){
        filter_important = this.handleFilter.bind(this, 'I', '');
        filter_important_klass = "";
    }
    var filter_hidden = this.handleFilter.bind(this, '', 'H');
    var filter_hidden_klass = "disabled";
    if (this.filters.indexOf('H') != -1){
        filter_hidden = this.handleFilter.bind(this, 'H', '');
        filter_hidden_klass = "";
    }
    var readClick = this.handleRead;

    return (
    <div>
      <div>
		<button className="btn btn-secondary" id="read-book-btn"
			onClick={readClick}>
		  <i className="fa fa-book"></i>&nbsp; {i18n_read}
		</button>
      </div>
	  <div>
		  <Pager current={current_page}
			  total={number_of_pages}
			  visiblePages={page_by_block}
			  onPageChanged={this.handlePageChanged}
			  titles={{
					 first:   first_label,
					 prev:    prev_label,
					 prevSet: "<<<",
					 nextSet: ">>>",
					 next:    next_label,
					 last:    last_label
			  }} />
	  </div>
	  <hr className='spacer'/>
      <div>
		  <ul className="filter-logs">
				<li><a title={i18n_filter_unread} className={filter_read_klass} onClick={filter_read} href='#'><i className='fa fa-check'></i></a></li>
				<li><a title={i18n_filter_important} className={filter_important_klass} onClick={filter_important} href='#'><i className='fa fa-exclamation-triangle'></i></a></li>
				<li><a title={i18n_filter_hidden} className={filter_hidden_klass} onClick={filter_hidden} href='#'><i className='fa fa-ban'></i></a></li>
		  </ul>
		  <ActionLogIcons url={getActionLogIconsUrl} pollInterval={longPollInterval}
		  				  actionLog={this} />
	  </div>
      <ul className="logs">
        {logNodes}
      </ul>
     </div>
    );
  }
});

var get_action_log_url = function(){
  return getActionLogUrl + current_action_log;
}


if ($('#action-log').length){
    ReactDOM.render(
      <ActionLog url={get_action_log_url} pollInterval={shortPollInterval}
	      target='#action-log-selector'/>,
      document.getElementById('action-log')
    );
}
*/

/*

var CharacterDescription = React.createClass({
  mixins: [loadFromServer],
  getInitialState: function() {
    return {data: {}, get_args: {}, key: this.props.url};
  },
  componentDidMount: function() {
    eventManager.registerListItem('character-desc', this.props.url, this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterListItem('character-desc', this.props.url);
  },
  render: function(){
    var itemNodes = '';
    var has_capacity = false;
    if (this.state.data.items){
        itemNodes = this.state.data.items.map(function(item){
            if (item.capacity) has_capacity = true;
            var key = item.id + '-' + time_key;
            return (<ItemFullNode name={item.current_name}
                                  key={key}
                                  id={item.id}
                                  equipped={item.equipped}
                                  equipable={item.equipable}
                                  areas={item.areas}
                                  natural={item.natural}
                                  size={item.size}
                                  capacity={item.capacity}
                                  is_inside={item.is_inside}
                                  image={item.image}
                                  description={item.description}
                                  icon={item.icon} />)
        }, this);
    }
    if (itemNodes != ''){
        if (has_capacity){
            itemNodes = (
              <table className='character-items table-responsive'>
                <tbody>
                <tr>
                  <th colSpan='4'></th>
                  <th>{str_is_inside}</th>
                  <th>{str_equipped}</th>
                  <th>{str_natural}</th>
                  <th>{str_areas}</th>
                  <th>{str_size}</th>
                  <th>{str_capacity}</th>
                </tr>
                {itemNodes}
                </tbody>
              </table>
            )
        } else {
            itemNodes = (
              <table className='character-items'>
                <tbody>
                <tr>
                  <th colSpan='4'></th>
                  <th>{str_is_inside}</th>
                  <th>{str_equipped}</th>
                  <th>{str_natural}</th>
                  <th>{str_areas}</th>
                  <th>{str_size}</th>
                </tr>
                {itemNodes}
                </tbody>
              </table>
            )
        }
    }
    var image = "";
    if (this.state.data.image){
    	image = (
    	<div className='character-full-image'>
        	<img src={this.state.data.image}/>
        </div>
    	)
    }
    return (
      <div className="description">
		{image}
        <div className='character-description'>
        {this.state.data.description}
        </div>
        <div className='character-current-description'>
        {this.state.data.current_description}
        </div>
        <hr className='spacer' />
        {itemNodes}
      </div>
    )
  }
});
*/
/*
var ItemFullNode = React.createClass({
  getInitialState: function() {
    return {selected: false};
  },
  handleSelect: function(event){
      event.preventDefault();
  },
  render: function(){
    var key = this.props.id + '-' + time_key;
    return (
        <tr>
          <td className='item-image'>
             {this.props.image && <img src={this.props.image}/>}
		  </td>
          <td className='item-icon'>
            <ul className='items'>
                <ItemNode name={this.props.name}
                          key={key}
                          id={this.props.id}
                          equipped={this.props.equipped}
                          equipable={this.props.equipable}
                          natural={this.props.natural}
                          size={this.props.size}
                          capacity={this.props.capacity}
                          own={true}
                          icon={this.props.icon} />
            </ul>
          </td>
          <td className='item-name'>{this.props.name}</td>
          <td className='item-description'>{this.props.description}</td>
          <td className='item-inside'>{this.props.is_inside}</td>
          <td className='item-equipped'>{this.props.equipped ? <i className='fa fa-check-circle'></i> : <i className='fa fa-times-circle'></i>}</td>
          <td className='item-natural'>{this.props.natural ? <i className='fa fa-check-circle'></i> : <i className='fa fa-times-circle'></i>}</td>
          <td className='item-areas'>{this.props.areas}</td>
          <td className='item-size'>{this.props.size}</td>
          <td className='item-capacity'>{this.props.capacity}</td>
        </tr>
    );
  }
});
*/
/*
var ItemNode = React.createClass({
  getInitialState: function() {
    return {selected: false};
  },
  componentDidMount: function() {
    eventManager.registerListItem('item-list', this.props.id, this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterListItem('item-list', this.props.id);
  },
  handleSelect: function(event){
      event.preventDefault();

      // action based on items not characters
      unselect_characters();

      if (this.state.selected){ // will be unselect
          var idx = selectedItems.indexOf(this.props.id);
          if (idx >= 0) selectedItems.splice(idx, 1);
          if (this.props.location){
              idx = selectedLocationItems.indexOf(this.props.id);
              if (idx >= 0) selectedLocationItems.splice(idx, 1);
          }
          else if (this.props.character == current_character ||
                   this.props.own){
              idx = selectedOwnItems.indexOf(this.props.id);
              if (idx >= 0) selectedOwnItems.splice(idx, 1);
          }
          if (this.props.equipped){
              idx = selectedEquippedItems.indexOf(this.props.id);
              if (idx >= 0) selectedEquippedItems.splice(idx, 1);
          } else {
              idx = selectedNonEquippedItems.indexOf(this.props.id);
              if (idx >= 0) selectedNonEquippedItems.splice(idx, 1);
          }
          if (this.props.natural){
              idx = selectedNaturalItems.indexOf(this.props.id);
              if (idx >= 0) selectedNaturalItems.splice(idx, 1);
          } else {
              idx = selectedNonNaturalItems.indexOf(this.props.id);
              if (idx >= 0) selectedNonNaturalItems.splice(idx, 1);
          }
          if (this.props.equipable){
              idx = selectedEquipableItems.indexOf(this.props.id);
              if (idx >= 0) selectedEquipableItems.splice(idx, 1);
          } else {
              idx = selectedNonEquipableItems.indexOf(this.props.id);
              if (idx >= 0) selectedNonEquipableItems.splice(idx, 1);
          }
      } else if (selectedItems.indexOf(this.props.id) == -1) {
          selectedItems.push(this.props.id);
          if (this.props.location){
              selectedLocationItems.push(this.props.id);
          }
          else if (this.props.character == current_character || this.props.own){
              selectedOwnItems.push(this.props.id);
          }
          if (this.props.equipped){
              selectedEquippedItems.push(this.props.id);
          }
          else {
              selectedNonEquippedItems.push(this.props.id);
          }
          if (this.props.natural){
              selectedNaturalItems.push(this.props.id);
          }
          else {
              selectedNonNaturalItems.push(this.props.id);
          }
          if (this.props.equipable){
              selectedEquipableItems.push(this.props.id);
          }
          else {
              selectedNonEquipableItems.push(this.props.id);
          }
      }
      this.setState({'selected':!this.state.selected});

      var action_type = '';
      if (selectedOwnItems.length > 0){
          action_type += 'ownitem';
      }
      if (selectedLocationItems.length > 0){
          if(action_type != '') action_type += '-';
          action_type += 'locationitem';
      }
      if (selectedItems.length > (selectedOwnItems.length +
                                  selectedLocationItems.length)){
          if(action_type != '') action_type += '-';
          action_type += 'otheritem';
      }
      if (selectedEquippedItems.length > 0
          && selectedNonEquippedItems.length <= 0 ){
          action_type += '-equippeditem';
      } else if (selectedNonEquippedItems.length > 0
          && selectedEquippedItems.length <= 0 ){
          action_type += '-nonequippeditem';
      }
      if (selectedEquipableItems.length > 0
          && selectedNonEquipableItems.length <= 0 ){
          action_type += '-equipableitem';
      } else if (selectedNonEquipableItems.length > 0
          && selectedEquipableItems.length <= 0 ){
          action_type += '-nonequipableitem';
      }
      if (selectedNaturalItems.length > 0
          && selectedNonNaturalItems.length <= 0 ){
          action_type += '-naturalitem';
      } else if (selectedNonNaturalItems.length > 0
          && selectedNaturalItems.length <= 0 ){
          action_type += '-nonnaturalitem';
      }
      if (selectedItems.length > 1){
          action_type += '-multipleitem';
      }
      var url = baseActionsUrl;
      if (action_type == '') {
          eventManager.setState('action-list', {'actions':[]});
          return;
      }
      url += action_type + "/";
      ajaxGet(url, function(res){
          eventManager.setState('action-list', {'actions':res['actions']});
      }, this);
  },
  render: function(){
    var selectclick = this.handleSelect;
    var selectklass = 'item-check';
    if (! this.state.selected){
        selectklass += ' disabled';
    }
    return (
        <li data-toggle="tooltip" data-placement="bottom" title={this.props.name}>
          <span><a className={selectklass} onClick={selectclick} href='#'><i className={this.props.icon}></i></a></span>
        </li>
    );
  }
});
*/

/*
var CharacterNode = React.createClass({
  getInitialState: function() {
    return {detail: false, selected: false, along: false};
  },
  componentDidMount: function() {
    eventManager.registerListItem('character-list', this.props.slug, this);
    if (current_character ===  this.props.slug && !this.state.selected) this.handleSelect(null);
  },
  componentWillUnmount: function() {
    eventManager.unregisterListItem('character-list', this.props.slug);
  },
  handleShowHideDesc: function(event){
      event.preventDefault();
      this.setState({'detail':!this.state.detail});
  },
  handleShowHideAlong: function(event){
      event.preventDefault();
      this.setState({'along':!this.state.along});
  },
  descNote: function(event){
      event.preventDefault();
	  ajaxPost(
		  postNote,
		  {character_slug: this.props.slug},
		  function(){
            refresh_modal(i18n_desc_added);
            $('.modal-action').modal('show');
        	$('.modal-action .submit').hide();
		  });

  },
  handleSelect: function(event){
	  if (event) event.preventDefault();

      // action based on characters not items
      unselect_items();
      if (this.state.selected){
        idx = selectedCharacters.indexOf(this.props.slug);
        if (idx >= 0) selectedCharacters.splice(idx, 1);
      } else {
        if (selectedCharacters.indexOf(this.props.slug) == -1) {
        	selectedCharacters.push(this.props.slug);
        }
      }
      this.setState({'selected':!this.state.selected});

      var action_type = '';
      if (selectedCharacters.indexOf(current_character) >= 0){
        action_type += 'self';
        if (selectedCharacters.length > 1){
            action_type += '-other';
        }
      } else {
        if (selectedCharacters.length >= 1){
            action_type += 'other';
        }
      }
      if (selectedCharacters.length > 1){
        action_type += '-multiple';
      }
      var url = baseActionsUrl;
      if (action_type == '') {
          eventManager.setState('action-list', {'actions':[]});
          return;
      }
      url += action_type + "/";
      ajaxGet(url, function(res){
            eventManager.setState('action-list', {'actions':res['actions']});
      }, this);
  },
  render: function(){
    var showclick = this.handleShowHideDesc;
    var alongclick = this.handleShowHideAlong;
    var selectclick = this.handleSelect;

    var detailklass = 'disabled';
    var description = '';
    var colklass = 'col-12 col-lg-6';
    if (this.state.detail){
        detailklass = '';
     	  colklass = 'col-12';
        var character_url = getShapeUrl + this.props.slug + "/";
        description = (<CharacterDescription url={character_url} />);
    }
    if (this.props.owned){
     	  colklass = 'col-12';
    }
    var selectklass = 'character-check';
    if (! this.state.selected){
        selectklass += ' disabled';
    }
    var trusted = '';
    var trusted_bis = '';
    if (this.props.is_trusted){
    	if (this.props.is_trusted == 'full-full'){
    		trusted = ( <span className="character-trust" title={i18n_mutual_trust}><i className='fa fa-handshake-o'></i></span>);
    	} else if (this.props.is_trusted == 'temporary-temporary'){
    		trusted = ( <span className="character-temporary-trust"><i className='fa fa-handshake-o'></i></span>);
    	} else {
    		if (this.props.is_trusted.startsWith("temporary")){
    			trusted = ( <span className="character-temporary-trust" title={i18n_temp_trust}><i className='fa fa-hand-lizard-o
    			 fa-flip-horizontal'></i></span>);
    		}
    		else if (this.props.is_trusted.startsWith("full")){
    			trusted = ( <span className="character-trust" title={i18n_trust}><i className='fa fa-hand-lizard-o fa-flip-horizontal'></i></span>);
    		}
    		if (this.props.is_trusted.endsWith("temporary")){
    			trusted_bis = ( <span className="character-temporary-trust" title={i18n_temp_trust_you}><i className='fa fa-hand-lizard-o'></i></span>);
    		}
    		else if (this.props.is_trusted.endsWith("full")){
    			trusted_bis = ( <span className="character-trust" title={i18n_trust_you}><i className='fa fa-hand-lizard-o'></i></span>);
    		}
    	}
    }
    var healthlbl = this.props.health[0];
    var healthklass = this.props.health[1] + " m-1";
    var character_id = "character-detail-" + this.props.slug;
    var itemNodes = this.props.items.map(function(item){
        var key = item.id + '-' + time_key;
        return (<ItemNode name={item.current_name}
                          key={key}
                          id={item.id}
                          equipped={item.equipped}
                          equipable={item.equipable}
                          natural={item.natural}
                          character={this.props.slug}
                          icon={item.icon} />)
    }, this);

    var sex = "";
    if (this.props.sex){
    	if (this.props.sex.icon){
        	sex = (<i className={this.props.sex.icon}></i>);
    	} else {
        	sex = this.props.sex.name;
    	}
    }
    var subcharacters = '';
    if (this.props.subcharacters){
      subcharacters = this.props.subcharacters.map(
        function(character){
          var owned = true;
          return (<CharacterNode name={character.name}
                                 slug={character.shape_slug}
                                 key={character.shape_slug}
                                 sex={character.sex}
                                 health={character.health}
                                 is_trusted={character.is_trusted}
                                 items={character.items}
                                 owned={owned}
                                 subcharacters={character.subcharacters}
                                 image={character.avatar}
                                 race={character.race} />)
        });

    }
    var along = "";
    var along_displayed = "disabled";
    var alongklass = 'sub-characters disabled';
    if (this.state.along){
      alongklass = 'sub-characters';
      along_displayed = '';
    }
    if (subcharacters.length){
      along = new Array()
      for (idx=0; idx < subcharacters.length; idx++){
        var sub = subcharacters[idx];
        var along_str = sub.props.name;
        if (sub.props.race){
            along_str += " (" + sub.props.race + ")";
        }
        along.push((<span>{along_str} ; </span>));
      }
      along = (<div className='along'>
        <a className={along_displayed} onClick={alongclick} href='#'><i className='fa fa-eye'></i></a>
    &nbsp;{i18n_along} {along}</div>);
    }

    return (
        <li className={colklass}>
        <div className={healthklass} id={character_id} >
          <div className='character-image'>
            {this.props.image ? <img src={this.props.image} /> : ''}
          </div>
          <span className="character-view-detail">
          	<a className={detailklass} onClick={showclick} href='#'>
          		<i className='fa fa-eye'></i>
          	</a>
          	<a className={selectklass} onClick={selectclick} href='#'>
          		<i className='fa fa-check'></i>
          	</a>
          	<a className="disabled" onClick={this.descNote} href='#'
	          	title={i18n_history_book}>
          		<i className='fa fa-book'></i>
          	</a>
          </span>
          <div>
            <h4 className="character-name">
              {this.props.name}
              {trusted}
              {trusted_bis}
            </h4>
            <div className="attribute">
              <span className="character-race">
                {this.props.race} {sex}
              </span> &ndash; <span className="attribute character-health">
                {healthlbl}
              </span>
            </div>
          <ul className='items'>
            {itemNodes}
          </ul>
          <hr className='spacer'/>
          </div>
          {description}
          {along}
          <ul className={alongklass}>
            {subcharacters}
          </ul>
        </div>
        </li>
    );

  }
});
*/
/*
var current_audio_level = 0.5;

var loadAudio = function(){
    if (!$("audio").length){
      $("#btn-music-edit").hide();
    } else {
      $("#btn-music-edit").show();
    }
    if (!$("audio[data-play=true]").length){
      $("#toggle-music").hide();
    } else {
      $("#toggle-music").show();
    }

    if ($("#toggle-music .active").length){
      $(".audio-volume-container").show();
      $("audio[data-play=false]").animate({volume: 0}, 2000, function(){
        $("audio[data-play=false]").trigger("pause");
      });
      $("audio[data-play=true]").prop("volume", current_audio_level);
      $("audio[data-play=true]").trigger("play");
    } else {
      $(".audio-volume-container").hide();
      $("audio").animate({volume: 0}, 2000, function(){
        $("audio").trigger("pause");
      });
    }
};
*/

/*
var display_self_actions = function(){
  ajaxGet(baseActionsUrl + "self/", function(res){
	eventManager.setState('action-list', {'actions':res['actions']});
  }, this);
}
*/
/*
var Location = React.createClass({
  mixins: [loadWithPollingMethods],
  getInitialState: function() {
    return {
        data: {
            "name": "", "type": "", "description": "", "sector": "",
            "size_index": "", "population_index": "", "image": "",
            "characters": [], "items": [], "slug": "", "parent_images": [],
            "audios": [], "is_game_master": false
        }
    };
  },
  componentDidMount: function() {
    eventManager.registerItem('location', this);
    load_audio();
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem('location');
  },
  componentDidUpdate: function() {
    load_audio();
    $('.items [data-toggle="tooltip"]').tooltip();
    if (this.state.data.is_game_master && this.state.data.audios.length) {
      ReactDOM.render(
        <AudioPlayerForm audiolist={this.state.data.audios} />,
        document.getElementById('music-edit'));
    }
  },
  imageClick: function(){
      if (this.state.data.image){
		    displayDocument("location-image-" + this.state.data.slug);
      }
  },
  imageNote: function(){
    if (this.state.data.image){
		  ajaxPost(
		  	postNote,
		  	{image: this.state.data.image},
		  	function(){
            	refresh_modal(i18n_image_added);
            	$('.modal-action').modal('show');
        		$('.modal-action .submit').hide();
			});
    }
  },
  descNote: function(){
      var description = "### " + this.state.data.name;
      description += "\n" + this.state.data.description;
	  ajaxPost(
		postNote,
		{description: description},
		function(){
			refresh_modal(i18n_desc_added);
			$('.modal-action').modal('show');
        	$('.modal-action .submit').hide();
		});
  },
  render: function() {
    currentLocationSlug = this.state.data.slug;
    init_location_chat();
	if (!display_location){
	  display_self_actions();
	  return (<div></div>);
	}

    var characterNodes = this.state.data.characters.map(function(character){
        return (<CharacterNode name={character.name}
                               slug={character.shape_slug}
                               key={character.shape_slug}
                               sex={character.sex}
                               health={character.health}
                               is_trusted={character.is_trusted}
                               items={character.items}
                               owned={false}
                               subcharacters={character.subcharacters}
                               image={character.avatar}
                               race={character.race} />)
    }, this);
    var itemNodes = this.state.data.items.map(function(item){
        var key = item.id + '-' + time_key;
        return (<ItemNode name={item.current_name}
                          key={key}
                          id={item.id}
                          equipable={item.equipable}
                          natural={item.natural}
                          location={this.state.data.slug}
                          icon={item.icon} />)
    }, this);
    var parentImages = this.state.data.parent_images.map(
    	function(parent_image, index){
    		var slug = "location-image-" + index;
 			var onClick = function(){
		    	displayDocument(slug);
 			};
 			var onImageNote = function(){
				ajaxPost(
					postNote,
					{image: parent_image},
					function(){
						refresh_modal(i18n_image_added);
						$('.modal-action').modal('show');
        				$('.modal-action .submit').hide();
					}
				);
 			};
    		return (
    			<li className='parent-location-image document-item'
    				data-slug={slug} data-target={parent_image} key={index}>
    				<img src={parent_image} onClick={onClick} />
    				<a href='#' className='image-note' onClick={onImageNote}
	    				title={i18n_history_book}>
    					<i className="fa fa-book"></i>
    				</a>
    			</li>
    		);
    	}
    );
    var image_slug = "location-noimage";
    if(this.state.data.image){
      image_slug = "location-image-" + this.state.data.slug;
    }
    var audio = this.state.data.audios.map(function(audio){
      return (
        <AudioPlayer key={audio.slug}
            url={audio.url}
            type={audio.type}
            loop={audio.loop}
            autoplay={audio.autoplay} />
      );
    });
    return (
    <div>
      <div id="audio-players">
        {audio}
      </div>
      <div className='location-image'>
        <span className='document-item'
            data-slug={image_slug} data-target={this.state.data.image}>
            {this.state.data.image ? <img src={this.state.data.image} onClick={this.imageClick} /> : ''}
			<a href='#' className='image-note' onClick={this.imageNote}
				title={i18n_history_book}>
				<i className="fa fa-book"></i>
			</a>
	      </span>
	      <ul className='parent-images'>
		      {parentImages}
  	    </ul>
      </div>
      <div>
        <h3>{this.state.data.name}</h3>
        <ul className='items'>
          {itemNodes}
        </ul>
        <p className='description'>{this.state.data.description}</p>
		<a href='#' className='description-note' onClick={this.descNote}
			title={i18n_history_book}>
			<i className="fa fa-book"></i>
		</a>
     </div>
     <hr className='spacer'/>
     <ul className='characters row'>
      {characterNodes}
     </ul>
   </div>
   );
  }
});

if ($('#location').length) {
	ReactDOM.render(
	  <Location url={getLocationUrl} pollInterval={mediumPollInterval}
	  		target='#location' />,
	  document.getElementById('location')
	);
}
*/

// initiative

/*
var renderInit = function(init){
  var klass = 'btn';
  if (init.myself) klass += " myself";
  var badge_klass = "badge ";
  if (init.is_current_init){
    klass += " btn-primary";
    badge_klass += " badge-light";
  } else {
    badge_klass += " badge-secondary";
  }
  return (
  <li key={init.slug} className='breadcrumb-item'>
    <div className={klass}>{init.name}&nbsp;
    <span className={badge_klass}>{init.value}</span>
    </div>
  </li>
  );
}


var Initiative = React.createClass({
  mixins: [loadFromServer],
  componentDidMount: function() {
    eventManager.registerItem('initiative', this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem('initiative');
  },
  getInitialState: function() {
    return {data: {initiatives: [], editable_fields: []}};
  },
  handleSubmit: function(e) {
    e.preventDefault();
    var form = $("#quick-edit-form");
    $.ajax({
      url: formQuickEditUrl,
      type: "POST",
      data: new FormData(form[0]),
      contentType: false,
      cache: false,
      processData:false,
      success: function(data) {
        $("#quick-form-msg").html(
          "<div class='text-success pb-2'>" +
          i18n_form_saved
          + "</div>");
        setTimeout(function(){
          $("#quick-form-msg").html("");
        }, 4000);
      },
      error: function(xhr, status, err) {
        $("#quick-form-msg").html(
          "<div class='text-danger pb-2'>" +
          i18n_form_save_failed
          + "</div>");
        setTimeout(function(){
          $("#quick-form-msg").html("");
        }, 4000);
      }
    });

  },
  render: function() {
    var inits = "";
    var top_klass= "initiative";
    if (this.state.data.initiatives.length){
      inits = this.state.data.initiatives.map(renderInit);
    } else {
      top_klass += " d-none"
    }
    var form = "";
    if (this.state.data.editable_fields){
 		var fields = this.state.data.editable_fields.map(function(item){
 			return (<div className='col-3 p-2' key={item[1]}>
 					<label>{item[0]}</label>
 					<input className='form-control' name={item[1]}
 						defaultValue={item[2]} />
 					</div>);
 		});
    	var csrftoken = getCookie('csrftoken');
 		form = (
        <form onSubmit={this.handleSubmit} className='row' id='quick-edit-form'>
          <input type="hidden" name="csrfmiddlewaretoken" value={csrftoken} />
 		{fields}
 		<div className='col-3 p-3'>
 			<button type='submit' className='btn btn-primary'>
 				{i18n_edit}
 			</button>
 		</div>
        <div className="col-12" id='quick-form-msg'></div>
 		</form>);
    } else {
    	top_klass += " no-form";
    }
    return (
      <nav aria-label="breadcrumb" className={top_klass}>
        <h4>Initiative</h4>
        <ol className="breadcrumb">
          {inits}
        </ol>
	  	{form}
      </nav>
    );
  }
});

if ($("#initiative").length){
	ReactDOM.render(
	  <Initiative url={getInitiativeUrl} />,
	  document.getElementById('initiative')
	);
}
*/
/*
// Character sheet

var CharacterSheetItem = React.createClass({
  render: function() {
    var icon = this.props.icon;
    if (!icon){
      icon = "fa fa-genderless";
    }
    return (
      <tr key={this.props.slug}>
        <td className='sheet-item-icon'><i className={icon}></i>
        	&nbsp;
        </td>
        <td className='sheet-item-name'>{this.props.name}</td>
        <td className='sheet-item-value'>{this.props.value}</td>
      </tr>
    );
  }
});

var CharacterSheetSection = React.createClass({
  render: function() {
    var sectionItemsNodes = this.props.items.map(function(item){
        return (<CharacterSheetItem name={item.name}
                            slug={item.slug}
                            value={item.value}
                            key={item.slug}
                            icon={item.icon} />)
    }, this);

    var klass = "character-sheet-section col-lg-6 " + this.props.type;
    return (
      <div className={klass} key={this.props.slug}>
        <h4><i className={this.props.icon}></i> {this.props.name}</h4>
        <table className="character-sheet-items table table-hover table-striped">
          <tbody>
            {sectionItemsNodes}
          </tbody>
        </table>
      </div>
    );
  }
});

var CharacterSheet = React.createClass({
  mixins: [loadWithPollingMethods],
  getInitialState: function() {
    return {
        data: {
          sections: []
        }
    };
  },
  componentDidMount: function() {
    eventManager.registerItem('character-sheet', this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem('character-sheet');
  },
  handleEdit: function(event){
      event.preventDefault();
      var url = getEditCharacter + current_character + "/";
      ajaxGet(url, function(content){
          refresh_edit_modal(content);
          $('.modal-edit').modal('show');
      }, this);
  },
  render: function() {
    var sectionNodes = this.state.data.sections.map(function(section){
        return (<CharacterSheetSection name={section.name}
                            slug={section.slug}
                            type={section.type}
                            items={section.items}
                            key={section.slug}
                            icon={section.icon} />)
    }, this);

    var editClick = this.handleEdit;

    var klass = this.state.data.klass ? this.state.data.klass : "-";
    var race = this.state.data.race ? this.state.data.race : "-";

    var mp = "";
    if (this.state.data.mp){
    	mp = (<div className="col-4 col-lg-2">
            <i className={mpIcon}></i>
            <span className="character-detail-main-attribute">
              {this.state.data.mp}
            </span>
            <span className="character-detail-main-attribute-max">
              {this.state.data.max_mp}
            </span>
          </div>);
    }

    var background = "";
    if (this.state.data.background){
    	background = (
			<div className="row">
			  <div className="col-12">
				<div id="sheet-background" dangerouslySetInnerHTML={this.state.data.background} ></div>
			  </div>
			</div>
    	)
    }

    var biography = "";
    if (this.state.data.biography){
    	biography = (
			<div className="row">
			  <div className="col-12">
				<div id="sheet-biography" dangerouslySetInnerHTML={this.state.data.biography} ></div>
			  </div>
			</div>
    	)
    }

    return (
      <div id="character-sheet-detail">
        <div className="row character-detail-general">
          <div className="col-6 col-lg-3">
            <span className="character-detail-name">
              {i18n_name}
            </span>
            <span className="character-detail-value">
              {this.state.data.current_name}
            </span>
          </div>
          <div className="col-6 col-lg-3">
            <span className="character-detail-name">
              {i18n_people}
            </span>
            <span className="character-detail-value">
              {race}
            </span>
          </div>
          <div className="col-6 col-lg-3">
            <span className="character-detail-name">
              {i18n_klass}
            </span>
            <span className="character-detail-value">
              {klass}
            </span>
          </div>
          <div className="col-6 col-lg-3">
            <button className="btn btn-secondary"
                    id="edit-character-btn" onClick={editClick}>
              <i className="fa fa-pencil"></i>&nbsp; {i18n_edit}
            </button>
          </div>
        </div>
        <div className="row character-detail-general">
          <div className="col-4 col-lg-2 character-hp">
            <i className={hpIcon}></i>
            <span className="character-detail-main-attribute">
              {this.state.data.hp}
            </span>
            <span className="character-detail-main-attribute-max">
              {this.state.data.max_hp}
            </span>
          </div>
          <div className="col-4 col-lg-2 character-fp">
            <i className={fpIcon}></i>
            <span className="character-detail-main-attribute">
              {this.state.data.fp}
            </span>
            <span className="character-detail-main-attribute-max">
              {this.state.data.max_fp}
            </span>
          </div>
          {mp}
        </div>
        {background}
        {biography}
        <div className="row">
        {sectionNodes}
        </div>
        <div className="row">
          <div className="col-12">
            <h4>{i18n_notes}</h4>
            <div id="sheet-notes" dangerouslySetInnerHTML={this.state.data.notes} ></div>
          </div>
        </div>
      </div>
    );
  }
});

if ($("#character-sheet").length){
	ReactDOM.render(
	  <CharacterSheet url={getCharacterSheetUrl} pollInterval={longPollInterval} />,
	  document.getElementById('character-sheet')
	);
}

var CharactersSheetSectionRow = React.createClass({
  render: function() {
    var values = null;
    if (this.props.values.length){
      values = this.props.values.map(function(item, index){
        return (<td key={index}>{item}</td>);
      });
    }
    return (
      <tr key={this.props.slug}>
        <th className="ability">{this.props.name}</th>
        {values}
      </tr>
    );
  }
});

var CharactersSheetSection = React.createClass({
  render: function() {
    var sectionItemsNodes = null;
    var col_number = 1;
    if (this.props.values.length){
      col_number = this.props.values[0].values.length + 1;
      var sectionItemsNodes = this.props.values.map(function(item, index){
        return (<CharactersSheetSectionRow
                  key={item.slug}
                  slug={item.slug}
                  name={item.name}
                  values={item.values} />);
      }, this);
    }

    return (
      <tbody key={this.props.slug}>
        <tr>
          <th className="table-characteristic" colSpan={col_number}>{this.props.name}</th>
        </tr>
        {sectionItemsNodes}
      </tbody>);
  }
});


var CharactersSheet = React.createClass({
  mixins: [loadWithPollingMethods],
  getInitialState: function() {
    return {
        data: {
          sections: []
        }
    };
  },
  componentDidMount: function() {
    eventManager.registerItem('character-sheet', this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem('character-sheet');
  },
  handleEdit: function(event){
      event.preventDefault();
      var url = getEditCharacter + current_character + "/";
      ajaxGet(url, function(content){
          refresh_edit_modal(content);
          $('.modal-edit').modal('show');
      }, this);
  },
  render: function() {
    var editClick = this.handleEdit;

    var characters = null;
    var tables = null;
    if (this.state.data.sections.length){
      characters = this.state.data.sections[0].items.map(
        function(section, index){
          return (<th className='ability-header' key={index}>{section}</th>);
        }, this);
      tables = this.state.data.sections.slice(1).map(
        function(section){
          return (<CharactersSheetSection
                      name={section.name}
                      slug={section.slug}
                      type={section.type}
                      values={section.values}
                      key={section.slug}
                      icon={section.icon} />);
        }, this);
    }

    return (
      <div id="character-sheet-detail">
        <div className="row character-detail-general">
          <div className="col-6 col-lg-3">
            <button className="btn btn-secondary"
                    id="edit-character-btn" onClick={editClick}>
              <i className="fa fa-pencil"></i>&nbsp; {i18n_edit}
            </button>
          </div>
        </div>
        <table className='table table-hover table-striped table-responsive table-sm'>
        <thead>
          <tr>
            <th></th>
       	    {characters}
          </tr>
        </thead>
        {tables}
        </table>
      </div>
    );
  }
});


if ($("#characters-sheet").length){
	ReactDOM.render(
	  <CharactersSheet url={getCharactersSheetUrl} pollInterval={longPollInterval} />,
	  document.getElementById('characters-sheet')
	);
};

*/

// action list

//var action_point_short_label;

/*
var modal_prevent_submit = function(submit, e){
	var key = e.which || e.keyCode;
	if (e.ctrlKey && key == 13) {
		$(submit).click();
	}
	if (key == 13) {
		return;
	}
};

var refresh_modal = function(content){
	$('.modal-action .submit').show();
    $('#action-form .modal-body').html(content);
    $('.modal-action').modal('handleUpdate');
    var inputs_str = '.modal-action input[name=targets][value=';
    var selected = selectedCharacters.concat(selectedItems);
    for (var idx in selected){
        $(inputs_str + selected[idx] + ']').click();
    }
}
*/
/*
var ActionNode = React.createClass({
    handleDoAction: function(event){
        event.preventDefault();
        var url = baseDoActionUrl + this.props.slug + "/";
        ajaxGet(url, function(content){
            refresh_modal(content);
            $('.modal-action').modal('show');
    		$(".modal-action .form-control").first().focus();
        }, this);
    },
    render: function(){
      var doaction = this.handleDoAction;
      var label = this.props.name;
      if (this.props.ap_cost){
        var ap_cost = - this.props.ap_cost;
        label += ' - ' + ap_cost + " " + action_point_short_label;
      }
      var action_id = 'action-' + this.props.slug;
      return (
        <li id={action_id} className="character-action" data-toggle="tooltip" data-placement="bottom" title={label}>
          <a onClick={doaction} href='#'><i className={this.props.icon}></i></a>
        </li>
      );

  }

});

var ActionList = React.createClass({
  getInitialState: function() {
    return {
        actions: []
    };
  },
  componentDidMount: function() {
    eventManager.registerItem('action-list', this);
  },
  componentDidUpdate: function() {
    $('.actions [data-toggle="tooltip"]').tooltip();
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem('action-list');
  },
  render: function() {
    var actionNodes = this.state.actions.map(function(action){
        return (<ActionNode name={action.name}
                            slug={action.slug}
                            ap_cost={action.ap_cost}
                            key={action.slug}
                            icon={action.icon} />)
    }, this);
    return (
    <div>
      <ul className='actions'>
        {actionNodes}
      </ul>
     </div>
    );
  }
});

if ($('#action-list').length){
    ReactDOM.render(
      <ActionList url={getActionsUrl} />,
      document.getElementById('action-list')
    );
}
*/

var SimpleLocation = React.createClass({
  mixins: [loadFromServer],
  getInitialState: function() {
    return {
        data: {
            "name": "",
            "description": "",
            "image": ""
        }
    };
  },
  render: function() {
    currentLocationSlug = this.state.data.slug;
    var image_slug = "location-noimage";
    if(this.state.data.image){
      image_slug = "location-image-" + this.state.data.slug;
    }
    return (
    <div>
      <div className='location-image'>
        <span className='document-item'
            data-slug={image_slug} data-target={this.state.data.image}>
            {this.state.data.image ? <img src={this.state.data.image} onClick={this.imageClick} /> : ''}
	      </span>
      </div>
      <div>
        <h3>{this.state.data.name}</h3>
        <p className='description'>{this.state.data.description}</p>
     </div>
   </div>
   );
  }
});

var FullMap = React.createClass({
  mixins: [loadWithPollingMethods],
  getInitialState: function() {
    return {
        data: {
 			"live_action": false,
            "sector_image": "",
            "points": []
        }
    };
  },
  componentDidMount: function() {
    eventManager.registerItem('fullmap', this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem('fullmap');
  },
  render: function() {
  	if (this.state.data.live_action) return (<div></div>);
	const text_style_shadow =  {
		cursor: "pointer",
		filter: 'url(#shadow)',
		fill: 'white'
	};
	const text_style =  {
		cursor: "pointer",
		fill: 'black'
	};
	const image_border = 30;
	var dimensions = {};
	if (this.state.data.sector_image_dimensions){
		dimensions = {"width": this.state.data.sector_image_dimensions[0],
					  "height": this.state.data.sector_image_dimensions[1]};
	}

	var points = this.state.data.points.map(function(point) {
			var location_url = "/near-location/" + current_character + "/" +
			 	point.slug + "/";
			var clicked = function(){
	  			var display = function(){
					$("#map-description").html($("#temp-container").html());
				};
				ReactDOM.render(
				  <SimpleLocation url={location_url}
	  				target='#temp-container'
	  				key={location_url}
	  				extra_callback={display} />,
	  				document.getElementById('temp-container')
				);
			};
			var location_style =  {
				"cursor": "pointer",
				"fill": point.color,
			};
			return (
			<g>
				<circle className="locations" r="6"
					onClick={clicked}
					cursor="pointer"
					fill="black"
					cx={point.x + image_border}
					cy={point.y + image_border} />
				<circle style={location_style} className="locations" r="5"
					cx={point.x + image_border}
					cy={point.y + image_border} />
				<text style={text_style_shadow}
					onClick={clicked}
					x={point.x - 15 + image_border}
					y={point.y + 20 + image_border}>
					{point.name}
				</text>
				<text style={text_style}
					onClick={clicked}
					x={point.x - 15 + image_border}
					y={point.y + 20 + image_border}>
					{point.name}
				</text>
			</g>
			);
		}
	);

  	var svg_map = (
		<svg id="svg-map"
			style={dimensions}>
			<defs>
				<filter id="shadow" x="-20%" y="-20%" width="140%" height="140%">
				<feGaussianBlur stdDeviation="2 2" result="shadow"/>
				<feOffset dx="2" dy="2"/>
				</filter>
			</defs>
			{points}
		</svg>
  	);

    return (
    <div>
		<div className='row'>
			<div id='map-description' className='col-12'></div>
		</div>
		<div id="map-container" className='row'>
			<div className='col-12'>
				{svg_map}
				<img id="drag-image" src={this.state.data.sector_image}/>
			</div>
		</div>
	</div>
    );
  }
});


function getXYPosition(el) {
  var xPosition = 0;
  var yPosition = 0;

  while (el) {
    if (el.tagName == "BODY") {
      // deal with browser quirks with body/window/document and page scroll
      var xScrollPos = el.scrollLeft || document.documentElement.scrollLeft;
      var yScrollPos = el.scrollTop || document.documentElement.scrollTop;

      xPosition += (el.offsetLeft - xScrollPos + el.clientLeft);
      yPosition += (el.offsetTop - yScrollPos + el.clientTop);
    } else {
      xPosition += (el.offsetLeft - el.scrollLeft + el.clientLeft);
      yPosition += (el.offsetTop - el.scrollTop + el.clientTop);
    }

    el = el.offsetParent;
  }
  return {
    x: xPosition,
    y: yPosition
  };
}


var LiveMap = React.createClass({
  mixins: [loadWithPollingMethods],
  getInitialState: function() {
    return {
        data: {
            "live_action": false,
            "live_image": "",
            "over_live_image": ""
        }
    };
  },
  componentWillMount: function() {
 	if (!this.state.live_map_position_clicked) {
		this.setState(
			{'live_map_position_clicked': false});
 	}
  },
  componentDidMount: function() {
    eventManager.registerItem('overlivemap', this);
  },
  componentWillUnmount: function() {
    eventManager.unregisterItem('overlivemap');
  },
  positionButtonClick: function(){
	this.state.live_map_position_clicked = !this.state.live_map_position_clicked;
	this.forceUpdate();
  },
  positionClick: function(event){
  	if (!this.state.live_map_position_clicked) return;
  	var url = setLiveMapPositionUrl;
  	var position = getXYPosition(document.getElementById("live-image"));
  	url += event.clientX - position.x + "/";
  	url += event.clientY - position.y + "/";
  	ajaxGet(url, function(){
  		eventManager.reload('overlivemap');
  	});
  },
  render: function() {
  	if (!this.state.data.live_action) return (<div></div>);
  	var position_class = this.state.live_map_position_clicked ?
	  	"btn btn-warning" : "btn btn-primary";
  	var map_class = this.state.live_map_position_clicked ?
	  	"target-hover" : "";
    return (
    <div id="live-map-container">
		<div className='row'>
			<div className="col-3 p-3">
				<button type="button"
					onClick={this.positionButtonClick}
					className={position_class}>
					{i18n_map_set_position}
				</button>
			</div>
		</div>
		<div className='row'>
			<div className="col-12">
				<img className={map_class} src={this.state.data.over_live_image}
					onClick={this.positionClick}
					id='live-over-image'
				/>
				<img id='live-image'
					className={map_class} src={this.state.data.live_image}
					onClick={this.positionClick}
				/>
			</div>
		</div>
	</div>
    );
  }
});

if ($('#maps').length) {
	ReactDOM.render(
	  <FullMap url={getMapUrl} pollInterval={mediumPollInterval}
	  		target='#full-map' />,
	  document.getElementById('full-map')
	);
	ReactDOM.render(
	  <LiveMap url={getLiveMapUrl} pollInterval={mediumPollInterval}
	  		target='#live-map' />,
	  document.getElementById('live-map')
	);
}
