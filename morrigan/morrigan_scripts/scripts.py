from django.utils.translation import ugettext_lazy as _

from morrigan_common.models import Size
from morrigan_inventory.models import GenericItem, ItemType


def initialize_pockets(character):
    """
    To carry a minimum number of items each character has "natural" pockets
    (yes like kangaroos)
    """
    size, created = Size.objects.get_or_create(
        index=0, defaults={
            "name": _("No size")
        }
    )
    gen_pocket, created = GenericItem.objects.get_or_create(
        slug='pockets', defaults={
            'name': _("Pockets"),
            'type': ItemType.objects.get(slug='object'),
            'icon': 'fa fa-get-pocket',
            'contain_capacity': 8,
            'bag_reduction_factor': 0.8,
            'size': size,
        }
    )
    pocket_created = 0
    for shape in character.shapes.all():
        q = shape.areas.filter(area__slug='tronc')
        if not q.count():
            continue
        default_area = q.all()[0]
        if default_area.items.filter(generic_item__slug='pockets').count():
            continue
        pockets = gen_pocket.create_item()
        pockets.natural = True
        pockets.save()
        default_area.items.add(pockets)
        pocket_created += 1
    return pocket_created

