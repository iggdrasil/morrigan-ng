#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.
from itertools import chain

from django import forms
from django.forms.widgets import Select
# # from django.forms.util import flatatt
from django.utils.encoding import force_text
from django.utils.html import conditional_escape, mark_safe

'''
class BootstrapCheckbox(forms.CheckboxInput):

    def render(self, name, value, attrs=None):
        final_attrs = self.build_attrs(attrs, name=name)
        try:
            result = self.check_test(value)
        except:  # Silently catch exceptions
            result = False
        active, val = '', ''
        if result:
            final_attrs['checked'] = 'checked'
            active = ' active'
        if not (value is True or value is False or value is None
                or value == ''):
            # Only add the 'value' attribute if a value is non-empty.
            val = force_text(value)
        html = """
<label class="btn btn-primary%s">
    <input type="checkbox" autocomplete="off"%s%s>%s
</label>
        """ % (active, flatatt(final_attrs), val)
        return mark_safe(html)
'''

"""
class BSRadioInput(RadioChoiceInput):
    def render(self, name=None, value=None, attrs=None, choices=()):
        if self.id_for_label:
            label_for = format_html(' for="{}"', self.id_for_label)
        else:
            label_for = ''
        attrs = dict(self.attrs, **attrs) if attrs else self.attrs
        return format_html(
            '<label class="btn btn-success"{}>{} {}</label>', label_for,
            self.tag(attrs), self.choice_label
        )


class BSRadioRenderer(ChoiceFieldRenderer):
    outer_html = '<div class="btn-group btn-check" data-toggle="buttons"'\
                 '{id_attr}>{content}</div>'
    inner_html = '{choice_value}{sub_widgets}'
    choice_input_class = BSRadioInput

class BootstrapRadio(RendererMixin, Select):
    renderer = BSRadioRenderer
    _empty_value = ''
"""


class BootstrapRadio(Select):
    pass


class BootstrapCheckboxMultiple(forms.CheckboxSelectMultiple):
    def render(self, name, value, attrs=None, choices=(), renderer=None):
        if value is None:
            value = []
        has_id = attrs and 'id' in attrs
        attrs['name'] = name
        final_attrs = self.build_attrs(attrs)
        output = [
            '<div class="btn-group btn-check d-inline" data-toggle="buttons">']
        # Normalize to strings
        str_values = set([force_text(v) for v in value])
        for i, (option_value, option_label) in \
                enumerate(chain(self.choices, choices)):
            # If an ID attribute was given, add a numeric index as a suffix,
            # so that the checkboxes don't all have the same ID attribute.
            if has_id:
                final_attrs = dict(final_attrs, id='%s_%s' % (attrs['id'], i))

            cb = forms.CheckboxInput(
                final_attrs, check_test=lambda value: value in str_values)
            option_value = force_text(option_value)
            rendered_cb = cb.render(name, option_value)
            option_label = conditional_escape(force_text(option_label))
            output.append('<label class="btn btn-secondary">%s %s</label>' % (
                rendered_cb, option_label))
        output.append('</div>')
        return mark_safe('\n'.join(output))
