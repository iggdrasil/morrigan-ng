from django import template

register = template.Library()


@register.inclusion_tag('blocks/bs_field.html')
def bs_field(field, cls_size_lbl, cls_size_field, post_input="", cls=''):
    # if cls_size_lbl is not provided the label is not displayed
    return {'field': field, 'cls_size_lbl': cls_size_lbl, 'cls': cls,
            'cls_size_field': cls_size_field, 'post_input': post_input}


@register.inclusion_tag('blocks/bs_inline_field.html')
def bs_inline_field(field):
    # if cls_size_lbl is not provided the label is not displayed
    return {'field': field}
