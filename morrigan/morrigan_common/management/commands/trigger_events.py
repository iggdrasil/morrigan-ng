import sys

from django.core.management.base import BaseCommand
from morrigan_common.event_manager import trigger_all_events


class Command(BaseCommand):
    help = """Trigger all events"""

    def add_arguments(self, parser):
        parser.add_argument(
            '--verbose', action='store_true', help="Verbose run")

    def handle(self, *args, **options):
        if options["verbose"]:
            sys.stdout.write("* Trigger all events\n")
        nb = trigger_all_events(verbose=options["verbose"])
        if options["verbose"]:
            sys.stdout.write("\n- {} events triggered\n".format(nb))
