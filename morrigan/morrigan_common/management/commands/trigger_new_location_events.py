import sys

from django.core.management.base import BaseCommand
from morrigan_characters.models import Character
from morrigan_common.event_manager import event_trigger


class Command(BaseCommand):
    help = """Explicitly re-trigger location events"""

    def add_arguments(self, parser):
        parser.add_argument(
            '--verbose', action='store_true', help="Verbose run")

    def handle(self, *args, **options):
        if options["verbose"]:
            sys.stdout.write("* Re-trigger location events\n")
        new_events = 0
        q = Character.objects
        nb = q.count()
        for idx, character in enumerate(q.order_by("pk").all()):
            if options["verbose"]:
                sys.stdout.write("\rProcessing: {}/{} - {} %".format(
                    idx + 1, nb, round((idx + 1) / nb) * 100, 2))
                sys.stdout.flush()
            new_events += event_trigger(character)
        if options["verbose"]:
            sys.stdout.write("\n- {} new events triggered\n".format(new_events))
