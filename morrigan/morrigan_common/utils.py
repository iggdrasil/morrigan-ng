from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
import os
import markdown2
from PIL import Image
from django.conf import settings
from tempfile import mkdtemp


def format_markdown(message):
    message = markdown2.markdown(message).replace("\n", "<br>")
    full = ""
    for m in message.split("<br>"):
        m = m.strip()
        if not m:
            continue
        if not m.startswith("<p>"):
            full += "<br>"
        full += m
    return full


def put_cross_on_map(image, x, y):
    image_copy = Image.open(image.path).copy()
    width, height = image_copy.size
    cross = Image.open(settings.CROSS_IMAGE)
    if cross.mode != 'RGBA':
        cross = cross.convert('RGBA')
    transparent = Image.new('RGB', (width, height), (0, 0, 0, 0))
    transparent.paste(image_copy, (0, 0))
    dw, dh = cross.size
    x -= dw // 2
    y -= dh // 2
    transparent.paste(cross, (x, y), mask=cross)
    temp_dir = mkdtemp()
    dest = os.sep.join([temp_dir, image.name])
    if dest.lower().endswith(".jpg") or dest.lower().endswith(".jpeg"):
        transparent.save(dest, quality=90, optimize=True, progressive=True)
    else:
        transparent.save(dest)
    return dest, temp_dir


def margin_image_path(image_path):
    s = image_path.split(".")
    return ".".join(s[:-1] + ["border"] + [s[-1]])


def add_margin(image, top, right, bottom, left, color):
    pil_img = Image.open(image.path).copy()
    width, height = pil_img.size
    new_width = width + right + left
    new_height = height + top + bottom
    result = Image.new(pil_img.mode, (new_width, new_height), color)
    result.paste(pil_img, (left, top))
    temp_dir = mkdtemp()
    dest = os.sep.join([temp_dir, margin_image_path(image.name)])
    if dest.lower().endswith(".jpg") or dest.lower().endswith(".jpeg"):
        result.save(dest, quality=90, optimize=True, progressive=True)
    else:
        result.save(dest)
    return dest, temp_dir


def get_full_path(request, image):
    if image and image.startswith("/"):  # not absolute path
        base = "http://"
        if isinstance(request, dict):
            for k, v in request["headers"]:
                if k == b"origin":
                    base = v.decode("utf-8")
                    break
        else:
            port = request.get_port()
            if port == 443:
                base = "https://"
            base += request.get_host()
        image = "{}{}".format(base, image)
    return image


def refresh_interfaces(location, interfaces, target=None):
    channel_layer = get_channel_layer()
    room_group_name = 'chat_location_{}'.format(location)
    async_to_sync(channel_layer.group_send)(room_group_name, {
        "type": "reload",
        "interfaces": interfaces,
        "target": target
    })