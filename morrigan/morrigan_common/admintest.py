#!/usr/bin/env python3
# -*- coding: utf-8 -
#
# Inspired from an old snippet:
# @contact: marcoberi@gmail.com
# @version: 1.0
# @license: MIT http://www.opensource.org/licenses/mit-license.php
#

from django.apps import apps
from django.db.models.base import ModelBase
from django.urls import reverse, NoReverseMatch
from django.contrib.auth.models import User
from morrigan_common.admin import admin_site


def adminviewstest(self, app, user=None, password=None):
    if user is None:
        if password is None:
            password = "test"
        user = User.objects.create_superuser('test', 'test@test.com', password)
    self.client.login(username=user.username, password=password)

    myapp = apps.get_app_config(app)
    for model in myapp.models.values():
        # Get ModelAdmin for this Model
        if isinstance(model, ModelBase) \
                and model in admin_site._registry:
            try:
                # Prevent error 405 if model_admin.has_add_permission always
                # return False
                if admin_site._registry[model].has_add_permission(
                        type("request", (), {"user": user})):
                    url = reverse("admin:%s_%s_add" % (
                        model._meta.app_label, model._meta.model_name))
                    response = self.client.get(url, follow=True)
                    self.failUnlessEqual(
                        response.status_code, 200,
                        "%s != %s -> %s, url: %s" % (response.status_code, 200,
                                                     repr(model), url))
                    self.assertFalse(
                        "this_is_the_login_form" in str(response.content),
                        "login requested for %s" % str(model))
                url = reverse("admin:%s_%s_changelist" %
                              (model._meta.app_label, model._meta.model_name))
                response = self.client.get(url, follow=True)
                self.failUnlessEqual(
                    response.status_code, 200,
                    "%s != %s -> %s, url: %s" % (response.status_code, 200,
                                                 repr(model), url))
                self.assertFalse(
                    "this_is_the_login_form" in str(response.content),
                    "login requested for %s" % str(model))
            except NoReverseMatch:
                continue
