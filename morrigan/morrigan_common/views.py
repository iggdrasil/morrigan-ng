#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2010-2017 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

"""
Main views of the project
"""

from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import JsonResponse
from django.shortcuts import redirect
from django.views.generic.base import TemplateView

from rest_framework.permissions import BasePermission

from morrigan_common import models


def register_action(cls):
    models.Action.register_view(cls)
    return cls


class MorriganPermission(BasePermission):
    def has_permission(self, request, view):
        if not request.user or not request.user.is_authenticated:
            return False
        return True

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return True
        return obj.has_permission(view.permission_type, request.user)


class MorriganAdminPermission(BasePermission):
    def has_permission(self, request, view):
        #TODO: manage GameMaster permisssion
        return request.user.is_superuser

    def has_object_permission(self, request, view, obj):
        #TODO: manage GameMaster permisssion
        return request.user.is_superuser


class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)


def game_master_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME,
                         login_url=None):
    actual_decorator = user_passes_test(
        lambda x: x.is_superuser,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


class GameMasterRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(GameMasterRequiredMixin, cls).as_view(**initkwargs)
        return game_master_required(view)


class JSONResponseMixin(object):
    """
    A mixin that can be used to render a JSON response.
    """
    def render_to_json_response(self, context, **response_kwargs):
        return JsonResponse(self.get_data(context), **response_kwargs)

    def get_data(self, context):
        """
        Returns an object that will be serialized as JSON by json.dumps().
        """
        return context


class HomePageView(LoginRequiredMixin, TemplateView):
    template_name = "main.html"

    def dispatch(self, request, *args, **kwargs):
        # by default redirect to the main page
        if request.user.is_authenticated \
           and request.user.profile:
            q = request.user.profile.characters
            if q.count():
                character = q.order_by('-main').all()[0]
                if character.has_permission('full', request.user):
                    return redirect('main-page', character.slug)
        return super(HomePageView, self).dispatch(request, *args, **kwargs)


class TranslationJSView(TemplateView):
    template_name = "blocks/translations.js.html"
    content_type = "application/x-javascript"
