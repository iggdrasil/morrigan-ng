#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2017  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

"""
Main models description
"""

import parser
from random import randint
import re
import os
from PIL import Image, ImageDraw
from io import BytesIO
from adminsortable.models import SortableMixin

from django.conf import settings
from django.core.cache import cache
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.contrib.auth.models import User
from django.core.files import File
from django.forms.models import model_to_dict
from django.utils import timezone
from django.utils.functional import lazy
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _


def _gen_slug(sender, instance):
    base_slug = slugify(instance.name)
    slug = base_slug
    idx = 0
    while sender.objects.filter(slug=slug).count():
        idx += 1
        slug = base_slug + '-' + str(idx)
    return slug


def migration_generate_slug(app, model_name):
    def func(apps, schema_editor):
        model = apps.get_model(app, model_name)
        for item in model.objects.all():
            if item.slug:
                continue
            slug = _gen_slug(model, item)
            setattr(item, 'slug', slug)
            item.save()
    return func


def pre_save_slug(sender, instance, *args, **kwargs):
    if not instance.slug:
        slug = _gen_slug(sender, instance)
        instance.slug = slug


class SlugManager(models.Manager):
    def get_by_natural_key(self, slug):
        return self.get(slug=slug)


class NameManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


def make_avatar(image_path):
    """
    Create a round avatar from an image.

    No exception management. It should be implemented by the caller :
    FileNotFoundError, OSError, ValueError are possible errors for bad file
    or bad image.

    :param image_path: disk path
    :return: in memory image of the avatar
    """
    img = Image.open(image_path)
    width = img.size[0]
    height = img.size[1]
    size = width
    mem_output = BytesIO()

    if height != width:
        # take the smaller side
        if height < width:
            size = height
        out = img.crop((0, 0, size, size))
        img = out

    # bigger size for the mask for better antialias
    bigsize = (size * 3, size * 3)
    mask = Image.new('L', bigsize, 0)
    draw = ImageDraw.Draw(mask)
    draw.ellipse((0, 0) + bigsize, fill=255)
    mask = mask.resize((size, size), Image.ANTIALIAS)
    img.putalpha(mask)

    out = img.resize(settings.MORRIGAN_AVATAR_SIZE, Image.ANTIALIAS)
    out.save(mem_output, 'PNG')
    return mem_output


class AvatarModel(models.Model):
    AVATAR_FIELD = ''
    avatar = models.ImageField(
        _("Avatar"), blank=True, null=True, upload_to='avatars')

    class Meta:
        abstract = True

    def create_avatar(self):
        if not hasattr(self, 'AVATAR_FIELD' or not self.AVATAR_FIELD):
            return
        try:
            image = getattr(self, self.AVATAR_FIELD)
            if image:
                avatar_img = make_avatar(image.path)
                splt = image.path.split('.')
                avatar_path = ".".join(splt[:-1]) + "_avatar.png"
                self.avatar.save(
                    os.path.basename(avatar_path),
                    File(avatar_img), save=False)
                self.save(avatar_generated=True)
                return
            if not self.avatar:
                return
        except (FileNotFoundError, OSError, ValueError):
            pass
        self.avatar = None
        self.save(avatar_generated=True)

    def save(self, *args, **kwargs):
        avatar_generated = 'avatar_generated' in kwargs and kwargs.pop(
            'avatar_generated')
        super(AvatarModel, self).save(*args, **kwargs)
        if not avatar_generated:
            self.create_avatar()


class GenericType(models.Model):
    """
    Base abstract class for generic type
    """
    name = models.TextField()
    description = models.TextField(null=True, blank=True)
    order = models.IntegerField(default=10)
    available = models.BooleanField(default=True)

    class Meta:
        abstract = True
        ordering = ('order', 'name')

    def __str__(self):
        return self.name


class GenericSlugType(GenericType):
    slug = models.SlugField(_("Slug"), blank=True, null=True, unique=True)
    objects = SlugManager()

    class Meta:
        abstract = True
        ordering = ('order', 'name')

    def natural_key(self):
        return (self.slug,)

    @classmethod
    def cache(cls, slug, obj=None):
        key = cls.__name__ + '-' + slug
        value = cache.get(key, None)
        if value:
            return value
        elif obj:
            cache.set(key, model_to_dict(obj), settings.CACHE_TIMEOUT)
            return cache.get(key, None)
        value = cls.objects.filter(slug=slug).all()[:1]
        if not value:
            raise cls.DoesNotExist
        cache.set(key, model_to_dict(value[0]), settings.CACHE_TIMEOUT)
        return cache.get(key, None)

    @classmethod
    def cache_get_all(cls):
        key = cls.__name__ + '_get_all'
        value = cache.get(key, None)
        if value:
            return value
        items = []
        for item in cls.objects.order_by(*cls._meta.ordering).all():
            items.append(cls.cache(item.slug, obj=item))
        cache.set(key, items, settings.CACHE_TIMEOUT)
        return cache.get(key, None)


class Stylesheet(models.Model):
    """
    Custom CSS
    """
    name = models.TextField(_("Name"), max_length=32, unique=True)
    klass = models.CharField(_("Class name"), max_length=50)
    default = models.BooleanField(_("Default"), default=True)

    def __str__(self):
        return self.name


class Tips(models.Model):
    name = models.CharField(_("Name"), max_length=200)
    available = models.BooleanField(_("Available"), default=True)
    tip = models.TextField(_("Tip description"), help_text=_("Use markdown syntax"))

    def __str__(self):
        return self.name


class Player(models.Model):
    """
    Morrigan user
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE,
                                primary_key=True,
                                related_name='profile')
    timezone = models.CharField(_("Timezone"), max_length=256,
                                default=settings.TIME_ZONE)
    stylesheet = models.ForeignKey(
        Stylesheet, blank=True, null=True, verbose_name=_("Stylesheet"),
        on_delete=models.SET_NULL, related_name='players')
    display_tips = models.BooleanField(_("Display tips on start"), default=True)

    class Meta:
        verbose_name = _("Player")
        verbose_name_plural = _("Players")

    def __str__(self):
        return str(self.user)


def auto_create_player(sender, instance, *args, **kwargs):
    if not instance or Player.objects.filter(pk=instance.pk).count():
        return
    Player.objects.create(user_id=instance.pk)


post_save.connect(auto_create_player, sender=User)


class AbilityKind(GenericType):
    """
    Kind of an ability. Eg.: intellectual, physical, etc.
    """

    class Meta:
        verbose_name = _("Type: Ability kind")
        verbose_name_plural = _("Types: Ability kind")
        ordering = ('order',)


ABILITY_TYPES = (
    ('A', _("Ability")),
    ('S', _("Skill")),
    ('L', _("Language"))
)


class AbilityType(GenericSlugType):
    objects = SlugManager()

    class Meta:
        verbose_name = _("Type: Ability")
        verbose_name_plural = _("Types: Ability")


class GenericAbility(GenericSlugType):
    ability_type = models.ForeignKey(
        AbilityType, verbose_name=_("Ability Type"), blank=True, null=True,
        on_delete=models.SET_NULL)
    kind = models.ForeignKey("AbilityKind", blank=True, null=True,
                             on_delete=models.SET_NULL)
    visible = models.BooleanField(
        default=True, help_text=_("Visible by the player"))
    editable = models.BooleanField(_("Editable by the player"), default=False)
    editable_live = models.BooleanField(_("Editable by the player in live "
                                          "action"), default=False)
    objects = SlugManager()

    class Meta:
        abstract = True


class Ability(GenericAbility):
    """
    Ability is a generic model that list ability, skills and languages
    """
    delta_min_max = models.IntegerField(
        default=0,
        help_text=_("What amount can be lost if no training is done"))
    objects = SlugManager()

    class Meta:
        verbose_name = _("Ability")
        verbose_name_plural = _("Abilities")
        ordering = ('ability_type', 'kind', 'order',)


pre_save.connect(pre_save_slug, sender=Ability)


class CalculatedAbility(GenericAbility):
    """
    An ability that can be calculated
    """
    formula = models.ForeignKey(
        "BasicFormula", related_name='calculated_abilities',
        on_delete=models.CASCADE)
    objects = SlugManager()

    class Meta:
        verbose_name = _("Calculated ability")
        verbose_name_plural = _("Calculated abilities")
        ordering = ('ability_type', 'order',)


pre_save.connect(pre_save_slug, sender=CalculatedAbility)


class SkillType(GenericSlugType):
    objects = SlugManager()

    class Meta:
        verbose_name = _("Type: Skill")
        verbose_name_plural = _("Types: Skill")


class Skill(GenericSlugType):
    """
    Skill are non numeric abilities
    """
    skill_type = models.ForeignKey(
        SkillType, verbose_name=_("Skill type"), blank=True, null=True,
        on_delete=models.SET_NULL)
    visible = models.BooleanField(
        default=True, help_text=_("Visible by the player"))
    objects = SlugManager()

    class Meta:
        verbose_name = _("Skill")
        verbose_name_plural = _("Skills")


class EffectType(GenericType):
    pass


class Effect(GenericType):
    """
    Effects that can affect character
    """
    effect_type = models.ForeignKey(
        "EffectType", verbose_name=_("Effect type"), on_delete=models.CASCADE)
    length = models.IntegerField(_("Length"), null=True, blank=True)
    physical_description = models.TextField(_("Physical description"),
                                            blank=True, null=True)
    period = models.IntegerField(_("Period"), null=True, blank=True)
    formula = models.ForeignKey(
        "Formula", blank=True, null=True, related_name='effects',
        on_delete=models.SET_NULL)


class FormulaParseError(Exception):
    pass


class BaseFormula(models.Model):
    name = models.TextField(_("Name"), unique=True)
    formula = models.TextField(_("Main formula"),
                               blank=True, null=True)
    RE_ABILITY = re.compile(r'\[([\w_-]+)\]')
    RE_OPPONENT_ABILITY = re.compile(r'\[>([\w_-]+)\]')
    RE_DICE = re.compile(r'\[(\d*)[Dd](\d+)\]')
    MAIN_ABILITIES = ('ap', 'fp', 'mp', 'hp', 'bp')
    MAX_ABILITIES = ('max_ap', 'max_fp', 'max_mp', 'max_hp', 'max_bp',)
    TARGETS = ('', )

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,)

    def test(self, values):
        """
        Test formula with arbitrary values
        """
        logs = {}
        res, vals = self._evaluate(self.TARGETS, logs=logs)
        for target in self.TARGETS:
            self._final_eval(res, target, values)
        res['main'] = res.pop('')
        return res

    def evaluate(self, character, opponent=None, extra_values=None,
                 targets=None, succeded=None, log=False):
        """
        Apply formula to a character
        """
        if not extra_values:
            extra_values = {}
        if not targets:
            targets = []
        logs = {}
        if not targets:
            targets = self.TARGETS
        res, values = self._evaluate(targets, extra_values, logs=logs)
        values.update(extra_values)
        # the result is already know
        if succeded is not None:
            values['SUCCESS'] = succeded and 1 or 0
        for value in values.keys():
            c_character = character
            slug = value
            if value.startswith('>'):
                c_character = opponent
                slug = value[1:]
                if not opponent:
                    raise FormulaParseError(
                        _("Opponent ability are requested in the formula "
                          "but not opponent id provided in this action"))
            if slug == 'SUCCESS' or slug in extra_values:
                pass
            elif slug in self.MAIN_ABILITIES or slug in self.MAX_ABILITIES:
                values[value] = getattr(c_character, slug)
            else:
                values[value] = c_character.get_ability_score(
                    values[value]['id'])
            if 'parameters' not in logs:
                logs['parameters'] = {}
            logs['parameters'][value] = values[value]
        for target in targets:
            # the result is already know
            if succeded is not None and not target:
                continue
            self._final_eval(res, target, values)
        if '' in res:
            res['main'] = res.pop('')
        if log:
            if 'SUCCESS' in values:
                if 'parameters' not in logs:
                    logs['parameters'] = {}
                logs['parameters']['SUCCESS'] = values['SUCCESS']
            if '' in logs:
                logs['main'] = logs.pop('')
            for k in res:
                if k not in logs:
                    logs[k] = {}
                logs[k]['result'] = res[k]
            return res, logs
        return res

    def _evaluate(self, targets, extra_values=None, logs=None):
        if not extra_values:
            extra_values = {}
        if not logs:
            logs = {}
        res, abilities = {}, {}
        for target in targets:
            if target not in logs:
                logs[target] = {}
            if target:
                formula = getattr(self, target + '_formula')
            else:
                formula = getattr(self, 'formula')
            logs[target]['formula'] = formula
            if not formula:
                if not target:
                    # if main formula is not provided: always true
                    res[target] = '1'
                else:
                    res[target] = '0'
                continue
            # dices
            for m in self.RE_DICE.finditer(formula):
                nb, dice = m.groups()
                if not nb:
                    nb = 1
                else:
                    nb = int(nb)
                slug_key = m.group()
                while slug_key in formula:
                    score = 0
                    for launch in range(nb):
                        score += randint(1, int(dice))
                    logs[target][slug_key] = score
                    formula = formula.replace(slug_key, str(score), 1)
            # abilities
            for R, prefix in ((self.RE_ABILITY, ''),
                              (self.RE_OPPONENT_ABILITY, '>')):
                for m in R.finditer(formula):
                    slug = m.groups()[0]
                    p_slug = prefix + slug
                    if slug == 'SUCCESS':
                        if not target:
                            raise FormulaParseError(
                                _("SUCCESS key cannot be used for the main "
                                  "formula"))
                    elif slug in self.MAIN_ABILITIES \
                            or slug in self.MAX_ABILITIES \
                            or slug in extra_values.keys():
                        abilities[slug] = slug
                        formula = formula.replace(m.group(),
                                                  "%%(%s)d" % slug)
                    # test if it is a valid ability
                    elif p_slug not in abilities:
                        try:
                            ability = Ability.cache(slug=slug)
                        except Ability.DoesNotExist:
                            raise FormulaParseError(
                                _("%s is not a valid slug for abilities") %
                                p_slug)
                        abilities[p_slug] = ability
                    formula = formula.replace(m.group(),
                                              "%%(%s)d" % (p_slug))
            res[target] = formula
        return res, abilities

    def _final_eval(self, res, target, values):
        formula = res[target] % values
        try:
            res[target] = float(eval(parser.expr(formula).compile()))
        except:  # catch large
            raise FormulaParseError(
                _("Cannot parse %s. Check the syntax.") % formula)
        if not target:  # main formula
            values['SUCCESS'] = res[target] > 0 and 1 or 0


class BasicFormula(BaseFormula):
    objects = NameManager()

    class Meta:
        verbose_name = _("Basic formula")
        verbose_name_plural = _("Basic formulas")
        ordering = ('name',)


class Formula(BaseFormula):
    """
    Example for an action:
    * formula: [strenght] * [2D10] - [>strenght] * 10
      character strenght multiplied by the result of two ten faces
      dices minus opponent strenght multiplied by ten
    * ap_formula: -2
      minus two (it costs two action points)
    * hp_formula: ([SUCCESS] - 1) * 5
      lost of two health point on fail
    * xp_formula: [SUCCESS] * [D2] * 10 + (1 - [SUCCESS]) * 5
      on success: result of two two faces dices multiplied by ten
      on fail: five
    """
    TARGETS = ('', 'ap', 'fp', 'mp', 'hp', 'bp', 'xp')
    ap_formula = models.TextField(_("Action point formula"),
                                  blank=True, null=True)
    fp_formula = models.TextField(_("Feed point formula"),
                                  blank=True, null=True)
    mp_formula = models.TextField(_("Magic point formula"),
                                  blank=True, null=True)
    hp_formula = models.TextField(_("Hit point formula"),
                                  blank=True, null=True)
    bp_formula = models.TextField(_("Battle point formula"),
                                  blank=True, null=True)
    xp_formula = models.TextField(_("Experience point formula"),
                                  blank=True, null=True)
    objects = NameManager()

    class Meta:
        verbose_name = _("Formula")
        verbose_name_plural = _("Formulas")
        ordering = ('name',)


def get_available_slugs():
    views = []
    for view in Action._views:
        views.append((view.slug, view.verbose_name))
    return views


def apply_result(character, result):
    changed = False
    for k in result:
        if k == 'xp':  # TODO
            continue
        elif k == 'main':
            continue
        if result[k]:
            res = getattr(character, k) + result[k]
            if hasattr(character, "max_" + k):
                max_val = getattr(character, "max_" + k)
                if res > max_val:
                    res = max_val
            if getattr(character, k) != res:
                setattr(character, k, res)
                changed = True
    if changed:
        character.save()


class Audio(models.Model):
    name = models.TextField(_("Name"))
    slug = models.SlugField(_("Slug"), blank=True, null=True, unique=True)
    url = models.URLField(_("URL"), blank=True, null=True)
    audio_file = models.FileField(
        _("File"), blank=True, null=True, upload_to='audio')
    duration = models.DurationField(
        _("Duration"), blank=True, null=True,
        help_text=_("Necessary if not used as a loop"))

    class Meta:
        verbose_name = _("Audio")
        verbose_name_plural = _("Audios")
        ordering = ('name',)

    @property
    def link(self):
        if self.audio_file:
            return self.audio_file.url
        if self.url:
            return self.url

    def __str__(self):
        return self.name


class GameAudio(models.Model):
    audio = models.ForeignKey("Audio", on_delete=models.CASCADE,
                              related_name="games")
    game = models.ForeignKey("Game", on_delete=models.CASCADE)
    autoplay = models.BooleanField(verbose_name=_("Autoplay"), default=False)
    loop = models.BooleanField(verbose_name=_("Loop"), default=True)
    order = models.IntegerField(verbose_name=_("Order"), default=10)
    expiration = models.DateTimeField(
        verbose_name=_("Expiration"), blank=True, null=True)

    class Meta:
        verbose_name = _("Game - Audio")
        verbose_name_plural = _("Game - Audios")
        ordering = ('order',)


class Game(models.Model):
    name = models.TextField(_("Name"), unique=True)
    slug = models.SlugField(_("Slug"), unique=True)
    background_image = models.ImageField(_("Image"), null=True, blank=True)
    hp_editable = models.BooleanField(_("HP - Editable by the player"),
                                      default=False)
    hp_editable_live = models.BooleanField(_("HP - Editable in live action"),
                                           default=False)
    hp_custom_icon = models.CharField(_("HP - Custom icon class"),
                                      max_length=200, blank=True, null=True)
    mp_editable = models.BooleanField(_("MP - Editable by the player"),
                                      default=False)
    mp_editable_live = models.BooleanField(_("MP - Editable in live action"),
                                           default=False)
    mp_custom_icon = models.CharField(_("MP - Custom icon class"),
                                      max_length=200, blank=True, null=True)
    fp_editable = models.BooleanField(_("FP - Editable by the player"),
                                      default=False)
    fp_editable_live = models.BooleanField(_("FP - Editable in live action"),
                                           default=False)
    fp_custom_icon = models.CharField(_("FP - Custom icon class"),
                                      max_length=200, blank=True, null=True)
    ap_editable = models.BooleanField(_("AP - Editable by the player"),
                                      default=False)
    ap_editable_live = models.BooleanField(_("AP - Editable in live action"),
                                           default=False)
    ap_custom_icon = models.CharField(_("AP - Custom icon class"),
                                      max_length=200, blank=True, null=True)
    reverse_hp = models.BooleanField(
        _("Reverse HP count"), default=False,
        help_text=_("When set to true, higher HP is worse - HP is in this "
                    "case an injury counter"))
    can_edit_actionlog = models.BooleanField(
        _("Players can edit their action log"), default=False)
    display_location = models.BooleanField(
        _("Display location tab"), default=True
    )
    display_map = models.BooleanField(
        _("Display map tab"), default=True
    )
    display_action_log = models.BooleanField(
        _("Display action log tab"), default=True
    )
    display_character_log = models.BooleanField(
        _("Display character log tab"), default=True
    )
    custom_css = models.TextField(_("Custom CSS rules"), blank=True, default="")
    audios = models.ManyToManyField(
        Audio, verbose_name=_("Audio URL"), through=GameAudio, blank=True)
    objects = SlugManager()

    class Meta:
        verbose_name = _("Game")
        verbose_name_plural = _("Games")
        ordering = ('name',)

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.slug,)


class OracleCategory(GenericSlugType):
    class Meta:
        verbose_name = _("Oracle - category")
        verbose_name_plural = _("Oracle - categories")


class Oracle(models.Model):
    name = models.TextField(_("Name"))
    slug = models.SlugField(_("Slug"))
    game = models.ForeignKey(Game, verbose_name=_("Game"),
                             on_delete=models.CASCADE)
    available = models.BooleanField(_("Available"), default=True)
    category = models.ForeignKey(
        OracleCategory, blank=True, null=True, on_delete=models.SET_NULL)
    gm_only = models.BooleanField(_("For game master only"), default=True)

    class Meta:
        verbose_name = _("Oracle")
        verbose_name_plural = _("Oracles")
        unique_together = (("name", "game"), ("slug", "game"))

    def __str__(self):
        return self.name

    def roll(self):
        total = self.choices.count()
        if not total:
            return ""
        return self.choices.all()[randint(0, total - 1)].name


class OracleChoice(SortableMixin):
    name = models.TextField(_("Name"), unique=True)
    oracle = models.ForeignKey(Oracle, verbose_name=_("Oracle"),
                               on_delete=models.CASCADE, related_name="choices")
    available = models.BooleanField(_("Available"), default=True)
    order = models.PositiveIntegerField(default=0, editable=False,
                                        db_index=True)

    class Meta:
        ordering = ["order"]
        verbose_name = _("Oracle - choice")
        verbose_name_plural = _("Oracle - choices")

    def __str__(self):
        return self.name


class DocumentCategory(GenericSlugType):
    class Meta:
        verbose_name = _("Document category")
        verbose_name_plural = _("Document categories")


class Document(models.Model):
    name = models.TextField(_("Name"))
    slug = models.SlugField(_("Slug"), unique=True)
    game = models.ForeignKey(Game, verbose_name=_("Game"),
                             on_delete=models.CASCADE)
    category = models.ForeignKey(
        DocumentCategory, blank=True, null=True, on_delete=models.SET_NULL)
    image = models.ImageField(_("Image"))
    preload = models.BooleanField(_("Preload the image"), default=True)
    hidden = models.BooleanField(_("Is hidden"), default=False)
    locations = models.ManyToManyField(
        "morrigan_geography.Location", verbose_name=_("Location"),
        blank=True, help_text=_("Limit to this locations"),
        related_name="documents"
    )
    characters = models.ManyToManyField(
        "morrigan_characters.Character", verbose_name=_("Character"),
        blank=True, help_text=_("Limit to this characters"),
        related_name = "documents"
    )
    objects = SlugManager()

    class Meta:
        verbose_name = _("Document")
        verbose_name_plural = _("Documents")
        ordering = ('name',)
        unique_together = ("name", "category", "game")

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.slug,)


class Action(GenericSlugType):
    _views = set()
    available_in_chat = models.BooleanField(_("Available as slash action in chat"),
                                            default=False)
    formula = models.ForeignKey(
        Formula, verbose_name=_('Main formula'), blank=True, null=True,
        related_name='actions', on_delete=models.SET_NULL)
    opponent_formula = models.ForeignKey(
        Formula, verbose_name=_('Opponent formula'), blank=True, null=True,
        help_text=_("Action for gain and loss: the main formula is not used"),
        related_name='opponent_actions', on_delete=models.SET_NULL)
    help_text = models.TextField(_('Help'), null=True, blank=True)
    fighting_action = models.BooleanField(_("Is a fighting action?"),
                                          default=False)
    need_conscious = models.BooleanField(_("Character must be conscious"),
                                         default=True)
    need_fight = models.BooleanField(_("Character must be engaged in a fight"),
                                     default=False)
    need_no_fight = models.BooleanField(
        _("Character must not be engaged in a fight"), default=False)
    icon = models.CharField(
        _('Icon name'), blank=True, null=True, max_length=50,
        help_text=_('Class name of the icon'))
    target_self = models.BooleanField(_("Self can be a target"), default=False)
    target_other = models.BooleanField(_("Others can be a target"),
                                       default=False)
    target_trusting = models.NullBooleanField(_("Target is trusting"))
    target_multiple = models.BooleanField(_("Target can be multiple"),
                                          default=False)
    target_own_item = models.BooleanField(_("Own item can be a target"),
                                          default=False)
    target_location_item = models.BooleanField(
        _("Locations's item can be a target"), default=False)
    target_other_item = models.BooleanField(_("Other's item can be a target"),
                                            default=False)
    target_can_be_carried = models.BooleanField(
        _("Item can be carried"), default=False,
        help_text=_("Check if the character have enough space to carry the "
                    "item. If False the check is not done.")
    )
    TARGET_NATURAL = (
        ('A', _("All")),
        ('Y', _("Only natural")),
        ('N', _("Only non-natural")),
    )
    target_natural_item = models.CharField(
        _("Natural status of an item to be a target"), default='N',
        max_length=1, choices=TARGET_NATURAL)
    TARGET_EQUIPPED = (
        ('A', _("All")),
        ('Y', _("Only equipped")),
        ('N', _("Only non-equipped")),
    )
    target_equipped_item = models.CharField(
        _("Equipped status of an item to be a target"), default='N',
        max_length=1, choices=TARGET_EQUIPPED)
    TARGET_EQUIPPABLE = (
        ('A', _("All")),
        ('Y', _("Only equippable")),
        ('N', _("Only non-equippable")),
    )
    target_equippable_item = models.CharField(
        _("Equippable status of an item to be a target"), default='N',
        max_length=1, choices=TARGET_EQUIPPABLE)
    view_slug = models.CharField(_("View"), blank=True, null=True,
                                 max_length=60, choices=[])

    objects = SlugManager()

    def __init__(self, *args, **kwargs):
        super(Action, self).__init__(*args, **kwargs)
        self._meta.get_field('view_slug')._choices = \
            lazy(get_available_slugs, list)()

    def natural_key(self):
        return (self.slug,)

    def do(self, character, opponents, extra_values=None, log=True,
           force=False):
        if not extra_values:
            extra_values = {}
        # force is usually used by tasks
        if not force and not self.is_available(character, extra_values):
            return {}
        success = True
        logs = {}
        opponent = None
        if opponents:
            # TODO: only evaluating on one opponent - group action are not yet
            # managed
            if type(opponents) in (list, tuple):
                opponent = opponents[0]
            else:
                opponent = opponents
        if self.formula:
            res = self.formula.evaluate(character, opponent, extra_values,
                                        log=log)
            if log:
                res, logs['character'] = res
            success = res.pop('main')
            apply_result(character, res)
        if self.opponent_formula and opponent:
            res = self.opponent_formula.evaluate(
                opponent, character, extra_values, succeded=success, log=log)
            if log:
                res, logs['opponent'] = res
            res.pop('main')
            apply_result(opponent, res)
        if logs:
            return logs

    def is_available(self, character, extra_values=None):
        if not extra_values:
            extra_values = {}
        if not self.available:
            return False
        if self.need_conscious and not character.is_conscious:
            return False
        if self.need_fight and not character.is_in_a_fight:
            return False
        if self.need_no_fight and character.is_in_a_fight:
            return False
        if self.formula:
            res = self.formula.evaluate(character, None, extra_values,
                                        targets=['ap'])
            if (-res['ap']) > character.ap:
                return False
        return True

    @classmethod
    def register_view(cls, form):
        cls._views.add(form)

    def get_view(self):
        for view in Action._views:
            if self.view_slug == view.slug:
                return view

    def get_targets(self, character):
        targets = []
        if self.target_self:
            targets.append(character)
        if self.target_other:
            for character in character.get_neighbors():
                targets.append(character)
        return targets


class DamageType(GenericSlugType):
    objects = SlugManager()

    class Meta:
        verbose_name = _("Type: Damage")
        verbose_name_plural = _("Types: Damage")


class Material(GenericType):
    durability = models.IntegerField(_("Durability"), null=True, blank=True)

    class Meta:
        verbose_name = _("Type: Material")
        verbose_name_plural = _("Types: Material")


class IndexManager(models.Manager):
    def get_by_natural_key(self, index):
        return self.get(index=index)


class Size(GenericType):
    index = models.IntegerField(
        _("Index"), unique=True,
        help_text=_(
            "Standard character can carry at max an item with size of 200 ("
            "with 2 hands)")
    )
    objects = IndexManager()

    class Meta:
        verbose_name = _("Type: Size")
        verbose_name_plural = _("Types: Size")

    def __str__(self):
        return "{} ({})".format(self.name, self.index)

    def natural_key(self):
        return (self.index, )


class Technology(GenericType):
    pass


class BodyArea(GenericSlugType):
    """
    Available body areas
    """
    default_number_available = models.IntegerField(
        _("Default number available"), default=1,
        help_text=_("Pourcent available: default is 100. Could be for instance "
                    "200 for arms when you have two."))
    is_default = models.BooleanField(
        _("Is the default body shape"), default=False,
        help_text=_("Default body shape is used when, for instance, you want "
                    "to pick up a thing"))
    available_by_default = models.BooleanField(_("Available by default"),
                                               default=True)
    objects = SlugManager()

    class Meta:
        verbose_name = _("Type: Body area")
        verbose_name_plural = _("Types: Body area")
        ordering = ('order',)


pre_save.connect(pre_save_slug, sender=BodyArea)


class TaskManager(models.Manager):
    def get_by_natural_key(self, action_slug, order):
        return self.get(action__slug=action_slug, order=order)


class Task(models.Model):
    action = models.ForeignKey(Action, verbose_name=_("Action"),
                               on_delete=models.CASCADE)
    order = models.IntegerField(_("Order"), default=10)
    # PARAMETER_HELP = _('Value can be: DEFAULT, FIRST, ALL, EACH, RANDOM, '
    #                    'CHARACTER or free text.')
    objects = TaskManager()

    class Meta:
        verbose_name = _("Events - Task")
        verbose_name_plural = _("Events - Tasks")
        ordering = ('order',)
        unique_together = ("action", "order")

    def __str__(self):
        return str(self.action)

    def natural_key(self):
        return (self.action.slug, self.order)

    def apply(self, character, opponents):
        result = self.action.do(character, opponents, force=True)
        action_view = self.action.get_view()
        if not action_view:
            character.write_log(self.action.name, from_is_hidden=True,
                                icon=self.action.icon)
            return
        kwargs = {}
        if "targets" in action_view.form_class.base_fields:
            kwargs["targets"] = opponents
        action_view.form_class(**kwargs).save(character, opponents, result)


class Event(models.Model):
    available = models.BooleanField(_("Available"), default=True)
    order = models.IntegerField(_("Order"), default=1)
    first_delay = models.IntegerField(
        _("First delay"), default=0,
        help_text=_("Delay before the first reactions (in hours)")
    )
    delay = models.IntegerField(
        _("Delay"), blank=True, null=True,
        help_text=_("Delay between reactions (in hours) - if left to "
                    "None only one reaction is executed.")
    )
    maximum = models.IntegerField(
        _("Maximum of reaction"), blank=True, null=True,
        help_text=_("Null value for no maximum")
    )
    tasks = models.ManyToManyField(Task, blank=True)
    # logical: between each action/state/... trigger the operator is OR,
    # between different type of trigger the operator is AND
    action_triggers = models.ManyToManyField(Action, blank=True,
                                             verbose_name=_("Action triggers"))
    state_triggers = models.ManyToManyField(
        BasicFormula, blank=True, verbose_name=_("State triggers (formulas)"))

    class Meta:
        abstract = True


class LocationEvent(Event):
    locations = models.ManyToManyField("morrigan_geography.Location",
                                       blank=True)
    LOCATION_TRIGGERS = (
        ("E", _("enter")),
        ("Q", _("quit")),
    )
    location_trigger = models.CharField(_("Location trigger"), max_length=1,
                                        choices=LOCATION_TRIGGERS, blank=True,
                                        null=True)

    class Meta:
        verbose_name = _("Events - Location")
        verbose_name_plural = _("Events - Location")
        ordering = ("order",)

    def __str__(self):
        lbl = self.locations_lbl()
        if self.location_trigger in self.LOCATION_TRIGGERS:
            lbl += " - " + self.LOCATION_TRIGGERS[self.location_trigger]
        lbl += " - " + self.tasks_lbl()
        return lbl

    def locations_lbl(self):
        locations = []
        for location in self.locations.all():
            locations.append(str(location))
        lbl = " ; ".join(locations)
        if not lbl:
            lbl = _("Everywhere")
        return str(lbl)

    def tasks_lbl(self):
        return " ; ".join([str(task) for task in self.tasks.all()])

    def first_trigger(self, location_id, involved_character):
        if not self.locations.count():  # apply to all locations
            location_id = None
        et, created = LocationEventTarget.objects.get_or_create(
            target_character=involved_character, location_id=location_id,
            event=self)
        if created:
            et.trigger()
        return created


class CharacterEvent(Event):
    characters = models.ManyToManyField("morrigan_characters.Character")

    class Meta:
        verbose_name = _("Events - Character")
        verbose_name_plural = _("Events - Character")
        ordering = ("order",)

    def first_trigger(self, character_id, involved_character):
        et, created = CharacterEventTarget.objects.get_or_create(
            target_character=involved_character, character_id=character_id,
            event=self)
        if created:
            et.trigger()
        return created


class EventTarget(models.Model):
    target_character = models.ForeignKey(
        "morrigan_characters.Character", verbose_name=_("Source character"),
        related_name="%(model_name)s_event_target", on_delete=models.CASCADE)
    start = models.DateTimeField(_("Start"), default=timezone.now)
    last = models.DateTimeField(_("Last"), default=timezone.now)
    number = models.IntegerField(_("Number"), default=0)

    class Meta:
        abstract = True

    def check_current_apply(self):
        now = timezone.now()
        start_elapsed = now - self.start
        start_elapsed_hours = start_elapsed.days * 24 + int(
            start_elapsed.seconds / 3600)
        if not self.number and self.event.first_delay and (
                start_elapsed_hours) < self.event.first_delay:
            return False
        last_elapsed = now - self.last
        last_elapsed_hours = last_elapsed.days * 24 + int(
            last_elapsed.seconds / 3600)
        if self.number and self.event.delay and (
                last_elapsed_hours) < self.event.delay:
            return False
        return True

    def check_apply(self):
        apply = False
        if not self.event.state_triggers.count():
            apply = True
        for state_trigger in self.event.state_triggers.all():
            result = state_trigger.evaluate(self.target_character)
            if result.get("main", None):
                apply = True
                break
        if not apply:
            return False
        if not self.event.delay and self.number:
            return False
        if self.event.maximum and self.number >= self.event.maximum:
            return False
        return True


class CharacterEventTarget(EventTarget):
    character = models.ForeignKey(
        "morrigan_characters.Character", verbose_name=_("Character"),
        related_name="event_trigger", on_delete=models.CASCADE)
    event = models.ForeignKey(CharacterEvent, verbose_name=_("Event"),
                              on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Events - Character - Scheduled")
        verbose_name_plural = _("Events - Character - Scheduled")
        ordering = ("start",)

    def check_apply(self):
        if not super(CharacterEventTarget, self).check_apply():
            return False
        if self.character_id not in self.target_character.get_neighbors(
                need_conscious=False).values_list('id', flat=True):
            self.delete()
            return False
        return True

    def trigger(self):
        if not self.check_apply():
            self.delete()
            return
        if not self.check_current_apply():
            return
        self.last = timezone.now()
        self.number += 1
        self.save()
        for task in self.event.tasks.all():
            task.apply(self.character, [self.target_character])
        return True


class LocationEventTarget(EventTarget):
    location = models.ForeignKey("morrigan_geography.Location",
                                 verbose_name=_("Location"), blank=True,
                                 null=True, on_delete=models.CASCADE)
    event = models.ForeignKey(LocationEvent, verbose_name=_("Event"),
                              on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Events - Location - Scheduled")
        verbose_name_plural = _("Events - Location - Scheduled")
        ordering = ("start",)

    def check_apply(self):
        if not super(LocationEventTarget, self).check_apply():
            return False
        # not here anymore
        if self.location_id and \
                self.location_id != self.target_character.location_id:
            return False
        if self.number >= 1 and self.event.location_trigger:
            # quit and enter event apply only one time
            return False
        return True

    def trigger(self):
        if not self.check_apply():
            self.delete()
            return
        if not self.check_current_apply():
            return
        self.last = timezone.now()
        self.number += 1
        self.save()
        opponents = []
        if self.location:
            opponents = [self.location]
        for task in self.event.tasks.all():
            task.apply(self.target_character, opponents)
        return True
