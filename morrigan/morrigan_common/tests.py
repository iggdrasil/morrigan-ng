#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015-2016  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import datetime
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from django.contrib.auth import get_user_model
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone

from morrigan_common import models, event_manager
from morrigan_common.admintest import adminviewstest

from morrigan_characters.models import Character

from rest_framework.test import APITestCase

DEFAULT_FIXTURES = [
    'fixtures/bakkasab-fr/001-users.json',
    'fixtures/bakkasab-fr/002-abilities.json',
    'fixtures/bakkasab-fr/003-races-sex-steptypes.json',
    'fixtures/bakkasab-fr/004-actions.json',
    'fixtures/bakkasab-fr/005-world.json',
    'fixtures/bakkasab-fr/006-characters.json',
    'fixtures/bakkasab-fr/007-items.json',
]


class TestContextProcessor(APITestCase):
    def test_systemmessage(self):
        url = reverse("login")  # no redirection with the login url
        with self.settings(SYSTEM_MESSAGE="Catapulte !"):
            client = Client()
            response = client.get(url)
            self.assertTrue("Catapulte !" in str(response.content))

User = get_user_model()


class UserInit:
    def create_superuser(self):
        user = User.objects.create_user("admin", "e@e.net", "pass")
        user.is_superuser = True
        user.save()
        player, c = models.Player.objects.get_or_create(user=user)
        return user


class TestAction(APITestCase):
    fixtures = DEFAULT_FIXTURES

    def test_formula(self):
        nim = Character.objects.get(slug='nimnae')
        ark = Character.objects.get(slug='arkenlond')
        slap = models.Action.objects.get(slug='slap')
        # use SUCCESS key in main formula
        with self.assertRaises(models.FormulaParseError):
            slap.formula.formula = "[SUCCESS]"
            slap.formula.evaluate(nim, ark, log=True)
        # use an invalid ability in formula
        with self.assertRaises(models.FormulaParseError):
            slap.formula.formula = "[invalid-ability]"
            slap.formula.evaluate(nim, ark, log=True)

        slap = models.Action.objects.get(slug='slap')
        # missing opponent
        with self.assertRaises(models.FormulaParseError):
            slap.formula.evaluate(nim, log=True)

        current_ap = nim.ap
        logs = slap.do(nim, ark, log=True)
        nim = Character.objects.get(slug='nimnae')
        self.assertEqual(current_ap + logs['character']['ap']['result'],
                         nim.ap)


class TestMorrigan(APITestCase):
    fixtures = DEFAULT_FIXTURES

    def setUp(self):
        self.user = User.objects.get(username='arkenlond')
        self.user.set_password('password')
        self.user.save()


class TestMorriganAction(StaticLiveServerTestCase):
    fixtures = DEFAULT_FIXTURES

    def setUp(self):
        self.user = User.objects.get(username='arkenlond')
        self.user.set_password('password')
        self.user.save()
        self.selenium = webdriver.Firefox()
        self.selenium.maximize_window()
        super(TestMorriganAction, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(TestMorriganAction, self).tearDown()

    def location_page(self, slug):
        location_url = reverse('character-page-location',
                               kwargs={"slug": slug})
        self.selenium.get('%s%s' % (self.live_server_url, location_url))
        WebDriverWait(self.selenium, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME,
                                            "character-view-detail"))
        )

    def identification(self, login, passwd):
        self.selenium.get('%s%s' % (self.live_server_url, "/login/"))
        username = self.selenium.find_element_by_id("id_username")
        username.send_keys(login)
        password = self.selenium.find_element_by_id("id_password")
        password.send_keys(passwd)
        self.selenium.find_element_by_xpath('//input[@value="Login"]').click()
        WebDriverWait(self.selenium, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, "characters"))
        )
        time.sleep(1)

    def action_show(self, name):
        action_icon = self.selenium.find_element_by_xpath(
            '//li[@id="action-' + name + '"]/a'
        )
        action_icon.click()
        WebDriverWait(self.selenium, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, "action-form"))
        )

    def action_submit(self, wait_for_result=False):
        self.selenium.find_element_by_xpath(
            '//div[@class="modal-footer"]/button[contains(@class, "submit")]'
        ).click()
        if wait_for_result:
            WebDriverWait(self.selenium, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "action-result"))
            )

    def action_close(self):
        self.selenium.find_element_by_xpath(
            '//div[@class="modal-footer"]/button[@data-dismiss="modal"]'
        ).click()
        time.sleep(1)

    def select_character(self, name):
        character_path = '//li[@id="character-detail-%s"]'\
            '/span[contains(@class, "character-view-detail")]'\
            '/a[contains(@class, "character-check")]'
        self.selenium.find_element_by_xpath(
            character_path % name
        ).click()
        time.sleep(1)  # wait for the JS to load...

    def select_item(self, character, item, action_awaited=''):
        self.selenium.find_element_by_xpath(
            '//li[@id="character-detail-' + character + '"]/div/'
            'ul[@class="items"]/li/span/a/i[contains(@class, "' +
            item + '")]').click()
        if action_awaited:
            WebDriverWait(self.selenium, 10).until(
                EC.presence_of_element_located((By.ID,
                                                "action-" + action_awaited))
            )
        else:
            WebDriverWait(self.selenium, 10).until(
                EC.presence_of_element_located((By.ID,
                                                "action-list"))
            )

    def select_location_item(self, item):
        self.selenium.find_element_by_xpath(
            '//div[@id="location"]/div/ul[@class="items"]/'
            'li/span/a/i[contains(@class, "' + item + '")]').click()
        time.sleep(1)  # wait for the JS to load...


class TestAdminViews(TestCase):
    def test_admin_views(self):
        adminviewstest(self, 'morrigan_common')


class TestLocationEvents(TestCase):
    fixtures = DEFAULT_FIXTURES + [
        'fixtures/bakkasab-fr/008-events/001-daily-reset.json',
    ]

    def _first_trigger(self):
        q = Character.objects
        for character in q.all():
            event_manager.event_trigger(character)

    def test_location_event_creation(self):
        nb = models.LocationEventTarget.objects.count()
        self._first_trigger()
        # all character have the daily reset event
        q = Character.objects
        self.assertEqual(
            models.LocationEventTarget.objects.count(),
            nb + q.count()
        )

    def test_event_trigger(self):
        nim = Character.objects.get(slug="nimnae")
        log_nb = nim.logs.count()
        for character in Character.objects.all():
            character.hp = 0
            character.ap = 0
            character.save()
        self._first_trigger()
        q = models.LocationEventTarget.objects
        # one trigger on creation
        for target in q.all():
            self.assertEqual(target.number, 1)
        for character in Character.objects.all():
            self.assertEqual(character.hp, max(int(character.max_hp / 20), 1))
            self.assertEqual(character.ap, max(int(character.max_ap / 5), 1))
        self.assertEqual(nim.logs.count(), log_nb + 1)

        # no direct re-trigger
        event_manager.trigger_all_events()
        for target in q.all():
            self.assertEqual(target.number, 1)
        # change last trigger to several days before to force a re-trigger
        date = timezone.now() - datetime.timedelta(days=5, hours=1)
        for target in q.all():
            target.start = date
            target.last = date
            target.save()
        event_manager.trigger_all_events()
        for target in q.all():  # only one trigger
            self.assertEqual(target.number, 2)
        for character in Character.objects.all():
            self.assertEqual(character.hp,
                             2 * max(int(character.max_hp / 20), 1))
            self.assertEqual(character.ap,
                             2 * max(int(character.max_ap / 5), 1))
        # no direct re-trigger
        event_manager.trigger_all_events()
        for target in q.all():
            self.assertEqual(target.number, 2)


class TestCharacterEvents(TestCase):
    fixtures = DEFAULT_FIXTURES + [
        'fixtures/bakkasab-fr/008-events/002-bot-example.json',
    ]

    def _first_trigger(self):
        q = Character.objects
        for character in q.all():
            event_manager.event_trigger(character)

    def test_character_event_creation(self):
        nb = models.CharacterEventTarget.objects.count()
        self._first_trigger()
        sulfure = Character.objects.get(slug="sulfure")
        # all character have the daily reset event
        q = Character.objects.filter(location_id=sulfure.location_id).exclude(
            slug="sulfure")
        self.assertEqual(
            models.CharacterEventTarget.objects.count(),
            nb + q.count()
        )

    def test_event_trigger(self):
        nim = Character.objects.get(slug="nimnae")
        log_nb = nim.logs.count()
        self._first_trigger()
        q = models.CharacterEventTarget.objects
        # one trigger on creation
        targets = list(q.all())
        for target in targets:
            self.assertEqual(target.number, 1)
        self.assertEqual(nim.logs.count(), log_nb + len(targets))

        # no direct re-trigger
        event_manager.trigger_all_events()
        for target in q.all():
            self.assertEqual(target.number, 1)

        # change last trigger to several days before to force a re-trigger
        date = timezone.now() - datetime.timedelta(days=5, hours=1)
        for target in q.all():
            target.start = date
            target.last = date
            target.save()
        event_manager.trigger_all_events()
        for target in q.all():  # only one trigger
            self.assertEqual(target.number, 2)
        self.assertEqual(nim.logs.count(), log_nb + len(targets) * 2)

        # no direct re-trigger
        event_manager.trigger_all_events()
        for target in q.all():
            self.assertEqual(target.number, 2)
