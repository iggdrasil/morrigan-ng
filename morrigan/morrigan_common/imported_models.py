
# generated from database: to be reintegrated in the software

# classes

raise "Don't import this yet!"

from django.db import models

from morrigan_common.models import Action, Effect, Sex, GameMaster
from morrigan_characters.models import Character, Player
from morrigan_geography.models import Location, MapLocation
from morrigan_inventory.models import Item, Currency, GenericItem


class PersoClasse(models.Model):
    pce_id = models.IntegerField(primary_key=True)
    pce_prs = models.ForeignKey("Character")
    pce_cln = models.ForeignKey("ClasseNiveau")
    pce_titre = models.CharField(max_length=64, blank=True)

    class Meta:
        db_table = u'perso_classe'


class Classe(models.Model):
    cls_id = models.IntegerField(primary_key=True)
    cls_nom = models.CharField(max_length=50, unique=True)
    cls_desc_mj = models.CharField(max_length=128, blank=True)
    cls_tcl = models.ForeignKey("TypeClasse")
    cls_insc_ouverte = models.BooleanField()

    class Meta:
        db_table = u'classe'


class ClasseniveauAction(models.Model):
    cna_id = models.IntegerField(primary_key=True)
    cna_act = models.ForeignKey(Action)
    cna_cln = models.ForeignKey("ClasseNiveau")
    cna_autorise = models.NullBooleanField(null=True, blank=True)

    class Meta:
        db_table = u'classeniveau_action'


class ClasseNiveau(models.Model):
    cln_id = models.IntegerField(primary_key=True)
    cln_cls = models.ForeignKey(Classe)
    cln_niveau = models.SmallIntegerField()
    cln_mld = models.ForeignKey(Effect, null=True, blank=True)
    cln_titre = models.CharField(max_length=50, unique=True)
    cln_titre_visible = models.NullBooleanField(null=True, blank=True)
    cln_desc_mj = models.CharField(max_length=128, blank=True)

    class Meta:
        db_table = u'classe_niveau'


class TypeClasse(models.Model):
    tcl_id = models.IntegerField(primary_key=True)
    tcl_nom = models.CharField(max_length=50)

    class Meta:
        db_table = u'type_classe'


class PrerequisCarac(models.Model):
    pqa_id = models.IntegerField(primary_key=True)
    pqa_cln = models.ForeignKey("ClasseNiveau", unique=True)
    pqa_form_carac = models.IntegerField()

    class Meta:
        db_table = u'prerequis_carac'


class InterditClasse(models.Model):
    inc_id = models.IntegerField(primary_key=True)
    inc_cls = models.ForeignKey(Classe, related_name='forbid_class')
    inc_interdit_cls = models.ForeignKey(
        Classe, related_name='forbid_forbiden_class')

    class Meta:
        db_table = u'interdit_classe'


class PersoClasseInt(models.Model):
    pci_id = models.IntegerField(primary_key=True)
    pci_prs = models.ForeignKey("Character")
    pci_cls = models.ForeignKey("Classe")
    pci_type_interdit = models.SmallIntegerField()

    class Meta:
        db_table = u'perso_classe_int'


class PrerequisClasse(models.Model):
    pqc_id = models.IntegerField(primary_key=True)
    pqc_cln = models.ForeignKey("ClasseNiveau")
    pqc_cls = models.ForeignKey("Classe")
    pqc_niveau_min = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = u'prerequis_classe'

###############################################################################

# companies


class Compagnie(models.Model):
    cmp_id = models.IntegerField(primary_key=True)
    cmp_nom = models.CharField(max_length=100, unique=True)

    class Meta:
        db_table = u'compagnie'


class PlaceCompagnie(models.Model):
    plc_id = models.IntegerField(primary_key=True)
    plc_prs = models.ForeignKey("Character", unique=True)
    plc_cmp = models.ForeignKey("Compagnie")
    plc_place = models.SmallIntegerField(null=True, blank=True)

    class Meta:
        db_table = u'place_compagnie'

# #############################################################################

# banques et autres


class DebitElement(models.Model):
    dbt_id = models.IntegerField(primary_key=True)
    dbt_lie = models.ForeignKey("Location")
    dbt_egn = models.ForeignKey("GenericItem")

    class Meta:
        db_table = u'debit_element'


class DepotElement(models.Model):
    dpt_id = models.IntegerField(primary_key=True)
    dpt_lie = models.ForeignKey(Location)
    dpt_elm = models.ForeignKey(Item)
    dpt_prs = models.ForeignKey(Character, null=True, blank=True)

    class Meta:
        db_table = u'depot_element'


class VenteElement(models.Model):
    vnt_id = models.IntegerField(primary_key=True)
    vnt_lie = models.ForeignKey(Location)
    vnt_elm = models.ForeignKey(Item)

    class Meta:
        db_table = u'vente_element'


class Banque(models.Model):
    bnq_id = models.IntegerField(primary_key=True)
    bnq_lie = models.ForeignKey(Location)
    bnq_prs = models.ForeignKey(Character)
    bnq_mnn = models.ForeignKey(Currency)
    bnq_somme = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = u'banque'


# #############################################################################

# Vehicles, Vaisseaux...

class Monture(models.Model):
    mon_id = models.IntegerField(primary_key=True)
    mon_prs = models.ForeignKey(Character, related_name='mount')
    mon_cavalier_prs = models.ForeignKey(Character, null=True, blank=True,
                                         related_name='rider')
    mon_clef_elm = models.ForeignKey(Item, null=True, blank=True)
    mon_vivante = models.BooleanField()
    mon_egn = models.ForeignKey(GenericItem, null=True, blank=True)
    mon_consomation = models.IntegerField(null=True, blank=True)
    mon_usure = models.IntegerField(null=True, blank=True)
    mon_surplus_cout_pn = models.IntegerField(null=True, blank=True)
    mon_formule_difficulte = models.CharField(max_length=60)

    class Meta:
        db_table = u'monture'


class VaisseauGen(models.Model):
    vgn_id = models.IntegerField(primary_key=True)
    vgn_egn = models.ForeignKey(GenericItem, unique=True)
    vgn_charge_max = models.IntegerField()
    vgn_tvs = models.ForeignKey("TypeVaisseau")

    class Meta:
        db_table = u'vaisseau_gen'


class TypeVaisseau(models.Model):
    tvs_id = models.IntegerField(primary_key=True)
    tvs_nom = models.CharField(max_length=50)

    class Meta:
        db_table = u'type_vaisseau'


class TypePosteTmp(models.Model):
    tpt_id = models.IntegerField(primary_key=True)
    tpt_nom = models.CharField(max_length=50)

    class Meta:
        db_table = u'type_poste_tmp'


class VaisseauGenPoste(models.Model):
    vgp_id = models.IntegerField(primary_key=True)
    vgp_vgn = models.ForeignKey(VaisseauGen)
    vgp_tpt = models.ForeignKey(TypePosteTmp)
    vgp_nb_perso = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = u'vaisseau_gen_poste'


class Vaisseau(models.Model):
    vss_id = models.IntegerField(primary_key=True)
    vss_elm = models.ForeignKey(Item, unique=True)
    vss_sct = models.ForeignKey("Secteur", unique=True)

    class Meta:
        db_table = u'vaisseau'


class Poste(models.Model):
    pst_id = models.IntegerField(primary_key=True)
    pst_tps = models.ForeignKey("TypePoste")
    pst_prs = models.ForeignKey(Character, null=True, blank=True)
    pst_vhc = models.ForeignKey("Vehicule")

    class Meta:
        db_table = u'poste'


class TypePoste(models.Model):
    tps_id = models.IntegerField(primary_key=True)
    tps_nom = models.CharField(max_length=50)
    tps_rendement_min = models.IntegerField()
    tps_propulsion_active = models.NullBooleanField(null=True, blank=True)
    tps_formule_difficulte = models.CharField(max_length=60)

    class Meta:
        db_table = u'type_poste'


class TypeVehiculePoste(models.Model):
    ttp_id = models.IntegerField(primary_key=True)
    ttp_tvh = models.ForeignKey("TypeVehicule")
    ttp_tps = models.ForeignKey("TypePoste", null=True, blank=True)

    class Meta:
        db_table = u'type_vehicule_poste'


class RestrictionCalque(models.Model):
    rtc_id = models.IntegerField(primary_key=True)
    rtc_tvh = models.ForeignKey("TypeVehicule")
    rtc_clq = models.ForeignKey("Calque")
    rtc_rendement = models.IntegerField()

    class Meta:
        db_table = u'restriction_calque'


###############################################################################

# Season and cycles

class Periode(models.Model):
    prd_id = models.IntegerField(primary_key=True)
    prd_nom = models.CharField(max_length=50)
    prd_ccl = models.ForeignKey("Cycle")
    prd_min = models.IntegerField(null=True, blank=True)
    prd_max = models.IntegerField(null=True, blank=True)
    prd_complement_description = models.CharField(max_length=1500, blank=True)

    class Meta:
        db_table = u'periode'


class CycleSecteur(models.Model):
    csc_id = models.IntegerField(primary_key=True)
    csc_sct = models.ForeignKey("Secteur")
    csc_ccl = models.ForeignKey("Cycle")

    class Meta:
        db_table = u'cycle_secteur'


class CycleWorld(models.Model):
    cmn_id = models.IntegerField(primary_key=True)
    cmn_mnd = models.ForeignKey("World")
    cmn_ccl = models.ForeignKey("Cycle")

    class Meta:
        db_table = u'cycle_monde'


class Cycle(models.Model):
    ccl_id = models.IntegerField(primary_key=True)
    ccl_nom = models.CharField(max_length=50)
    ccl_nombre_periode = models.IntegerField()
    ccl_duree_periode = models.IntegerField(null=True, blank=True)
    ccl_duree_nombre_cycles = models.IntegerField(null=True, blank=True)
    ccl_duree_ccl = models.ForeignKey('self', null=True, blank=True)
    ccl_duree_nombre_cycles_ecoules = models.IntegerField()
    ccl_pourcentage_alea = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = u'cycle'


class LieuCycle(models.Model):
    lcd_id = models.IntegerField(primary_key=True)
    lcd_ccl = models.ForeignKey("Cycle")
    lcd_lie = models.ForeignKey(Location)

    class Meta:
        db_table = u'lieu_cycle'


class LieuCyclique(models.Model):
    lcc_id = models.IntegerField(primary_key=True)
    lcc_prd = models.ForeignKey(Periode)
    lcc_lie = models.ForeignKey(Location)
    lcc_description = models.CharField(max_length=1500, blank=True)

    class Meta:
        db_table = u'lieu_cyclique'


###############################################################################

# Mime


class TypeMime(models.Model):
    tmm_id = models.IntegerField(primary_key=True)
    tmm_nom = models.CharField(max_length=50)

    class Meta:
        db_table = u'type_mime'


class Mime(models.Model):
    mim_id = models.IntegerField(primary_key=True)
    mim_nom = models.CharField(max_length=24)
    mim_label = models.CharField(max_length=64)
    mim_tmm = models.ForeignKey(TypeMime)

    class Meta:
        db_table = u'mime'

###############################################################################

# winds


class TypeVent(models.Model):
    tvt_id = models.IntegerField(primary_key=True)
    tvt_nom = models.CharField(max_length=50)

    class Meta:
        db_table = u'type_vent'


class InfluenceVent(models.Model):
    inv_id = models.IntegerField(primary_key=True)
    inv_tvh = models.ForeignKey("TypeVehicule")
    inv_tvt = models.ForeignKey(TypeVent)
    inv_puissance = models.IntegerField()

    class Meta:
        db_table = u'influence_vent'


class VentMapLocation(models.Model):
    cvt_id = models.IntegerField(primary_key=True)
    cvt_tvt = models.ForeignKey(TypeVent)
    cvt_lic = models.ForeignKey(MapLocation)
    cvt_force = models.TextField(blank=True)  # This field type is a guess.

    class Meta:
        db_table = u'vent_lieu_carto'

###############################################################################
# inscriptions


class Candidature(models.Model):
    can_id = models.IntegerField(primary_key=True)
    can_jou = models.ForeignKey(Player)
    can_pro = models.ForeignKey("ProfilAnnonce")
    can_statut = models.IntegerField()
    can_nomperso = models.CharField(max_length=255)
    can_rac = models.ForeignKey("Race")
    can_cls = models.ForeignKey(Classe)
    can_sex = models.ForeignKey(Sex)
    can_descriptionperso = models.TextField()
    can_backgroundperso = models.TextField()
    can_butsperso = models.IntegerField()
    can_debatjoumj = models.TextField(blank=True)
    can_debatmj = models.TextField(blank=True)
    can_historique = models.TextField(blank=True)
    can_commentaire = models.TextField(blank=True)
    can_datemodification = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = u'candidature'


class CategorieProfil(models.Model):
    ctp_id = models.IntegerField(primary_key=True)
    ctp_nom = models.CharField(max_length=255)

    class Meta:
        db_table = u'categorie_profil'


class ProfilAnnonce(models.Model):
    pro_id = models.IntegerField(primary_key=True)
    pro_nom = models.CharField(max_length=255)
    pro_statut = models.IntegerField()
    pro_ctp = models.ForeignKey(CategorieProfil)
    pro_mji = models.ForeignKey(GameMaster)
    pro_desc = models.TextField()
    pro_debat = models.TextField(blank=True)
    pro_nbpersos = models.IntegerField()
    pro_historique = models.TextField(blank=True)
    pro_demandeaction = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = u'profil_annonce'


class News(models.Model):
    new_id = models.IntegerField(primary_key=True)
    new_titre = models.TextField()
    new_txt = models.TextField()
    new_date = models.DateTimeField()
    new_dateedit = models.DateTimeField(null=True, blank=True)
    new_mji = models.ForeignKey(GameMaster)

    class Meta:
        db_table = u'news'


class ProfilRace(models.Model):
    pra_id = models.IntegerField(primary_key=True)
    pra_pro = models.ForeignKey(ProfilAnnonce)
    pra_rac = models.ForeignKey("Race")

    class Meta:
        db_table = u'profil_race'


class ProfilSexe(models.Model):
    psx_id = models.IntegerField(primary_key=True)
    psx_pro = models.ForeignKey(ProfilAnnonce)
    psx_sex = models.ForeignKey(Sex)

    class Meta:
        db_table = u'profil_sexe'


class Question(models.Model):
    que_id = models.IntegerField(primary_key=True)
    que_intitule = models.TextField()

    class Meta:
        db_table = u'question'


class ProfilClasse(models.Model):
    pcl_id = models.IntegerField(primary_key=True)
    pcl_pro = models.ForeignKey(ProfilAnnonce)
    pcl_cls = models.ForeignKey(Classe)

    class Meta:
        db_table = u'profil_classe'


class Categorie(models.Model):
    ctg_id = models.SmallIntegerField(primary_key=True)
    ctg_libelle = models.CharField(max_length=50, unique=True)
    ctg_systeme = models.BooleanField()

    class Meta:
        db_table = u'categorie'


class Parametre(models.Model):
    prm_id = models.SmallIntegerField(primary_key=True)
    prm_ctg = models.ForeignKey(Categorie)
    prm_libelle = models.CharField(max_length=50)
    prm_valeur = models.CharField(max_length=30)
    prm_description = models.CharField(max_length=500)

    class Meta:
        db_table = u'parametre'


class Reponse(models.Model):
    rep_id = models.IntegerField(primary_key=True)
    rep_que = models.ForeignKey(Question)
    rep_intitule = models.TextField()
    rep_bonne = models.BooleanField()

    class Meta:
        db_table = u'reponse'
