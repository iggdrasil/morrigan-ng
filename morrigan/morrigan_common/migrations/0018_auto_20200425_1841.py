# Generated by Django 2.2 on 2020-04-25 16:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('morrigan_common', '0017_auto_20200423_1243'),
    ]

    operations = [
        migrations.CreateModel(
            name='DocumentCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('description', models.TextField(blank=True, null=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
                ('slug', models.SlugField(blank=True, null=True, unique=True, verbose_name='Slug')),
            ],
            options={
                'verbose_name': 'Document category',
                'verbose_name_plural': 'Document categories',
            },
        ),
        migrations.AddField(
            model_name='document',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='morrigan_common.DocumentCategory'),
        ),
    ]
