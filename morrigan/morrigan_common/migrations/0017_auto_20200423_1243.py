# Generated by Django 2.2 on 2020-04-23 10:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('morrigan_common', '0016_bodyarea_available_by_default'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='abilitytype',
            options={'verbose_name': 'Type: Ability', 'verbose_name_plural': 'Types: Ability'},
        ),
        migrations.AlterModelOptions(
            name='action',
            options={'ordering': ('order', 'name')},
        ),
        migrations.AlterModelOptions(
            name='damagetype',
            options={'verbose_name': 'Type: Damage', 'verbose_name_plural': 'Types: Damage'},
        ),
        migrations.AlterModelOptions(
            name='effect',
            options={'ordering': ('order', 'name')},
        ),
        migrations.AlterModelOptions(
            name='effecttype',
            options={'ordering': ('order', 'name')},
        ),
        migrations.AlterModelOptions(
            name='material',
            options={'verbose_name': 'Type: Material', 'verbose_name_plural': 'Types: Material'},
        ),
        migrations.AlterModelOptions(
            name='player',
            options={'verbose_name': 'Player', 'verbose_name_plural': 'Players'},
        ),
        migrations.AlterModelOptions(
            name='size',
            options={'verbose_name': 'Type: Size', 'verbose_name_plural': 'Types: Size'},
        ),
        migrations.AlterModelOptions(
            name='skilltype',
            options={'verbose_name': 'Type: Skill', 'verbose_name_plural': 'Types: Skill'},
        ),
        migrations.AlterModelOptions(
            name='technology',
            options={'ordering': ('order', 'name')},
        ),
        migrations.AlterField(
            model_name='bodyarea',
            name='default_number_available',
            field=models.IntegerField(default=1, help_text='Pourcent available: default is 100. Could be for instance 200 for arms when you have two.', verbose_name='Default number available'),
        ),
        migrations.AlterField(
            model_name='skill',
            name='skill_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='morrigan_common.SkillType', verbose_name='Skill type'),
        ),
    ]
