import sys

from django.db.models import Q

from morrigan_common import models


def event_trigger(character, action=None, location_action=None,
                  location=None):
    """
    Trigger an event

    :param character: character involved
    :param action: action made
    :param location_action: location action - must be ENTER or QUIT
    :param location: explicit location
    :return: return number of new event triggered
    """
    events = []
    base_q = Q(locations=None)
    if not location:
        location = character.location_id
    if location:
        base_q |= Q(locations__id=location)
    q = models.LocationEvent.objects.filter(base_q).filter(available=True)

    base_q = Q(action_triggers=None)
    if action:
        base_q |= Q(action_triggers__pk=action.pk)
    q = q.filter(base_q)

    base_q = Q(location_trigger=None) | Q(location_trigger="")
    if location_action:  # strict evaluation when location_action is set
        base_q = Q(location_trigger=location_action[0])
    q = q.filter(base_q)
    events += [(character.location_id, e) for e in q.all()]

    neighbors = character.get_neighbors(
        need_conscious=False).values_list('id', flat=True)
    for neighbor in neighbors:
        for event in models.CharacterEvent.objects.filter(
                characters__id=neighbor, available=True).all():
            events.append((neighbor, event))

    new_events = 0
    for source, event in sorted(events, key=lambda x: x[1].order):
        apply = False
        if not event.state_triggers.count():
            apply = True
        for state_trigger in event.state_triggers.all():
            result = state_trigger.evaluate(character)
            if result.get("main", None):
                apply = True
                break
        if not apply:
            continue
        if event.first_trigger(source, character):
            new_events += 1
    return new_events


def trigger_all_events(verbose=False):
    event_targets = [
        ("location", v)
        for v in models.LocationEventTarget.objects.values_list("id", "start")]
    event_targets += [
        ("character", v)
        for v in models.CharacterEventTarget.objects.values_list("id", "start")]
    nb = len(event_targets)
    triggered = 0
    for idx, key_event_target in enumerate(
            sorted(event_targets, key=lambda x: x[1][1])):
        key, event_target = key_event_target
        if verbose:
            sys.stdout.write("\rProcessing: {}/{} - {} %".format(
                idx + 1, nb, round((idx + 1) / nb) * 100, 2))
            sys.stdout.flush()
        if key == "location":
            event_target = models.LocationEventTarget.objects.get(
                id=event_target[0])
        else:
            event_target = models.CharacterEventTarget.objects.get(
                id=event_target[0])
        if event_target.trigger():
            triggered += 1
    return triggered
