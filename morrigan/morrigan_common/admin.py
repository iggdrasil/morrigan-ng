#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2010-2017  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

"""
Settings for administration pages
"""
import csv
from io import TextIOWrapper

from adminsortable.admin import SortableAdmin, SortableTabularInline
from django.contrib import admin, messages
from django import forms
from django.db import IntegrityError
from django.http import HttpResponseRedirect, HttpResponse
from django.conf.urls import url
from django.shortcuts import render
from django.urls import reverse
from django.db.models.fields import BooleanField, IntegerField, FloatField, \
    CharField, FieldDoesNotExist
from django.db.models.fields.related import ForeignKey
from django.utils.translation import ugettext_lazy as _

from admin import admin_site

from morrigan_common import models
from morrigan_characters.models import Character, CharacterAbility


def export_as_csv_action(description=_("Export selected as CSV file"),
                         fields=None, exclude=None, header=True):
    """
    This function returns an export csv action
    'fields' and 'exclude' work like in django ModelForm
    'header' is whether or not to output the column names as the first row
    """
    def export_as_csv(modeladmin, request, queryset):
        opts = modeladmin.model._meta
        field_names = set([field.name for field in opts.fields])
        if fields:
            fieldset = set(fields)
            field_names = field_names & fieldset
        elif exclude:
            excludeset = set(exclude)
            field_names = field_names - excludeset

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s.csv' % \
            str(opts).replace('.', '_')

        writer = csv.writer(response)
        if header:
            writer.writerow(list(field_names))
        for obj in queryset.order_by('pk'):
            row = []
            for field in field_names:
                value = getattr(obj, field)
                if hasattr(value, 'slug'):
                    value = getattr(value, 'slug')
                elif value is None:
                    value = ""
                else:
                    value = str(value)
                row.append(value)

            writer.writerow(row)
        return response
    export_as_csv.short_description = description
    return export_as_csv


class ImportGenericForm(forms.Form):
    csv_file = forms.FileField(
        label=_("CSV file"),
        help_text=_("Only unicode encoding is managed - convert your"
                    " file first")
    )


class ImportActionAdmin(admin.ModelAdmin):
    change_list_template = "admin/gen_change_list.html"
    import_keys = ['slug', "id"]

    def get_urls(self):
        urls = super(ImportActionAdmin, self).get_urls()
        my_urls = [
            url(r'^import-from-csv/$', self.import_generic),
        ]
        return my_urls + urls

    def import_generic(self, request):
        form = None

        if 'apply' in request.POST:
            form = ImportGenericForm(request.POST, request.FILES)
            if form.is_valid():
                encoding = request.encoding or 'utf-8'
                csv_file = TextIOWrapper(request.FILES['csv_file'].file,
                                         encoding=encoding)
                reader = csv.DictReader(csv_file)
                created, updated, missing_parent, errors = 0, 0, [], 0
                for row in reader:
                    slug_col = None
                    for key in self.import_keys:
                        if key in row:
                            slug_col = key
                            break
                    if not slug_col:
                        self.message_user(
                            request,
                            str(_("The CSV file should at least have a "
                                  "{} column")).format(
                                "/".join(self.import_keys)),
                            level=messages.ERROR
                        )
                        return render(
                            request, 'admin/import_from_file.html',
                            {'file_form': form,
                             'current_action': 'import_generic'}
                        )
                    slug = row.pop(slug_col)
                    if 'id' in row:
                        row.pop('id')
                    if 'pk' in row:
                        row.pop('pk')
                    for k in list(row.keys()):
                        value = row[k]
                        if value == 'None':
                            value = ''
                        try:
                            field = self.model._meta.get_field(k)
                        except FieldDoesNotExist:
                            row.pop(k)
                            errors += 1
                            continue
                        if isinstance(field, CharField):
                            if not value:
                                value = ""
                        elif isinstance(field, IntegerField):
                            value = None if not value else int(value)
                        elif isinstance(field, FloatField):
                            value = None if not value else float(value)
                        elif isinstance(field, BooleanField):
                            if value in ('true', 'True', '1'):
                                value = True
                            elif value in ('false', 'False', '0'):
                                value = False
                            else:
                                value = None
                        elif isinstance(field, ForeignKey):
                            if value:
                                model = field.related_model
                                try:
                                    value = model.objects.get(slug=value)
                                except model.DoesNotExist:
                                    missing_parent.append(row.pop(k))
                                    continue
                            else:
                                value = None
                        row[k] = value
                    values = {
                        'defaults': row
                    }
                    if slug:
                        values[slug_col] = slug
                        try:
                            obj, c = self.model.objects.get_or_create(
                                **values)
                        except self.model.MultipleObjectsReturned:
                            errors += 1
                            continue
                    else:
                        try:
                            obj = self.model.objects.create(
                                **values["defaults"])
                        except IntegrityError:
                            print(values)
                            errors += 1
                            continue
                        c = True
                    if c:
                        created += 1
                    else:
                        updated += 1
                        self.model.objects.filter(pk=obj.pk).update(**row)
                if created:
                    self.message_user(
                        request,
                        str(_("%d item(s) created.")) % created)
                if updated:
                    self.message_user(
                        request,
                        str(_("%d item(s) updated.")) % updated)
                if missing_parent:
                    self.message_user(
                        request,
                        str(_("These parents are missing: {}")).format(
                            " ; ".join(missing_parent)),
                        level=messages.WARNING
                    )
                if errors:
                    self.message_user(
                        request,
                        str(_("{} line(s) with errors")).format(errors),
                        level=messages.ERROR
                    )
                url = reverse(
                    'admin:%s_%s_changelist' % (
                        self.model._meta.app_label, self.model._meta.model_name)
                )
                return HttpResponseRedirect(url)
        if not form:
            form = ImportGenericForm()
        return render(
            request, 'admin/import_from_file.html',
            {'file_form': form, 'current_action': 'import_generic'}
        )


admin_site.register(models.Player)

admin_site.register(models.Stylesheet)

admin_site.register(models.Tips)


class SimpleNameForm(forms.ModelForm):
    name = forms.CharField()


class GenericAdmin(admin.ModelAdmin):
    list_display = ('name', 'order', 'available')
    list_filter = ('available',)


class GenericSlugAdmin(GenericAdmin):
    prepopulated_fields = {"slug": ("name",)}


admin_site.register(models.AbilityKind, GenericAdmin)


def add_to_all_characters(modeladmin, request, queryset):
    created = 0
    for ability in queryset.all():
        for c in Character.objects.exclude(
                abilities__ability_id=ability.pk).all():
            created += 1
            CharacterAbility.objects.create(character=c, ability=ability)
    messages.add_message(request, messages.INFO,
                         str(_("{} character ability created")).format(
                             created))


class AbilityAdmin(admin.ModelAdmin):
    list_display = ('name', 'ability_type', 'kind', 'order', 'available',
                    'visible')
    list_filter = ('available', 'ability_type', 'kind')
    prepopulated_fields = {"slug": ("name",)}
    actions = [add_to_all_characters]


admin_site.register(models.Ability, AbilityAdmin)


class ActionAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'order', 'available', 'view_slug', 'formula',
        'opponent_formula', 'fighting_action', 'need_conscious', 'target_self',
        'target_other', 'target_multiple', 'target_own_item',
        'target_location_item', 'target_other_item', 'target_can_be_carried',
        'target_equipped_item')
    list_filter = (
        'available', 'fighting_action', 'need_conscious',
        'target_self', 'target_other', 'target_multiple', 'target_own_item',
        'target_location_item', 'target_other_item', 'target_can_be_carried',
        'target_equipped_item')
admin_site.register(models.Action, ActionAdmin)


class BodyAreaAdmin(GenericAdmin):
    list_display = ('name', 'slug', 'order', 'default_number_available',
                    'available')
admin_site.register(models.BodyArea, BodyAreaAdmin)


class SizeAdmin(GenericAdmin):
    list_display = ('name', 'order', 'index', 'available')


admin_site.register(models.Size, SizeAdmin)

admin_site.register(models.DamageType, GenericAdmin)
admin_site.register(models.AbilityType, GenericAdmin)
admin_site.register(models.SkillType, GenericAdmin)

admin_site.register(models.Formula)
admin_site.register(models.BasicFormula)
admin_site.register(models.Task)
admin_site.register(models.CharacterEvent)
admin_site.register(models.DocumentCategory, GenericSlugAdmin)
admin_site.register(models.Document)


class AudioAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


admin_site.register(models.Audio, AudioAdmin)


class AudioInline(admin.TabularInline):
    model = models.GameAudio


class GameAdmin(admin.ModelAdmin):
    inlines = [AudioInline]


admin_site.register(models.Game, GameAdmin)


def add_skill_to_all_characters(modeladmin, request, queryset):
    created = 0
    for skill in queryset.all():
        for c in Character.objects.exclude(
                skills__id=skill.pk).all():
            created += 1
            c.skills.add(skill)
    messages.add_message(request, messages.INFO,
                         str(_("{} character skill created")).format(
                             created))


class SkillAdmin(GenericSlugAdmin):
    list_display = ('name', 'order', 'available', 'skill_type')
    list_filter = ('available', 'skill_type')
    search_fields = ('name',)
    actions = (add_skill_to_all_characters,)


admin_site.register(models.Skill, SkillAdmin)


def add_calculatedability_to_all_characters(modeladmin, request, queryset):
    created = 0
    for ability in queryset.all():
        for c in Character.objects.exclude(
                calculated_abilities__id=ability.pk).all():
            created += 1
            c.calculated_abilities.add(ability)
    messages.add_message(request, messages.INFO,
                         str(_("{} character calc ability created")).format(
                             created))


class CalculatedAbilityAdmin(GenericSlugAdmin):
    search_fields = ('name',)
    actions = (add_calculatedability_to_all_characters,)


admin_site.register(models.CalculatedAbility, CalculatedAbilityAdmin)


class LocationEventAdmin(admin.ModelAdmin):
    list_display = ("locations_lbl", "tasks_lbl", "location_trigger", "order",
                    "first_delay", "delay")


admin_site.register(models.LocationEvent, LocationEventAdmin)


class CharacterEventTargetAdmin(admin.ModelAdmin):
    list_display = ("target_character", "character", "event", "number",
                    "start", "last")


admin_site.register(models.CharacterEventTarget, CharacterEventTargetAdmin)


class LocationEventTargetAdmin(admin.ModelAdmin):
    list_display = ("target_character", "location", "event", "number",
                    "start", "last")


admin_site.register(models.LocationEventTarget, LocationEventTargetAdmin)


admin_site.register(models.OracleCategory, GenericAdmin)


class OracleChoiceModelForm(SimpleNameForm):
    class Meta:
        model = models.OracleChoice
        exclude = []


class OracleChoiceInline(SortableTabularInline):
    model = models.OracleChoice
    form = OracleChoiceModelForm


class OracleModelForm(SimpleNameForm):
    class Meta:
        model = models.Oracle
        exclude = []


class OracleAdmin(SortableAdmin, ImportActionAdmin):
    form = OracleModelForm
    list_filter = ("game", "available", "gm_only")
    list_display = ("name", "category", "available", "gm_only")
    inlines = [OracleChoiceInline]
    actions = [export_as_csv_action()]


admin_site.register(models.Oracle, OracleAdmin)


class OracleChoiceAdmin(SortableAdmin, ImportActionAdmin):
    change_list_template_extends = 'admin/gen_change_list.html'
    list_filter = ("oracle", "available")
    list_display = ("name", "oracle", "order", "available")
    form = OracleChoiceModelForm
    actions = [export_as_csv_action()]
    search_fields = ('name',)


admin_site.register(models.OracleChoice, OracleChoiceAdmin)
