
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MorriganCharactersConfig(AppConfig):
    name = 'morrigan_characters'
    verbose_name = _("Characters")

    def ready(self):
        # register actions
        from morrigan_characters import views
        views


class MorriganCommonConfig(AppConfig):
    name = 'morrigan_common'
    verbose_name = _("Generic")


class MorriganGeographyConfig(AppConfig):
    name = 'morrigan_geography'
    verbose_name = _("Geography")


class MorriganInventoryConfig(AppConfig):
    name = 'morrigan_inventory'
    verbose_name = _("Inventory")

    def ready(self):
        # register actions
        from morrigan_inventory import views
        views


class MorriganFightConfig(AppConfig):
    name = 'morrigan_fight'
    verbose_name = _("Fight")

    def ready(self):
        # register actions
        from morrigan_fight import views
        views
