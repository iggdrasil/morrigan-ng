# Actions programmées

Les actions programmées sont des actions qui sont exécutées automatiquement de manière ponctuelle ou à une certaine fréquence. L’exécution de ces actions peut être conditionné par certains critères : présence dans un lieu particulier, en présence d'un personnage, suite à l’exécution d'une action, etc.

La définition d'une action programmée se fait en deux temps : définition de l'événement déclencheur puis définition de l'action en elle-même.

## Événement déclencheur

Il y a deux types d'événements : les événements liés à des lieux et les événements liés à des personnages. Ces deux types d'événements partagent des paramètres en commun.

### Paramètres génériques

* disponibilité : à vrai si cet événement est actif
* ordre : ordre d'exécution - si plusieurs événements sont à prendre en compte les événements avec un numéro d'ordre plus faible sont exécutés en premier
* premier délai (en heures) : délai avant la première exécution
* délai entre les réactions : délai entre chaque réaction - si laissé à vide une seule réaction est faite.
* maximum de réactions : nombre maximum de réactions - si laissé à vide les réactions s'enchaînent tant que les conditions de déclenchement restent valable sans limite de nombre.
* tâches : tâches à exécuter lorsque l'événement est validé. Une tâche (en administration « Générique > Événements - Tâches » ) est une action associée à un numéro d'ordre. Si plusieurs tâches sont associées à un événement elles sont exécutées dans l'ordre défini par les numéros d'ordre.
* actions déclencheuses : actions du personnage cible qui provoquent le déclenchement de l'action. Si aucune action déclencheuse n'est définie, l'événement n'est pas contraint par la réalisation d'une action. Si plusieurs actions déclencheuses sont définies, l'execution d'une seule de ces actions permet la validation de l'événement.
* états déclencheurs (formules) : états du personnage cible qui provoquent le déclenchement de l'action. Un « état » correspond à la validation d'une formule. Si aucun état déclencheur n'est défini, l'événement n'est pas contraint par la validation d'un état. Si plusieurs états déclenchers sont définis, la validation d'un seul état permet la validation de l'événement.


*Note* : si au moins une action déclencheuse est définie et au moins un état déclencheur est défini, il faut qu'à la fois une action déclencheuse et un état déclencheur soient validés pour que l’événement puisse être validé.

### Événement de type lieu

Dans l'administration : « Générique > Événements - Lieu »

* lieux : lieux permettant le déclenchement de l'événement. Ce champ peut rester vide : l'événement peut alors être déclenché dans tous les lieux.
* déclencheur de lieu : ce champ permet de définir si l'événement est déclenché lors d'une entrée dans un lieu ou lors d'une sortie d'un lieu. Ce paramètre n'a pas d'effet si aucun lieu n'est défini.


*Note* : un événement avec un déclencheur de lieu n'est exécuté qu'une fois même si un nombre maximum de réaction et un délai entre réactions est défini.

### Événement de type personnage

Dans l'administration : « Générique > Événements - Personnage »

* personnages : personnages a proximité (personnage cible dans le même lieu) desquels l'événement est validé.

## Exemples d'événements déclencheurs

### Remise journalière

Nous souhaitons qu'avec le temps tous les personnages se régénèrent sans condition particulière autre que d'être conscient. Pour cela nous allons définir un événement de type lieu sans lieu associé.

#### Définition de la formule

**Générique > Formules** 

* Nom : remise journalière
* Formule pour les points d'action : `max( int([max_ap] / 5 ), 1 )` 
* Formule pour les points de nourriture : `max( int([max_fp] / 5 ), 1 )`
* Formule pour les points de magie : `max( int([max_mp] / 5 ), 1 )`
* Formule pour les points de vie : `max( int([max_hp] / 20 ), 1 )`
* Formule pour les points de combat : `max( int([max_bp] / 5 ), 1 )`

La formule associé à l'action de remise va augmenter chaque « jauge » d'un cinquième de sa valeur maximale, sauf pour les points de vie qui vont augmenter d'un vingtième. Un minimum de un point est gagné.

*Note* : intrinsèquement Morrigan ne permet pas d'aller au delà des maximums. Ainsi si un personnage a une valeur maximale de point de magie de 0, il ne recevra pas de point de magie lors des remises journalières. 

#### Action

**Générique > Actions**

* Nom : remise journalière
* Formule : « remise journalière »
* Le personnage doit être conscient : oui

#### Tâche associée

**Générique > Événements - Tâches**

* Action : « Remise journalière »
* Ordre : 1000

Un ordre de 1000 a été mis pour que cette remise ait lieu après tous les autres événements.

#### Événement

**Générique > Événements - Lieux**

* Premier délai : 0 - aucun délai d'application
* Délai : 23 - 23 heures pour introduire un petit décalage chaque jours
* Maximum : aucun - les remises sont toujours valables
* Lieux : aucun - valable en tous lieux
